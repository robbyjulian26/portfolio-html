USE [master]
GO
/****** Object:  Database [REBOOK_ITA]    Script Date: 16/03/2022 20:28:16 ******/
CREATE DATABASE [REBOOK_ITA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'REBOOK_ITA', FILENAME = N'D:\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\REBOOK_ITA.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'REBOOK_ITA_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\REBOOK_ITA_log.ldf' , SIZE = 270336KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [REBOOK_ITA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [REBOOK_ITA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET ARITHABORT OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [REBOOK_ITA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [REBOOK_ITA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [REBOOK_ITA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET  DISABLE_BROKER 
GO
ALTER DATABASE [REBOOK_ITA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [REBOOK_ITA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET RECOVERY FULL 
GO
ALTER DATABASE [REBOOK_ITA] SET  MULTI_USER 
GO
ALTER DATABASE [REBOOK_ITA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [REBOOK_ITA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [REBOOK_ITA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [REBOOK_ITA] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
USE [REBOOK_ITA]
GO
/****** Object:  StoredProcedure [dbo].[GenerateProperty]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenerateProperty] 
@TableName varchar(250)
AS
BEGIN
	SELECT 
	tb.name As [TableName], 
	cl.name as [FieldName],
	cl.is_nullable As [Nullable],
	tp.name As [SqlDataType],
	cl.max_length AS [Length],
	CONCAT('public'
		,CASE tp.name
		WHEN 'bigint' THEN ' int '
		WHEN 'binary' THEN ' bool '
		WHEN 'bit' THEN ' bool '
		WHEN 'char' THEN ' char '
		WHEN 'date' THEN ' DateTime '
		WHEN 'datetime' THEN ' DateTime '
		WHEN 'datetime2' THEN ' DateTime '
		WHEN 'datetimeoffset' THEN ' Object '
		WHEN 'decimal' THEN ' decimal '
		WHEN 'float' THEN ' float '
		WHEN 'geography' THEN ' Object  '
		WHEN 'geometry' THEN 'Object '
		WHEN 'hierarchyid' THEN ' Object '
		WHEN 'image' THEN ' Object '
		WHEN 'int' THEN ' int '
		WHEN 'money' THEN ' decimal '
		WHEN 'nchar' THEN ' char '
		WHEN 'ntext' THEN ' string '
		WHEN 'numeric' THEN ' decimal '
		WHEN 'nvarchar' THEN ' string '
		WHEN 'real' THEN ' decimal '
		WHEN 'smalldatetime' THEN ' DateTime '
		WHEN 'smallint' THEN ' small '
		WHEN 'smallmoney' THEN ' decimal '
		WHEN 'sql_variant' THEN ' Object '
		WHEN 'sysname' THEN ' Object '
		WHEN 'text' THEN ' string '
		WHEN 'time' THEN ' DateTime '
		WHEN 'timestamp' THEN ' Object '
		WHEN 'tinyint' THEN ' small '
		WHEN 'uniqueidentifier' THEN ' Object ' 
		WHEN 'varbinary' THEN ' Object '
		WHEN 'varchar' THEN ' string '
		WHEN 'xml' THEN ' Object '
		ELSE ' Object '
		END
		,'/*'+ tp.name + CASE WHEN cl.is_nullable = 0 THEN ' Not Null' ELSE '' END + '*/ '
		,cl.name
		,' { get; set; } //'
		,cast(cl.max_length as varchar)
	) AS [Property],
	CONCAT('param.Add("@',cl.name,'", request.',cl.name,');') AS [AddParamCode],
	CONCAT(',@',cl.name,' [',tp.name,']',(CASE WHEN tp.name LIKE '%char%' THEN '('+CAST(cl.max_length AS VARCHAR)+')' ELSE '' END)) AS [AddParamSql],
	CONCAT(cl.name,' = fromModel.',cl.name,';') AS [ModelMapper]
	FROM sys.tables tb 
	JOIN sys.all_columns cl
		ON tb.object_id = cl.object_id
	JOIN sys.types tp
		ON cl.system_type_id = tp.system_type_id
	WHERE tb.name = @TableName
	ORDER BY cl.column_id
END

GO
/****** Object:  StoredProcedure [dbo].[GenerateRegistrationNo]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenerateRegistrationNo]
@BookName varchar(50),
@SubBookName varchar(50),
@DeptCode varchar(50),
@Year varchar(10)
AS
BEGIN
	DECLARE 
		@RegistrationNumber Varchar(255),
		@BookID varchar(25),
		@SubBookID varchar(25),
		@LastSequence int,
		@Sequence varchar(10) = '0000'

		SELECT @BookID = VALUE FROM MS_PARAMETER 
		WHERE [GROUP] = 'ReBOOK_BOOK_ID' AND DESCS = @BookName
		SELECT @SubBookID = VALUE FROM MS_PARAMETER
		WHERE [GROUP] = 'SUB_BOOK_ID' AND DESCS = @SubBookName

		SET @RegistrationNumber = CONCAT(@BookID,'.',@SubBookID,'.',@DeptCode,'.',@Year,'.')

		IF(@BookName = 'IMPORTANT ARTICLES')
		BEGIN
			SELECT  @LastSequence = MAX(coalesce(TRY_PARSE(RIGHT(REGIS_NO, 4) AS INT),0)) 
			FROM TR_IMPORTANT_ART
			WHERE REGIS_NO LIKE '%' + @RegistrationNumber + '%'

			SET @Sequence = RIGHT(@Sequence + CAST(@LastSequence+1 as varchar), 4)
		END
		ELSE IF(@BookName = 'OUTGOING LETTERS') 
		BEGIN
			SELECT  @LastSequence = MAX(coalesce(TRY_PARSE(RIGHT(REGIS_NO, 4) AS INT),0)) 
			FROM TR_OUTGOING_LETTER
			WHERE REGIS_NO LIKE '%' + @RegistrationNumber + '%'

			SET @Sequence = RIGHT(@Sequence + CAST(@LastSequence+1 as varchar), 4)
		END

		SET @RegistrationNumber = CONCAT(@RegistrationNumber, @Sequence)

		SELECT @RegistrationNumber
END



GO
/****** Object:  StoredProcedure [dbo].[GetArticleRegistrationNo]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetArticleRegistrationNo]
@ArticleID varchar(50),
@DepartmentCode varchar(50),
@Year varchar(10)
AS
BEGIN
	DECLARE 
		@RegistrationNumber Varchar(255),
		@BookID varchar(250) = 'GEN.IA', 
		@SubBookID varchar(250) = @ArticleID,
		@DeptCode varchar(250) = @DepartmentCode,
		@LastSequence int = 0, 
		@Sequence varchar(10) = '0000'

		SELECT @BookID = COALESCE(BOOK_ID, @BookID)
		FROM MS_BOOK_PROFILE
		WHERE DEPT_BOOK = @DepartmentCode
		AND BOOK_NAME = 'IMPORTANT ARTICLES'

		SET @RegistrationNumber = CONCAT(@BookID,'.',@SubBookID,'.',@DeptCode,'.',@Year,'.')
		SELECT  @LastSequence = MAX(coalesce(TRY_PARSE(RIGHT(REGIS_NO, 4) AS INT),0)) 
		FROM TR_IMPORTANT_ART
		WHERE REGIS_NO LIKE '%' + @RegistrationNumber + '%'

		SET @Sequence = RIGHT(@Sequence + CAST(ISNULL(@LastSequence,0)+1 as varchar), 4)
		SET @RegistrationNumber = CONCAT(@RegistrationNumber, @Sequence)

		SELECT @RegistrationNumber
END



GO
/****** Object:  StoredProcedure [dbo].[GetLetterRegistrationNo]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLetterRegistrationNo]
--@LetterProfileID int,
@DeptCd varchar(50),
@Year varchar(10)
AS
BEGIN
	DECLARE 
		@RegistrationNumber Varchar(255),
		@BookID varchar(250),
		@LastSequence int, @Sequence varchar(10) = '0000'

		SET @BookID = 'GEN.OLT';


		SET @RegistrationNumber = CONCAT(@BookID,'.',@DeptCd,'.',@Year,'.')
		SELECT  @LastSequence = MAX(coalesce(TRY_PARSE(RIGHT(REGIS_NO, 4) AS INT),0)) 
		FROM TR_OUTGOING_LETTER
		WHERE REGIS_NO LIKE '%' + @RegistrationNumber + '%'

		SET @Sequence = RIGHT(@Sequence + CAST(ISNULL(@LastSequence+1,0) as varchar), 4)
		SET @RegistrationNumber = CONCAT(@RegistrationNumber, @Sequence)

		SELECT @RegistrationNumber
END


GO
/****** Object:  StoredProcedure [dbo].[GetMasterArticleProfile]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetMasterArticleProfile]
AS
BEGIN
	SELECT * 
	FROM MS_ARTICLE_PROFILE
	WHERE IS_ACTIVE = 1
END


GO
/****** Object:  StoredProcedure [dbo].[GetRegistrationTypeOptions]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetRegistrationTypeOptions]
@ArticleID varchar(255),
@UserDept varchar(50)
AS
BEGIN

	SELECT P.VALUE AS KeyField
	,P.DESCS AS ValueField 
	FROM MS_PARAMETER P
	INNER JOIN MS_ARTICLE_PROFILE AP
	ON AP.ARTICLE_ID = @ArticleID
	AND AP.DEPT_ARTICLE = @UserDept
	AND CASE P.VALUE
	WHEN 'Stock in Vault' THEN AP.IS_STOCK_IN_VAULT
	WHEN 'Stock in Hand' THEN AP.IS_STOCK_IN_HAND
	WHEN 'Usage' THEN AP.IS_USAGE
	WHEN 'Correction' THEN AP.IS_CORRECTION
	END = 1
	WHERE P.[GROUP] = 'REGISTRATION_TYPE'
	AND P.KEYFIELD = 'IMPORTANT_ARTICLE'
END


GO
/****** Object:  StoredProcedure [dbo].[GetStockValueByArticleID]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetStockValueByArticleID]
@ArticleID VARCHAR(255),
@ValueType VARCHAR(255),
@UserDept Varchar(50)
AS
BEGIN
	DECLARE @Result numeric = 0;

	SELECT @Result = CASE 
	WHEN @ValueType = 'VAULT' 
	THEN STOCK_IN_VAULT
	ELSE BALANCE_IN_HAND
	END
	FROM MS_ARTICLE_PROFILE AP
	INNER JOIN MS_STOCK_PROFILE SP
	ON AP.ARTICLE_ID = SP.ARTICLE_ID
	WHERE SP.ARTICLE_ID = @ArticleID
	AND AP.DEPT_ARTICLE = @UserDept
	AND SP.DEPT_OF_ARTICLE = @UserDept 


	SELECT @Result;
END


GO
/****** Object:  StoredProcedure [dbo].[GetUserDetail]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserDetail]
@UserName Varchar(250)
AS 
BEGIN

	SELECT 
	u.Username as UserId,
	u.RealName as FullName,
	dp.ShortName as DepartmentShortName,
	dp.LongName as DeparmentLongName,
	un.ShortName as UnitShortName,
	un.LongName as UnitLongName,
	rl.ShortName as RoleShortName,
	rl.LongName as RoleLongName
	FROM [dbo].[SN_BTMUDirectory_User] u
	INNER JOIN [dbo].[SN_BTMUDirectory_Role_Parent] rp 
	ON rp.ID = u.RoleParentID
	INNER JOIN [dbo].[SN_BTMUDirectory_Department] dp
	ON rp.DepartmentID = dp.ID
	INNER JOIN [dbo].[SN_BTMUDirectory_Unit] un
	ON rp.UnitID = un.ID
	INNER JOIN [dbo].[SN_BTMUDirectory_Role] rl
	ON rp.RoleID = rl.ID
	WHERE ISNULL(u.ISDELETED,0) <> 1 
	AND u.Username = @UserName

END

GO
/****** Object:  StoredProcedure [dbo].[UDPD_ACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPD_ACTIONTAKEN]
	@Id INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @ApprovalStatus VARCHAR(20)

	--SELECT @ApprovalStatus = APPROVAL_STS
	--FROM TR_BOOK
	--WHERE [UID] = @Id

	--IF (
	--		@ApprovalStatus != 'Approve'
	--		AND @ApprovalStatus != 'Reject'
	--		)
	--BEGIN
	--	DELETE TR_BOOK_ACTION_TAKEN WHERE [UID] = @Id
	--END
	--ELSE
	--BEGIN
		UPDATE dbo.TR_BOOK_ACTION_TAKEN 
		SET IS_ACTIVE = 0,
			MODIFIED_BY = @ModifiedBy,
			MODIFIED_DT = GETDATE(),
			MODIFIED_HOST = @ModifiedHost
		WHERE UID = @Id
	--END
	SELECT @Id;
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_ARTICLEPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_ARTICLEPROFILE]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE dbo.MS_ARTICLE_PROFILE
	WHERE [UID] = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPD_ATTACHMENT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_ATTACHMENT]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.TR_ATTACHMENT
	WHERE [UID] = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPD_ATTACHMENTTEMP]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_ATTACHMENTTEMP]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.TR_ATTACHMENT_TEMP
	WHERE [UID] = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPD_BOOK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPD_BOOK]
	@Id INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @ApprovalStatus VARCHAR(20)

	--SELECT @ApprovalStatus = APPROVAL_STS
	--FROM TR_BOOK
	--WHERE [UID] = @Id

	--IF (
	--		@ApprovalStatus != 'Approve'
	--		AND @ApprovalStatus != 'Reject'
	--		)
	--BEGIN
	--	DELETE TR_BOOK WHERE [UID] = @Id
	--END
	--ELSE
	--BEGIN
		UPDATE dbo.TR_BOOK
		SET IS_ACTIVE = 0,
			MODIFIED_BY = @ModifiedBy,
			MODIFIED_DT = @ModifiedDate,
			MODIFIED_HOST = @ModifiedHost
		WHERE UID = @Id
	--END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_BOOKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_BOOKPROFILE]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE dbo.MS_BOOK_PROFILE
	WHERE [UID] = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPD_DESTROYEDOUTGOINGLETTER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPD_DESTROYEDOUTGOINGLETTER]
	@Id INT,
	@DestroyedDate DATETIME,
	@DestroyedBy VARCHAR(255),
	@DestroyedWitness VARCHAR(255),
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

		UPDATE dbo.TR_OUTGOING_LETTER
		SET LETTER_STS = 'Destroyed',
			DEST_DT = @DestroyedDate,
			DEST_BY = @DestroyedBy,
			DEST_WITNESS = @DestroyedWitness,
			MODIFIED_BY = @ModifiedBy,
			MODIFIED_DT = @ModifiedDate,
			MODIFIED_HOST = @ModifiedHost
		WHERE UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_DRAFTARTICLECORRECTION]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPD_DRAFTARTICLECORRECTION]
@UID INT
,@ModifiedBy varchar(255)
,@ModifiedDate DateTime
,@ModifiedHost varchar(255)
AS
BEGIN
	UPDATE TR_IMPORTANT_ART_ACTIVITY
	SET IS_ACTIVE = 0,
	MODIFIED_BY = @ModifiedBy,
	MODIFIED_DT = @ModifiedDate,
	MODIFIED_HOST = @ModifiedHost
	WHERE IS_DRAFT = 1 AND UID = @UID
END

GO
/****** Object:  StoredProcedure [dbo].[UDPD_LETTERPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_LETTERPROFILE]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE dbo.MS_LETTER_PROFILE
	WHERE [UID] = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPD_OUTGOINGLETTER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPD_OUTGOINGLETTER]
	@Id INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @ApprovalStatus VARCHAR(20)

	--SELECT @ApprovalStatus = APPROVAL_STS
	--FROM TR_BOOK
	--WHERE [UID] = @Id

	--IF (
	--		@ApprovalStatus != 'Approve'
	--		AND @ApprovalStatus != 'Reject'
	--		)
	--BEGIN
	--	DELETE TR_BOOK WHERE [UID] = @Id
	--END
	--ELSE
	--BEGIN
		UPDATE dbo.TR_OUTGOING_LETTER
		SET IS_ACTIVE = 0,
			MODIFIED_BY = @ModifiedBy,
			MODIFIED_DT = @ModifiedDate,
			MODIFIED_HOST = @ModifiedHost
		WHERE UID = @Id
	--END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_PARAMETER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_PARAMETER]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE dbo.MS_PARAMETER
	WHERE [UID] = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPD_RECEIVENOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPD_RECEIVENOTICE]
	@Id INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	--DECLARE @ApprovalStatus VARCHAR(20)

	--SELECT @ApprovalStatus = APPROVAL_STS
	--FROM TR_BOOK
	--WHERE [UID] = @Id

	--IF (
	--		@ApprovalStatus != 'Approve'
	--		AND @ApprovalStatus != 'Reject'
	--		)
	--BEGIN
	--	DELETE TR_BOOK_ACTION_TAKEN WHERE [UID] = @Id
	--END
	--ELSE
	--BEGIN
		UPDATE dbo.TR_BOOK_RECEIVED_NOTICE 
		SET IS_ACTIVE = 0,
			MODIFIED_BY = @ModifiedBy,
			MODIFIED_DT = @ModifiedDate,
			MODIFIED_HOST = @ModifiedHost
		WHERE UID = @Id
	--END
	SELECT @Id;
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_STOCKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_STOCKPROFILE]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE dbo.MS_STOCK_PROFILE
	WHERE [UID] = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPI_ACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ACTIONTAKEN]
	-- Add the parameters for the stored procedure here
	@DOC_ID varchar (50)
    ,@REGIS_NO varchar (50)
    ,@CREATOR varchar (50)
    ,@CREATOR_DEPT varchar (30)
    ,@CREATOR_UNIT varchar (30)
    ,@CREATOR_ROLE varchar (30)
    ,@REQUEST_DT varchar (50)
    ,@ACT_DT varchar (50)
    ,@ACT_ACTION varchar (255)
    ,@ACTOR varchar (100)
    ,@REMARKS varchar (8000)
    ,@RESULT varchar (8000)
    ,@APPROVAL_STS varchar (50)
    ,@IS_DRAFT varchar (50)
    ,@IS_ACTIVE varchar (50)
    ,@CREATED_BY varchar (50)
    --,@CREATED_DT varchar (50)
    ,@CREATED_HOST varchar (50)
    --,@MODIFIED_BY varchar (50)
    --,@MODIFIED_DT varchar (50)
    --,@MODIFIED_HOST varchar (50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO TR_BOOK_ACTION_TAKEN 
	(
	   [DOC_ID]
      ,[REGIS_NO]
      ,[CREATOR]
      ,[CREATOR_DEPT]
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      ,[REQUEST_DT]
      ,[ACT_DT]
      ,[ACT_ACTION]
      ,[ACTOR]
      ,[REMARKS]
      ,[RESULT]
      ,[APPROVAL_STS]
      ,[IS_DRAFT]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]
      --,[MODIFIED_BY]
      --,[MODIFIED_DT]
      --,[MODIFIED_HOST]
	)
	VALUES
	(   
		@DOC_ID
       ,@REGIS_NO
       ,@CREATOR
       ,@CREATOR_DEPT
       ,@CREATOR_UNIT
       ,@CREATOR_ROLE
       ,@REQUEST_DT
       ,@ACT_DT
       ,@ACT_ACTION
       ,@ACTOR
       ,@REMARKS
       ,@RESULT
       ,@APPROVAL_STS
       ,@IS_DRAFT
       ,@IS_ACTIVE
       ,@CREATED_BY
       ,GETDATE()
       ,@CREATED_HOST
       --,@MODIFIED_BY
       --,@MODIFIED_DT
       --,@MODIFIED_HOST
	);

	SELECT TOP 1 [UID] FROM [TR_BOOK_ACTION_TAKEN] ORDER BY [UID] DESC;
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_ADDIMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ADDIMPORTANTART]
@REGIS_NO [varchar](50)
,@CREATOR [varchar](50)
,@CREATOR_DEPT [varchar](30)
,@CREATOR_UNIT [varchar](30)
,@CREATOR_ROLE [varchar](30)
,@REQUEST_DT [date]
,@CUST_BRANCH [varchar](255)
,@ARTICLE_NAME [varchar](255)
,@REGIS_TYPE [varchar](255)
,@BALANCE_IN_HAND [numeric]
,@VALUE [numeric]
,@DATE [date]
,@REF_NO [varchar](255)
,@REMARKS [varchar](8000)
,@ART_STATUS [varchar](8000)
,@APPROVAL_STS [varchar](50)
,@IS_DRAFT [bit]
,@IS_ACTIVE [bit]
,@CREATED_BY [varchar](50)
,@CREATED_DT [datetime]
,@CREATED_HOST [varchar](20)

,@ARTICLE_ID [varchar](255) = ''
,@BALANCE_IN_VAULT [numeric] = null
,@SP_DATE [datetime] = null
,@PL_DATE [datetime] = null
,@PURPOSE [varchar](8000) = ''
,@CUSTOMER [varchar](255) = ''
AS
BEGIN
	
	INSERT INTO TR_IMPORTANT_ART
	(
	 REGIS_NO
	,CREATOR
	,CREATOR_DEPT
	,CREATOR_UNIT
	,CREATOR_ROLE
	,REQUEST_DT
	,CUST_BRANCH
	,ARTICLE_NAME
	,REGIS_TYPE
	,BALANCE_IN_HAND
	,VALUE
	,[DATE]
	,REF_NO
	,REMARKS
	,ART_STATUS
	,APPROVAL_STS
	,IS_DRAFT
	,IS_ACTIVE
	,CREATED_BY
	,CREATED_DT
	,CREATED_HOST

	,ARTICLE_ID
	,BALANCE_IN_VAULT
	,SP_DATE
	,PL_DATE

	,PURPOSE
	,CUSTOMER
	)
	SELECT
	 @REGIS_NO
	,@CREATOR
	,@CREATOR_DEPT
	,@CREATOR_UNIT
	,@CREATOR_ROLE
	,@REQUEST_DT
	,@CUST_BRANCH
	,@ARTICLE_NAME
	,@REGIS_TYPE
	,@BALANCE_IN_HAND
	,@VALUE
	,@DATE
	,@REF_NO
	,@REMARKS
	,@ART_STATUS
	,@APPROVAL_STS
	,@IS_DRAFT
	,@IS_ACTIVE
	,@CREATED_BY
	,@CREATED_DT
	,@CREATED_HOST

	,@ARTICLE_ID
	,@BALANCE_IN_VAULT
	,@SP_DATE
	,@PL_DATE

	,@PURPOSE
	,@CUSTOMER

	SELECT @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_ADDIMPORTANTARTACTIVITY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPI_ADDIMPORTANTARTACTIVITY]
 @DOC_ID [bigint]
,@REGIS_NO [varchar](50)
,@ACTIVITY_CD [varchar](20)
,@CREATOR [varchar](50)
,@CREATOR_DEPT [varchar](30)
,@CREATOR_UNIT [varchar](30)
,@CREATOR_ROLE [varchar](30)
,@REQUEST_DT [date]
,@CORR_BALANCE_IN_HAND [numeric]
,@CORR_VALUE [numeric]
,@CORR_DATE [date]
,@CORR_REF_NO [varchar](255)
,@CORR_REMARKS [varchar](8000)
,@CAN_REASON [varchar](8000)
,@INSP_DP [date]
,@INSP_BY [varchar](255)
,@APPROVAL_STS [varchar](50)
,@IS_DRAFT [bit]
,@IS_ACTIVE [bit]
,@CREATED_BY [varchar](50)
,@CREATED_DT [datetime]
,@CREATED_HOST [varchar](20)

,@DESTROYED_DT [datetime] = null
,@DESTROYED_BY [varchar](255) = null
,@WITNESSED_BY [varchar](255) = null
AS 
BEGIN
	INSERT INTO TR_IMPORTANT_ART_ACTIVITY
	(
	DOC_ID
	,REGIS_NO
	,ACTIVITY_CD
	,CREATOR
	,CREATOR_DEPT
	,CREATOR_UNIT
	,CREATOR_ROLE
	,REQUEST_DT
	,CORR_BALANCE_IN_HAND
	,CORR_VALUE
	,CORR_DATE
	,CORR_REF_NO
	,CORR_REMARKS
	,CAN_REASON
	,INSP_DP
	,INSP_BY
	,APPROVAL_STS
	,IS_DRAFT
	,IS_ACTIVE
	,CREATED_BY
	,CREATED_DT
	,CREATED_HOST

	,DESTROYED_DT
	,DESTROYED_BY
	,WITNESSED_BY
	)
	SELECT
	@DOC_ID
	,@REGIS_NO
	,@ACTIVITY_CD
	,@CREATOR
	,@CREATOR_DEPT
	,@CREATOR_UNIT
	,@CREATOR_ROLE
	,@REQUEST_DT
	,@CORR_BALANCE_IN_HAND
	,@CORR_VALUE
	,@CORR_DATE
	,@CORR_REF_NO
	,@CORR_REMARKS
	,@CAN_REASON
	,@INSP_DP
	,@INSP_BY
	,@APPROVAL_STS
	,@IS_DRAFT
	,@IS_ACTIVE
	,@CREATED_BY
	,@CREATED_DT
	,@CREATED_HOST

	,@DESTROYED_DT
	,@DESTROYED_BY
	,@WITNESSED_BY

	SELECT @@IDENTITY

END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_ARTICLEPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_ARTICLEPROFILE]
	-- Add the parameters for the stored procedure here
	@ARTICLE_ID VARCHAR (3000),
	@NAME VARCHAR (3000),
	@DEPT_ARTICLE VARCHAR (255),
	@IS_STOCK_IN_VAULT bit,
	@IS_STOCK_IN_HAND bit,
	@IS_USAGE bit,
	@IS_CORRECTION bit,
	@IS_ACTIVE bit,
	@CREATED_BY VARCHAR(50),
	@CREATED_DT DATETIME,
	@CREATED_HOST VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.MS_ARTICLE_PROFILE
	(
	    [ARTICLE_ID]  ,
		[NAME] ,
		[DEPT_ARTICLE] ,
		[IS_STOCK_IN_VAULT] ,
		[IS_STOCK_IN_HAND] ,
		[IS_USAGE] ,
		[IS_CORRECTION],
		[IS_ACTIVE] ,
		[CREATED_BY] ,
		[CREATED_DT] ,
		[CREATED_HOST] 
	)
	VALUES
	(   @ARTICLE_ID  ,
		@NAME ,
		@DEPT_ARTICLE ,
		@IS_STOCK_IN_VAULT ,
		@IS_STOCK_IN_HAND ,
		@IS_USAGE ,
		@IS_CORRECTION,
		@IS_ACTIVE,
		@CREATED_BY,
		@CREATED_DT,
		@CREATED_HOST
	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_ATTACHMENT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ATTACHMENT]
	@DocumentId INT,
	@IssueNumber VARCHAR(255),
	@FileType VARCHAR(20),
	@FileName VARCHAR(80),
	@SaveFileName VARCHAR(80),
	@FileUrl VARCHAR(200),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.TR_ATTACHMENT (
		DOC_ID,
		REGIS_NO,
		FILE_TYPE,
		FILE_NM,
		SAVE_FILE_NM,
		FILE_URL,
		REF_ID,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
		)
	VALUES (
		@DocumentId,    -- DOC_ID - bigint
		@IssueNumber,   -- ISSUE_NO - varchar(50)
		@FileType,      -- FILE_TYPE - varchar(20)
		@FileName,      -- FILE_NM - varchar(80)
		@SaveFileName,  -- SAVE_FILE_NM - varchar(80)
		@FileUrl,       -- FILE_URL - varchar(200)
		0,              -- REF_ID - bigint
		@CreatedBy,     -- CREATED_BY - varchar(50)
		@CreatedDate,   -- CREATED_DT - datetime
		@CreatedHost    -- CREATED_HOST - varchar(20)
		)
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_BOOKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_BOOKPROFILE]
	-- Add the parameters for the stored procedure here
	@BOOK_ID VARCHAR (3000),
	@BOOK_NAME VARCHAR (3000),
	@DEPT_BOOK VARCHAR (255),
	@BOOK_SEQ int,
	@BEFORE_OFFSET_DAYS int,
	@AFTER_OFFSET_DAYS int,
	@IS_ACTIVE bit,
	@CREATED_BY VARCHAR(50),
	@CREATED_DT DATETIME,
	@CREATED_HOST VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.MS_BOOK_PROFILE
	(
	    [BOOK_ID]  ,
		[BOOK_NAME] ,
		[DEPT_BOOK] ,
		[BOOK_SEQ] ,
		[BEFORE_OFFSET_DAYS] ,
		[AFTER_OFFSET_DAYS] ,
		[IS_ACTIVE] ,
		[CREATED_BY] ,
		[CREATED_DT] ,
		[CREATED_HOST] 
	)
	VALUES
	(   @BOOK_ID  ,
		@BOOK_NAME ,
		@DEPT_BOOK ,
		@BOOK_SEQ ,
		@BEFORE_OFFSET_DAYS ,
		@AFTER_OFFSET_DAYS ,
		@IS_ACTIVE,
		@CREATED_BY,
		@CREATED_DT,
		@CREATED_HOST
	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_DOCUMENTHISTORY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DOCUMENTHISTORY]
	@HistoryType VARCHAR(20),
	@RegisNo VARCHAR(50) = 0,
	@RefId BIGINT,
	@ActionBy VARCHAR(50),
	@Status VARCHAR(20),
	@Comment VARCHAR(2000) = '',
	@CreatedBy VARCHAR(50) = '',
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(50),
	@ModifiedBy VARCHAR(50)= null,
	@ModifiedDate DATETIME=null,
	@ModifiedHost VARCHAR(50)=null,
	@WfStep INT=null
AS
BEGIN
	DECLARE @SeqNo INT = 1

	SELECT @SeqNo = max(isnull(seq_no, 0)) + 1
	FROM TR_DOCUMENT_HISTORY
	WHERE 1 = 1
		AND HIST_TYPE = @HistoryType
		AND REF_ID = @RefId

	SET @SeqNo = ISNULL(@SeqNo, 1)
	
    IF charindex('JAKARTA01\', @ActionBy) > 0
		SET @ActionBy = replace(@ActionBy, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @ActionBy) > 0
		SET @ActionBy = replace(@ActionBy, 'jakarta01\', '')

	INSERT INTO TR_DOCUMENT_HISTORY (
		SEQ_NO,
		HIST_TYPE,
		REGIS_NO,
		REF_ID,
		STATUS,
		COMMENT,
		ACTION_BY,
		ACTION_DT,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST,
		WFSTEP
		)
	VALUES (
		@SeqNo,
		@HistoryType,
		@RegisNo,
		@RefId,
		@Status,
		@Comment,
		@ActionBy,
		getdate(),
		@CreatedBy,
		@CreatedDate,
		@CreatedHost,
		@ModifiedBy,
		@ModifiedDate,
		@ModifiedHost,
		@WfStep
		)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_DRAFTIMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DRAFTIMPORTANTART]
@REGIS_NO [varchar](50)
,@CREATOR [varchar](50)
,@CREATOR_DEPT [varchar](30)
,@CREATOR_UNIT [varchar](30)
,@CREATOR_ROLE [varchar](30)
,@REQUEST_DT [date]
,@CUST_BRANCH [varchar](255)
,@ARTICLE_NAME [varchar](255)
,@REGIS_TYPE [varchar](255)
,@BALANCE_IN_HAND [numeric]
,@VALUE [numeric]
,@DATE [date]
,@REF_NO [varchar](255)
,@REMARKS [varchar](8000)
,@ART_STATUS [varchar](8000)
,@APPROVAL_STS [varchar](50)
,@IS_DRAFT [bit]
,@IS_ACTIVE [bit]
,@CREATED_BY [varchar](50)
,@CREATED_DT [datetime]
,@CREATED_HOST [varchar](20)

,@ARTICLE_ID [varchar](255) = ''
,@BALANCE_IN_VAULT [numeric] = null
,@SP_DATE [datetime] = null
,@PL_DATE [datetime] = null
,@PURPOSE [varchar](8000) = ''
,@CUSTOMER [varchar](255) = ''

AS
BEGIN
	INSERT INTO TR_IMPORTANT_ART
	(
	 REGIS_NO
	,CREATOR
	,CREATOR_DEPT
	,CREATOR_UNIT
	,CREATOR_ROLE
	,REQUEST_DT
	,CUST_BRANCH
	,ARTICLE_NAME
	,REGIS_TYPE
	,BALANCE_IN_HAND
	,VALUE
	,[DATE]
	,REF_NO
	,REMARKS
	,ART_STATUS
	,APPROVAL_STS
	,IS_DRAFT
	,IS_ACTIVE
	,CREATED_BY
	,CREATED_DT
	,CREATED_HOST

	,ARTICLE_ID
	,BALANCE_IN_VAULT
	,SP_DATE
	,PL_DATE

	,PURPOSE
	,CUSTOMER
	)
	SELECT
	 @REGIS_NO
	,@CREATOR
	,@CREATOR_DEPT
	,@CREATOR_UNIT
	,@CREATOR_ROLE
	,@REQUEST_DT
	,@CUST_BRANCH
	,@ARTICLE_NAME
	,@REGIS_TYPE
	,@BALANCE_IN_HAND
	,@VALUE
	,@DATE
	,@REF_NO
	,@REMARKS
	,@ART_STATUS
	,@APPROVAL_STS
	,@IS_DRAFT
	,@IS_ACTIVE
	,@CREATED_BY
	,@CREATED_DT
	,@CREATED_HOST

	,@ARTICLE_ID
	,@BALANCE_IN_VAULT
	,@SP_DATE
	,@PL_DATE

	,@PURPOSE
	,@CUSTOMER

	SELECT @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_DRAFTIMPORTANTARTACTIVITY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPI_DRAFTIMPORTANTARTACTIVITY]
@DOC_ID int
,@REGIS_NO varchar(255)
,@ACTIVITY_CD varchar(255)
,@CREATOR varchar(255)
,@CREATOR_DEPT varchar(255)
,@CREATOR_UNIT varchar(255)
,@CREATOR_ROLE varchar(255)
,@REQUEST_DT DATETIME
,@CORR_BALANCE_IN_HAND decimal
,@CORR_VALUE decimal
,@CORR_DATE [date]
,@CORR_REF_NO varchar(255)
,@CORR_REMARKS varchar(8000)
,@CAN_REASON varchar(8000)
,@INSP_DP DATE
,@INSP_BY varchar(255)
,@APPROVAL_STS varchar(255)
,@IS_DRAFT bit
,@IS_ACTIVE bit
,@CREATED_BY varchar(255)
,@CREATED_DT datetime
,@CREATED_HOST varchar(255)
AS
BEGIN 
	INSERT INTO TR_IMPORTANT_ART_ACTIVITY
	(
		DOC_ID
		,REGIS_NO
		,ACTIVITY_CD
		,CREATOR
		,CREATOR_DEPT
		,CREATOR_UNIT
		,CREATOR_ROLE
		,REQUEST_DT
		,CORR_BALANCE_IN_HAND
		,CORR_VALUE
		,CORR_DATE
		,CORR_REF_NO
		,CORR_REMARKS
		--,CAN_REASON
		--,INSP_DP
		--,INSP_BY
		,APPROVAL_STS
		,IS_DRAFT
		,IS_ACTIVE
		,CREATED_BY
		,CREATED_DT
		,CREATED_HOST
	)
	SELECT 
	@DOC_ID
	,@REGIS_NO
	,@ACTIVITY_CD
	,@CREATOR
	,@CREATOR_DEPT
	,@CREATOR_UNIT
	,@CREATOR_ROLE
	,@REQUEST_DT
	,@CORR_BALANCE_IN_HAND
	,@CORR_VALUE
	,@CORR_DATE
	,@CORR_REF_NO
	,@CORR_REMARKS
	--,@CAN_REASON
	--,@INSP_DP
	--,@INSP_BY
	,@APPROVAL_STS
	,@IS_DRAFT
	,@IS_ACTIVE
	,@CREATED_BY
	,@CREATED_DT
	,@CREATED_HOST


	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_LETTERPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_LETTERPROFILE]
	-- Add the parameters for the stored procedure here
	@LETTER_TYPE VARCHAR(255),
	@DESCS VARCHAR(255),
	@DEPT_LETTER VARCHAR(255),
	@IS_ACTIVE BIT,
	@CREATED_BY VARCHAR(50),
	@CREATED_DT DATETIME,
	@CREATED_HOST VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.MS_LETTER_PROFILE
	(
	    [LETTER_TYPE],
	    [DESCS],
	    [DEPT_LETTER],
		[IS_ACTIVE],
	    [CREATED_BY],
	    [CREATED_DT],
	    [CREATED_HOST]
	)
	VALUES
	(   @LETTER_TYPE,        -- LETTER  TYPE - varchar(30)
	    @DESCS,        -- DESCS - varchar(5000)
	    @DEPT_LETTER,        -- DEPT LETTER - varchar(8000)
	    @IS_ACTIVE,         -- IS ACTIVE - int
	    @CREATED_BY,        -- CREATED_BY - varchar(50)
	    @CREATED_DT, -- CREATED_DT - datetime
	    @CREATED_HOST        -- CREATED_HOST - varchar(20)
	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_OUTGOINGLETTER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Robby>
-- Create date: <20211220>
-- Description:	<[UDPI_OUTGOINGLETTER>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_OUTGOINGLETTER]
	@RegistrationNo VARCHAR(2555),
	@Creator VARCHAR(50),
	@CreatorDept VARCHAR(30),
	@CreatorUnit VARCHAR(100),
	@CreatorRole VARCHAR(30),
	@RequestDate DATETIME,
	@OnBehalfOf VARCHAR(50),
	@BranchCode VARCHAR(50),
	@LetterType VARCHAR(255),
	@CustomerCode VARCHAR(255),
	@AccountNo VARCHAR(255),
	@VirtualAccount VARCHAR(255),
	@LetCreatedDate DATETIME,
	@Subject VARCHAR(255),
	@SubjectEng VARCHAR(255),
	@Attention VARCHAR(255),
	@Location VARCHAR(8000),
	@Address VARCHAR(MAX),
	@Remarks VARCHAR(8000),
	@SubmitDate DATETIME,
	@SubmitBy VARCHAR(50),
	@SubmitMethod VARCHAR(50),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRAN

	DECLARE @DocId BIGINT = 0
	
	INSERT INTO dbo.TR_OUTGOING_LETTER(
		REGIS_NO,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		ON_BEHALF_OF,
		BRANCH_CD,
		LETTER_TYPE,
		CUST_CD,
		CUST_ACC_NO,
		VIRTUAL_ACC,
		LET_CREATED_DT,
		[SUBJECT],
		SUBJECT_ENG,
		ATTENTION,
		LOCATION,
		[ADDRESS],
		REMARKS,
		SUBMIT_DT,
		SUBMIT_BY,
		SUBMIT_METHOD,
		LETTER_STS,
		APPROVAL_STS,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST
		,IS_DRAFT
		)
	VALUES (
		@RegistrationNo,
		@Creator,
		@CreatorDept,
		@CreatorUnit,
		@CreatorRole,
		@RequestDate,
		@OnBehalfOf,
		@BranchCode,
		'Others',
		@CustomerCode,
		@AccountNo,
		@VirtualAccount,
		@LetCreatedDate,
		@Subject,
		@SubjectEng,
		@Attention,
		@Location,
		@Address,
		@Remarks,
		@SubmitDate,
		@SubmitBy,
		@SubmitMethod,
		'Active',
		'Awaiting Approval',
		1,
		@CreatedBy,
		@CreatedDate,
		@CreatedHost,
		@CreatedBy,
		@CreatedDate,
		@CreatedHost
		,CASE WHEN @SubmitType='Save as Draft' THEN 1 ELSE 0 END
		)

	SELECT @DocId = @@IDENTITY

	IF @@ERROR > 0
		ROLLBACK TRAN
	ELSE
		COMMIT TRAN

	SELECT @DocId
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_PARAMETER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_PARAMETER]
	-- Add the parameters for the stored procedure here
	@Group VARCHAR(3000),
	@Keyfield VARCHAR(30),
	@Value VARCHAR(3000),
	@Description VARCHAR(3000),
	@Sequence INT = NULL,
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.MS_PARAMETER
	(
	    [GROUP],
		KEYFIELD,
	    VALUE,
	    DESCS,
	    SEQ,
	    CREATED_BY,
	    CREATED_DT,
	    CREATED_HOST
	)
	VALUES
	(   @Group,        -- GROUP - varchar(30)
		@Keyfield,
	    @Value,        -- VALUE - varchar(5000)
	    @Description,        -- DESCS - varchar(8000)
	    @Sequence,         -- SEQ - int
	    @CreatedBy,        -- CREATED_BY - varchar(50)
	    @CreatedDate, -- CREATED_DT - datetime
	    @CreatedHost        -- CREATED_HOST - varchar(20)
	)

	SELECT @@IDENTITY
END




GO
/****** Object:  StoredProcedure [dbo].[UDPI_RECEIVENOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPI_RECEIVENOTICE]
	@DocId BIGINT,
    @RegistrationNo VARCHAR(2555),
	@Creator VARCHAR(50),
	@CreatorDept VARCHAR(30),
	@CreatorUnit VARCHAR(100),
	@CreatorRole VARCHAR(30),
	@RequestDate DATETIME,
    @ReceivedDate DATETIME,
	@ReceivedMethod VARCHAR(30),
	@ReceivedBy VARCHAR(30),
	@Remarks VARCHAR(8000),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRAN

	DECLARE @NDocId BIGINT = 0

    -- Insert statements for procedure here
	INSERT INTO TR_BOOK_RECEIVED_NOTICE 
	(
	   [DOC_ID]
      ,[REGIS_NO]
      ,[CREATOR]
      ,[CREATOR_DEPT]
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      ,[REQUEST_DT]
      ,[RCV_DT]
      ,[RCV_METHOD]
      ,[RCV_BY]
      ,[REMARKS]
      ,[APPROVAL_STS]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]
      ,[IS_DRAFT]
	)
	VALUES
	(   
		@DocId,
		@RegistrationNo,
		@Creator,
		@CreatorDept,
		@CreatorUnit,
		@CreatorRole,
		@RequestDate,
		@ReceivedDate,
		@ReceivedMethod,
		@ReceivedBy,
		@Remarks,
		'Awaiting Approval',
		1,
		@CreatedBy,
		@CreatedDate,
		@CreatedHost
		,CASE WHEN @SubmitType='Save as Draft' THEN 1 ELSE 0 END
		
	)
	
	SELECT @NDocId = @@IDENTITY

	IF @@ERROR > 0
		ROLLBACK TRAN
	ELSE
		COMMIT TRAN

	SELECT @NDocId
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_REGISTRATION]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<[UDPI_REGISTRATION>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_REGISTRATION]
	@RegistrationNo VARCHAR(2555)='-',
	@BookType VARCHAR(50),
	@Creator VARCHAR(50),
	@CreatorDept VARCHAR(30),
	@CreatorUnit VARCHAR(100),
	@CreatorRole VARCHAR(30),
	@RequestDate DATETIME,
	@BranchCode VARCHAR(50),
	@ReportedDate DATETIME,
	@Type VARCHAR(50),
	@ReceivedMethod VARCHAR(30),
	@CustomerCode VARCHAR(255),
	@AccountNo VARCHAR(255),
	@HasAmount BIT,
	@AmountCurrency VARCHAR(255),
	@Amount NUMERIC(18,2),
	@Remarks VARCHAR(8000),
	@PolrReceivedDate DATETIME,
	@PolrReceivedBy VARCHAR(8000),
	@PolrReceivedRemarks VARCHAR(MAX),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRAN

	DECLARE @DocId BIGINT = 0
	--IF(@SubmitType='Submit')
	--BEGIN
	--	EXEC UDPS_GETNEWREGISNO @RegistrationNo out, @BookType, @CreatorDept
	--	--SELECT @RegistrationNo
	--END
	--ELSE
	--	SET @RegistrationNo = '-'
	
	INSERT INTO dbo.TR_BOOK (
		REGIS_NO,
		BOOK_TYPE,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		BRANCH_CD,
		REPORTED_DT,
		TYPE,
		RECEIVED_METHOD,
		CUSTOMER_CD,
		ACCOUNT_NO,
		HAS_AMOUNT,
		AMOUNT_CURR,
		AMOUNT,
		REMARKS,
		POLR_RECEIVED_DT,
		POLR_RECEIVED_BY,
		POLR_RECEIVED_REMARKS,
		APPROVAL_STS,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
		,IS_DRAFT
		)
	VALUES (
		@RegistrationNo,
		@BookType,
		@Creator,
		@CreatorDept,
		@CreatorUnit,
		@CreatorRole,
		@RequestDate,
		@BranchCode,
		@ReportedDate,
		@Type,
		@ReceivedMethod,
		@CustomerCode,
		@AccountNo,
		@HasAmount,
		@AmountCurrency,
		@Amount,
		@Remarks,
		@PolrReceivedDate,
		@PolrReceivedBy,
		@PolrReceivedRemarks,
		'Awaiting Approval',
		1,
		@CreatedBy,
		@CreatedDate,
		@CreatedHost
		,CASE WHEN @SubmitType='Save as Draft' THEN 1 ELSE 0 END
		)

	SELECT @DocId = @@IDENTITY

	IF @@ERROR > 0
		ROLLBACK TRAN
	ELSE
		COMMIT TRAN

	SELECT @DocId
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_REMINDERLOG]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPI_REMINDERLOG]
	@DocId BIGINT
	,@RegistrationNo varchar(255)
	,@RebookType VARCHAR(MAX)
	,@SentTo VARCHAR(MAX)
	,@SentCC VARCHAR(MAX)
	,@MailSubject VARCHAR(MAX)
	,@MailBody VARCHAR(MAX)
	,@SentOn DATETIME
	,@LastStatus VARCHAR(200)
	,@CreatedBy VARCHAR(50)
	,@CreatedDate DATETIME
	,@CreatedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.TR_REMINDER_LOG
	(
	    DOC_ID,
	    REGIS_NO,
		TRX_TYPE,
	    SENT_ON,
		SENT_TO,
		SENT_CC,
		MAIL_SUBJECT,
		MAIL_BODY,
		LAST_STATUS,	
	    CREATED_BY,
	    CREATED_DT,
	    CREATED_HOST
	)
	VALUES
	(	@DocId
		,@RegistrationNo 
		,@RebookType
		,@SentOn
		,@SentTo
		,@SentCC
		,@MailSubject
		,@MailBody
		,@LastStatus 
		,@CreatedBy 
		,@CreatedDate
		,@CreatedHost
	)

	SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_STOCKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_STOCKPROFILE]
	-- Add the parameters for the stored procedure here
	@ARTICLE_ID VARCHAR(255),
	@DEPT_OF_ARTICLE VARCHAR(255),
	@STOCK_IN_VAULT NUMERIC (18,2),
	@STOCK_IN_HAND NUMERIC (18,2),
	@USAGE NUMERIC (18,2),
	@INCOMING_STOCK NUMERIC (18,2), 
	@CORRECTION_IN_VAULT NUMERIC (18,2),
	@CORRECTION_IN_HAND NUMERIC (18,2),
	@CORRECTION_IN_USAGE NUMERIC (18,2),
	@BALANCE_IN_HAND NUMERIC (18,2),
	@CREATED_BY VARCHAR(50),
	@CREATED_DT DATETIME,
	@CREATED_HOST VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.MS_STOCK_PROFILE
	(
	    [ARTICLE_ID],
	    [DEPT_OF_ARTICLE],
	    [STOCK_IN_VAULT],
		[STOCK_IN_HAND],
		[USAGE],
		[INCOMING_STOCK],
		[CORRECTION_IN_VAULT],
		[CORRECTION_IN_HAND],
		[CORRECTION_IN_USAGE],
		[BALANCE_IN_HAND],
	    [CREATED_BY],
	    [CREATED_DT],
	    [CREATED_HOST]
	)
	VALUES
	(   @ARTICLE_ID,        -- LETTER  TYPE - varchar(30)
	    @DEPT_OF_ARTICLE,        -- DESCS - varchar(5000)
	    @STOCK_IN_VAULT,        -- DEPT LETTER - varchar(8000)
	    @STOCK_IN_HAND,
		@USAGE,
		@INCOMING_STOCK,
		@CORRECTION_IN_VAULT,
		@CORRECTION_IN_HAND,
		@CORRECTION_IN_USAGE,
		@BALANCE_IN_HAND,         
	    @CREATED_BY,        -- CREATED_BY - varchar(50)
	    @CREATED_DT, -- CREATED_DT - datetime
	    @CREATED_HOST        -- CREATED_HOST - varchar(20)
	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_USERACTIVITYLOGDTL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_USERACTIVITYLOGDTL]
	-- Add the parameters for the stored procedure here
	@DocumentId INT,
	@ActivityCode VARCHAR(50),
	@FieldName VARCHAR(80),
	@OldValue VARCHAR(8000),
	@NewValue VARCHAR(8000),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@OldValue = '(null)')
		SET @OldValue = ''

	IF (@NewValue = '(null)')
		SET @NewValue = ''

	-- Insert statements for procedure here
	INSERT INTO dbo.TR_USER_ACTIVITY_LOG_DTL (
		DOC_ID,
		ACT_CD,
		FIELD_NM,
		OLD_VALUE,
		NEW_VALUE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
		)
	VALUES (
		@DocumentId, -- DOC_ID - bigint
		@ActivityCode, -- ACT_CD - varchar(50)
		@FieldName, -- FIELD_NM - varchar(80)
		@OldValue, -- OLD_VALUE - varchar(8000)
		@NewValue, -- NEW_VALUE - varchar(8000)
		@CreatedBy, -- CREATED_BY - varchar(50)
		@CreatedDate, -- CREATED_DT - datetime
		@CreatedHost -- CREATED_HOST - varchar(20)
		)
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_USERACTIVITYLOGHDR]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_USERACTIVITYLOGHDR]
	-- Add the parameters for the stored procedure here
	@ActivityCode VARCHAR(50),
	@ActivityDescription VARCHAR(100),
	@ActivityValue VARCHAR(1000),
	@ActivityResult VARCHAR(50),
	@ActivityUserName VARCHAR(50),
	@ActivityDepartment VARCHAR(100),
	@ActivityDate DATETIME,
	@ActivityWorkstation VARCHAR(30),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20),
	@WFGUID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DocumentID BIGINT

	-- Insert statements for procedure here
	INSERT INTO dbo.TR_USER_ACTIVITY_LOG_HDR (
		ACT_CD,
		ACT_DESC,
		ACT_VAL,
		ACT_RESULT,
		ACT_USER,
		ACT_DEPT,
		ACT_DT,
		ACT_WORKSTATION,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		WFGUID
		)
	VALUES (
		@ActivityCode, -- ACT_CD - varchar(50)
		@ActivityDescription, -- ACT_DESC - varchar(100)
		@ActivityValue, -- ACT_VAL - varchar(500)
		'Success', -- ACT_RESULT - varchar(50)
		@ActivityUserName, -- ACT_USER - varchar(50)
		@ActivityDepartment, -- ACT_DEPT - varchar(30)
		@ActivityDate, -- ACT_DT - datetime
		@ActivityWorkstation, -- ACT_WORKSTATION - varchar(30)
		@CreatedBy, -- CREATED_BY - varchar(50)
		@CreatedDate, -- CREATED_DT - datetime
		@CreatedHost, -- CREATED_HOST - varchar(20)
		@WFGUID
		)

	SELECT @DocumentID = @@IDENTITY

	SELECT @DocumentID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_WORKFLOWPROCESS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPI_WORKFLOWPROCESS] 
	@DOCID BIGINT,
	@REFID BIGINT,
	@WFTASK NVARCHAR(100),
	@WFACTION NVARCHAR(40),
	@REGISNO NVARCHAR(500),
	@SUBMITTEDBY NVARCHAR(100),
	@SUBMITTEDDATE DATETIME,
    @EDITDRAFTID BIGINT = 0
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedure
	INSERT INTO TR_WORKFLOWPROCESS (
		DOCID,
		REFID,
		WFTASK,
		WFACTION,
		REGIS_NO,
		CREATED_BY,
		CREATED_DT,
        ISACTIVE,
        WFTASKNAME
		)
	VALUES (
		@DOCID,
		@REFID,
		@WFTASK,
        @WFACTION,
		@REGISNO,
		@SUBMITTEDBY,
		@SUBMITTEDDATE,
        1,
        'Approve'
		)

	SELECT @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[UDPP_RESTOREDELETEOUTGOING]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPP_RESTOREDELETEOUTGOING]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@RegistrationNo VARCHAR(100),
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here

		UPDATE dbO.TR_OUTGOING_LETTER
		SET IS_ACTIVE = 1,
			IS_DRAFT = 1,
			MODIFIED_BY = @ModifiedBy,
			MODIFIED_DT = @ModifiedDate,
			MODIFIED_HOST = @ModifiedHost
		WHERE UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPP_REVOKESUBMITTED]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPP_REVOKESUBMITTED]
	-- add more stored procedure parameters here
	@ProcessId VARCHAR(20)
AS
BEGIN
	
	DECLARE 
	@ArticleID varchar(255)
	,@RegistrationType varchar(255)
	,@Value decimal = 0
	,@IsCorrection bit = 0
	,@IsCancellation bit = 0
	,@IsRejected bit = 0

	-- body of the stored procedure
	DECLARE @wftask VARCHAR(100),
		@wfaction VARCHAR(100),
		@docid BIGINT

	SELECT @wftask = WFTASK,
		@wfaction = WFACTION,
		@docid = DOCID
	FROM TR_WORKFLOWPROCESS
	WHERE PROCID = @ProcessId

	IF (@wftask IN ('BOOK_CLAIM','BOOK_NOTICE', 'BOOK_REQUEST'))
	BEGIN
		UPDATE TR_BOOK
		SET IS_DRAFT = 1
		WHERE [UID] = @docid
	END
	ELSE IF (@wftask = 'ACTION_TAKEN')
	BEGIN
		UPDATE TR_BOOK_ACTION_TAKEN
		SET IS_DRAFT = 1
		WHERE [UID] = @docid
	END
	ELSE IF (@wftask = 'RECEIVED_NOTICE')
	BEGIN
		UPDATE TR_BOOK_RECEIVED_NOTICE
		SET IS_DRAFT = 1
		WHERE [UID] = @docid
	END
	ELSE IF (@wftask = 'IMPORT_ARTICLE')
	BEGIN
		UPDATE TR_IMPORTANT_ART
		SET IS_DRAFT = 1
		WHERE [UID] = @docid

		SELECT 
		@ArticleID = Art.ARTICLE_NAME
		,@RegistrationType = Art.REGIS_TYPE
		,@Value = Art.VALUE
		,@IsCorrection = 0
		,@IsCancellation = 0
		,@IsRejected = 1
		FROM TR_IMPORTANT_ART Art
		WHERE [UID] = @docid

		EXEC UDPU_STOCKPROFILEVALUE
		@docID,@ArticleID,@RegistrationType,@Value,@IsCorrection,@IsCancellation,@IsRejected

	END
	ELSE IF (@wftask = 'IMPORT_CORRECTION')
	BEGIN
		UPDATE TR_IMPORTANT_ART_ACTIVITY
		SET IS_DRAFT = 1
		WHERE [UID] = @docid

		SELECT 
		@ArticleID = Art.ARTICLE_NAME
		,@RegistrationType = Art.REGIS_TYPE
		,@Value = Act.CORR_VALUE
		,@IsCorrection = 1
		,@IsCancellation = 0
		,@IsRejected = 1
		FROM TR_IMPORTANT_ART_ACTIVITY Act
		LEFT JOIN TR_IMPORTANT_ART Art
		ON Act.[DOC_ID] = Art.[UID]
		WHERE Act.[UID] = @docid

		EXEC UDPU_STOCKPROFILEVALUE
		@docID,@ArticleID,@RegistrationType,@Value,@IsCorrection,@IsCancellation,@IsRejected
	END
	ELSE IF (@wftask = 'OUT_LETTER')
	BEGIN
		UPDATE TR_OUTGOING_LETTER
		SET IS_DRAFT = 1
		WHERE [UID] = @docid
	END
		

	UPDATE TR_WORKFLOWPROCESS
	SET ISACTIVE = 0
	WHERE PROCID = @ProcessId
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_ACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ACTIONTAKEN]
	-- Add the parameters for the stored procedure here
	@ReqDateStart varchar(15),
	@ReqDateEnd varchar(15),
	@RegisNo varchar(50)=NULL,
	@varForm varchar(50)=NULL,
	@varCust varchar(50)=NULL,
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortField NVARCHAR(100) = 'UID',
	@SortOrder NVARCHAR(100) = 'DESC',
	@allPage varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	DECLARE @sql nvarchar(MAX);
	DECLARE @offset nvarchar(10);
	DECLARE @newsize nvarchar(10);
	
	SET @PageNo=@PageNo-1;
	if (@PageNo<1)
	BEGIN
		SET @offset=0
		SET @newsize=@PageSize
	END
	ELSE
	BEGIN
		SET @offset=@PageNo*@PageSize
		SET @newsize=@PageSize-1
	END
	SET @sql='
	SELECT b.[UID]
      ,b.[DOC_ID]
      ,b.[REGIS_NO]
      ,b.[CREATOR]
      ,b.[CREATOR_DEPT]
      ,b.[CREATOR_UNIT]
      ,b.[CREATOR_ROLE]
      ,FORMAT(b.[REQUEST_DT],''dd/MMM/yyyy'') as [REQUEST_DT]
	  ,FORMAT(b.[ACT_DT],''dd/MMM/yyyy HH:mm:ss'') as [ACT_DT]
      ,b.[ACT_ACTION]
      ,b.[ACTOR]
      ,ISNULL(NULLIF(b.[REMARKS], ''''),b.[REMARKS]) as [REMARKS]
      ,b.[RESULT]
      ,b.[APPROVAL_STS]
      ,b.[IS_DRAFT]
      ,b.[IS_ACTIVE]
      ,b.[CREATED_BY]
      ,FORMAT(b.[CREATED_DT],''dd/MMM/yyyy HH:mm:ss'') as [CREATED_DT]
      ,b.[CREATED_HOST]
      ,b.[MODIFIED_BY]
      ,b.[MODIFIED_DT]
      ,b.[MODIFIED_HOST]
	  ,a.[UID_BOOK]
	  ,FORMAT(a.[REPORTED_DT],''dd/MMM/yyyy HH:mm:ss'') as [REPORTED_DT]
	  ,a.[RECEIVED_METHOD]
	  ,a.[ACCOUNT_NO]
	  ,a.[HAS_AMOUNT]
	  ,a.[AMOUNT_CURR]
	  ,a.[AMOUNT]
	  ,a.[POLR_RECEIVED_DT]
	  ,a.[POLR_RECEIVED_BY]
	  ,a.[POLR_RECEIVED_REMARKS]
	  ,a.[BOOK_TYPE]
	  ,a.[BRANCH_CD]
	  ,(SELECT ISNULL(NULLIF(Value, ''''),Value) as Value  FROM dbo.[MS_PARAMETER] WHERE [KEYFIELD]=a.[BRANCH_CD] AND [GROUP]=''BRANCH_CODE'') as [LONG_BRANCH] 
	  ,a.[TYPE]
	  ,cust.LegalName as [CUSTOMER_CD]
	  ,(SELECT ISNULL(NULLIF(LongName, ''''),LongName) as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as [DEPARTEMEN_LONG_NAME] 
	FROM [TR_BOOK_ACTION_TAKEN] as b
	LEFT JOIN (SELECT 
		UID as UID_BOOK,REPORTED_DT,RECEIVED_METHOD,ACCOUNT_NO,HAS_AMOUNT,AMOUNT_CURR,
		AMOUNT,POLR_RECEIVED_DT,POLR_RECEIVED_BY,POLR_RECEIVED_REMARKS,
		BOOK_TYPE,BRANCH_CD,TYPE,CUSTOMER_CD FROM TR_BOOK ) as a ON a.UID_BOOK=b.DOC_ID 
	LEFT JOIN (SELECT CustomerCode,LegalName FROM EASY_Customer_Code ) as cust ON cust.CustomerCode=a.CUSTOMER_CD
	WHERE 1=1 AND b.IS_ACTIVE=1'; 

	if (@ReqDateStart IS NOT NULL OR @ReqDateStart !='')
	BEGIN
		SET @sql=@sql + ' AND [REQUEST_DT] >= CONVERT(datetime,'''+@ReqDateStart +''')';
	END

	if ( @ReqDateEnd ='')
	BEGIN
		SET @sql=@sql
	END
	ELSE
	BEGIN
		if (@ReqDateEnd IS NOT NULL OR @ReqDateEnd !='' )
		BEGIN
			SET @sql=@sql + ' AND b.[REQUEST_DT] <= CONVERT(datetime,'''+@ReqDateEnd +''')';
		END
	END
	

	if (@RegisNo IS NOT NULL OR @RegisNo !='')
	BEGIN
		SET @sql=@sql + ' AND b.[REGIS_NO] LIKE ''%'+@RegisNo +'%'' ';
	END

	if (@varCust IS NOT NULL OR @varCust !='')
	BEGIN
		SET @sql=@sql + ' AND cust.LegalName LIKE ''%'+@varCust +'%'' ';
	END

	
	if (@varForm IS NOT NULL OR @varForm !='')
	BEGIN
		SET @sql=@sql + ' AND a.[TYPE] LIKE ''%'+@varForm +'%'' ';
	END

	/*

	if CHARINDEX(',', @cr)>0
		BEGIN
		
		if @fi='CREATOR_DEPT' 
		
		
			SET @sql=@sql +' AND 
			CASE 
			WHEN (charindex('''+@key+''', [CREATOR_DEPT])-1)>1 THEN 
				LEFT([CREATOR_DEPT],charindex('''+@key+''', [CREATOR_DEPT])-1) 
			ELSE [CREATOR_DEPT] END IN ('+@cr+') ORDER BY UID DESC';
			
			--SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ';

		
		ELSE
		
			SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ';
		END
	ELSE
		BEGIN
		--SET @cr = Replace(@cr, '%[^0-9a-zA-Z]%', '')
		SET @sql=@sql + ' AND ['+@fi+'] LIKE ''%'+@cr +'%'' ';
	END

	*/

	if (@SortOrder='ASC')
	BEGIN
		SET @SortOrder='ASC';
	END
	ELSE
	BEGIN
		SET @SortOrder='DESC';
	END
	SET @sql=@sql+' ORDER BY '+@SortField+' '+@SortOrder; 

	if (@allPage IS NOT NULL OR @allPage !='')
		BEGIN
		SET @sql=@sql;
		END
	ELSE
		BEGIN
		SET @sql=@sql + ' OFFSET '+@offset+' ROWS FETCH NEXT '+@newsize+' ROWS ONLY';
		END
	
	EXECUTE( @sql );
	--SELECT @offset;
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_ACTIONTAKEN_ATTACH]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<UDPS_ATTACHMENTBYDOCID>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ACTIONTAKEN_ATTACH]
	@DocId VARCHAR(10) ,
	@DocType VARCHAR(100) 
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT ta.UID AS Id
		,ta.DOC_ID AS DocumentId
		,ta.REGIS_NO AS RegistrationNo
		,ta.FILE_TYPE AS FileType
		,ta.FILE_NM AS FileName
		,ta.SAVE_FILE_NM AS SaveFileName
		,ta.FILE_URL AS FileUrl
		,ta.REF_ID AS RefId
		,ta.CREATED_BY AS CreatedBy
		,ta.CREATED_DT AS CreatedDate
		,ta.CREATED_HOST AS CreatedHost
		,ta.MODIFIED_BY AS ModifiedBy
		,ta.MODIFIED_DT AS ModifiedDate
		,ta.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_ATTACHMENT AS ta
	WHERE 1 = 1
		AND [ta].[DOC_ID] = @DocId 
		AND [ta].[FILE_TYPE]=@DocType
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_ACTIONTAKEN_DET]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ACTIONTAKEN_DET]
	-- Add the parameters for the stored procedure here
	@fi varchar(15)
	,@cr varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @sql nvarchar(MAX);
	/*
	SET @sql='
	SELECT [UID]
      ,[DOC_ID]
      ,[REGIS_NO]
      ,[CREATOR]
      ,[CREATOR_DEPT]
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      ,[REQUEST_DT]
      ,[ACT_DT]
      ,[ACT_ACTION]
      ,[ACTOR]
      ,[REMARKS]
      ,[RESULT]
      ,[APPROVAL_STS]
      ,[IS_DRAFT]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]
      ,[MODIFIED_BY]
      ,[MODIFIED_DT]
      ,[MODIFIED_HOST]
  FROM [TR_BOOK_ACTION_TAKEN] 
  WHERE 1=1';
  */
  SET @sql='
	SELECT b.[UID]
      ,b.[DOC_ID]
      ,b.[REGIS_NO]
      ,b.[CREATOR]
      ,b.[CREATOR_DEPT]
      ,b.[CREATOR_UNIT]
      ,b.[CREATOR_ROLE]
      ,FORMAT(b.[REQUEST_DT],''dd/MMM/yyyy'') as [REQUEST_DT]
	  ,FORMAT(b.[ACT_DT],''dd/MMM/yyyy HH:mm:ss'') as [ACT_DT]
      --,b.[ACT_DT]
      ,b.[ACT_ACTION]
      ,b.[ACTOR]
      ,ISNULL(NULLIF(b.[REMARKS], '' ''),b.[REMARKS]) as [REMARKS]
      ,b.[RESULT]
      ,b.[APPROVAL_STS]
      ,b.[IS_DRAFT]
      ,b.[IS_ACTIVE]
      ,b.[CREATED_BY]
      ,FORMAT(b.[CREATED_DT],''dd/MMM/yyyy H:m:s'') as [CREATED_DT]
      ,b.[CREATED_HOST]
      ,b.[MODIFIED_BY]
      ,b.[MODIFIED_DT]
      ,b.[MODIFIED_HOST]
	  ,a.[UID_BOOK]
	  ,a.[REPORTED_DT]
	  ,a.[RECEIVED_METHOD]
	  ,a.[ACCOUNT_NO]
	  ,a.[HAS_AMOUNT]
	  ,a.[AMOUNT_CURR]
	  ,a.[AMOUNT]
	  ,a.[POLR_RECEIVED_DT]
	  ,a.[POLR_RECEIVED_BY]
	  ,a.[POLR_RECEIVED_REMARKS]
	  ,a.[BOOK_TYPE]
	  ,a.[BRANCH_CD]
	  ,a.[TYPE]
	  ,a.[CUSTOMER_CD]
	  ,Dept.LongName as [DEPARTEMEN_LONG_NAME] 
	FROM [TR_BOOK_ACTION_TAKEN] as b
	LEFT JOIN (SELECT 
		UID AS UID_BOOK,REPORTED_DT,RECEIVED_METHOD,ACCOUNT_NO,HAS_AMOUNT,AMOUNT_CURR,
		AMOUNT,POLR_RECEIVED_DT,POLR_RECEIVED_BY,POLR_RECEIVED_REMARKS,
		BOOK_TYPE,BRANCH_CD,TYPE,CUSTOMER_CD FROM TR_BOOK ) as a ON a.UID_BOOK=b.DOC_ID
	LEFT JOIN (SELECT 
		ShortName,LongName 
		FROM dbo.SN_BTMUDirectory_Department) as Dept ON Dept.ShortName=b.[CREATOR_DEPT]
	WHERE 1=1'; 

  if @fi IS NOT NULL
  BEGIN
	SET @sql=@sql+ ' AND '+ @fi +' LIKE '+ @cr
  END

  EXECUTE( @sql );

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_APPROVALRJTCMPBYHIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_APPROVALRJTCMPBYHIST]
	-- add more stored procedure parameters here
	@Department NCHAR(10) = NULL,
	@HistType NVARCHAR(100) = NULL
AS
BEGIN
	-- body of the stored procedure
	SELECT DISTINCT Username,
		RealName,
		Email
	FROM (
		SELECT Username,
			Realname,
			Email
		FROM vw_UserByDepartment
		WHERE 1 = 1
			AND (
				(
					@Department = 'DPS'
					AND DepartmentCode = @Department
					AND UnitID = '134'
					AND RoleShortName IN (
						'UH',
						'CHECKER'
						)
					)
				OR (
					@Department = 'SRB'
					AND DepartmentCode = @Department
					AND RoleShortName IN (
						'UH',
						'CHECKER'
						)
					AND @HistType NOT IN (
						'OUT_LETTER',
						'IMPORT_ARTICLE',
						'IMPORT_CORRECTION'
						)
					)
				)
		
		UNION
		
		SELECT Username,
			Realname,
			Email
		FROM vw_UserByDepartment
		WHERE 1 = 1
			AND (
				(
					@Department = 'DPS'
					AND DepartmentCode = @Department
					AND (
						RoleShortName = 'DH'
						OR (
							RoleShortName = 'UH'
							AND UnitID = '134'
							)
						)
					)
				OR (
					@Department = 'SRB'
					AND DepartmentCode = @Department
					AND RoleShortName IN (
						'UH',
						'DH'
						)
					AND @HistType NOT IN (
						'OUT_LETTER',
						'IMPORT_ARTICLE',
						'IMPORT_CORRECTION'
						)
					)
				)
		) A
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_ARTICLEIDCHECK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ARTICLEIDCHECK]
	-- Add the parameters for the stored procedure here
	--@Id INT,
	@ARTICLE_ID VARCHAR (3000),
	@NAME VARCHAR (3000),
	@DEPT_ARTICLE VARCHAR (255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

	--IF EXISTS (
	--		SELECT 1
	--		FROM MS_ARTICLE_PROFILE
	--		WHERE 1 = 1
	--			AND ARTICLE_ID = @ARTICLE_ID 
	--			AND IS_ACTIVE = 1
	--		)
	--BEGIN
	--	SELECT 1
	--END
	--ELSE 
	--BEGIN
	--	SELECT 0
	--END
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT AP.UID AS [UID]
		,AP.ARTICLE_ID AS [ARTICLE_ID]
		,AP.NAME AS [NAME]
		,AP.DEPT_ARTICLE AS [DEPT_ARTICLE]
		,AP.CREATED_BY AS [CREATED_BY]
		,AP.CREATED_DT AS [CREATED_DT]
		,AP.CREATED_HOST AS [CREATED_HOST]
		,AP.MODIFIED_BY AS [MODIFIED_BY]
		,AP.MODIFIED_DT AS [MODIFIED_DT]
		,AP.MODIFIED_HOST AS [MODIFIED_HOST]
	FROM dbo.MS_ARTICLE_PROFILE AS AP
	WHERE (AP.ARTICLE_ID = @ARTICLE_ID
	OR AP.NAME = @NAME )
	AND AP.DEPT_ARTICLE = @DEPT_ARTICLE
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_ARTICLEPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ARTICLEPROFILE]
	-- Add the parameters for the stored procedure here
	@UserDept VARCHAR(200) = '',
	@SearchArticleId NVARCHAR(100) = '',
	@SearchDepartment NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		SELECT *
		FROM MS_ARTICLE_PROFILE AS AP
		WHERE 
		(ISNULL(@SearchArticleId,'') = ''
			OR AP.ARTICLE_ID LIKE '%' + @SearchArticleId + '%'
		)
		AND(ISNULL(@SearchDepartment,'') = ''
			OR AP.DEPT_ARTICLE LIKE '%' + @SearchDepartment + '%'  
		)
		AND (
				AP.DEPT_ARTICLE  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		
		ORDER BY CASE 
				WHEN @SortOrder = 'ArticleID'
					THEN [ARTICLE_ID]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ArticleID_desc'
					THEN [ARTICLE_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Name'
					THEN [NAME]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Name_desc'
					THEN [NAME]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Department'
					THEN [DEPT_ARTICLE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Department_desc'
					THEN [DEPT_ARTICLE]
				END DESC,
			
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM MS_ARTICLE_PROFILE AS AP
		WHERE (ISNULL(@SearchArticleId,'') = ''
			OR AP.ARTICLE_ID LIKE '%' + @SearchArticleId + '%'
		)
		AND(ISNULL(@SearchDepartment,'') = ''
			OR AP.DEPT_ARTICLE LIKE '%' + @SearchDepartment + '%'  
		)
		AND (
				AP.DEPT_ARTICLE  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		)
	SELECT TotalRows,
		AP.UID AS [UID],
		AP.[ARTICLE_ID] AS [ARTICLE_ID],
		AP.NAME AS [NAME],
		AP.DEPT_ARTICLE AS [DEPT_ARTICLE],
		AP.IS_STOCK_IN_VAULT AS [IS_STOCK_IN_VAULT],
		AP.IS_STOCK_IN_HAND AS [IS_STOCK_IN_HAND],
		AP.IS_USAGE AS [IS_USAGE],
		AP.IS_CORRECTION AS [IS_CORRECTION],
		AP.IS_ACTIVE AS [IS_ACTIVE],
		AP.CREATED_BY AS CreatedBy,
		AP.CREATED_DT AS CreatedDate,
		AP.CREATED_HOST AS CreatedHost,
		AP.MODIFIED_BY AS ModifedBy,
		AP.MODIFIED_DT AS ModifiedDate,
		AP.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_ARTICLE_PROFILE AS AP,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = AP.UID
			)
	OPTION (RECOMPILE)
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_ARTICLEPROFILEDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ARTICLEPROFILEDETAIL]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT AP.UID AS [UID]
		,AP.ARTICLE_ID AS [ARTICLE_ID]
		,AP.NAME AS [NAME]
		,AP.DEPT_ARTICLE AS [DEPT_ARTICLE]
		,AP.IS_STOCK_IN_VAULT AS [IS_STOCK_IN_VAULT]
		,AP.IS_STOCK_IN_HAND AS [IS_STOCK_IN_HAND]
		,AP.IS_USAGE AS [IS_USAGE]
		,AP.IS_CORRECTION AS [IS_CORRECTION]
		,AP.IS_ACTIVE AS IS_ACTIVE
		,AP.CREATED_BY AS CREATED_BY
		,AP.CREATED_DT AS CREATED_DT
		,AP.CREATED_HOST AS CREATED_HOST
		,AP.MODIFIED_BY AS MODIFIED_BY
		,AP.MODIFIED_DT AS MODIFIED_DT
		,AP.MODIFIED_HOST AS MODIFIED_HOST
	FROM dbo.MS_ARTICLE_PROFILE AS AP
	WHERE AP.UID = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_ATTACHMENTBYDOCID]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<UDPS_ATTACHMENTBYDOCID>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ATTACHMENTBYDOCID]
	@DocId INT,
	@FileType VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ta.UID AS Id
		,ta.DOC_ID AS DocumentId
		,ta.REGIS_NO AS RegistrationNo
		,ta.FILE_TYPE AS FileType
		,ta.FILE_NM AS FileName
		,ta.SAVE_FILE_NM AS SaveFileName
		,ta.FILE_URL AS FileUrl
		,ta.REF_ID AS RefId
		,ta.CREATED_BY AS CreatedBy
		,ta.CREATED_DT AS CreatedDate
		,ta.CREATED_HOST AS CreatedHost
		,ta.MODIFIED_BY AS ModifiedBy
		,ta.MODIFIED_DT AS ModifiedDate
		,ta.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_ATTACHMENT AS ta
	WHERE 1 = 1
		AND [ta].[DOC_ID] = @DocId
		AND ta.FILE_TYPE = @FileType
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_ATTACHMENTBYID]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_ATTACHMENTBYID]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT ta.UID AS Id
		,ta.DOC_ID AS DocumentId
		,ta.REGIS_NO AS IssueNumber
		,ta.FILE_TYPE AS FileType
		,ta.FILE_NM AS FileName
		,ta.SAVE_FILE_NM AS SaveFileName
		,ta.FILE_URL AS FileUrl
		,ta.REF_ID AS RefId
		,ta.CREATED_BY AS CreatedBy
		,ta.CREATED_DT AS CreatedDate
		,ta.CREATED_HOST AS CreatedHost
		,ta.MODIFIED_BY AS ModifiedBy
		,ta.MODIFIED_DT AS ModifiedDate
		,ta.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_ATTACHMENT AS ta
	WHERE 1 = 1
		AND [ta].[UID] = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_BOOK]
	@UserDept VARCHAR(100) = '',
	@SearchByString NVARCHAR(100) = '',
	@AuthorString NVARCHAR(2000) = '',
	@CustomerNameString NVARCHAR(2000) = '',
	@ReceivedByString NVARCHAR(2000) = '',
	@ReceivedDateFrom DATETIME = NULL,
	@ReceivedDateTo DATETIME = NULL,
	@ReceivedMethodString NVARCHAR(2000) = '',
	@RegistrationNoString NVARCHAR(100) = '',
	@ReportedDateFrom DATETIME = NULL,
	@ReportedDateTo DATETIME = NULL,
	@RequestDateFrom DATETIME = NULL,
	@RequestDateTo DATETIME = NULL,
	@TypeString NVARCHAR(2000) = '',
	@FullSearchString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		-- Insert statements for procedure here
		SELECT BK.UID AS Id
		FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = BK.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
			LEFT JOIN TR_DOCUMENT_HISTORY tdh ON tdh.REF_ID = BK.UID AND tdh.HIST_TYPE LIKE 'BOOK%' AND tdh.UID = (SELECT MAX(UID) FROM TR_DOCUMENT_HISTORY tdh2 WHERE tdh2.REF_ID = BK.UID AND tdh2.HIST_TYPE LIKE 'BOOK%')
		WHERE BK.IS_ACTIVE = 1 AND BK.IS_DRAFT = 0
			AND (
				@UserDept = BK.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND (
				(
					(
						CASE 
							WHEN @SearchByString = 'Author'
								AND ISNULL(@AuthorString,'') <> ''
								THEN BK.CREATOR
							END
						) LIKE '%' + @AuthorString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Customer Name'
								AND ISNULL(@CustomerNameString,'') <> ''
								THEN cc.LegalName
							END
						) LIKE '%' + @CustomerNameString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received By'
								AND ISNULL(@ReceivedByString,'') <> ''
								THEN BK.POLR_RECEIVED_BY
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@ReceivedByString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received Date'
								AND @ReceivedDateFrom IS NOT NULL
								AND @ReceivedDateTo IS NOT NULL
								THEN BK.POLR_RECEIVED_DT
							END
						) BETWEEN @ReceivedDateFrom
						AND @ReceivedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Registration Number'
								AND ISNULL(@RegistrationNoString,'') <> ''
								THEN BK.REGIS_NO
							END
						) LIKE '%' + @RegistrationNoString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received Method'
								AND ISNULL(@ReceivedMethodString,'') <> ''
								THEN BK.RECEIVED_METHOD
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@ReceivedMethodString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Reported Date'
								AND @ReportedDateFrom IS NOT NULL
								AND @ReportedDateTo IS NOT NULL
								THEN BK.REPORTED_DT
							END
						) BETWEEN @ReportedDateFrom
						AND @ReportedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Request Date'
								AND @RequestDateFrom IS NOT NULL
								AND @RequestDateTo IS NOT NULL
								THEN BK.REQUEST_DT
							END
						) BETWEEN @RequestDateFrom
						AND @RequestDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Type'
								AND ISNULL(@TypeString,'') <> ''
								THEN TY.VALUE
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@TypeString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN ISNULL(@SearchByString,'') <> ''
								AND ISNULL(@AuthorString,'') = ''
								AND ISNULL(@CustomerNameString,'') = ''
								AND ISNULL(@ReceivedByString,'') = ''
								AND @ReceivedDateFrom IS NULL
								AND @ReceivedDateTo IS NULL
								AND ISNULL(@ReceivedMethodString,'') = ''
								AND ISNULL(@RegistrationNoString, '') = ''
								AND @ReportedDateFrom IS NULL
								AND @ReportedDateTo IS NULL
								AND @RequestDateFrom IS NULL
								AND @RequestDateTo IS NULL
								AND ISNULL(@TypeString, '') = ''
								THEN 1
							END
						) = 1
					)
				OR ISNULL(@SearchByString,'') = ''
				)
			AND (
				ISNULL(@FullSearchString,'') = ''
				OR BK.BRANCH_CD + ' - ' + BR.VALUE LIKE '%' + @FullSearchString + '%'
				OR isnull(BK.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') LIKE '%' + @FullSearchString + '%'
				OR FORM.VALUE LIKE '%' + @FullSearchString + '%'
				OR BK.REGIS_NO LIKE '%' + @FullSearchString + '%'
				OR BK.CREATOR LIKE '%' + @FullSearchString + '%'
				OR BK.CREATED_DT LIKE '%' + @FullSearchString + '%'
				OR BK.REPORTED_DT LIKE '%' + @FullSearchString + '%'
				OR TY.VALUE LIKE '%' + @FullSearchString + '%'
				OR RCVM.VALUE LIKE '%' + @FullSearchString + '%'
				OR cc.LegalName LIKE '%' + @FullSearchString + '%'
				OR BK.POLR_RECEIVED_DT LIKE '%' + @FullSearchString + '%'
				OR BK.POLR_RECEIVED_BY LIKE '%' + @FullSearchString + '%'
				OR BK.REMARKS LIKE '%' + @FullSearchString + '%'
				OR (CASE tdh.STATUS
						WHEN 'APPROVE' THEN 
							CASE tdh.WFSTEP 
								WHEN 1 THEN 'Approval UH / Checker' 
								WHEN 2 THEN 'Approval DH / UH' 
							END
						WHEN 'SUBMIT' THEN 'Submitted by Creator' 
						WHEN 'REVISE' THEN 'Revise' 
						WHEN 'REJECT' THEN 'Reject'
					END) LIKE '%' + @FullSearchString + '%'
				OR BK.APPROVAL_STS LIKE '%' + @FullSearchString + '%'
				)
		),
	CTE_TotalRows
	AS (
		SELECT COUNT(BK.UID) AS TotalRows
		FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = BK.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
			LEFT JOIN TR_DOCUMENT_HISTORY tdh ON tdh.REF_ID = BK.UID AND tdh.HIST_TYPE LIKE 'BOOK%' AND tdh.UID = (SELECT MAX(UID) FROM TR_DOCUMENT_HISTORY tdh2 WHERE tdh2.REF_ID = BK.UID AND tdh2.HIST_TYPE LIKE 'BOOK%')
		WHERE BK.IS_ACTIVE = 1 AND BK.IS_DRAFT = 0
			AND (
				@UserDept = BK.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND (
				(
					(
						CASE 
							WHEN @SearchByString = 'Author'
								AND ISNULL(@AuthorString,'') <> ''
								THEN BK.CREATOR
							END
						) LIKE '%' + @AuthorString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Customer Name'
								AND ISNULL(@CustomerNameString,'') <> ''
								THEN cc.LegalName
							END
						) LIKE '%' + @CustomerNameString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received By'
								AND ISNULL(@ReceivedByString,'') <> ''
								THEN BK.POLR_RECEIVED_BY
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@ReceivedByString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received Date'
								AND @ReceivedDateFrom IS NOT NULL
								AND @ReceivedDateTo IS NOT NULL
								THEN BK.POLR_RECEIVED_DT
							END
						) BETWEEN @ReceivedDateFrom
						AND @ReceivedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Registration Number'
								AND ISNULL(@RegistrationNoString,'') <> ''
								THEN BK.REGIS_NO
							END
						) LIKE '%' + @RegistrationNoString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received Method'
								AND ISNULL(@ReceivedMethodString,'') <> ''
								THEN BK.RECEIVED_METHOD
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@ReceivedMethodString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Reported Date'
								AND @ReportedDateFrom IS NOT NULL
								AND @ReportedDateTo IS NOT NULL
								THEN BK.REPORTED_DT
							END
						) BETWEEN @ReportedDateFrom
						AND @ReportedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Request Date'
								AND @RequestDateFrom IS NOT NULL
								AND @RequestDateTo IS NOT NULL
								THEN BK.REQUEST_DT
							END
						) BETWEEN @RequestDateFrom
						AND @RequestDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Type'
								AND ISNULL(@TypeString,'') <> ''
								THEN TY.VALUE
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@TypeString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN ISNULL(@SearchByString,'') <> ''
								AND ISNULL(@AuthorString,'') = ''
								AND ISNULL(@CustomerNameString,'') = ''
								AND ISNULL(@ReceivedByString,'') = ''
								AND @ReceivedDateFrom IS NULL
								AND @ReceivedDateTo IS NULL
								AND ISNULL(@ReceivedMethodString,'') = ''
								AND ISNULL(@RegistrationNoString, '') = ''
								AND @ReportedDateFrom IS NULL
								AND @ReportedDateTo IS NULL
								AND @RequestDateFrom IS NULL
								AND @RequestDateTo IS NULL
								AND ISNULL(@TypeString, '') = ''
								THEN 1
							END
						) = 1
					)
				OR ISNULL(@SearchByString,'') = ''
				)
			AND (
				ISNULL(@FullSearchString,'') = ''
				OR BK.BRANCH_CD + ' - ' + BR.VALUE LIKE '%' + @FullSearchString + '%'
				OR isnull(BK.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') LIKE '%' + @FullSearchString + '%'
				OR FORM.VALUE LIKE '%' + @FullSearchString + '%'
				OR BK.REGIS_NO LIKE '%' + @FullSearchString + '%'
				OR BK.CREATOR LIKE '%' + @FullSearchString + '%'
				OR BK.CREATED_DT LIKE '%' + @FullSearchString + '%'
				OR BK.REPORTED_DT LIKE '%' + @FullSearchString + '%'
				OR TY.VALUE LIKE '%' + @FullSearchString + '%'
				OR RCVM.VALUE LIKE '%' + @FullSearchString + '%'
				OR cc.LegalName LIKE '%' + @FullSearchString + '%'
				OR BK.POLR_RECEIVED_DT LIKE '%' + @FullSearchString + '%'
				OR BK.POLR_RECEIVED_BY LIKE '%' + @FullSearchString + '%'
				OR BK.REMARKS LIKE '%' + @FullSearchString + '%'
				OR (CASE tdh.STATUS
						WHEN 'APPROVE' THEN 
							CASE tdh.WFSTEP 
								WHEN 1 THEN 'Approval UH / Checker' 
								WHEN 2 THEN 'Approval DH / UH' 
							END
						WHEN 'SUBMIT' THEN 'Submitted by Creator' 
						WHEN 'REVISE' THEN 'Revise' 
						WHEN 'REJECT' THEN 'Reject'
					END) LIKE '%' + @FullSearchString + '%'
				OR BK.APPROVAL_STS LIKE '%' + @FullSearchString + '%'
				)
		)
	SELECT 
		BK.UID AS Id,
		isnull(BK.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS BookType,
		isnull(FORM.VALUE, '') AS BookTypeDesc,
		isnull(BK.CREATOR, '') AS Creator,
		isnull(BK.CREATOR_DEPT, '') AS CreatorDept,
		isnull(BK.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') AS CreatorDeptDesc,
		isnull(BK.CREATOR_ROLE, '') AS CreatorRole,
		BK.REQUEST_DT AS RequestDate,
		isnull(BK.BRANCH_CD, '') AS BranchCode,
		isnull(BK.BRANCH_CD + ' - ' + BR.VALUE, '') AS BranchCodeDesc,
		BK.REPORTED_DT AS ReportedDate,
		isnull(BK.TYPE, '') AS Type,
		isnull(TY.VALUE, '') AS TypeDesc,
		isnull(BK.RECEIVED_METHOD, '') AS ReceivedMethod,
		isnull(RCVM.VALUE, '') AS ReceivedMethodDesc,
		isnull(BK.CUSTOMER_CD, '') AS CustomerCode,
		isnull(cc.LegalName, '') AS CustomerCodeDesc,
		isnull(BK.ACCOUNT_NO, '') AS AccountNo,
		isnull(BK.HAS_AMOUNT, 0) AS HasAmount,
		isnull(BK.AMOUNT_CURR, '') AS AmountCurrency,
		isnull(BK.AMOUNT, 0) AS Amount,
		isnull(BK.REMARKS, '') AS Remarks,
		(CASE tdh.STATUS
			WHEN 'APPROVE' THEN 
				CASE tdh.WFSTEP 
					WHEN 1 THEN 'Approval UH / Checker' 
					WHEN 2 THEN 'Approval DH / UH' 
				END
			WHEN 'SUBMIT' THEN 'Submitted by Creator' 
			WHEN 'REVISE' THEN 'Revise' 
			WHEN 'REJECT' THEN 'Reject'
		END) AS Staging,
		BK.POLR_RECEIVED_DT AS PolrReceivedDate,
		isnull(BK.POLR_RECEIVED_BY, '') AS PolrReceivedBy,
		isnull(BK.POLR_RECEIVED_REMARKS, '') AS PolrReceivedRemarks,
		isnull(BK.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(BK.APPROVAL_STS, '') AS ApprovalStatusDesc,
		isnull(BK.IS_ACTIVE, '') AS IsActive,
		isnull(BK.CREATED_BY, '') AS CreatedBy,
		BK.CREATED_DT AS CreatedDate,
		isnull(BK.CREATED_HOST, '') AS CreatedHost,
		isnull(BK.MODIFIED_BY, '') AS ModifiedBy,
		BK.MODIFIED_DT AS ModifiedDate,
		isnull(BK.MODIFIED_HOST, '') AS ModifiedHost
		, ISNULL((select TOP 1 1 FROM TR_BOOK_ACTION_TAKEN AT WHERE AT.DOC_ID = BK.UID AND AT.APPROVAL_STS = 'Awaiting Approval' AND AT.IS_DRAFT = 0),0) AS IsAwaitingActionTaken
		, ISNULL((select TOP 1 1 FROM TR_BOOK_RECEIVED_NOTICE RN WHERE RN.DOC_ID = BK.UID AND RN.APPROVAL_STS = 'Awaiting Approval' AND RN.IS_DRAFT = 0),0) AS IsAwaitingReceivedNotice
		,TotalRows
	FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = BK.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
			LEFT JOIN TR_DOCUMENT_HISTORY tdh ON tdh.REF_ID = BK.UID AND tdh.HIST_TYPE LIKE 'BOOK%' AND tdh.UID = (SELECT MAX(UID) FROM TR_DOCUMENT_HISTORY tdh2 WHERE tdh2.REF_ID = BK.UID AND tdh2.HIST_TYPE LIKE 'BOOK%')
			,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = BK.UID
			)
		ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN BK.BRANCH_CD + ' - ' + BR.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN BK.BRANCH_CD + ' - ' + BR.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN BK.CREATOR_DEPT + isnull(' - '+ dept.LongName, '') 
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN BK.CREATOR_DEPT + isnull(' - '+ dept.LongName, '') 
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Form')
					THEN FORM.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Form_desc')
					THEN FORM.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN BK.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN BK.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN BK.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN BK.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReportedDate')
					THEN BK.REPORTED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReportedDate_desc')
					THEN BK.REPORTED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Type')
					THEN TY.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Type_desc')
					THEN TY.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod')
					THEN BK.RECEIVED_METHOD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod_desc')
					THEN BK.RECEIVED_METHOD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN cc.LegalName
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN cc.LegalName
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate')
					THEN BK.POLR_RECEIVED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate_desc')
					THEN BK.POLR_RECEIVED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy')
					THEN BK.POLR_RECEIVED_BY
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy_desc')
					THEN BK.POLR_RECEIVED_BY
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Remarks')
					THEN BK.REMARKS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Remarks_desc')
					THEN BK.REMARKS
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Staging')
					THEN (CASE tdh.STATUS
						WHEN 'APPROVE' THEN 
							CASE tdh.WFSTEP 
								WHEN 1 THEN 'Approval UH / Checker' 
								WHEN 2 THEN 'Approval DH / UH' 
							END
						WHEN 'SUBMIT' THEN 'Submitted by Creator' 
						WHEN 'REVISE' THEN 'Revise' 
						WHEN 'REJECT' THEN 'Reject'
					END)
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Staging_desc')
					THEN (CASE tdh.STATUS
						WHEN 'APPROVE' THEN 
							CASE tdh.WFSTEP 
								WHEN 1 THEN 'Approval UH / Checker' 
								WHEN 2 THEN 'Approval DH / UH' 
							END
						WHEN 'SUBMIT' THEN 'Submitted by Creator' 
						WHEN 'REVISE' THEN 'Revise' 
						WHEN 'REJECT' THEN 'Reject'
					END)
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Status')
					THEN BK.APPROVAL_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Status_desc')
					THEN BK.APPROVAL_STS
				END DESC,
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN BK.UID
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOK_ACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_BOOK_ACTIONTAKEN]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID,
		DOC_ID,
		REGIS_NO,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		ACT_DT,
		ACT_ACTION,
		ACTOR,
		REMARKS,
		RESULT,
		APPROVAL_STS,
		IS_DRAFT,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_BOOK_ACTION_TAKEN
	WHERE 1 = 1
		AND [UID] = @RefID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOK_RECEIVEDNOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_BOOK_RECEIVEDNOTICE]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID,
		DOC_ID,
		REGIS_NO,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		RCV_DT,
		RCV_METHOD,
		RCV_BY,
		REMARKS,
		APPROVAL_STS,
		IS_DRAFT,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_BOOK_RECEIVED_NOTICE
	WHERE 1 = 1
		AND [UID] = @RefID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOKACTIONTAKENBYDOCID]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_BOOKACTIONTAKENBYDOCID]
	@DocId INT
AS
BEGIN
	SELECT 
		BAT.UID AS Id,
		BAT.CREATOR AS Creator,
		isnull(BAT.REQUEST_DT, '') AS RequestDate,
		isnull(BAT.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(BAT.APPROVAL_STS, '') AS ApprovalStatusDesc,
		isnull(BAT.IS_ACTIVE, '') AS IsActive,
		isnull(BAT.CREATED_BY, '') AS CreatedBy,
		isnull(BAT.CREATED_DT, '') AS CreatedDate,
		isnull(BAT.CREATED_HOST, '') AS CreatedHost,
		isnull(BAT.MODIFIED_BY, '') AS ModifiedBy,
		isnull(BAT.MODIFIED_DT, '') AS ModifiedDate,
		isnull(BAT.MODIFIED_HOST, '') AS ModifiedHost
	FROM TR_BOOK_ACTION_TAKEN BAT
	WHERE BAT.DOC_ID = @DocId AND IS_DRAFT = 0
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOKDATA]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_BOOKDATA]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID,
		REGIS_NO,
		BOOK_TYPE,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		BRANCH_CD,
		REPORTED_DT,
		TYPE,
		RECEIVED_METHOD,
		CUSTOMER_CD,
		ACCOUNT_NO,
		HAS_AMOUNT,
		AMOUNT_CURR,
		AMOUNT,
		REMARKS,
		POLR_RECEIVED_DT,
		POLR_RECEIVED_BY,
		POLR_RECEIVED_REMARKS,
		APPROVAL_STS,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_BOOK
	WHERE 1 = 1
		AND [UID] = @RefID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOKDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_BOOKDETAIL]
	@Id INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT BK.UID AS Id,
		BK.REGIS_NO AS RegistrationNo,
		BK.BOOK_TYPE AS BookType,
		FORM.VALUE AS BookTypeDesc,
		BK.CREATOR AS Creator,
		BK.CREATOR_DEPT AS CreatorDept,
		BK.CREATOR_UNIT AS CreatorUnit,
		BK.CREATOR_DEPT AS CreatorDeptDesc,
		BK.CREATOR_ROLE AS CreatorRole,
		BK.REQUEST_DT AS RequestDate,
		BK.BRANCH_CD AS BranchCode,
		BK.BRANCH_CD + ' - ' + BR.VALUE AS BranchCodeDesc,
		BK.REPORTED_DT AS ReportedDate,
		BK.TYPE AS Type,
		TY.VALUE AS TypeDesc,
		BK.RECEIVED_METHOD AS ReceivedMethod,
		RCVM.VALUE AS ReceivedMethodDesc,
		BK.CUSTOMER_CD AS CustomerCode,
		EC.LegalName AS CustomerCodeDesc,
		BK.ACCOUNT_NO AS AccountNo,
		BK.ACCOUNT_NO AS SelectedAccountNo,
		BK.HAS_AMOUNT AS HasAmount,
		BK.AMOUNT_CURR AS AmountCurrency,
		BK.AMOUNT AS Amount,
		BK.REMARKS AS Remarks,
		BK.POLR_RECEIVED_DT AS PolrReceivedDate,
		BK.POLR_RECEIVED_BY AS PolrReceivedBy,
		BK.POLR_RECEIVED_BY + ' - ' +PRBY.RealName AS PolrReceivedByRealName,
		BK.POLR_RECEIVED_REMARKS AS PolrReceivedRemarks,
		BK.APPROVAL_STS AS ApprovalStatus,
		BK.APPROVAL_STS AS ApprovalStatusDesc,
		BK.IS_ACTIVE AS IsActive,
		BK.CREATED_BY AS CreatedBy,
		BK.CREATED_DT AS CreatedDate,
		BK.CREATED_HOST AS CreatedHost,
		BK.MODIFIED_BY AS ModifiedBy,
		BK.MODIFIED_DT AS ModifiedDate,
		BK.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON BK.CUSTOMER_CD = EC.CustomerCode
			LEFT JOIN SN_BTMUDirectory_User PRBY WITH (NOLOCK) ON BK.POLR_RECEIVED_BY = PRBY.UserName
	WHERE BK.UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOKIDCHECK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_BOOKIDCHECK]
	-- Add the parameters for the stored procedure here
	--@Id INT,
	@BOOK_ID VARCHAR (3000),
	@BOOK_NAME VARCHAR (3000),
	@DEPT_BOOK VARCHAR (255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;

	--IF EXISTS (
	--		SELECT 1
	--		FROM MS_BOOK_PROFILE
	--		WHERE 1 = 1
	--			AND BOOK_ID = @BOOK_ID OR BOOK_NAME = @BOOK_NAME
	--			AND IS_ACTIVE = 1
	--		)
	--BEGIN
	--	SELECT 1
	--END
	--ELSE
	--BEGIN
	--	SELECT 0
	--END

	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT BP.UID AS [UID]
		,BP.BOOK_ID AS [BOOK_ID]
		,BP.BOOK_NAME AS [BOOK_NAME]
		,BP.DEPT_BOOK AS [DEPT_BOOK]
		,BP.CREATED_BY AS [CREATED_BY]
		,BP.CREATED_DT AS [CREATED_DT]
		,BP.CREATED_HOST AS [CREATED_HOST]
		,BP.MODIFIED_BY AS [MODIFIED_BY]
		,BP.MODIFIED_DT AS [MODIFIED_DT]
		,BP.MODIFIED_HOST AS [MODIFIED_HOST]
	FROM dbo.MS_BOOK_PROFILE AS BP
	WHERE (BP.BOOK_ID = @BOOK_ID
	OR BP.BOOK_NAME = @BOOK_NAME)
	AND BP.DEPT_BOOK = @DEPT_BOOK

END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_BOOKPROFILE]
	-- Add the parameters for the stored procedure here
	@UserDept VARCHAR(200) = '',
	@SearchBookId NVARCHAR(100) = '',
	@SearchBookName NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		SELECT *
		FROM MS_BOOK_PROFILE AS BP
		WHERE BP.IS_ACTIVE = 1
		
		AND(ISNULL(@SearchBookId,'') = ''
			OR BP.BOOK_ID LIKE '%' + @SearchBookId + '%'
		)
		AND(ISNULL(@SearchBookName,'') = ''
			OR BP.BOOK_NAME LIKE '%' + @SearchBookName + '%'
		)
		AND (
				BP.DEPT_BOOK  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)

		ORDER BY CASE 
				WHEN @SortOrder = 'BookID'
					THEN [BOOK_ID]
				END ASC,
			CASE 
				WHEN @SortOrder = 'BookID_desc'
					THEN [BOOK_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'BookName'
					THEN [BOOK_NAME]
				END ASC,
			CASE 
				WHEN @SortOrder = 'BookName_desc'
					THEN [BOOK_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Sequence'
					THEN [BOOK_SEQ]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Sequence_desc'
					THEN [BOOK_SEQ]
				END DESC,
			
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM MS_BOOK_PROFILE AS BP
		WHERE BP.IS_ACTIVE = 1
		
		AND(ISNULL(@SearchBookId,'') = ''
			OR BP.BOOK_ID LIKE '%' + @SearchBookId + '%'
		)
		AND(ISNULL(@SearchBookName,'') = ''
			OR BP.BOOK_NAME LIKE '%' + @SearchBookName + '%'
		)
		AND (
				BP.DEPT_BOOK  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		)
	SELECT TotalRows,
		BP.UID AS [UID],
		BP.[BOOK_ID] AS [BOOK_ID],
		BP.BOOK_NAME AS [BOOK_NAME],
		BP.DEPT_BOOK AS [DEPT_BOOK],
		BP.BOOK_SEQ AS [BOOK_SEQ],
		BP.BEFORE_OFFSET_DAYS AS [BEFORE_OFFSET_DAYS],
		BP.AFTER_OFFSET_DAYS AS [AFTER_OFFSET_DAYS],
		BP.CREATED_BY AS CreatedBy,
		BP.CREATED_DT AS CreatedDate,
		BP.CREATED_HOST AS CreatedHost,
		BP.MODIFIED_BY AS ModifedBy,
		BP.MODIFIED_DT AS ModifiedDate,
		BP.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_BOOK_PROFILE AS BP,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = BP.UID
			)
	OPTION (RECOMPILE)
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOKPROFILEDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_BOOKPROFILEDETAIL]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT BP.UID AS [UID]
		,BP.BOOK_ID AS [BOOK_ID]
		,BP.BOOK_NAME AS [BOOK_NAME]
		,BP.DEPT_BOOK AS [DEPT_BOOK]
		,BP.BOOK_SEQ AS [BOOK_SEQ]
		,BP.BEFORE_OFFSET_DAYS AS [BEFORE_OFFSET_DAYS]
		,BP.AFTER_OFFSET_DAYS AS [AFTER_OFFSET_DAYS]
		,BP.IS_ACTIVE AS IS_ACTIVE
		,BP.CREATED_BY AS CREATED_BY
		,BP.CREATED_DT AS CREATED_DT
		,BP.CREATED_HOST AS CREATED_HOST
		,BP.MODIFIED_BY AS MODIFIED_BY
		,BP.MODIFIED_DT AS MODIFIED_DT
		,BP.MODIFIED_HOST AS MODIFIED_HOST
	FROM dbo.MS_BOOK_PROFILE AS BP
	WHERE BP.UID = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_BOOKRECEIVEDNOTICEBYDOCID]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_BOOKRECEIVEDNOTICEBYDOCID]
	@DocId INT
AS
BEGIN
	SELECT 
		RN.UID AS Id,
		RN.CREATOR AS Creator,
		isnull(RN.REQUEST_DT, '') AS RequestDate,
		isnull(RN.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(RN.APPROVAL_STS, '') AS ApprovalStatusDesc,
		isnull(RN.IS_ACTIVE, '') AS IsActive,
		isnull(RN.CREATED_BY, '') AS CreatedBy,
		isnull(RN.CREATED_DT, '') AS CreatedDate,
		isnull(RN.CREATED_HOST, '') AS CreatedHost,
		isnull(RN.MODIFIED_BY, '') AS ModifiedBy,
		isnull(RN.MODIFIED_DT, '') AS ModifiedDate,
		isnull(RN.MODIFIED_HOST, '') AS ModifiedHost
	FROM TR_BOOK_RECEIVED_NOTICE RN
	WHERE RN.DOC_ID = @DocId AND RN.IS_DRAFT = 0
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_CHECKREGISNO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<[UDPS_CHECKREGISNO>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_CHECKREGISNO]
	@RegistrationNo NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (
			SELECT 1
			FROM TR_BOOK
			WHERE 1 = 1
				AND REGIS_NO = @RegistrationNo
				AND IS_ACTIVE = 1
			)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_ChkREGISNO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_ChkREGISNO]
	@RegistrationNo NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS (
			SELECT 1
			FROM TR_OUTGOING_LETTER
			WHERE 1 = 1
				AND REGIS_NO = @RegistrationNo
				AND IS_ACTIVE = 1
			)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_DELETEDACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_DELETEDACTIONTAKEN]
	-- Add the parameters for the stored procedure here
	@ReqDateStart varchar(15),
	@ReqDateEnd varchar(15),
	@RegisNo varchar(50)=NULL,
	@varForm varchar(50)=NULL,
	@varCust varchar(50)=NULL,
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortField NVARCHAR(100) = 'UID',
	@SortOrder NVARCHAR(100) = 'DESC',
	@allPage varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	DECLARE @sql nvarchar(MAX);
	DECLARE @offset nvarchar(10);
	DECLARE @newsize nvarchar(10);
	
	SET @PageNo=@PageNo-1;
	if (@PageNo<1)
	BEGIN
		SET @offset=0
		SET @newsize=@PageSize
	END
	ELSE
	BEGIN
		SET @offset=@PageNo*@PageSize
		SET @newsize=@PageSize-1
	END
	SET @sql='
	SELECT b.[UID]
      ,b.[DOC_ID]
      ,b.[REGIS_NO]
      ,b.[CREATOR]
      ,b.[CREATOR_DEPT]
      ,b.[CREATOR_UNIT]
      ,b.[CREATOR_ROLE]
      ,FORMAT(b.[REQUEST_DT],''dd/MMM/yyyy'') as [REQUEST_DT]
	  ,FORMAT(b.[ACT_DT],''dd/MMM/yyyy'') as [ACT_DT]
      ,b.[ACT_ACTION]
      ,b.[ACTOR]
      ,b.[REMARKS]
      ,b.[RESULT]
      ,b.[APPROVAL_STS]
      ,b.[IS_DRAFT]
      ,b.[IS_ACTIVE]
      ,b.[CREATED_BY]
      ,FORMAT(b.[CREATED_DT],''dd/MMM/yyyy HH:mm:ss'') as [CREATED_DT]
      ,b.[CREATED_HOST]
      ,b.[MODIFIED_BY]
      ,b.[MODIFIED_DT]
      ,b.[MODIFIED_HOST]
	  ,a.[UID_BOOK]
	  ,FORMAT(a.[REPORTED_DT],''dd/MMM/yyyy HH:mm:ss'') as [REPORTED_DT]
	  ,a.[RECEIVED_METHOD]
	  ,a.[ACCOUNT_NO]
	  ,a.[HAS_AMOUNT]
	  ,a.[AMOUNT_CURR]
	  ,a.[AMOUNT]
	  ,a.[POLR_RECEIVED_DT]
	  ,a.[POLR_RECEIVED_BY]
	  ,a.[POLR_RECEIVED_REMARKS]
	  ,a.[BOOK_TYPE]
	  ,a.[BRANCH_CD]
	  ,(SELECT ISNULL(NULLIF(Value, ''''),Value) as Value  FROM dbo.[MS_PARAMETER] WHERE [KEYFIELD]=a.[BRANCH_CD] AND [GROUP]=''BRANCH_CODE'') as [LONG_BRANCH] 
	  ,a.[TYPE]
	  ,cust.LegalName as [CUSTOMER_CD]
	  ,(SELECT ISNULL(NULLIF(LongName, ''''),LongName) as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as [DEPARTEMEN_LONG_NAME] 
	FROM [TR_BOOK_ACTION_TAKEN] as b
	LEFT JOIN (SELECT 
		UID as UID_BOOK,REPORTED_DT,RECEIVED_METHOD,ACCOUNT_NO,HAS_AMOUNT,AMOUNT_CURR,
		AMOUNT,POLR_RECEIVED_DT,POLR_RECEIVED_BY,POLR_RECEIVED_REMARKS,
		BOOK_TYPE,BRANCH_CD,TYPE,CUSTOMER_CD FROM TR_BOOK ) as a ON a.UID_BOOK=b.DOC_ID 
	LEFT JOIN (SELECT CustomerCode,LegalName FROM EASY_Customer_Code ) as cust ON cust.CustomerCode=a.CUSTOMER_CD
	WHERE 1=1 AND b.IS_ACTIVE=0'; 

	if (@ReqDateStart IS NOT NULL OR @ReqDateStart !='')
	BEGIN
		SET @sql=@sql + ' AND [REQUEST_DT] >= CONVERT(datetime,'''+@ReqDateStart +''')';
	END

	if ( @ReqDateEnd ='')
	BEGIN
		SET @sql=@sql
	END
	ELSE
	BEGIN
		if (@ReqDateEnd IS NOT NULL OR @ReqDateEnd !='' )
		BEGIN
			SET @sql=@sql + ' AND b.[REQUEST_DT] <= CONVERT(datetime,'''+@ReqDateEnd +''')';
		END
	END
	

	if (@RegisNo IS NOT NULL OR @RegisNo !='')
	BEGIN
		SET @sql=@sql + ' AND b.[REGIS_NO] LIKE ''%'+@RegisNo +'%'' ';
	END

	if (@varCust IS NOT NULL OR @varCust !='')
	BEGIN
		SET @sql=@sql + ' AND cust.LegalName LIKE ''%'+@varCust +'%'' ';
	END

	
	if (@varForm IS NOT NULL OR @varForm !='')
	BEGIN
		SET @sql=@sql + ' AND a.[TYPE] LIKE ''%'+@varForm +'%'' ';
	END

	/*

	if CHARINDEX(',', @cr)>0
		BEGIN
		
		if @fi='CREATOR_DEPT' 
		
		
			SET @sql=@sql +' AND 
			CASE 
			WHEN (charindex('''+@key+''', [CREATOR_DEPT])-1)>1 THEN 
				LEFT([CREATOR_DEPT],charindex('''+@key+''', [CREATOR_DEPT])-1) 
			ELSE [CREATOR_DEPT] END IN ('+@cr+') ORDER BY UID DESC';
			
			--SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ';

		
		ELSE
		
			SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ';
		END
	ELSE
		BEGIN
		--SET @cr = Replace(@cr, '%[^0-9a-zA-Z]%', '')
		SET @sql=@sql + ' AND ['+@fi+'] LIKE ''%'+@cr +'%'' ';
	END

	*/

	if (@SortOrder='ASC')
	BEGIN
		SET @SortOrder='ASC';
	END
	ELSE
	BEGIN
		SET @SortOrder='DESC';
	END
	SET @sql=@sql+' ORDER BY '+@SortField+' '+@SortOrder; 

	if (@allPage IS NOT NULL OR @allPage !='')
		BEGIN
		SET @sql=@sql;
		END
	ELSE
		BEGIN
		SET @sql=@sql + ' OFFSET '+@offset+' ROWS FETCH NEXT '+@newsize+' ROWS ONLY';
		END
	
	EXECUTE( @sql );
	--SELECT @offset;
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DELETEDBOOK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DELETEDBOOK]
	@UserDept VARCHAR(100) = '',
	@FormByString NVARCHAR(100) = '',
	@CustomerString NVARCHAR(2000) = '',
	@RegistrationNoString NVARCHAR(2000) = '',
	@ReportedDateFrom DATETIME = NULL,
	@ReportedDateTo DATETIME = NULL,
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		-- Insert statements for procedure here
		SELECT BK.UID AS Id
		FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
		WHERE BK.IS_ACTIVE = 0
			AND (
				@UserDept = BK.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND 
			(
				(
					ISNULL(@FormByString,'') <> ''
					AND FORM.VALUE = CASE 
							WHEN @FormByString = 'All' THEN FORM.VALUE
							ELSE @FormByString
						END
				) OR ISNULL(@FormByString,'') = ''
			)
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND CC.LegalName LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND BK.REGIS_NO LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
			AND 
			(
				(ISNULL(@ReportedDateFrom,'') <> '' AND ISNULL(@ReportedDateTo,'') <> '' AND CAST(BK.REPORTED_DT AS DATE) BETWEEN @ReportedDateFrom AND @ReportedDateTo) 
				OR ISNULL(@ReportedDateFrom,'') = '' OR ISNULL(@ReportedDateTo,'') = ''
			)
		),
	CTE_TotalRows
	AS (
		SELECT COUNT(BK.UID) AS TotalRows
		FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
		WHERE BK.IS_ACTIVE = 0
			AND (
				@UserDept = BK.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND 
			(
				(
					ISNULL(@FormByString,'') <> ''
					AND FORM.VALUE = CASE 
							WHEN @FormByString = 'All' THEN FORM.VALUE
							ELSE @FormByString
						END
				) OR ISNULL(@FormByString,'') = ''
			)
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND cc.LegalName LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND BK.REGIS_NO LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
			AND 
			(
				(ISNULL(@ReportedDateFrom,'') <> '' AND ISNULL(@ReportedDateTo,'') <> '' AND CAST(BK.REPORTED_DT AS DATE) BETWEEN @ReportedDateFrom AND @ReportedDateTo) 
				OR ISNULL(@ReportedDateFrom,'') = '' OR ISNULL(@ReportedDateTo,'') = ''
			)
		)
	SELECT TotalRows,
		BK.UID AS Id,
		isnull(BK.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS BookType,
		isnull(FORM.VALUE, '') AS BookTypeDesc,
		isnull(BK.CREATOR, '') AS Creator,
		isnull(BK.CREATOR_DEPT, '') AS CreatorDept,
		isnull(BK.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') AS CreatorDeptDesc,
		isnull(BK.CREATOR_ROLE, '') AS CreatorRole,
		isnull(BK.CREATOR_UNIT, '') AS CreatorUnit,
		BK.REQUEST_DT AS RequestDate,
		isnull(BK.BRANCH_CD, '') AS BranchCode,
		isnull(BK.BRANCH_CD + ' - ' + BR.VALUE, '') AS BranchCodeDesc,
		BK.REPORTED_DT AS ReportedDate,
		isnull(BK.TYPE, '') AS Type,
		isnull(TY.DESCS, '') AS TypeDesc,
		isnull(BK.RECEIVED_METHOD, '') AS ReceivedMethod,
		isnull(RCVM.VALUE, '') AS ReceivedMethodDesc,
		isnull(BK.CUSTOMER_CD, '') AS CustomerCode,
		isnull(CC.LegalName, '') AS CustomerCodeDesc,
		isnull(BK.ACCOUNT_NO, '') AS AccountNo,
		isnull(BK.HAS_AMOUNT, 0) AS HasAmount,
		isnull(BK.AMOUNT_CURR, '') AS AmountCurrency,
		isnull(BK.AMOUNT, 0) AS Amount,
		isnull(BK.REMARKS, '') AS Remarks,
		BK.POLR_RECEIVED_DT AS PolrReceivedDate,
		isnull(BK.POLR_RECEIVED_BY, '') AS PolrReceivedBy,
		isnull(BK.POLR_RECEIVED_REMARKS, '') AS PolrReceivedRemarks,
		isnull(BK.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(BK.APPROVAL_STS, '') AS ApprovalStatusDesc,
		isnull(BK.IS_ACTIVE, '') AS IsActive,
		isnull(BK.CREATED_BY, '') AS CreatedBy,
		BK.CREATED_DT AS CreatedDate,
		isnull(BK.CREATED_HOST, '') AS CreatedHost,
		isnull(BK.MODIFIED_BY, '') AS ModifiedBy,
		BK.MODIFIED_DT AS ModifiedDate,
		isnull(BK.MODIFIED_HOST, '') AS ModifiedHost
	FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = BK.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = BK.UID
			)
		ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN BK.BRANCH_CD + ' - ' + BR.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN BK.BRANCH_CD + ' - ' + BR.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN BK.CREATOR_DEPT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN BK.CREATOR_DEPT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Form')
					THEN FORM.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Form_desc')
					THEN FORM.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN BK.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN BK.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN BK.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN BK.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReportedDate')
					THEN BK.REPORTED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReportedDate_desc')
					THEN BK.REPORTED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Type')
					THEN TY.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Type_desc')
					THEN TY.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod')
					THEN BK.RECEIVED_METHOD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod_desc')
					THEN BK.RECEIVED_METHOD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN cc.LegalName
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN cc.LegalName
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate')
					THEN BK.POLR_RECEIVED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate_desc')
					THEN BK.POLR_RECEIVED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Remarks')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Remarks_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Status')
					THEN BK.APPROVAL_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Status_desc')
					THEN BK.APPROVAL_STS
				END DESC,
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN BK.CREATED_DT
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_DELETEDBOOKACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DELETEDBOOKACTIONTAKEN]
	@UserDept VARCHAR(100) = '',
	@FormByString NVARCHAR(100) = '',
	@CustomerString NVARCHAR(2000) = '',
	@RegistrationNoString NVARCHAR(2000) = '',
	@ReportedDateFrom DATETIME = NULL,
	@ReportedDateTo DATETIME = NULL,
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		-- Insert statements for procedure here
		SELECT BRN.UID AS Id
		FROM dbo.TR_BOOK_ACTION_TAKEN AS BRN
			LEFT JOIN TR_BOOK BK WITH (NOLOCK) ON BRN.DOC_ID = BK.UID
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
		WHERE BRN.IS_ACTIVE = 0
			AND (
				@UserDept = BRN.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND 
			(
				(
					ISNULL(@FormByString,'') <> ''
					AND FORM.VALUE = CASE 
							WHEN @FormByString = 'All' THEN FORM.VALUE
							ELSE @FormByString
						END
				) OR ISNULL(@FormByString,'') = ''
			)
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND CC.LegalName LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND BK.REGIS_NO LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
			AND 
			(
				(ISNULL(@ReportedDateFrom,'') <> '' AND ISNULL(@ReportedDateTo,'') <> '' AND CAST(BRN.ACT_DT AS DATE) BETWEEN @ReportedDateFrom AND @ReportedDateTo) 
				OR ISNULL(@ReportedDateFrom,'') = '' OR ISNULL(@ReportedDateTo,'') = ''
			)
		),
	CTE_TotalRows
	AS (
		SELECT COUNT(BRN.UID) AS TotalRows
		FROM dbo.TR_BOOK_ACTION_TAKEN AS BRN
			LEFT JOIN TR_BOOK BK WITH (NOLOCK) ON BRN.DOC_ID = BK.UID
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
		WHERE BRN.IS_ACTIVE = 0
			AND (
				@UserDept = BRN.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND 
			(
				(
					ISNULL(@FormByString,'') <> ''
					AND FORM.VALUE = CASE 
							WHEN @FormByString = 'All' THEN FORM.VALUE
							ELSE @FormByString
						END
				) OR ISNULL(@FormByString,'') = ''
			)
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND cc.LegalName LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND BK.REGIS_NO LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
			AND 
			(
				(ISNULL(@ReportedDateFrom,'') <> '' AND ISNULL(@ReportedDateTo,'') <> '' AND CAST(BRN.ACT_DT AS DATE) BETWEEN @ReportedDateFrom AND @ReportedDateTo) 
				OR ISNULL(@ReportedDateFrom,'') = '' OR ISNULL(@ReportedDateTo,'') = ''
			)
		)
	SELECT TotalRows,
		BRN.UID AS Id,
		isnull(BK.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS BookType,
		isnull(FORM.VALUE, '') AS BookTypeDesc,
		isnull(BRN.CREATOR, '') AS Creator,
		isnull(BRN.CREATOR_DEPT, '') AS CreatorDept,
		isnull(BRN.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') AS CreatorDeptDesc,
		isnull(BRN.CREATOR_ROLE, '') AS CreatorRole,
		isnull(BRN.CREATOR_UNIT, '') AS CreatorUnit,
		BRN.REQUEST_DT AS RequestDate,
		isnull(BK.BRANCH_CD, '') AS BranchCode,
		isnull(BK.BRANCH_CD, '')  + ' - ' + isnull(BR.VALUE, '') AS BranchCodeDesc,
		isnull(CC.LegalName, '') AS CustomerCodeDesc,
		BK.REPORTED_DT AS ReportedDate,
		BRN.ACT_DT AS ActDate,
		isnull(BRN.ACT_ACTION, '') AS ActAction,
		isnull(BRN.ACTOR, '') AS Actor,
		isnull(BRN.RESULT, '') AS Result,
		isnull(BRN.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(BRN.APPROVAL_STS, '') AS ApprovalStatusDesc,
		isnull(BRN.IS_ACTIVE, '') AS IsActive,
		isnull(BRN.CREATED_BY, '') AS CreatedBy,
		BRN.CREATED_DT AS CreatedDate,
		isnull(BRN.CREATED_HOST, '') AS CreatedHost,
		isnull(BRN.MODIFIED_BY, '') AS ModifiedBy,
		BRN.MODIFIED_DT AS ModifiedDate,
		isnull(BRN.MODIFIED_HOST, '') AS ModifiedHost
	FROM dbo.TR_BOOK_ACTION_TAKEN AS BRN
			LEFT JOIN TR_BOOK BK WITH (NOLOCK) ON BRN.DOC_ID = BK.UID
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = BRN.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode,
		CTE_TotalRows
	WHERE BRN.IS_ACTIVE = 0 AND EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = BRN.UID
			)
		ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN BR.DESCS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN BR.DESCS
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN isnull(BRN.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '')
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN isnull(BRN.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '')
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Form')
					THEN FORM.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Form_desc')
					THEN FORM.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN BK.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN BK.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN BRN.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN BRN.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN BRN.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN BRN.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN cc.LegalName
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN cc.LegalName
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ActionDate')
					THEN BRN.ACT_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ActionDate_desc')
					THEN BRN.ACT_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Action')
					THEN BRN.ACT_ACTION
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Action_desc')
					THEN BRN.ACT_ACTION
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Actor')
					THEN BRN.ACTOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Actor_desc')
					THEN BRN.ACTOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Result')
					THEN BRN.RESULT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Result_desc')
					THEN BRN.RESULT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Status')
					THEN BRN.APPROVAL_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Status_desc')
					THEN BRN.APPROVAL_STS
				END DESC,
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN BRN.UID
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DELETEDOUTGOING]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DELETEDOUTGOING]
	@UserDept VARCHAR(100) = '',
	@SearchByDateFrom DATE,
	@SearchByDateTo DATE,
	@SubjectString NVARCHAR(100) = '',
	@AttentionString NVARCHAR(2000) = '',
	@RegistrationNoString NVARCHAR(2000) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		-- Insert statements for procedure here
		SELECT OL.UID AS Id
		FROM dbo.TR_OUTGOING_LETTER AS OL
		LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = OL.BRANCH_CD
		WHERE OL.IS_ACTIVE = 0 AND OL.IS_DRAFT = 1
			AND (
			--@UserDept = ''
			 @UserDept = OL.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			--AND CONVERT(VARCHAR(10), REQUEST_DT, 111) = convert(VARCHAR(10), @SearchByDateFrom, 111)
			--AND CONVERT(VARCHAR(10), REQUEST_DT, 111) = convert(VARCHAR(10), @SearchByDateTo, 111)
			AND(
					CAST(REQUEST_DT AS DATE) BETWEEN @SearchByDateFrom AND @SearchByDateTo
					OR ISNULL(@SearchByDateFrom,'')= '' AND ISNULL(@SearchByDateTo, '') = ''
				)
			AND (
				ISNULL(@SubjectString,'') = ''
				OR OL.SUBJECT LIKE '%' + @SubjectString+ '%'
				)
			AND (
			ISNULL(@AttentionString,'') = ''
			OR OL.ATTENTION LIKE '%' + @AttentionString+ '%'
				)
			AND (
			ISNULL(@RegistrationNoString,'') = ''
			OR OL.REGIS_NO LIKE '%' + @RegistrationNoString+ '%'
				)
		),
	CTE_TotalRows
	AS (
		SELECT COUNT(OL.UID) AS TotalRows
		FROM dbo.TR_OUTGOING_LETTER AS OL
		LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = OL.BRANCH_CD
		WHERE OL.IS_ACTIVE = 0 AND OL.IS_DRAFT = 1
			AND (
				@UserDept = OL.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			--AND CONVERT(VARCHAR(10), REQUEST_DT, 111) = convert(VARCHAR(10), @SearchByDateFrom, 111)
			--AND CONVERT(VARCHAR(10), REQUEST_DT, 111) = convert(VARCHAR(10), @SearchByDateTo, 111)
			AND(
					CAST(REQUEST_DT AS DATE) BETWEEN @SearchByDateFrom AND @SearchByDateTo
					OR ISNULL(@SearchByDateFrom,'')= '' AND ISNULL(@SearchByDateTo, '') = ''
				)
			AND (
				ISNULL(@SubjectString,'') = ''
				OR OL.SUBJECT LIKE '%' + @SubjectString+ '%'
				)
			AND (
			ISNULL(@AttentionString,'') = ''
			OR OL.ATTENTION LIKE '%' + @AttentionString+ '%'
				)
			AND (
			ISNULL(@RegistrationNoString,'') = ''
			OR OL.REGIS_NO LIKE '%' + @RegistrationNoString+ '%'
				)
		)
	SELECT TotalRows,
		OL.UID AS Id,
		isnull(OL.REGIS_NO, '') AS RegistrationNo,
		isnull(OL.CREATOR, '') AS Creator,
		isnull(OL.CREATOR_DEPT, '') AS CreatorDepartment,
		ISNULL(OL.CREATOR_DEPT,'')+ isnull('-' + dept.LONGNAME,'') AS CreatorDepartmentDesc,
		isnull(OL.CREATOR_ROLE, '') AS CreatorRole,
		OL.REQUEST_DT AS RequestDate,
		isnull(OL.BRANCH_CD, '') AS BranchCode,
		isnull(OL.BRANCH_cD,'') + ' - ' + isnull(BR.VALUE,'') AS BranchCodeDesc,
		OL.LET_CREATED_DT AS LetCreatedDate,
		isnull(OL.LETTER_TYPE, '') AS LetterType,
		isnull(OL.SUBJECT,'') AS Subject,
		isnull(OL.ATTENTION,'') AS Attention,
		isnull(OL.LOCATION,'') AS Location,
		isnull(OL.SUBMIT_DT,'') AS SubmitDate,
		OL.SUBMIT_DT AS SubmitDate,
		isnull(OL.SUBMIT_BY,'') AS SubmitBy,
		isnull(OL.SUBMIT_METHOD,'') AS SubmitMethod,
		isnull(OL.LETTER_STS,'') AS LetterStatus,
		isnull(OL.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(OL.IS_ACTIVE, '') AS IsActive,
		isnull(OL.CREATED_BY, '') AS CreatedBy,
		OL.CREATED_DT AS CreatedDate,
		isnull(OL.CREATED_HOST, '') AS CreatedHost,
		isnull(OL.MODIFIED_BY, '') AS ModifiedBy,
		OL.MODIFIED_DT AS ModifiedDate,
		isnull(OL.MODIFIED_HOST, '') AS ModifiedHost
	FROM dbo.TR_OUTGOING_LETTER AS OL
	LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = OL.BRANCH_CD
	LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = OL.CREATOR_DEPT,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = OL.UID
			)
			AND OL.IS_ACTIVE = 0 AND OL.IS_DRAFT = 1
			ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN OL.BRANCH_CD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN OL.BRANCH_CD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN OL.CREATOR_DEPT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN OL.CREATOR_DEPT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN OL.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN OL.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN OL.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN OL.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN OL.REQUEST_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN OL.REQUEST_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Letter')
					THEN OL.LETTER_TYPE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Letter_desc')
					THEN OL.LETTER_TYPE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'CreatedDate')
					THEN OL.LET_CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'CreatedDate_desc')
					THEN OL.LET_CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Subject')
					THEN OL.SUBJECT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Subject_desc')
					THEN OL.SUBJECT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Attention')
					THEN OL.ATTENTION
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Attention_desc')
					THEN OL.ATTENTION
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Location')
					THEN OL.LOCATION
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Location_desc')
					THEN OL.LOCATION
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Date')
					THEN OL.SUBMIT_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Date_desc')
					THEN OL.SUBMIT_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'By')
					THEN OL.SUBMIT_BY
				END ASC,
			CASE 
				WHEN (@SortOrder = 'By_desc')
					THEN OL.SUBMIT_BY
				END DESC,
			CASE
				WHEN (@SortOrder = 'Method')
					THEN OL.SUBMIT_METHOD
				END ASC,
			CASE
				WHEN (@SortOrder = 'Method_Desc')
					THEN OL.SUBMIT_METHOD
				END DESC,
			CASE
				WHEN (@SortOrder = 'Status')
					THEN OL.SUBMIT_METHOD
				END ASC,
			CASE
				WHEN (@SortOrder = 'Status_Desc')
					THEN OL.SUBMIT_METHOD
				END DESC,
			CASE 
				WHEN (@SortOrder = '')
					THEN OL.[UID] 
				END DESC, 
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN OL.UID
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_DELETEDRECEIVEDNOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DELETEDRECEIVEDNOTICE]
	@UserDept VARCHAR(100) = '',
	@FormByString NVARCHAR(100) = '',
	@CustomerString NVARCHAR(2000) = '',
	@RegistrationNoString NVARCHAR(2000) = '',
	@ReportedDateFrom DATETIME = NULL,
	@ReportedDateTo DATETIME = NULL,
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		-- Insert statements for procedure here
		SELECT BRN.UID AS Id
		FROM dbo.TR_BOOK_RECEIVED_NOTICE AS BRN
			LEFT JOIN TR_BOOK BK WITH (NOLOCK) ON BRN.DOC_ID = BK.UID
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
		WHERE BRN.IS_ACTIVE = 0
			AND (
				@UserDept = BRN.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND 
			(
				(
					ISNULL(@FormByString,'') <> ''
					AND FORM.VALUE = CASE 
							WHEN @FormByString = 'All' THEN FORM.VALUE
							ELSE @FormByString
						END
				) OR ISNULL(@FormByString,'') = ''
			)
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND CC.LegalName LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND BK.REGIS_NO LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
			AND 
			(
				(ISNULL(@ReportedDateFrom,'') <> '' AND ISNULL(@ReportedDateTo,'') <> '' AND CAST(BRN.RCV_DT AS DATE) BETWEEN @ReportedDateFrom AND @ReportedDateTo) 
				OR ISNULL(@ReportedDateFrom,'') = '' OR ISNULL(@ReportedDateTo,'') = ''
			)
		),
	CTE_TotalRows
	AS (
		SELECT COUNT(BRN.UID) AS TotalRows
		FROM dbo.TR_BOOK_RECEIVED_NOTICE AS BRN
			LEFT JOIN TR_BOOK BK WITH (NOLOCK) ON BRN.DOC_ID = BK.UID
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode
		WHERE BRN.IS_ACTIVE = 0
			AND (
				@UserDept = BRN.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND 
			(
				(
					ISNULL(@FormByString,'') <> ''
					AND FORM.VALUE = CASE 
							WHEN @FormByString = 'All' THEN FORM.VALUE
							ELSE @FormByString
						END
				) OR ISNULL(@FormByString,'') = ''
			)
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND cc.LegalName LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND BK.REGIS_NO LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
			AND 
			(
				(ISNULL(@ReportedDateFrom,'') <> '' AND ISNULL(@ReportedDateTo,'') <> '' AND CAST(BRN.RCV_DT AS DATE) BETWEEN @ReportedDateFrom AND @ReportedDateTo) 
				OR ISNULL(@ReportedDateFrom,'') = '' OR ISNULL(@ReportedDateTo,'') = ''
			)
		)
	SELECT TotalRows,
		BRN.UID AS Id,
		isnull(BK.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS BookType,
		isnull(FORM.VALUE, '') AS BookTypeDesc,
		isnull(BRN.CREATOR, '') AS Creator,
		isnull(BRN.CREATOR_DEPT, '') AS CreatorDept,
		isnull(BRN.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') AS CreatorDeptDesc,
		isnull(BRN.CREATOR_ROLE, '') AS CreatorRole,
		isnull(BRN.CREATOR_UNIT, '') AS CreatorUnit,
		BRN.REQUEST_DT AS RequestDate,
		isnull(BK.BRANCH_CD, '') AS BranchCode,
		isnull(BK.BRANCH_CD, '')  + ' - ' + isnull(BR.VALUE, '') AS BranchCodeDesc,
		isnull(CC.LegalName, '') AS CustomerCodeDesc,
		BK.REPORTED_DT AS ReportedDate,
		isnull(BRN.RCV_METHOD, '') AS ReceivedMethod,
		isnull(RCVM.VALUE, '') AS ReceivedMethodDesc,
		BRN.RCV_DT AS ReceivedDate,
		isnull(BRN.RCV_BY, '') AS ReceivedBy,
		isnull(BRN.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(BRN.APPROVAL_STS, '') AS ApprovalStatusDesc,
		isnull(BRN.IS_ACTIVE, '') AS IsActive,
		isnull(BRN.CREATED_BY, '') AS CreatedBy,
		BRN.CREATED_DT AS CreatedDate,
		isnull(BRN.CREATED_HOST, '') AS CreatedHost,
		isnull(BRN.MODIFIED_BY, '') AS ModifiedBy,
		BRN.MODIFIED_DT AS ModifiedDate,
		isnull(BRN.MODIFIED_HOST, '') AS ModifiedHost
	FROM dbo.TR_BOOK_RECEIVED_NOTICE AS BRN
			LEFT JOIN TR_BOOK BK WITH (NOLOCK) ON BRN.REGIS_NO = BK.REGIS_NO
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD_RN' AND RCVM.KEYFIELD = BRN.RCV_METHOD
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = BRN.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE cc WITH (NOLOCK) ON BK.CUSTOMER_CD = cc.CustomerCode,
		CTE_TotalRows
	WHERE BRN.IS_ACTIVE = 0 AND EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = BRN.UID
			)
		ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN BR.DESCS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN BR.DESCS
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN isnull(BRN.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '')
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN isnull(BRN.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '')
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Form')
					THEN FORM.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Form_desc')
					THEN FORM.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN BK.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN BK.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN BRN.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN BRN.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN BRN.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN BRN.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod')
					THEN BRN.RCV_METHOD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod_desc')
					THEN BRN.RCV_METHOD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN cc.LegalName
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN cc.LegalName
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate')
					THEN BRN.RCV_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate_desc')
					THEN BRN.RCV_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy')
					THEN BRN.RCV_BY
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy_desc')
					THEN BRN.RCV_BY
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Status')
					THEN BRN.APPROVAL_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Status_desc')
					THEN BRN.APPROVAL_STS
				END DESC,
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN BRN.UID
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENINFO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Robby>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[UDPS_DEPARTMENINFO]
	-- Add the parameters for the stored procedure here
	@Id varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here


		SELECT  ID,
			SHORTNAME,
			LONGNAME
		FROM [SN_BTMUDirectory_Department]
		WHERE  SHORTNAME = @Id


END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPDH]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPDH]
	-- add more stored procedure parameters here
	@Department NCHAR(10) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivisionID NCHAR(5) = NULL

	-- Insert statements for procedure here
	-- SELECT Username,
	-- 	Realname,
	-- 	Email
	-- FROM vw_UserByDepartment
	-- WHERE DepartmentCode = @Department
	-- 	AND RoleShortName IN (
	-- 		'DH',
	-- 		'UH'
	-- 		)
	--------------------------------------
	-- AAU Unit ID : 134
	-- DPS JKT : DH, DPS AAU UH
	-- DPS SRB : DH, ALL SRB UH
	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE 1 = 1
		AND (
			(
				@Department = 'DPS'
				AND DepartmentCode = @Department
				AND (
					RoleShortName = 'DH'
					OR (
						RoleShortName = 'UH'
						AND UnitID = '134'
						)
					)
				)
			OR (
				@Department = 'SRB'
				AND DepartmentCode = @Department
				AND RoleShortName IN (
					'UH',
					'DH'
					)
				)
			)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPDHBYDEPTHIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPDHBYDEPTHIST]
	-- add more stored procedure parameters here
	@Department NCHAR(10) = NULL,
	@HistType NVARCHAR(100) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivisionID NCHAR(5) = NULL

	-- Insert statements for procedure here
	-- IF(@HistType IN ('OUT_LETTER','IMPORT_ARTICLE','IMPORT_CORRECTION') AND @Department <> 'SRB')
	-- BEGIN
	-- 	SELECT Username,
	-- 		Realname,
	-- 		Email
	-- 	FROM vw_UserByDepartment
	-- 	WHERE DepartmentCode = @Department
	-- END
	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE 1 = 1
		AND (
			(
				@Department = 'DPS'
				AND DepartmentCode = @Department
				AND (
					RoleShortName = 'DH'
					OR (
						RoleShortName = 'UH'
						AND UnitID = '134'
						)
					)
				)
			OR (
				@Department = 'SRB'
				AND DepartmentCode = @Department
				AND RoleShortName IN (
					'UH',
					'DH'
					)
				AND @HistType NOT IN (
					'OUT_LETTER',
					'IMPORT_ARTICLE',
					'IMPORT_CORRECTION'
					)
				)
			)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPUH]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPUH]
	-- add more stored procedure parameters here
	@Department NCHAR(10) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivisionID NCHAR(5) = NULL

	-- Insert statements for procedure here
	-- SELECT Username,
	-- 	Realname,
	-- 	Email
	-- FROM vw_UserByDepartment
	-- WHERE DepartmentCode = @Department
	-- 	AND RoleShortName IN ('UH','CHECKER')
    ------------------------------------------------
	-- AAU Unit ID : 134  
	-- DPS JKT : DPS AAU UH, DPS AAU CHECKER
	-- DPS SRB : ALL SRB UH, ALL SRB CHECKER
	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE 1 = 1
		AND (
			(
				@Department = 'DPS'
				AND DepartmentCode = @Department
				AND UnitID = '134'
				AND RoleShortName IN (
					'UH',
					'CHECKER'
					)
				)
			OR (
				@Department = 'SRB'
				AND DepartmentCode = @Department
				AND RoleShortName IN (
					'UH',
					'CHECKER'
					)
				)
			)
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPUHBYDEPTHIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPUHBYDEPTHIST]
	-- add more stored procedure parameters here
	@Department NCHAR(10) = NULL,
	@HistType NVARCHAR(100) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivisionID NCHAR(5) = NULL

	-- Insert statements for procedure here
	-- IF(@HistType IN ('OUT_LETTER','IMPORT_ARTICLE','IMPORT_CORRECTION') AND @Department <> 'SRB')
	-- BEGIN
	-- 	SELECT Username,
	-- 		Realname,
	-- 		Email
	-- 	FROM vw_UserByDepartment
	-- 	WHERE DepartmentCode = @Department
	-- 		AND RoleShortName IN ('UH','CHECKER')
	-- END
	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE 1 = 1
		AND (
			(
				@Department = 'DPS'
				AND DepartmentCode = @Department
				AND UnitID = '134'
				AND RoleShortName IN (
					'UH',
					'CHECKER'
					)
				)
			OR (
				@Department = 'SRB'
				AND DepartmentCode = @Department
				AND RoleShortName IN (
					'UH',
					'CHECKER'
					)
				AND @HistType NOT IN (
					'OUT_LETTER',
					'IMPORT_ARTICLE',
					'IMPORT_CORRECTION'
					)
				)
			)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_DOCUMENTHISTORY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DOCUMENTHISTORY]
	@RefId INT,
	@HistoryType VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EDITID BIGINT

	-- Insert statements for procedure here
	SELECT tdh.UID AS Id,
		tdh.SEQ_NO AS SequenceNumber,
		tdh.HIST_TYPE AS HistoryType,
		tdh.REGIS_NO AS RegistrationNo,
		tdh.REF_ID AS ReferalId,
		tdh.ACTION_BY AS ActionBy,
		tdh.ACTION_DT AS ActionDate,
		tdh.STATUS AS STATUS,
		tdh.COMMENT AS Comment,
		tdh.CREATED_BY AS CreatedBy,
		tdh.CREATED_DT AS CreatedDate,
		tdh.CREATED_HOST AS CreatedHost,
		tdh.MODIFIED_BY AS ModifiedBy,
		tdh.MODIFIED_DT AS ModifiedDate,
		tdh.MODIFIED_HOST AS ModifiedHost,
		tdh.WFSTEP AS WfStep,
		CASE tdh.STATUS
			WHEN 'APPROVE' THEN 
				CASE tdh.WFSTEP 
					WHEN 1 THEN 'Approval UH / Checker' 
					WHEN 2 THEN 'Approval DH / UH' 
				END
			WHEN 'SUBMIT' THEN 'Submitted by Creator' 
			WHEN 'REVISE' THEN 'Revise' 
			WHEN 'REJECT' THEN 'Reject'
		END AS Stage
	FROM dbo.TR_DOCUMENT_HISTORY AS tdh
	WHERE 1 = 1
		AND tdh.REF_ID = @RefId
		AND tdh.HIST_TYPE = @HistoryType
	ORDER BY tdh.SEQ_NO DESC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DRAFTLIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DRAFTLIST]
	@CurUser VARCHAR(100) = '',
	@TaskCategoryByString NVARCHAR(100) = '',
	@CustomerString NVARCHAR(2000) = '',
	@RegistrationNoString NVARCHAR(2000) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		-- Insert statements for procedure here
		SELECT DL.Id, DL.TaskCategoryDesc
		FROM dbo.VW_DRAFTLIST AS DL
		WHERE @CurUser = DraftBy
			AND 			
			(
				(
					ISNULL(@TaskCategoryByString,'') <> ''
					AND DL.TaskCategory = CASE 
							WHEN @TaskCategoryByString = 'Claim' THEN 'CLAIM' 
							WHEN @TaskCategoryByString = 'Notice' THEN 'NOTICE' 
							WHEN @TaskCategoryByString = 'Request' THEN 'REQUEST' 
							WHEN @TaskCategoryByString = 'Important Article' THEN 'IMPORT_ARTICLE' 
							WHEN @TaskCategoryByString = 'Outgoing Letter' THEN 'OUT_LETTER' 
							WHEN @TaskCategoryByString = 'All' THEN DL.TaskCategory
						END
				) OR ISNULL(@TaskCategoryByString,'') = ''
			)				
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND DL.CustomerAttentionDesc LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND DL.RegistrationNo LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
		),
	CTE_TotalRows
	AS (
		SELECT COUNT(DL.Id) AS TotalRows
		FROM dbo.VW_DRAFTLIST AS DL
		WHERE @CurUser = DraftBy
			AND 			
			(
				(
					ISNULL(@TaskCategoryByString,'') <> ''
					AND DL.TaskCategory = CASE 
							WHEN @TaskCategoryByString = 'Claim' THEN 'CLAIM' 
							WHEN @TaskCategoryByString = 'Notice' THEN 'NOTICE' 
							WHEN @TaskCategoryByString = 'Request' THEN 'REQUEST' 
							WHEN @TaskCategoryByString = 'Important Article' THEN 'IMPORT_ARTICLE' 
							WHEN @TaskCategoryByString = 'Outgoing Letter' THEN 'OUT_LETTER' 
							WHEN @TaskCategoryByString = 'All' THEN DL.TaskCategory
						END
				) OR ISNULL(@TaskCategoryByString,'') = ''
			)				
			AND 
			(
				(ISNULL(@CustomerString,'') <> '' AND DL.CustomerAttentionDesc LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
			)
			AND 
			(
				(ISNULL(@RegistrationNoString,'') <> '' AND DL.RegistrationNo LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
			)
		)
	SELECT TotalRows,
		DL.Id AS Id,
		CASE WHEN DL.RegistrationNo = '-' THEN '(Will be available later)' ELSE DL.RegistrationNo END AS RegistrationNo,
		DL.TaskCategory AS TaskCategory,
		DL.TaskCategoryDesc AS TaskCategoryDesc,
		DL.CustomerAttention AS CustomerAttention,
		DL.CustomerAttentionDesc AS CustomerAttentionDesc,
		DL.DraftBy AS DraftBy,
		DL.DraftDate AS DraftDate
		, DL.UrlDetail as UrlDetail
		, DL.UrlDelete as UrlDelete
		, DL.ControllerName AS ControllerName
	FROM dbo.VW_DRAFTLIST AS DL,
		CTE_TotalRows
	WHERE @CurUser = DraftBy
			AND EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = DL.Id AND CTE_Result.TaskCategoryDesc = DL.TaskCategoryDesc
			)
		ORDER BY CASE 
				WHEN (@SortOrder = 'TaskCategory')
					THEN DL.TaskCategoryDesc
				END ASC,
			CASE 
				WHEN (@SortOrder = 'TaskCategory_desc')
					THEN DL.TaskCategoryDesc
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN DL.RegistrationNo
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN DL.RegistrationNo
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN DL.CustomerAttentionDesc
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN DL.CustomerAttentionDesc
				END DESC,
			CASE 
				WHEN (@SortOrder = 'DraftBy')
					THEN DL.DraftBy
				END ASC,
			CASE 
				WHEN (@SortOrder = 'DraftBy_desc')
					THEN DL.DraftBy
				END DESC,
			CASE 
				WHEN (@SortOrder = 'DraftDate')
					THEN DL.DraftDate
				END ASC,
			CASE 
				WHEN (@SortOrder = 'DraftDate_desc')
					THEN DL.DraftDate
				END DESC,
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN DL.DraftDate
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAIL_SUBJECT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EMAIL_SUBJECT]
	@GROUP VARCHAR(50)
AS
BEGIN
	SELECT VALUE
	FROM MS_PARAMETER
	WHERE [GROUP] = @GROUP
		AND KEYFIELD = 'SUBJECT'
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILAPRVCOMP]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_EMAILAPRVCOMP]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @ApproveDate VARCHAR(100)
	DECLARE @UrlLink NVARCHAR(2000)
	DECLARE @PrevApprover VARCHAR(100) = ''
	DECLARE @CommentPrevApprover VARCHAR(8000) = ''
	
	SELECT @UrlLink = ISNULL([VALUE],'')+ 
		CASE 
			WHEN @HISTTYPE IN ('BOOK_CLAIM', 'BOOK_NOTICE', 'BOOK_REQUEST') THEN 'Claim/Detail/' 
			WHEN @HISTTYPE IN ('RECEIVED_NOTICE') THEN 'ReceivedNotice/Detail/' 
			WHEN @HISTTYPE IN ('ACTION_TAKEN') THEN 'ActionTaken/Detail/' 
			WHEN @HISTTYPE IN ('IMPORT_ARTICLE') THEN 'ImportantArticle/Detail/' 
			WHEN @HISTTYPE IN ('IMPORT_CORRECTION') THEN 'ImportantArticleCorrection/Detail/' 
			WHEN @HISTTYPE IN ('OUT_LETTER') THEN 'OutgoingLetter/Detail/' 
		END 
		+ CAST(@DOCID AS VARCHAR(10))
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_URL'

	SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY,''),
		@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') +')<br/>' + COMMENT + '<br/><br/>','')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND REF_ID = @DOCID
	ORDER BY SEQ_NO DESC
	
	SET @ApproveDate = convert(varchar,getdate(),107) + ' at ' + convert(varchar,getdate(),108)
	
	SELECT REPLACE(
		REPLACE(
			REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover)
			, '{{ ApproveDate }}', @ApproveDate)
		, '{{ UrlLink }}', @UrlLink)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_APRV_COMP'
		AND KEYFIELD = 'CONTENT'
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILAPRVREJECT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EMAILAPRVREJECT]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @RejectDate VARCHAR(100)
	DECLARE @UrlLink NVARCHAR(2000)
	DECLARE @PrevApprover VARCHAR(100) = ''
	DECLARE @CommentPrevApprover VARCHAR(8000) = ''
	
	SET @RejectDate = convert(varchar,getdate(),107) + ' at ' + convert(varchar,getdate(),108)
	
	SELECT @UrlLink = ISNULL([VALUE],'') + 
		CASE 
			WHEN @HISTTYPE IN ('BOOK_CLAIM', 'BOOK_NOTICE', 'BOOK_REQUEST') THEN 'Claim/Detail/' 
			WHEN @HISTTYPE IN ('RECEIVED_NOTICE') THEN 'ReceivedNotice/Detail/' 
			WHEN @HISTTYPE IN ('ACTION_TAKEN') THEN 'ActionTaken/Detail/' 
			WHEN @HISTTYPE IN ('IMPORT_ARTICLE') THEN 'ImportantArticle/Detail/' 
			WHEN @HISTTYPE IN ('IMPORT_CORRECTION') THEN 'ImportantArticleCorrection/Detail/' 
			WHEN @HISTTYPE IN ('OUT_LETTER') THEN 'OutgoingLetter/Detail/' 
		END 
		+ CAST(@DOCID AS VARCHAR(10))
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_URL'

	SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY,''),
		@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') +')<br/>' + COMMENT + '<br/><br/>','')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND REF_ID = @DOCID
	ORDER BY SEQ_NO DESC
	
	SELECT REPLACE(
		REPLACE(
			REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover)
			, '{{ RejectDate }}', @RejectDate)
		, '{{ UrlLink }}', @UrlLink)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_APRV_REJECT'
		AND KEYFIELD = 'CONTENT'
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILNOTICEITSO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILNOTICEITSO]
	-- add more stored procedure parameters here
	@INSTANCEID VARCHAR(100)
AS
BEGIN
	SELECT REPLACE([VALUE], '{{ instanceID }}', @INSTANCEID)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NOTICE_ITSO'
		AND [KEYFIELD] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDH]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDH]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@DEPT VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PrevApprover VARCHAR(100) = ''
	DECLARE @CommentPrevApprover VARCHAR(8000) = ''
	DECLARE @DateAssignment VARCHAR(100)
	DECLARE @CurrentApprover VARCHAR(2000) = ''

	-- SELECT @CurrentApprover = ISNULL(STUFF((
	-- 				SELECT DISTINCT ',' + RealName
	-- 				FROM vw_UserByDepartment
	-- 				WHERE DepartmentCode = @DEPT
	-- 					AND RoleShortName in ('DH','UH')
	-- 				FOR XML PATH('')
	-- 				), 1, 1, ''), '')
	------------------------------------
	-- AAU Unit ID : 134
	-- DPS JKT : DH, DPS AAU UH
	-- DPS SRB : DH, ALL SRB UH
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE 1 = 1
						AND (
							(
								@DEPT = 'DPS'
								AND DepartmentCode = @DEPT
								AND (
									RoleShortName = 'DH'
									OR (
										RoleShortName = 'UH'
										AND UnitID = '134'
										)
									)
								)
							OR (
								@DEPT = 'SRB'
								AND DepartmentCode = @DEPT
								AND RoleShortName IN (
									'UH',
									'DH'
									)
								)
							)
					FOR XML PATH('')
					), 1, 1, ''), '')

	SET @DateAssignment = convert(VARCHAR, getdate(), 107) + ' at ' + convert(VARCHAR, getdate(), 108)

	SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY, ''),
		@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') + ')<br/>' + COMMENT + '<br/><br/>', '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND REF_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SELECT REPLACE(REPLACE(REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover), '{{ DateAssignment }}', @DateAssignment), '{{ CurrentApprover }}', @CurrentApprover)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQ_APRV'
		AND KEYFIELD = 'CONTENT'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDH_REMINDER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDH_REMINDER]
	@EmailBody VARCHAR(8000) OUTPUT,
	@DocId BIGINT,
	@HistType VARCHAR(100),
	@Dept VARCHAR(50),
	@SN VARCHAR(50)
AS
BEGIN
	--DECLARE @DocId BIGINT=38
	--DECLARE @HistType VARCHAR(100)='OUT_LETTER'
	--DECLARE @Dept VARCHAR(50)='FCD'

	DECLARE @PrevApprover VARCHAR(100) = ''
	DECLARE @CommentPrevApprover VARCHAR(8000) = ''
	DECLARE @DateAssignment VARCHAR(100)
	DECLARE @Path VARCHAR(100)
	DECLARE @UrlLink VARCHAR(100)
	DECLARE @CurrentApprover VARCHAR(2000) = ''

	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = @DEPT
						AND RoleShortName = 'DH'
					FOR XML PATH('')
					), 1, 1, ''), '')
	
	SELECT @Path = VALUE FROM DBO.MS_PARAMETER WHERE [GROUP]='APP_URL'
	
	SELECT @UrlLink = CONCAT(@Path, 
		CASE @HistType 
			WHEN 'BOOK_CLAIM' THEN 'Claim'
			WHEN 'BOOK_NOTICE' THEN 'Claim'
			WHEN 'BOOK_REQUEST' THEN 'Claim'
			WHEN 'ACTION_TAKEN' THEN 'ActionTaken'
			WHEN 'RECEIVED_NOTICE' THEN 'ReceivedNotice'
			WHEN 'IMPORT_ARTICLE' THEN 'ImportantArticle'
			WHEN 'IMPORT_CORRECTION' THEN 'ImportantArticleCorrection'
			WHEN 'OUT_LETTER' THEN 'OutgoingLetter'
		END
		+'/Approval?id=' + cast(@DOCID as varchar(5)) + '&sn=' + @SN)

	SET @DateAssignment = convert(varchar,getdate(),107) + ' at ' + convert(varchar,getdate(),108)
	
	SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY,''),
		@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') +')<br/>' + COMMENT + '<br/><br/>','')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND REF_ID = @DOCID
	ORDER BY SEQ_NO DESC
	
	SELECT @EmailBody =
		REPLACE(
			REPLACE(
				REPLACE(				
						REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover)				
				, '{{ DateAssignment }}', @DateAssignment)
			, '{{ CurrentApprover }}', @CurrentApprover)
		, '{{ UrlLink }}', @UrlLink)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQ_APRV_SCH'
		AND KEYFIELD = 'CONTENT'

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTUH]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTUH]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@DEPT VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PrevApprover VARCHAR(100) = ''
	DECLARE @CommentPrevApprover VARCHAR(8000) = ''
	DECLARE @DateAssignment VARCHAR(100)
	DECLARE @CurrentApprover VARCHAR(2000) = ''

	-- SELECT @CurrentApprover = ISNULL(STUFF((
	-- 				SELECT DISTINCT ',' + RealName
	-- 				FROM vw_UserByDepartment
	-- 				WHERE DepartmentCode = @DEPT
	-- 					AND RoleShortName IN ('UH','CHECKER')
	-- 				FOR XML PATH('')
	-- 				), 1, 1, ''), '')
	--------------------------------------
	-- AAU Unit ID : 134  
	-- DPS JKT : DPS AAU UH, DPS AAU CHECKER
	-- DPS SRB : ALL SRB UH, ALL SRB CHECKER
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE 1 = 1
						AND (
							(
								@DEPT = 'DPS'
								AND DepartmentCode = @DEPT
								AND UnitID = '134'
								AND RoleShortName IN (
									'UH',
									'CHECKER'
									)
								)
							OR (
								@DEPT = 'SRB'
								AND DepartmentCode = @DEPT
								AND RoleShortName IN (
									'UH',
									'CHECKER'
									)
								)
							)
					FOR XML PATH('')
					), 1, 1, ''), '')

	SET @DateAssignment = convert(VARCHAR, getdate(), 107) + ' at ' + convert(VARCHAR, getdate(), 108)

	SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY, ''),
		@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') + ')<br/>' + COMMENT + '<br/><br/>', '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND REF_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SELECT REPLACE(REPLACE(REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover), '{{ DateAssignment }}', @DateAssignment), '{{ CurrentApprover }}', @CurrentApprover)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQ_APRV'
		AND KEYFIELD = 'CONTENT'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTREVISE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTREVISE]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PrevApprover VARCHAR(100) = ''
	DECLARE @CommentPrevApprover VARCHAR(8000) = ''
	DECLARE @ReviseDate VARCHAR(100)

	SET @ReviseDate = convert(varchar,getdate(),107) + ' at ' + convert(varchar,getdate(),108)
	
	SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY,''),
		@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') +')<br/>' + COMMENT + '<br/><br/>','')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND REF_ID = @DOCID
		AND STATUS='REVISE'
	ORDER BY SEQ_NO DESC

	SELECT REPLACE(
			REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover)
		, '{{ ReviseDate }}', @ReviseDate) AS VALUE
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQ_REV'
		AND KEYFIELD = 'CONTENT'
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_EXISTINGAPPROVAL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EXISTINGAPPROVAL] @DocId BIGINT,
	@SerialNumber VARCHAR(20)
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedure
	IF EXISTS (
			SELECT 1
			FROM TR_WORKFLOWPROCESS
			WHERE 1 = 1
				AND ISACTIVE = 1
				AND DOCID = @DocId
				AND SN = @SerialNumber
			)
		SELECT 1
	ELSE
		SELECT 1
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_EXPORTARTICLEPROFILELIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EXPORTARTICLEPROFILELIST]
	@UserDept VARCHAR(200) = '',
	@SearchArticleId NVARCHAR(100) = '',
	@SearchDepartment NVARCHAR(100) = '',
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

		-- Insert statements for procedure here
	SELECT BP.UID AS [UID],
		isnull(BP.ARTICLE_ID, '') AS [ARTICLE_ID],
		isnull(BP.NAME, '') AS [NAME],
		isnull(BP.DEPT_ARTICLE, '') AS [DEPT_ARTICLE],
		isnull(BP.IS_STOCK_IN_VAULT, '') AS [IS_STOCK_IN_VAULT],
		isnull(BP.IS_STOCK_IN_HAND, '') AS [IS_STOCK_IN_HAND],
		isnull(BP.IS_USAGE, '') AS [IS_USAGE],
		isnull(BP.IS_CORRECTION, '') AS [IS_CORRECTION]
		FROM dbo.MS_ARTICLE_PROFILE AS BP
		WHERE (ISNULL(ARTICLE_ID,'') LIKE  '%' + @SearchArticleId + '%' OR ISNULL(@SearchArticleId,'') = '')
		AND (ISNULL(DEPT_ARTICLE,'') LIKE  '%' + @SearchDepartment + '%' OR ISNULL(@SearchDepartment,'') = '')
		AND (
				BP.DEPT_ARTICLE  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)		
		ORDER BY CASE 
				WHEN @SortOrder = 'ArticleID'
					THEN [ARTICLE_ID]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ArticleID_desc'
					THEN [ARTICLE_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Name'
					THEN [NAME]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Name_desc'
					THEN [NAME]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Department'
					THEN [DEPT_ARTICLE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Department_desc'
					THEN [DEPT_ARTICLE]
				END DESC,
			
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC
		
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_EXPORTBOOKLIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EXPORTBOOKLIST]
	@UserDept VARCHAR(100) = '',
	@SearchByString NVARCHAR(100) = '',
	@AuthorString NVARCHAR(2000) = '',
	@CustomerNameString NVARCHAR(2000) = '',
	@ReceivedByString NVARCHAR(2000) = '',
	@ReceivedDateFrom DATETIME = NULL,
	@ReceivedDateTo DATETIME = NULL,
	@ReceivedMethodString NVARCHAR(2000) = '',
	@RegistrationNoString NVARCHAR(100) = '',
	@ReportedDateFrom DATETIME = NULL,
	@ReportedDateTo DATETIME = NULL,
	@RequestDateFrom DATETIME = NULL,
	@RequestDateTo DATETIME = NULL,
	@TypeString NVARCHAR(2000) = '',
	@FullSearchString NVARCHAR(100) = '',
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

		-- Insert statements for procedure here
	SELECT BK.UID AS Id,
		isnull(BK.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS BookType,
		isnull(FORM.VALUE, '') AS BookTypeDesc,
		isnull(BK.CREATOR, '') AS Creator,
		isnull(BK.CREATOR_DEPT, '') AS CreatorDept,
		isnull(BK.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') AS CreatorDeptDesc,
		isnull(BK.CREATOR_ROLE, '') AS CreatorRole,
		BK.REQUEST_DT AS RequestDate,
		isnull(BK.BRANCH_CD, '') AS BranchCode,
		isnull(BR.VALUE, '') AS BranchCodeDesc,
		BK.REPORTED_DT AS ReportedDate,
		isnull(BK.TYPE, '') AS Type,
		isnull(TY.VALUE, '') AS TypeDesc,
		isnull(BK.RECEIVED_METHOD, '') AS ReceivedMethod,
		isnull(RCVM.VALUE, '') AS ReceivedMethodDesc,
		isnull(BK.CUSTOMER_CD, '') AS CustomerCode,
		isnull(CC.LegalName, '') AS CustomerCodeDesc,
		isnull(BK.ACCOUNT_NO, '') AS AccountNo,
		isnull(BK.HAS_AMOUNT, 0) AS HasAmount,
		isnull(BK.AMOUNT_CURR, '') AS AmountCurrency,
		isnull(BK.AMOUNT, 0) AS Amount,
		isnull(BK.REMARKS, '') AS Remarks,
		BK.POLR_RECEIVED_DT AS PolrReceivedDate,
		isnull(BK.POLR_RECEIVED_BY, '') AS PolrReceivedBy,
		isnull(BK.POLR_RECEIVED_REMARKS, '') AS PolrReceivedRemarks,
		isnull(BK.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(BK.APPROVAL_STS, '') AS ApprovalStatusDesc,
		isnull(BK.IS_ACTIVE, '') AS IsActive,
		isnull(BK.CREATED_BY, '') AS CreatedBy,
		BK.CREATED_DT AS CreatedDate,
		isnull(BK.CREATED_HOST, '') AS CreatedHost,
		isnull(BK.MODIFIED_BY, '') AS ModifiedBy,
		BK.MODIFIED_DT AS ModifiedDate,
		isnull(BK.MODIFIED_HOST, '') AS ModifiedHost
		FROM dbo.TR_BOOK AS BK
			LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = BK.BRANCH_CD
			LEFT JOIN MS_PARAMETER FORM WITH (NOLOCK) ON FORM.[GROUP] = 'BOOK_TYPE' AND FORM.KEYFIELD = BK.BOOK_TYPE
			LEFT JOIN MS_PARAMETER RCVM WITH (NOLOCK) ON RCVM.[GROUP] = 'RECEIVED_METHOD' AND RCVM.KEYFIELD = BK.RECEIVED_METHOD
			LEFT JOIN MS_PARAMETER TY WITH (NOLOCK) ON 
				TY.[GROUP] = CASE WHEN BK.BOOK_TYPE='CLAIM' THEN 'BOOK_TYPE_CLAIM' WHEN BK.BOOK_TYPE='NOTICE' THEN 'BOOK_TYPE_NOTICE' WHEN BK.BOOK_TYPE='REQUEST' THEN 'BOOK_TYPE_REQUEST' END 
				AND TY.KEYFIELD = BK.TYPE
			LEFT JOIN VW_EASYCUSTOMERCODE CC WITH (NOLOCK) ON BK.CUSTOMER_CD = CC.CustomerCode
			LEFT JOIN SN_BTMUDirectory_Department dept WITH (NOLOCK) ON BK.CREATOR_DEPT = dept.ShortName
		WHERE BK.IS_ACTIVE = 1 AND BK.IS_DRAFT = 0
			AND (
				@UserDept = BK.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			AND (
				(
					(
						CASE 
							WHEN @SearchByString = 'Author'
								AND ISNULL(@AuthorString,'') <> ''
								THEN BK.CREATOR
							END
						) LIKE '%' + @AuthorString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Customer Name'
								AND ISNULL(@CustomerNameString,'') <> ''
								THEN CC.LegalName
							END
						) LIKE '%' + @CustomerNameString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received By'
								AND ISNULL(@ReceivedByString,'') <> ''
								THEN BK.POLR_RECEIVED_BY
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@ReceivedByString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received Date'
								AND @ReceivedDateFrom IS NOT NULL
								AND @ReceivedDateTo IS NOT NULL
								THEN BK.POLR_RECEIVED_DT
							END
						) BETWEEN @ReceivedDateFrom
						AND @ReceivedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Registration Number'
								AND ISNULL(@RegistrationNoString,'') <> ''
								THEN BK.REGIS_NO
							END
						) LIKE '%' + @RegistrationNoString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Received Method'
								AND ISNULL(@ReceivedMethodString,'') <> ''
								THEN BK.RECEIVED_METHOD
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@ReceivedMethodString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Reported Date'
								AND @ReportedDateFrom IS NOT NULL
								AND @ReportedDateTo IS NOT NULL
								THEN BK.REPORTED_DT
							END
						) BETWEEN @ReportedDateFrom
						AND @ReportedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Request Date'
								AND @RequestDateFrom IS NOT NULL
								AND @RequestDateTo IS NOT NULL
								THEN BK.REQUEST_DT
							END
						) BETWEEN @RequestDateFrom
						AND @RequestDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Type'
								AND ISNULL(@TypeString,'') <> ''
								THEN TY.VALUE
							END
						) IN (
						SELECT Name
						FROM dbo.splitstring(@TypeString, ',')
						)
					)
				OR (
					(
						CASE 
							WHEN ISNULL(@SearchByString,'') <> ''
								AND ISNULL(@AuthorString,'') = ''
								AND ISNULL(@CustomerNameString,'') = ''
								AND ISNULL(@ReceivedByString,'') = ''
								AND @ReceivedDateFrom IS NULL
								AND @ReceivedDateTo IS NULL
								AND ISNULL(@ReceivedMethodString,'') = ''
								AND ISNULL(@RegistrationNoString, '') = ''
								AND @ReportedDateFrom IS NULL
								AND @ReportedDateTo IS NULL
								AND @RequestDateFrom IS NULL
								AND @RequestDateTo IS NULL
								AND ISNULL(@TypeString, '') = ''
								THEN 1
							END
						) = 1
					)
				OR ISNULL(@SearchByString,'') = ''
				)
			AND (
				ISNULL(@FullSearchString,'') = ''
				OR BR.VALUE LIKE '%' + @FullSearchString + '%'
				OR isnull(BK.CREATOR_DEPT, '') + isnull(' - '+ dept.LongName, '') LIKE '%' + @FullSearchString + '%'
				OR FORM.VALUE LIKE '%' + @FullSearchString + '%'
				OR BK.REGIS_NO LIKE '%' + @FullSearchString + '%'
				OR BK.CREATOR LIKE '%' + @FullSearchString + '%'
				OR BK.CREATED_DT LIKE '%' + @FullSearchString + '%'
				OR BK.REPORTED_DT LIKE '%' + @FullSearchString + '%'
				OR TY.VALUE LIKE '%' + @FullSearchString + '%'
				OR RCVM.VALUE LIKE '%' + @FullSearchString + '%'
				OR CC.LegalName LIKE '%' + @FullSearchString + '%'
				OR BK.POLR_RECEIVED_DT LIKE '%' + @FullSearchString + '%'
				OR BK.POLR_RECEIVED_BY LIKE '%' + @FullSearchString + '%'
				OR BK.REMARKS LIKE '%' + @FullSearchString + '%'
				OR BK.APPROVAL_STS LIKE '%' + @FullSearchString + '%'
				)
		ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN BR.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN BR.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN BK.CREATOR_DEPT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN BK.CREATOR_DEPT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Form')
					THEN FORM.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Form_desc')
					THEN FORM.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN BK.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN BK.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN BK.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN BK.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReportedDate')
					THEN BK.REPORTED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReportedDate_desc')
					THEN BK.REPORTED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Type')
					THEN TY.VALUE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Type_desc')
					THEN TY.VALUE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod')
					THEN BK.RECEIVED_METHOD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedMethod_desc')
					THEN BK.RECEIVED_METHOD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN cc.LegalName
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN cc.LegalName
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate')
					THEN BK.POLR_RECEIVED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedDate_desc')
					THEN BK.POLR_RECEIVED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'ReceivedBy_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Remarks')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Remarks_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Staging')
					THEN BK.CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Staging_desc')
					THEN BK.CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Status')
					THEN BK.APPROVAL_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Status_desc')
					THEN BK.APPROVAL_STS
				END DESC,
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN BK.UID
				END DESC 
		
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_EXPORTBOOKPROFILELIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EXPORTBOOKPROFILELIST]
	@UserDept VARCHAR(200) = '',
	@SearchBookId NVARCHAR(100) = '',
	@SearchBookName NVARCHAR(100) = '',
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

		-- Insert statements for procedure here
	SELECT BP.UID AS [UID],
		isnull(BP.BOOK_ID, '') AS [BOOK_ID],
		isnull(BP.BOOK_NAME, '') AS [BOOK_NAME],
		isnull(BP.DEPT_BOOK, '') AS [DEPT_BOOK],
		isnull(BP.BOOK_SEQ, '') AS [BOOK_SEQ],
		isnull(BP.BEFORE_OFFSET_DAYS, '') AS [BEFORE_OFFSET_DAYS],
		isnull(BP.AFTER_OFFSET_DAYS, '') AS [AFTER_OFFSET_DAYS],
		isnull(BP.CREATED_BY, '') AS [CREATED_BY],
		isnull(BP.CREATED_DT, '') AS [CREATED_DT]
		FROM dbo.MS_BOOK_PROFILE AS BP
		WHERE (ISNULL(BOOK_ID,'') LIKE '%' + @SearchBookId + '%' OR ISNULL(@SearchBookId,'')= '')
			AND (ISNULL(BOOK_NAME,'') LIKE '%' + @SearchBookName + '%' OR ISNULL(@SearchBookName,'')= '')
			AND (
				BP.DEPT_BOOK  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		ORDER BY CASE 
				WHEN @SortOrder = 'BookID'
					THEN [BOOK_ID]
				END ASC,
			CASE 
				WHEN @SortOrder = 'BookID_desc'
					THEN [BOOK_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'BookName'
					THEN [BOOK_NAME]
				END ASC,
			CASE 
				WHEN @SortOrder = 'BookName_desc'
					THEN [BOOK_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Sequence'
					THEN [BOOK_SEQ]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Sequence_desc'
					THEN [BOOK_SEQ]
				END DESC,
			
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC 
		
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_EXPORTLETTERPROFILELIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EXPORTLETTERPROFILELIST]
@UserDept VARCHAR(200) = '',
	@SearchDescription NVARCHAR(100) = '',
	@SearchLetterType NVARCHAR(100) = '',
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

		-- Insert statements for procedure here
	SELECT LP.UID AS [UID],
		isnull(LP.LETTER_TYPE, '') AS [LETTER_TYPE],
		isnull(LP.DESCS, '') AS [DESCS],
		isnull(LP.DEPT_LETTER, '') AS [DEPT_LETTER],
		isnull(LP.CREATED_BY, '') AS [CREATED_BY],
		isnull(LP.CREATED_DT, '') AS [CREATED_DT]
		FROM dbo.MS_LETTER_PROFILE AS LP
		WHERE (ISNULL(DESCS,'') LIKE '%' + @SearchDescription+ '%' OR ISNULL(@SearchDescription,'')= '')
			AND (ISNULL(LETTER_TYPE,'') LIKE '%' +@SearchLetterType+ '%' OR ISNULL(@SearchLetterType,'')= '')
			AND (
				LP.DEPT_LETTER  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)	
			
				
		ORDER BY CASE 
				WHEN @SortOrder = 'LetterType'
					THEN [LETTER_TYPE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'LetterType_desc'
					THEN [LETTER_TYPE]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Description'
					THEN [DESCS]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Description_desc'
					THEN [DESCS]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Department'
					THEN [DEPT_LETTER]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Department_desc'
					THEN [DEPT_LETTER]
				END DESC,
			
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END ASC
		
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_EXPORTSTOCKPROFILELIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_EXPORTSTOCKPROFILELIST]
@UserDept VARCHAR(200) = '',
	@SearchArticleId NVARCHAR(100) = '',
	@SearchDepartment NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

		-- Insert statements for procedure here
	--SELECT SP.UID AS [UID],
	--	isnull(SP.ARTICLE_ID, '') AS [ARTICLE_ID],
	--	isnull(SP.DEPT_OF_ARTICLE, '') AS [DEPT_OF_ARTICLE],
	--	isnull(SP.STOCK_IN_VAULT, '') AS [STOCK_IN_VAULT],
	--	isnull(SP.STOCK_IN_HAND, '') AS [STOCK_IN_HAND],
	--	isnull(SP.INCOMING_STOCK, '') AS [INCOMING_STOCK],
	--	isnull(SP.USAGE, '') AS [USAGE],
	--	isnull(SP.CORRECTION_IN_HAND, '') AS [CORRECTION_IN_HAND],
	--	isnull(SP.CORRECTION_IN_VAULT, '') AS [CORRECTION_IN_VAULT],
	--	isnull(SP.CORRECTION_IN_USAGE, '') AS [CORRECTION_IN_USAGE]

	--	FROM dbo.MS_STOCK_PROFILE AS SP
			
				
	--	ORDER BY CASE 
	--			WHEN @SortOrder = 'ArticleID'
	--				THEN [ARTICLE_ID]
	--			END ASC,
	--		CASE 
	--			WHEN @SortOrder = 'ArticleID_desc'
	--				THEN [ARTICLE_ID]
	--			END DESC,
			
	--		CASE 
	--			WHEN @SortOrder = 'Department'
	--				THEN [DEPT_OF_ARTICLE]
	--			END ASC,
	--		CASE 
	--			WHEN @SortOrder = 'Department_desc'
	--				THEN [DEPT_OF_ARTICLE]
	--			END DESC,
			
	--		CASE 
	--			WHEN @SortOrder = ''
	--				THEN [UID]
	--			END ASC

	--SET NOCOUNT ON;
	--WITH CTE_Result
	--AS (
		SELECT *
		FROM MS_STOCK_PROFILE AS SP
		WHERE 
		
		(ISNULL(@SearchArticleId,'') = ''
			OR SP.ARTICLE_ID LIKE '%' + @SearchArticleId + '%'
		)
		AND(ISNULL(@SearchDepartment,'') = ''
			OR SP.DEPT_OF_ARTICLE LIKE '%' + @SearchDepartment + '%'
		)
		AND (
				SP.DEPT_OF_ARTICLE  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)	

		ORDER BY CASE 
				WHEN @SortOrder = 'ArticleID'
					THEN [ARTICLE_ID]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ArticleID_desc'
					THEN [ARTICLE_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Department'
					THEN [DEPT_OF_ARTICLE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Department_desc'
					THEN [DEPT_OF_ARTICLE]
				END DESC,
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC --OFFSET @PageSize * (@PageNo - 99) rows FETCH NEXT @PageSize rows ONLY
	--	),
	--CTE_TotalRows
	--AS (
	--	SELECT count(UID) AS TotalRows
	--	FROM MS_STOCK_PROFILE
	--	)
	--SELECT TotalRows,
	--	SP.UID AS [UID],
	--	SP.[ARTICLE_ID] AS [ARTICLE_ID],
	--	SP.DEPT_OF_ARTICLE AS [DEPT_OF_ARTICLE],
	--	SP.STOCK_IN_VAULT AS [STOCK_IN_VAULT],
	--	SP.STOCK_IN_VAULT AS [STOCK_IN_VAULT],
	--	SP.STOCK_IN_HAND AS [STOCK_IN_HAND],
	--	SP.USAGE AS [USAGE],
	--	SP.INCOMING_STOCK AS [INCOMING_STOCK],
	--	SP.CORRECTION_IN_VAULT AS [CORRECTION_IN_VAULT],
	--	SP.CORRECTION_IN_HAND AS [CORRECTION_IN_HAND],
	--	SP.CORRECTION_IN_USAGE AS [CORRECTION_IN_USAGE],
	--	SP.BALANCE_IN_HAND AS [BALANCE_IN_HAND],
	--	SP.CREATED_BY AS CreatedBy,
	--	SP.CREATED_DT AS CreatedDate,
	--	SP.CREATED_HOST AS CreatedHost,
	--	SP.MODIFIED_BY AS ModifedBy,
	--	SP.MODIFIED_DT AS ModifiedDate,
	--	SP.MODIFIED_HOST AS ModifiedHost
	--FROM dbo.MS_STOCK_PROFILE AS SP,
	--	CTE_TotalRows
	--WHERE EXISTS (
	--		SELECT 1
	--		FROM CTE_Result
	--		WHERE CTE_Result.UID = SP.UID
	--		)
	--OPTION (RECOMPILE)
		
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_GetCustomerNameTaskList]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[UDPS_GetCustomerNameTaskList]
	@Folio varchar(500),
	@Task varchar(100)
AS
	DECLARE @Id VARCHAR(100) = SUBSTRING(@Folio,1,CHARINDEX('_',@Folio)-1)

	IF(@Task IN ('BOOK_CLAIM','BOOK_NOTICE','BOOK_REQUEST'))
	BEGIN
		SELECT CC.LegalName
		FROM DBO.TR_BOOK BK
			LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUSTOMER_CD = CC.CustomerCode
		WHERE BK.UID=@Id
	END
	ELSE IF(@Task = 'ACTION_TAKEN')
	BEGIN
		SELECT CC.LegalName
		FROM DBO.TR_BOOK_ACTION_TAKEN AT
			JOIN DBO.TR_BOOK BK ON AT.DOC_ID = BK.UID
			LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUSTOMER_CD = CC.CustomerCode
		WHERE AT.UID=@Id
	END
	ELSE IF(@Task = 'RECEIVED_NOTICE')
	BEGIN
		SELECT CC.LegalName
		FROM DBO.TR_BOOK_RECEIVED_NOTICE RN
			JOIN DBO.TR_BOOK BK ON RN.DOC_ID = BK.UID
			LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUSTOMER_CD = CC.CustomerCode
		WHERE RN.UID=@Id
	END
	ELSE IF(@Task = 'OUT_LETTER')
	BEGIN
		SELECT CC.LegalName
		FROM DBO.TR_OUTGOING_LETTER OL
			LEFT JOIN VW_EASYCUSTOMERCODE CC ON OL.CUST_CD = CC.CustomerCode
		WHERE OL.UID=@Id
	END
	ELSE
		SELECT '-'


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GetDepartmentHeadByDept]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [UDPS_GetDepartmentHeadByDept] null, null, 'boby'
CREATE PROCEDURE [dbo].[UDPS_GetDepartmentHeadByDept]
	@Department NCHAR(10) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DivisionID NCHAR(5) = NULL

	-- Insert statements for procedure here
	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Department
		AND RoleShortName = 'DH'

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GetDepartmentHeadByUser]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [UDPS_GetDepartmentHead] null, null, 'boby'
CREATE PROCEDURE [dbo].[UDPS_GetDepartmentHeadByUser]
	@Originator NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @StrUsername NVARCHAR(50)

    SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE DepartmentCode = (
			SELECT DepartmentCode
			FROM vw_UserByDepartment
			WHERE Username = @StrUsername
			)
		AND RoleShortName = 'DH'
    
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETEASYACCOUNTBYCUSTOMER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_GETEASYACCOUNTBYCUSTOMER]
	@CustomerCode VARCHAR(100)
AS
BEGIN
--------Harusnya ambil dari JKTSVRDEV05 di table MST_ACCOUNT
	SELECT AccountNo as KeyField
		, CombineCode as ValueField
	FROM VW_EASYACCOUNTNO
	WHERE CustomerCode = @CustomerCode
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETEASYACCOUNTNO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<UDPS_GETEASYACCOUNTNO>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_GETEASYACCOUNTNO]
AS
BEGIN
--------Harusnya ambil dari JKTSVRDEV05 di table MST_ACCOUNT
	SELECT AccountNo as KeyField
		, '3' + OfficeCode + '-' + AccountCCY + '-CUA-' + AccountNo as ValueField
	FROM VW_EASYACCOUNTNO
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETEASYADDRESS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<UDPS_GETEASYACCOUNTNO>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_GETEASYADDRESS]
@search varchar(200) = ''
AS
BEGIN
		SELECT DISTINCT LTRIM(RTRIM(replace(REPLACE([ADDRESS],CHAR(13),''), CHAR(10),''))) AS [ADDRESS]
	FROM TR_OUTGOING_LETTER WHERE IS_ACTIVE = 1
	AND ([ADDRESS] LIKE '%' + ISNULL(@search,'') + '%' OR ISNULL(@search,'') = '' )
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETEASYADDRESSBYCUSTOMER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_GETEASYADDRESSBYCUSTOMER]
	@CustomerCode VARCHAR(100)
	--@Query VARCHAR(50) = ''
AS
BEGIN
--------Harusnya ambil dari JKTSVRDEV05 di table MST_address

	SELECT AddressLine1 as KeyField
		, AddressLine1 as ValueField
			FROM VW_EASYADDRESS
		WHERE ADDRESSLINE1 IS NOT NULL
		 AND CustomerCode = @CustomerCode
	
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETEASYATTENTION]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<UDPS_GETEASYACCOUNTNO>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_GETEASYATTENTION]
@search varchar(200) = ''
AS
BEGIN
		SELECT DISTINCT ATTENTION  AS Attention
	FROM TR_OUTGOING_LETTER WHERE IS_ACTIVE = 1
	AND ([ATTENTION] LIKE  '%' + ISNULL(@search,'') + '%' OR ISNULL(@search,'') = '')
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETEASYCUSTOMERCODE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UDPS_GETEASYCUSTOMERCODE]
	@Query VARCHAR(50) = ''
AS
BEGIN
--------Harusnya ambil dari JKTSVRDEV05 di table MST_CUSTOMER hanya yang AKTIF saja
	SELECT CustomerCode as KeyField
		, LegalName as ValueField
	FROM VW_EASYCUSTOMERCODE
	WHERE ISNULL(@Query,'') = '' OR LegalName LIKE @Query + '%'
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETEASYLOCATION]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<UDPS_GETEASYACCOUNTNO>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_GETEASYLOCATION]
@search varchar(200) = ''
AS
BEGIN
		SELECT DISTINCT LOCATION  AS Location
	FROM TR_OUTGOING_LETTER WHERE IS_ACTIVE = 1
	AND ( LOCATION LIKE '%' + ISNULL(@search,'')+ '%' OR ISNULL(@search,'') ='')
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETNEWREGISNO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<[UDPS_GETNEWREGISNO>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_GETNEWREGISNO]
	@RegisNo VARCHAR(50) OUTPUT,
	@BookType VARCHAR(10)='X',
	@DeptCd VARCHAR(10)='X'
AS
BEGIN
	DECLARE @LASTDATE SMALLDATETIME
		,@NOW SMALLDATETIME
		,@LASTSEQ INT
		,@PREFIX VARCHAR(10)

	SET @NOW = GETDATE()
	SET @RegisNo = '-'

	SET @BookType = RTRIM(LTRIM(@BookType))
	SET @DeptCd = RTRIM(LTRIM(@DeptCd))
	IF @BookType IS NULL SET @BookType = 'X'
	IF @DeptCd IS NULL SET @DeptCd = 'X'

	IF NOT EXISTS (SELECT 1 FROM PR_GENREGNO WHERE BOOK_TYPE = @BookType AND DEPT_CD = @DeptCd AND PR_YEAR = YEAR(@NOW))
		INSERT INTO PR_GENREGNO (BOOK_TYPE, DEPT_CD, PR_YEAR) VALUES (@BookType, @DeptCd, YEAR(@NOW))

	WHILE(@RegisNo = '-')
	BEGIN
		BEGIN TRAN
			UPDATE PR_GENREGNO SET PR_FLAG = 1 WHERE BOOK_TYPE = @BookType AND DEPT_CD = @DeptCd AND PR_YEAR = YEAR(@NOW)

			SELECT @LASTDATE = PR_DATE, @LASTSEQ = ISNULL(PR_SEQ, 0) + 1 
			FROM PR_GENREGNO
			WHERE BOOK_TYPE = @BookType AND DEPT_CD = @DeptCd AND PR_YEAR = YEAR(@NOW)

			IF(@BookType IS NOT NULL AND @BookType != '' AND @BookType <> 'X')
			BEGIN
				IF(@BookType = 'CLAIM') SET @PREFIX = 'CLM'
				IF(@BookType = 'NOTICE') SET @PREFIX = 'NOT'
				IF(@BookType = 'REQUEST') SET @PREFIX = 'REQ'
			END

			SET @RegisNo = 'GEN.' + ISNULL(@PREFIX,'X') + '.' + @DeptCd  + '.' + CAST(YEAR(@NOW) AS VARCHAR) +'.' + dbo.fn_enamkar(CONVERT(VARCHAR, @LASTSEQ), 4, '0')
			
			--print @RegisNo

			UPDATE PR_GENREGNO SET PR_FLAG = 0, PR_DATE = @NOW, PR_SEQ = @LASTSEQ WHERE BOOK_TYPE = @BookType AND DEPT_CD = @DeptCd AND PR_YEAR = YEAR(@NOW)

		COMMIT TRAN

		IF EXISTS (SELECT 1 FROM TR_BOOK WHERE REGIS_NO = @RegisNo)
			SET @RegisNo = '-'
	END

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETSUBJECT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211215>
-- Description:	<UDPS_GETEASYACCOUNTNO>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_GETSUBJECT]
@search varchar(200) = ''

AS
BEGIN
		SELECT DISTINCT [SUBJECT] AS Subject,
		 SUBJECT_ENG AS SubjectEng 
	FROM TR_OUTGOING_LETTER WHERE IS_ACTIVE = 1
	AND (CONCAT(SUBJECT,SUBJECT_ENG) LIKE '%' + ISNULL(@search, '')+ '%' OR ISNULL(@search, '') = '')
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GetUnitHeadByDept]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_GetUnitHeadByDept]
	@Department NCHAR(5) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DivisionID NCHAR(5) = NULL

	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Department
		AND RoleShortName = 'UH'

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GetUnitHeadByUser]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec UDPS_GetUnitHead null, 'Yudistira'
CREATE PROCEDURE [dbo].[UDPS_GetUnitHeadByUser]
	@Originator NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @StrUsername NVARCHAR(50)

    SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
    WHERE DepartmentCode = (
			SELECT DepartmentCode
			FROM vw_UserByDepartment
			WHERE Username = @StrUsername
			)
		AND RoleShortName = 'UH'
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETUSERCONE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_GETUSERCONE]
	@Query VARCHAR(50) = ''
AS
BEGIN
	SELECT UserName as UserName
		, RealName as RealName
		, RoleParentId as ParentRoleID
	FROM SN_BTMUDirectory_User
	WHERE IsDeleted = 0 AND ISNULL(@Query,'') = '' OR UserName LIKE @Query + '%'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_GetUserEmail]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec UDPS_GetUnitHead null, 'Yudistira'
CREATE PROCEDURE [dbo].[UDPS_GetUserEmail]
	@Originator NVARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @StrUsername NVARCHAR(50)

    SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
	
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETUSERPOLRRECEIVEDBY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_GETUSERPOLRRECEIVEDBY]
AS
BEGIN
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT DISTINCT BK.POLR_RECEIVED_BY AS UserName
		,BK.POLR_RECEIVED_BY AS RealName
	FROM dbo.TR_BOOK AS BK
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_IMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_IMPORTANTART] 
 @PAGE INT = 1
,@PAGE_SIZE INT = 10
,@CATEGORY VARCHAR(50) =''
,@SEARCH_BY VARCHAR(50) = ''
,@VALUE VARCHAR(2000) = ''
,@DATE_FROM DATETIME
,@DATE_TO DATETIME
,@FULL_SEARCH VARCHAR(2000) = ''
,@SORT_BY VARCHAR(50)
,@REGIS_TYPE VARCHAR(255)
,@USER_DEPT VARCHAR(50) = 'SKAI'
AS
BEGIN

	SELECT ART.*,
	COALESCE(DEPT.SHORTNAME + ' - ' +DEPT.LongName, Art.CREATOR_DEPT, '') as DEPT_LONGNAME,
	COALESCE(Art.ARTICLE_NAME +' - '+ PROF.NAME, Art.ARTICLE_NAME, '') AS ARTICLE_LONGNAME,
	COALESCE(Art.CUST_BRANCH + ' - ' + Mp.VALUE, Art.CUST_BRANCH, '') AS BranchDesc,
	CASE 
		WHEN Art.APPROVAL_STS = 'Revise' THEN 'Revise'
		WHEN Art.APPROVAL_STS = 'Reject' THEN 'Reject'
		WHEN DH.WFSTEP IS NULL THEN 'Submitted By Creator' 
		WHEN DH.WFSTEP = 0 THEN 'Revise'
		WHEN DH.WFSTEP = 1 THEN 'Approval UH / Checker'
		WHEN DH.WFSTEP = 2 Then 'Approval DH / UH' 
	END AS STAGING
	FROM TR_IMPORTANT_ART Art
	LEFT JOIN MS_PARAMETER Mp ON Mp.[GROUP] = 'BRANCH_CODE' AND Mp.KEYFIELD = Art.CUST_BRANCH
	INNER JOIN TR_DOCUMENT_HISTORY DH
	ON Art.UID = DH.REF_ID
	INNER JOIN ( 
			SELECT MAX(UID) UID, REF_ID 
			FROM TR_DOCUMENT_HISTORY 
			WHERE HIST_TYPE = 'IMPORT_ARTICLE'
			GROUP BY REF_ID ) LASTDH
	ON DH.UID = LASTDH.UID
	LEFT JOIN MS_ARTICLE_PROFILE PROF
	ON Art.ARTICLE_NAME = PROF.ARTICLE_ID
	AND PROF.DEPT_ARTICLE = Art.CREATOR_DEPT
	LEFT JOIN [dbo].[SN_BTMUDirectory_Department] DEPT
	ON Art.CREATOR_DEPT = DEPT.ShortName
	WHERE Art.IS_ACTIVE = 1 AND Art.IS_DRAFT = 0
	AND (@USER_DEPT = Art.CREATOR_DEPT OR @USER_DEPT = 'SKAI')
	AND (@CATEGORY LIKE '%'+Art.ART_STATUS+'%')
	AND (CONCAT(Art.CUST_BRANCH, Art.CREATOR_DEPT, Art.REGIS_NO,
				Art.CREATOR, Art.CREATED_DT,Art.ARTICLE_NAME, Art.REGIS_TYPE,
				Art.REF_NO, Art.ART_STATUS, Art.APPROVAL_STS, 
				COALESCE(DEPT.SHORTNAME + ' - ' +DEPT.LongName, Art.CREATOR_DEPT, ''),
				COALESCE(Art.ARTICLE_NAME +' - '+ PROF.NAME, Art.ARTICLE_NAME, ''),
				COALESCE(Art.CUST_BRANCH + ' - ' + Mp.VALUE, Art.CUST_BRANCH, ''),
				CAST(Art.VALUE AS VARCHAR),
				FORMAT(Art.[DATE], 'dd/MMM/yyyy'),
				FORMAT(Art.[SP_DATE], 'dd/MMM/yyyy'),
				FORMAT(Art.[PL_DATE], 'dd/MMM/yyyy'),
				CASE 
					WHEN Art.APPROVAL_STS = 'Revise' THEN 'Revise'
					WHEN Art.APPROVAL_STS = 'Reject' THEN 'Reject'
					WHEN DH.WFSTEP IS NULL THEN 'Submitted By Creator' 
					WHEN DH.WFSTEP = 0 THEN 'Revise'
					WHEN DH.WFSTEP = 1 THEN 'Approval UH / Checker'
					WHEN DH.WFSTEP = 2 Then 'Approval DH / UH' 
				END
		) LIKE '%'+@FULL_SEARCH+'%' OR ISNULL(@FULL_SEARCH,'') = '' )
	AND (
			CASE @SEARCH_BY
				WHEN 'CREATOR' THEN Art.CREATOR
				--WHEN 'CUSTOMER' THEN Art.CUSTOMER
				--WHEN 'REGIS_TYPE' THEN Art.REGIS_TYPE
				WHEN 'REGIS_NO' THEN Art.REGIS_NO
			END LIKE '%'+@VALUE+'%'
			OR ISNULL(@VALUE,'') = ''
			OR ISNULL(@SEARCH_BY,'') = 'REGIS_TYPE')
	AND (
			CASE @SEARCH_BY
				WHEN 'REGIS_TYPE' THEN Art.REGIS_TYPE
				ELSE ISNULL(@REGIS_TYPE, '') END
				LIKE '%'+ ISNULL(@REGIS_TYPE, '') +'%'
	)
	AND (
			CASE @SEARCH_BY
				WHEN 'DATE' THEN CAST(Art.[DATE] AS DATE)
				WHEN 'REQUEST_DT' THEN CAST(Art.[REQUEST_DT] AS DATE)
				WHEN 'REGIS_DT' THEN CAST(Art.[CREATED_DT] AS DATE)
				WHEN 'SP_DATE' THEN CAST(Art.[SP_DATE] AS DATE)
				WHEN 'PL_DATE' THEN CAST(Art.[PL_DATE] AS DATE)
			END >= CAST(@DATE_FROM AS DATE) OR CAST(@DATE_FROM AS DATE) IS NULL
		)
	AND (
			CASE @SEARCH_BY
				WHEN 'DATE' THEN CAST(Art.[DATE] AS DATE)
				WHEN 'REQUEST_DT' THEN CAST(Art.[REQUEST_DT] AS DATE)
				WHEN 'REGIS_DT' THEN CAST(Art.[CREATED_DT] AS DATE)
				WHEN 'SP_DATE' THEN CAST(Art.[SP_DATE] AS DATE)
				WHEN 'PL_DATE' THEN CAST(Art.[PL_DATE] AS DATE)
			END <= CAST(@DATE_TO AS DATE) OR CAST(@DATE_TO AS DATE) IS NULL
		)
	ORDER BY Art.CREATED_DT DESC -- DEFAULT 
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_IMPORTANTARTACTIVITY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_IMPORTANTARTACTIVITY]
@ArticleID bigint
AS
BEGIN
	SELECT * FROM 
	TR_IMPORTANT_ART_ACTIVITY
	WHERE IS_ACTIVE = 1 AND IS_DRAFT = 0
	AND DOC_ID = @ArticleID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_IMPORTANTARTACTIVITYDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_IMPORTANTARTACTIVITYDETAIL]
@UID bigint
AS
BEGIN
	
	SELECT IAA.*,
	COALESCE(IA.CREATOR_DEPT + ' - ' +DEPT.LongName, IA.CREATOR_DEPT, '') as DEPT_LONGNAME,
	COALESCE(IA.ARTICLE_NAME+' - '+PROF.NAME, IA.ARTICLE_NAME, '') AS ARTICLE_LONGNAME,
	COALESCE(IA.CUST_BRANCH +' - '+P.VALUE, IA.CUST_BRANCH, '') AS BRANCH_LONGNAME,
	COALESCE(IA.REGIS_TYPE, IA.REGIS_TYPE, '') AS TYPE_LONGNAME,
	IA.CUST_BRANCH AS ArticleBranch,
    IA.ARTICLE_NAME AS ArticleName,
    IA.REGIS_TYPE AS ArticleType
	FROM TR_IMPORTANT_ART_ACTIVITY IAA
	LEFT JOIN TR_IMPORTANT_ART IA
	ON IA.UID = IAA.DOC_ID
	LEFT JOIN MS_ARTICLE_PROFILE PROF
	ON IA.ARTICLE_NAME = PROF.ARTICLE_ID
	LEFT JOIN [dbo].[SN_BTMUDirectory_Department] DEPT
	ON IA.CREATOR_DEPT = DEPT.ShortName	 
	LEFT JOIN MS_PARAMETER P 
	ON P.[GROUP] = 'BRANCH_CODE' AND P.KEYFIELD = IA.CUST_BRANCH
	WHERE IAA.UID = @UID

END	



GO
/****** Object:  StoredProcedure [dbo].[UDPS_IMPORTANTARTDELETED]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_IMPORTANTARTDELETED]
@PAGE int = 1
,@PAGE_SIZE int = 10
,@CUSTOMER varchar(255) = ''
,@REGISTRATION_NO varchar(255) = ''
,@REFERENCE_NO varchar(255) =  ''
,@DATE_FROM date
,@DATE_TO date
,@SORT_BY varchar(255) = ''
,@USER_DEPT VARCHAR(50) = 'SKAI'
AS
BEGIN

	SELECT * FROM (
		SELECT ART.* ,
		COALESCE(DEPT.SHORTNAME + ' - ' + DEPT.LongName, Art.CREATOR_DEPT, '') as DEPT_LONGNAME,
		COALESCE(Art.ARTICLE_NAME + ' - ' + PROF.NAME, Art.ARTICLE_NAME, '') AS ARTICLE_LONGNAME,
		COALESCE(art.CUST_BRANCH + ' - ' + Mp.VALUE,'') AS BranchDesc,
		'' AS STAGING,
		CAST(0 AS BIT) AS IsCorrection,
		NULL AS CORR_UID
		FROM TR_IMPORTANT_ART Art
		LEFT JOIN MS_PARAMETER Mp 
		ON Mp.[GROUP] = 'BRANCH_CODE' 
		AND Mp.KEYFIELD = Art.CUST_BRANCH
		LEFT JOIN MS_ARTICLE_PROFILE PROF
		ON Art.ARTICLE_NAME = PROF.ARTICLE_ID
		AND PROF.DEPT_ARTICLE = Art.CREATOR_DEPT
		LEFT JOIN [dbo].[SN_BTMUDirectory_Department] DEPT
		ON Art.CREATOR_DEPT = DEPT.ShortName
		WHERE ART.IS_ACTIVE = 0
		AND (@USER_DEPT = ART.CREATOR_DEPT OR @USER_DEPT = 'SKAI')
		AND (ART.[DATE] >= @DATE_FROM OR @DATE_FROM IS NULL)
		AND (ART.[DATE] <= @DATE_TO OR @DATE_TO IS NULL)
		--AND (ART.CUSTOMER LIKE '%'+ISNULL(@CUSTOMER,'')+'%' OR ISNULL(@CUSTOMER,'') = '')
		AND (ART.REGIS_NO LIKE '%'+ISNULL(@REGISTRATION_NO,'')+'%' OR ISNULL(@REGISTRATION_NO,'') = '')
		AND (ART.REF_NO LIKE '%'+ISNULL(@REFERENCE_NO,'')+'%' OR ISNULL(@REFERENCE_NO,'') = '')

		UNION ALL 

		SELECT 
		ART.UID
		,ART.REGIS_NO
		,ART.CREATOR
		,ART.CREATOR_DEPT
		,ART.CREATOR_UNIT
		,ART.CREATOR_ROLE
		,ART.REQUEST_DT
		,ART.CUST_BRANCH
		,ART.ARTICLE_NAME
		,'Correction' AS REGIS_TYPE
		,ACT.CORR_BALANCE_IN_HAND AS BALANCE_IN_HAND
		,ACT.CORR_VALUE AS VALUE
		,ACT.CORR_DATE AS [DATE]
		,ACT.CORR_REF_NO AS REF_NO
		,ACT.CORR_REMARKS AS REMARKS
		,ART.ART_STATUS
		,ART.APPROVAL_STS
		,ART.IS_DRAFT
		,ART.IS_ACTIVE
		,ART.CREATED_BY
		,ART.CREATED_DT
		,ART.CREATED_HOST
		,ART.MODIFIED_BY
		,ART.MODIFIED_DT
		,ART.MODIFIED_HOST
		,ART.BALANCE_IN_VAULT
		,ART.ARTICLE_ID
		,ART.SP_DATE
		,ART.PL_DATE
		,ART.CUSTOMER
		,ART.PURPOSE
		,ART.L_DOC_ID
		,'' AS L_UNIV_ID
		,
		COALESCE(DEPT.SHORTNAME + ' - ' + DEPT.LongName, Art.CREATOR_DEPT, '') as DEPT_LONGNAME,
		COALESCE(Art.ARTICLE_NAME + ' - ' + PROF.NAME, Art.ARTICLE_NAME, '') AS ARTICLE_LONGNAME,
		COALESCE(art.CUST_BRANCH + ' - ' + Mp.VALUE,'') AS BranchDesc,
		'' AS STAGING,
		CAST(1 AS BIT) AS IsCorrection,
		ACT.UID AS CORR_UID
		FROM TR_IMPORTANT_ART_ACTIVITY ACT
		LEFT JOIN TR_IMPORTANT_ART ART
		ON ACT.DOC_ID = ART.UID
		LEFT JOIN MS_PARAMETER Mp 
		ON Mp.[GROUP] = 'BRANCH_CODE' 
		AND Mp.KEYFIELD = Art.CUST_BRANCH
		LEFT JOIN MS_ARTICLE_PROFILE PROF
		ON Art.ARTICLE_NAME = PROF.ARTICLE_ID
		AND PROF.DEPT_ARTICLE = Art.CREATOR_DEPT
		LEFT JOIN [dbo].[SN_BTMUDirectory_Department] DEPT
		ON Art.CREATOR_DEPT = DEPT.ShortName
		WHERE ACT.ACTIVITY_CD = 'CORRECTION'
		AND ACT.IS_ACTIVE = 0
		AND (@USER_DEPT = ACT.CREATOR_DEPT OR @USER_DEPT = 'SKAI')
		AND (ACT.[CORR_DATE] >= @DATE_FROM OR @DATE_FROM IS NULL)
		AND (ACT.[CORR_DATE] <= @DATE_TO OR @DATE_TO IS NULL)
		--AND (ART.CUSTOMER LIKE '%'+ISNULL(@CUSTOMER,'')+'%' OR ISNULL(@CUSTOMER,'') = '')
		AND (ACT.REGIS_NO LIKE '%'+ISNULL(@REGISTRATION_NO,'')+'%' OR ISNULL(@REGISTRATION_NO,'') = '')
		AND (ACT.CORR_REF_NO LIKE '%'+ISNULL(@REFERENCE_NO,'')+'%' OR ISNULL(@REFERENCE_NO,'') = '')

	) A

	ORDER BY CREATED_DT DESC
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_IMPORTANTARTDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_IMPORTANTARTDETAIL]
@UID BIGINT
AS 
BEGIN
	SELECT IA.*,
	COALESCE(DEPT.SHORTNAME + ' - ' +DEPT.LongName, IA.CREATOR_DEPT) as DEPT_LONGNAME,
	COALESCE(IA.ARTICLE_NAME +' - '+ PROF.NAME, IA.ARTICLE_NAME) AS ARTICLE_LONGNAME,
	COALESCE(IA.CUST_BRANCH + ' - ' + Mp.VALUE, IA.CUST_BRANCH) AS BranchDesc
	FROM TR_IMPORTANT_ART IA
	LEFT JOIN MS_ARTICLE_PROFILE PROF
	ON IA.ARTICLE_NAME = PROF.ARTICLE_ID
	AND PROF.DEPT_ARTICLE = IA.CREATOR_DEPT
	LEFT JOIN [dbo].[SN_BTMUDirectory_Department] DEPT
	ON IA.CREATOR_DEPT = DEPT.ShortName	 
	LEFT JOIN MS_PARAMETER Mp ON Mp.[GROUP] = 'BRANCH_CODE' AND Mp.KEYFIELD = IA.CUST_BRANCH
	WHERE IA.UID = @UID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_ISEXISTAWAITINGAPPROVAL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_ISEXISTAWAITINGAPPROVAL]
	@Id INT
AS
BEGIN
	IF EXISTS(SELECT 1 FROM TR_BOOK_RECEIVED_NOTICE WHERE UID = @Id)
		SELECT TOP 1 1 FROM TR_BOOK_RECEIVED_NOTICE WHERE DOC_ID IN (SELECT DOC_ID FROM TR_BOOK_RECEIVED_NOTICE WHERE UID = @Id) AND UID <> @Id AND IS_DRAFT = 0 AND APPROVAL_STS <> 'Approval Completed'
	ELSE
		SELECT 0
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_IsExistAwaitingApprovalIA]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_IsExistAwaitingApprovalIA]
@ArticleID INT
AS
BEGIN
	DECLARE @Res BIT = 0
	SELECT @Res = 1 FROM TR_IMPORTANT_ART_ACTIVITY
	WHERE DOC_ID = @ArticleID
	AND APPROVAL_STS IN ('Awaiting Approval', 'Revise')

	SELECT @Res
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_LETTERDESCSCHECK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_LETTERDESCSCHECK]
	-- Add the parameters for the stored procedure here
	@Id INT,
	@LETTER_TYPE VARCHAR (3000),
	@DESCS VARCHAR (3000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (
			SELECT 1
			FROM MS_LETTER_PROFILE
			WHERE 1 = 1
				AND DESCS = @DESCS
				AND IS_ACTIVE = 1
			)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_LETTERIDCHECK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_LETTERIDCHECK]
	-- Add the parameters for the stored procedure here
	--@Id INT,
	@LETTER_TYPE VARCHAR (3000),
	@DESCS VARCHAR (3000),
	@DEPT_LETTER VARCHAR (255)
AS
BEGIN
SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT LP.UID AS [UID]
		,LP.DESCS AS [DESCS]
		,LP.LETTER_TYPE AS [LETTER_TYPE]
		,LP.CREATED_BY AS [CREATED_BY]
		,LP.CREATED_DT AS [CREATED_DT]
		,LP.CREATED_HOST AS [CREATED_HOST]
		,LP.MODIFIED_BY AS [MODIFIED_BY]
		,LP.MODIFIED_DT AS [MODIFIED_DT]
		,LP.MODIFIED_HOST AS [MODIFIED_HOST]
	FROM dbo.MS_LETTER_PROFILE AS LP
	WHERE (LP.DESCS = @DESCS
	OR LP.LETTER_TYPE = @LETTER_TYPE)
	AND LP.DEPT_LETTER = @DEPT_LETTER
END
	
--	SET NOCOUNT ON;

--	IF EXISTS (
--			SELECT 1
--			FROM MS_LETTER_PROFILE
--			WHERE 1 = 1
--				AND LETTER_TYPE = @LETTER_TYPE
--				AND IS_ACTIVE = 1
--			)
--	BEGIN
--		SELECT 1
--	END
--	ELSE
--	BEGIN
--		SELECT 0
--	END
--END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_LETTERPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_LETTERPROFILE]
	-- Add the parameters for the stored procedure here
	@UserDept VARCHAR(200) = '',
	@SearchDescription NVARCHAR(100) = '',
	@SearchLetterType NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	WITH CTE_Result
	AS (
		SELECT *
		FROM MS_LETTER_PROFILE AS LP
		WHERE --LP.IS_ACTIVE = 1
		
		(ISNULL(@SearchDescription,'') = ''
			OR LP.DESCS LIKE '%' + @SearchDescription + '%'
		)
		AND(ISNULL(@SearchLetterType,'') = ''
			OR LP.LETTER_TYPE LIKE '%' + @SearchLetterType + '%'
		)AND (
				LP.DEPT_LETTER  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		
		
		ORDER BY CASE 
				WHEN @SortOrder = 'LetterType'
					THEN [LETTER_TYPE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'LetterType_desc'
					THEN [LETTER_TYPE]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Description'
					THEN [DESCS]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Description_desc'
					THEN [DESCS]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Department'
					THEN [DEPT_LETTER]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Department_desc'
					THEN [DEPT_LETTER]
				END DESC,
			
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM MS_LETTER_PROFILE AS LP
		WHERE --LP.IS_ACTIVE = 1
		
		(ISNULL(@SearchDescription,'') = ''
			OR LP.DESCS LIKE '%' + @SearchDescription + '%'
		)
		AND(ISNULL(@SearchLetterType,'') = ''
			OR LP.LETTER_TYPE LIKE '%' + @SearchLetterType + '%'
		)
		AND (
				LP.DEPT_LETTER  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		)
	SELECT TotalRows,
		LP.UID AS [UID],
		LP.[LETTER_TYPE] AS [LETTER_TYPE],
		LP.DESCS AS [DESCS],
		LP.DEPT_LETTER AS [DEPT_LETTER],
		LP.CREATED_BY AS CreatedBy,
		LP.CREATED_DT AS CreatedDate,
		LP.CREATED_HOST AS CreatedHost,
		LP.IS_ACTIVE AS [IS_ACTIVE],
		LP.MODIFIED_BY AS ModifedBy,
		LP.MODIFIED_DT AS ModifiedDate,
		LP.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_LETTER_PROFILE AS LP,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = LP.UID
			)
	OPTION (RECOMPILE)
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_LETTERPROFILEDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_LETTERPROFILEDETAIL]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT LP.UID AS [UID]
		,LP.LETTER_TYPE AS [LETTER_TYPE]
		,LP.DESCS AS [DESCS]
		,LP.DEPT_LETTER AS [DEPT_LETTER]
		,LP.IS_ACTIVE AS IS_ACTIVE
		,LP.CREATED_BY AS CREATED_BY
		,LP.CREATED_DT AS CREATED_DT
		,LP.CREATED_HOST AS CREATED_HOST
		,LP.MODIFIED_BY AS MODIFIED_BY
		,LP.MODIFIED_DT AS MODIFIED_DT
		,LP.MODIFIED_HOST AS MODIFIED_HOST
	FROM dbo.MS_LETTER_PROFILE AS LP
	WHERE LP.UID = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILTOITSO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILTOITSO]
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedure
	--DECLARE @DEPARTMENT NVARCHAR(50)
	--SET @Department = 'GAD'
	SELECT [VALUE] AS EmailAdmin
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_ADMIN'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_OUTGOINGLETTER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- =============================================
-- Author:		<Robby>
-- Create date: <20211220>
-- Description:	<UDPS_OUTGOINGLETTER With Param>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_OUTGOINGLETTER]

	@UserDept VARCHAR(100) = '',
	@SearchByString NVARCHAR(100) = '',
	@AttentionString NVARCHAR(2000) = '',
	@AuthorString NVARCHAR(2000) = '',
	@TypeString NVARCHAR(2000) = '',
	@LocationString NVARCHAR(2000) = '',
	@CreatedDateFrom DATETIME = NULL,
	@CreatedDateTo DATETIME = NULL,
	@RegistrationNoString NVARCHAR(100) = '',
	@SubmitDateFrom DATETIME = NULL,
	@SubmitDateTo DATETIME = NULL,
	@RequestDateFrom DATETIME = NULL,
	@RequestDateTo DATETIME = NULL,
	@SubmitMethodString NVARCHAR(2000) = '',
	@SubmitNameString NVARCHAR(2000) = '',
	@FullSearchString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = '',
	@ViewByString NVARCHAR(100) = ''
AS
BEGIN
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		-- Insert statements for procedure here
		SELECT OL.UID AS Id,
		CASE  
			WHEN OL.APPROVAL_STS = 'Reject' THEN 'Reject'
			WHEN OL.APPROVAL_STS = 'Revise' THEN 'Revise'
			WHEN DH.WFSTEP IS NULL THEN 'Submitted By Creator' 
			WHEN DH.WFSTEP = 0 THEN 'Revise'
			WHEN DH.WFSTEP = 1 THEN 'Approval UH / Checker'
			WHEN DH.WFSTEP = 2 Then 'Approval DH / UH' 

		END AS Staging
		FROM dbo.TR_OUTGOING_LETTER AS OL
		LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = OL.BRANCH_CD
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = OL.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON OL.CUST_CD = EC.CustomerCode
		INNER JOIN TR_DOCUMENT_HISTORY DH
		ON OL.UID = DH.REF_ID
		INNER JOIN ( 
				SELECT MAX(UID) UID, REF_ID 
				FROM TR_DOCUMENT_HISTORY 
				WHERE HIST_TYPE = 'OUT_LETTER'
				GROUP BY REF_ID ) LASTDH
		ON DH.UID = LASTDH.UID
		WHERE OL.IS_ACTIVE = 1 AND OL.IS_DRAFT = 0
			AND (
				@UserDept = OL.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)
			--AND (
			--		LETTER_STS LIKE '%' + @ViewByString + '%'
			--	)
			AND (
				(
					CASE 
						WHEN ISNULL(@ViewByString, '') != ''
							AND @ViewByString != 'ALL'
							THEN OL.LETTER_STS
						END
					) LIKE '%' + @ViewByString + '%'
				OR (@ViewByString = 'ALL')
				)
			AND (
					(
					   (
						CASE 
							WHEN @SearchByString = 'Attention'
								AND ISNULL(@AttentionString,'') <> ''
								THEN OL.ATTENTION
							END
						) LIKE '%' + @AttentionString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Author'
								AND ISNULL(@AuthorString,'') <> ''
								THEN OL.CREATOR
							END
						) LIKE '%' + @AuthorString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Created Date'
								AND @CreatedDateFrom IS NOT NULL
								AND @CreatedDateTo IS NOT NULL
								THEN OL.LET_CREATED_DT
							END
						)  BETWEEN @CreatedDateFrom
						AND @CreatedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Letter Type'
								AND ISNULL(@TypeString,'') <> ''
								THEN OL.LETTER_TYPE
							END
						) LIKE '%' + @TypeString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Location'
								AND ISNULL(@LocationString,'') <> ''
								THEN OL.LOCATION
							END
						) LIKE '%'+ @LocationString +'%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Registration Number'
								AND ISNULL(@RegistrationNoString,'') <> ''
								THEN Ol.REGIS_NO
							END
						) LIKE '%' + @RegistrationNoString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Request Date'
								AND @RequestDateFrom IS NOT NULL
								AND @RequestDateTo IS NOT NULL
								THEN OL.REQUEST_DT
							END
						) BETWEEN @RequestDateFrom
						AND @RequestDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Submit Date'
								AND @SubmitDateFrom IS NOT NULL
								AND @SubmitDateTo IS NOT NULL
								THEN OL.SUBMIT_DT
							END
						) BETWEEN @SubmitDateFrom
						AND @SubmitDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Submit Method'
								AND ISNULL(@SubmitMethodString,'') <> ''
								AND @SubmitMethodString IS NOT NULL
								THEN OL.SUBMIT_METHOD
							END
						) IN(
							SELECT Name
						FROM dbo.splitstring(@SubmitMethodString, ',')
						)
					)
				OR(
					(
						CASE
							WHEN @SearchByString = 'Submit Name'
								AND ISNULL(@SubmitNameString,'') <> ''
								AND @SubmitNameString IS NOT NULL
								THEN OL.SUBMIT_BY
						END
					) LIKE '%' +@SubmitNameString + '%'	
					)
				OR (
					(
						CASE 
							WHEN ISNULL(@SearchByString,'') <> ''
								AND ISNULL(@AttentionString,'') = ''
								AND ISNULL(@AuthorString,'') = ''
								AND @CreatedDateFrom IS NULL
								AND @CreatedDateTo IS NULL
								AND ISNULL(@TypeString, '') = ''
								AND ISNULL(@LocationString,'') = ''
								AND ISNULL(@RegistrationNoString, '') = ''
								AND @RequestDateFrom IS NULL
								AND @RequestDateTo IS NULL
								AND @SubmitDateFrom IS NULL
								AND @SubmitDateTo IS NULL
								AND ISNULL(@SubmitMethodString,'') = ''
								AND ISNULL(@SubmitNameString,'') = ''

								THEN 1
							END
						) = 1
					)
				OR ISNULL(@SearchByString,'') = ''
				)
			AND (
				ISNULL(@FullSearchString,'') = ''
				OR OL.BRANCH_CD LIKE '%' + @FullSearchString+ '%'
				OR OL.CREATOR_DEPT LIKE '%' + @FullSearchString + '%'
				OR OL.REGIS_NO LIKE '%' + @FullSearchString + '%'
				OR OL.CREATOR LIKE '%' + @FullSearchString + '%'
				OR OL.REQUEST_DT LIKE '%' + @FullSearchString + '%'
				OR OL.LET_CREATED_DT LIKE '%' + @FullSearchString + '%'
				OR OL.CUST_CD LIKE '%' +@FullSearchString+ '%'
				OR EC.LegalName LIKE '%' +@FullSearchString+ '%'
				OR OL.SUBJECT LIKE '%' + @FullSearchString+ '%'
				OR OL.ATTENTION LIKE '%' + @FullSearchString+ '%'
				OR OL.LOCATION LIKE '%' + @FullSearchString+ '%'
				OR OL.SUBMIT_BY LIKE '%' + @FullSearchString + '%'
				OR OL.SUBMIT_DT LIKE '%' + @FullSearchString + '%'
				OR OL.SUBMIT_METHOD LIKE '%' + @FullSearchString + '%'
				OR OL.LETTER_STS LIKE '%' + @FullSearchString + '%'
				OR CASE 
					WHEN OL.APPROVAL_STS = 'Reject' THEN 'Reject'
					WHEN OL.APPROVAL_STS = 'Revise' THEN 'Revise'
					WHEN DH.WFSTEP IS NULL THEN 'Submitted By Creator'
					WHEN DH.WFSTEP = 0 THEN 'Revise'
					WHEN DH.WFSTEP = 1 THEN 'Approval UH / Checker'
					WHEN DH.WFSTEP = 2 Then 'Approval DH / UH' 
					END LIKE '%' + @FullSearchString + '%'
				)
		),
	CTE_TotalRows
	AS (
		SELECT COUNT(OL.UID) AS TotalRows
		FROM dbo.TR_OUTGOING_LETTER AS OL
		LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = OL.BRANCH_CD
			LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = OL.CREATOR_DEPT
			LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON OL.CUST_CD = EC.CustomerCode
		INNER JOIN TR_DOCUMENT_HISTORY DH
		ON OL.UID = DH.REF_ID
		INNER JOIN ( 
				SELECT MAX(UID) UID, REF_ID 
				FROM TR_DOCUMENT_HISTORY 
				WHERE HIST_TYPE = 'OUT_LETTER'
				GROUP BY REF_ID ) LASTDH
		ON DH.UID = LASTDH.UID
		WHERE OL.IS_ACTIVE = 1 AND OL.IS_DRAFT = 0
			AND (
				@UserDept = OL.CREATOR_DEPT
				OR @UserDept = 'SKAI'
				)			
			--AND(
			--	(CONCAT([BRANCH_CD],[CREATOR_DEPT],[REGIS_NO],[CREATOR],CAST([REQUEST_DT] AS VARCHAR),[LETTER_TYPE],
			--		CAST([LET_CREATED_DT] AS VARCHAR),[SUBJECT],[ATTENTION],[LOCATION],CAST([SUBMIT_DT] AS VARCHAR),[SUBMIT_BY],[SUBMIT_METHOD],[LETTER_STS],[APPROVAL_STS] )
			--		) LIKE '%'+@FullSearchString+'%' OR ISNULL(@FullSearchString,'') = '' )
			AND (
				(
					CASE 
						WHEN ISNULL(@ViewByString, '') != ''
							AND @ViewByString != 'ALL'
							THEN OL.LETTER_STS
						END
					) LIKE '%' + @ViewByString + '%'
				OR (@ViewByString = 'ALL')
				)
			AND (
				(
					(
						CASE 
							WHEN @SearchByString = 'Attention'
								AND ISNULL(@AttentionString,'') <> ''
								AND @AttentionString IS NOT NULL
								THEN OL.ATTENTION
							END
						) LIKE '%' + @AttentionString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Author'
								AND ISNULL(@AuthorString,'') <> ''
								AND @AuthorString IS NOT NULL
								THEN OL.CREATOR
							END
						) LIKE '%' + @AuthorString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Created Date'
								AND @CreatedDateFrom IS NOT NULL
								AND @CreatedDateTo IS NOT NULL
								THEN OL.LET_CREATED_DT
							END
						)  BETWEEN @CreatedDateFrom
						AND @CreatedDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Letter Type'
								AND ISNULL(@TypeString,'') <> ''
								THEN OL.LETTER_TYPE
							END
						) LIKE '%' + @TypeString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Location'
								AND ISNULL(@LocationString,'') <> ''
								THEN OL.LOCATION
							END
						) LIKE '%'+ @LocationString +'%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Registration Number'
								AND ISNULL(@RegistrationNoString,'') <> ''
								THEN Ol.REGIS_NO
							END
						) LIKE '%' + @RegistrationNoString + '%'
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Request Date'
								AND @RequestDateFrom IS NOT NULL
								AND @RequestDateTo IS NOT NULL
								THEN OL.REQUEST_DT
							END
						) BETWEEN @RequestDateFrom
						AND @RequestDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Submit Date'
								AND @SubmitDateFrom IS NOT NULL
								AND @SubmitDateTo IS NOT NULL
								THEN OL.SUBMIT_DT
							END
						) BETWEEN @SubmitDateFrom
						AND @SubmitDateTo
					)
				OR (
					(
						CASE 
							WHEN @SearchByString = 'Submit Method'
								AND ISNULL(@SubmitMethodString,'') <> ''
								AND @SubmitMethodString IS NOT NULL
								THEN OL.SUBMIT_METHOD
							END
						) IN(
							SELECT Name
						FROM dbo.splitstring(@SubmitMethodString, ',')
						)
					)
				OR(
					(
						CASE
							WHEN @SearchByString = 'Submit Name'
								AND ISNULL(@SubmitNameString,'') <> ''
								AND @SubmitNameString IS NOT NULL
								THEN OL.SUBMIT_BY
						END
					) LIKE '%' +@SubmitNameString + '%'	
					)
				OR (
					(
						CASE 
							WHEN ISNULL(@SearchByString,'') <> ''
								AND ISNULL(@AttentionString,'') = ''
								AND ISNULL(@AuthorString,'') = ''
								AND @CreatedDateFrom IS NULL
								AND @CreatedDateTo IS NULL
								AND ISNULL(@TypeString, '') = ''
								AND ISNULL(@LocationString,'') = ''
								AND ISNULL(@RegistrationNoString, '') = ''
								AND @RequestDateFrom IS NULL
								AND @RequestDateTo IS NULL
								AND @SubmitDateFrom IS NULL
								AND @SubmitDateTo IS NULL
								AND ISNULL(@SubmitMethodString,'') = ''
								AND ISNULL(@SubmitNameString,'') = ''

								THEN 1
							END
						) = 1
					)
				OR ISNULL(@SearchByString,'') = ''
				)
			AND (
				ISNULL(@FullSearchString,'') = ''
				OR OL.BRANCH_CD LIKE '%' + @FullSearchString+ '%'
				OR OL.CREATOR_DEPT LIKE '%' + @FullSearchString + '%'
				OR OL.REGIS_NO LIKE '%' + @FullSearchString + '%'
				OR OL.CREATOR LIKE '%' + @FullSearchString + '%'
				OR OL.REQUEST_DT LIKE '%' + @FullSearchString + '%'
				OR OL.LET_CREATED_DT LIKE '%' + @FullSearchString + '%'
				OR OL.CUST_CD LIKE '%' +@FullSearchString+ '%'
				OR EC.LegalName LIKE '%' +@FullSearchString+ '%'
				OR OL.SUBJECT LIKE '%' + @FullSearchString+ '%'
				OR OL.ATTENTION LIKE '%' + @FullSearchString+ '%'
				OR OL.LOCATION LIKE '%' + @FullSearchString+ '%'
				OR OL.SUBMIT_BY LIKE '%' + @FullSearchString + '%'
				OR OL.SUBMIT_DT LIKE '%' + @FullSearchString + '%'
				OR OL.SUBMIT_METHOD LIKE '%' + @FullSearchString + '%'
				OR OL.LETTER_STS LIKE '%' + @FullSearchString + '%'
				OR CASE 
					WHEN OL.APPROVAL_STS = 'Reject' THEN 'Reject'
					WHEN OL.APPROVAL_STS = 'Revise' THEN 'Revise'
					WHEN DH.WFSTEP IS NULL THEN 'Submitted By Creator'
					WHEN DH.WFSTEP = 0 THEN 'Revise'
					WHEN DH.WFSTEP = 1 THEN 'Approval UH / Checker'
					WHEN DH.WFSTEP = 2 Then 'Approval DH / UH' 
					END LIKE '%' + @FullSearchString + '%'
				)
		)
	SELECT TotalRows,
		OL.UID AS Id,
		isnull(OL.REGIS_NO, '') AS RegistrationNo,
		isnull(OL.CREATOR, '') AS Creator,
		isnull(OL.CREATOR_DEPT, '') AS CreatorDepartment,
		ISNULL(OL.CREATOR_DEPT,'')+ isnull('-' + dept.LONGNAME,'') AS CreatorDepartmentDesc,
		isnull(OL.CREATOR_ROLE, '') AS CreatorRole,
		OL.REQUEST_DT AS RequestDate,
		isnull(OL.BRANCH_CD, '') AS BranchCode,
		isnull(OL.BRANCH_CD,'') + ' - ' + isnull(BR.VALUE,'') AS BranchCodeDesc,
		OL.LET_CREATED_DT AS LetCreatedDate,
		isnull(OL.LETTER_TYPE, '') AS LetterType,
		EC.LegalName AS CustomerCodeDesc,
		isnull(OL.SUBJECT,'') AS Subject,
		isnull(OL.ATTENTION,'') AS Attention,
		isnull(OL.LOCATION,'') AS Location,
		--isnull(OL.SUBMIT_DT,'') AS SubmitDate,
		OL.SUBMIT_DT AS SubmitDate,
		isnull(OL.SUBMIT_BY,'') AS SubmitBy,
		isnull(OL.SUBMIT_METHOD,'') AS SubmitMethod,
		isnull(OL.LETTER_STS,'') AS LetterStatus,
		isnull(OL.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(OL.IS_ACTIVE, '') AS IsActive,
		isnull(OL.CREATED_BY, '') AS CreatedBy,
		OL.CREATED_DT AS CreatedDate,
		isnull(OL.CREATED_HOST, '') AS CreatedHost,
		isnull(OL.MODIFIED_BY, '') AS ModifiedBy,
		OL.MODIFIED_DT AS ModifiedDate,
		isnull(OL.MODIFIED_HOST, '') AS ModifiedHost,
		CASE 
			WHEN OL.APPROVAL_STS = 'Reject' THEN 'Reject'
			WHEN OL.APPROVAL_STS = 'Revise' THEN 'Revise' 
			WHEN DH.WFSTEP IS NULL THEN 'Submitted By Creator' 
			WHEN DH.WFSTEP = 0 THEN 'Revise'
			WHEN DH.WFSTEP = 1 THEN 'Approval UH / Checker'
			WHEN DH.WFSTEP = 2 Then 'Approval DH / UH' 
		END AS Staging
	FROM dbo.TR_OUTGOING_LETTER AS OL
	LEFT JOIN MS_PARAMETER BR WITH (NOLOCK) ON BR.[GROUP] = 'BRANCH_CODE' AND BR.KEYFIELD = OL.BRANCH_CD
	LEFT JOIN dbo.SN_BTMUDirectory_Department dept WITH (NOLOCK) ON dept.shortname = OL.CREATOR_DEPT
	LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON OL.CUST_CD = EC.CustomerCode
	INNER JOIN TR_DOCUMENT_HISTORY DH
	ON OL.UID = DH.REF_ID
	INNER JOIN ( 
			SELECT MAX(UID) UID, REF_ID 
			FROM TR_DOCUMENT_HISTORY 
			WHERE HIST_TYPE = 'OUT_LETTER'
			GROUP BY REF_ID ) LASTDH
	ON DH.UID = LASTDH.UID
		,CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = OL.UID
			)
			ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN OL.BRANCH_CD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN OL.BRANCH_CD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN OL.CREATOR_DEPT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN OL.CREATOR_DEPT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN OL.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN OL.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN OL.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN OL.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN OL.REQUEST_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN OL.REQUEST_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Letter')
					THEN OL.LETTER_TYPE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Letter_desc')
					THEN OL.LETTER_TYPE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'CreatedDate')
					THEN OL.LET_CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'CreatedDate_desc')
					THEN OL.LET_CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN OL.CUST_CD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN OL.CUST_CD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Subject')
					THEN OL.SUBJECT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Subject_desc')
					THEN OL.SUBJECT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Attention')
					THEN OL.ATTENTION
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Attention_desc')
					THEN OL.ATTENTION
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Location')
					THEN OL.LOCATION
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Location_desc')
					THEN OL.LOCATION
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Date')
					THEN OL.SUBMIT_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Date_desc')
					THEN OL.SUBMIT_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'By')
					THEN OL.SUBMIT_BY
				END ASC,
			CASE 
				WHEN (@SortOrder = 'By_desc')
					THEN OL.SUBMIT_BY
				END DESC,
			CASE
				WHEN (@SortOrder = 'Method')
					THEN OL.SUBMIT_METHOD
				END ASC,
			CASE
				WHEN (@SortOrder = 'Method_Desc')
					THEN OL.SUBMIT_METHOD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Staging')
					THEN OL.LETTER_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Staging_desc')
					THEN OL.LETTER_STS
				END DESC,
				CASE 
				WHEN (@SortOrder = 'Status')
					THEN OL.APPROVAL_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Status_desc')
					THEN OL.APPROVAL_STS
				END DESC,
			CASE 
				WHEN (@SortOrder = '')
					THEN OL.UID 
				END DESC, 
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN OL.UID
				END DESC OFFSET @PageSize * (
				CASE 
					WHEN ISNULL(@PageNo, 0) = 0
						THEN 0
					ELSE (@PageNo - 1)
					END
				) ROWS FETCH NEXT @PageSize ROWS ONLY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_OUTGOINGLETTERBANKREF]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_OUTGOINGLETTERBANKREF]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
		--DECLARE @Id INT = 61774;

	DECLARE @CUST_CD VARCHAR(255)
		,@CUST_ACC_LIST VARCHAR(2000)
		,@CUST_ACC_LIST_DESC VARCHAR(2000)
		,@CUST_CCY_ALL VARCHAR(500)
		,@CUST_NPWP VARCHAR(2000)
        ,@CUST_ACC_CCY VARCHAR(2000)

	SELECT @CUST_CD = CUST_CD, @CUST_ACC_LIST = CUST_ACC_NO FROM TR_OUTGOING_LETTER WHERE UID = @Id

		SELECT 
	@CUST_NPWP = COALESCE(@CUST_NPWP + ',' + DD.documentno, DD.documentno)
	FROM VW_DOCUMENT_DETAIL DD  
	WHERE DD.DocumentType = 'NPWP (Tax Payer Number)' AND CustomerCode = @CUST_CD

	SELECT 
	@CUST_ACC_LIST_DESC = COALESCE(@CUST_ACC_LIST_DESC + ',' + ECA.CombineCode, ECA.CombineCode),
	@CUST_CCY_ALL = COALESCE(@CUST_CCY_ALL + ',' + ECA.AccountCCY, ECA.AccountCCY),
    @CUST_ACC_CCY = COALESCE(@CUST_ACC_CCY + ',' + concat(ECA.AccountCCY, ' ', ECA.AccountNo), concat(ECA.AccountCCY, ' ', ECA.AccountNo))
	FROM VW_EASYACCOUNTNO ECA  
	WHERE ECA.CustomerCode = @CUST_CD
	AND @CUST_ACC_LIST LIKE '%'+ECA.AccountNo+'%'


	SELECT OL.UID AS Id,
		OL.CREATOR_DEPT AS CreatorDepartment,
		OL.REGIS_NO AS RegistrationNo,
		OL.REQUEST_DT AS RequestDate,
		EC.LegalName AS CustomerCodeDesc,
		OL.CUST_ACC_NO AS CustAccNo,
		OL.BRANCH_CD AS BranchCode,
		@CUST_ACC_LIST_DESC AS CustAccNoDesc,
		OL.[SUBJECT] AS Subject,
		OL.SUBJECT_ENG AS SubjectEng,
		OL.[ATTENTION] AS Attention,
		OL.[ADDRESS] AS Address,
		@CUST_CCY_ALL AS AccountCcy,
		EC.BankName AS BankName,
		OL.VIRTUAL_ACC AS VirtualAccount,
		OL.LET_CREATED_DT AS LetCreatedDate,
		OL.CREATED_DT AS CreatedDate,
		@CUST_NPWP AS Npwp,
		OL.BRANCH_CD + ' - ' +EC.CIF AS Cif,
		--PRM.VALUE AS Name,
		USR.REALNAME AS Name,
		DEPT.LONGNAME AS Jabatan,
        @CUST_ACC_CCY AS CustAccountCcy
	FROM TR_OUTGOING_LETTER AS OL
	LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON OL.CUST_CD = EC.CustomerCode
	LEFT JOIN [dbo].[SN_BTMUDirectory_Department] DEPT ON OL.CREATOR_DEPT = DEPT.SHORTNAME
	--LEFT JOIN MS_PARAMETER PRM ON PRM.[GROUP] = 'OUTGOING_DH' AND KEYFIELD = OL.CREATOR_DEPT
	LEFT JOIN [dbo].[SN_BTMUDirectory_Role_Parent] PRN ON DEPT.ID = PRN.DEPARTMENTID
	LEFT JOIN [dbo].[SN_BTMUDirectory_Role] RL ON RL.ID = PRN.ROLEID
	LEFT JOIN [dbo].[SN_BTMUDirectory_User] USR ON USR.ROLEPARENTID = PRN.ID and usr.isdeleted = 0
	WHERE OL.UID = @Id AND RL.SHORTNAME = 'DH' --ORDER BY USR.USERNAME DESC

END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_OUTGOINGLETTERDATA]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_OUTGOINGLETTERDATA]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID,
		REGIS_NO,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		ON_BEHALF_OF,
		BRANCH_CD,
		LETTER_TYPE,
		CUST_CD,
		CUST_ACC_NO,
		LET_CREATED_DT,
		SUBJECT,ATTENTION
		LOCATION,
		ADDRESS,
		REMARKS,
		SUBMIT_DT,
		SUBMIT_BY,
		SUBMIT_METHOD,
		LETTER_STS,
		APPROVAL_STS,
		IS_DRAFT,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST,
		DEST_DT,
		DEST_BY,
		DEST_WITNESS,
		SUBJECT_ENG,
		VIRTUAL_ACC,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_OUTGOING_LETTER
	WHERE 1 = 1
		AND [UID] = @RefID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_OUTGOINGLETTERDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_OUTGOINGLETTERDETAIL]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT OL.UID AS Id,
		OL.REGIS_NO AS RegistrationNo,
		OL.CREATOR AS Creator,
		OL.CREATOR_DEPT + ' - ' + DEPT.LONGNAME AS CreatorDepartmentDesc ,
		OL.CREATOR_DEPT AS CreatorDepartment,
		OL.CREATOR_UNIT AS CreatorUnit,
		OL.CREATOR_ROLE AS CreatorRole,
		OL.REQUEST_DT AS RequestDate,
		OL.ON_BEHALF_OF AS OnBehalfOf,
		OL.BRANCH_CD AS BranchCode,
		OL.LETTER_TYPE AS LetterType,
		OL.CUST_CD AS CustCode,
		EC.LegalName AS CustomerCodeDesc,
		OL.CUST_ACC_NO AS CustAccNo,
		OL.CUST_ACC_NO AS SelectedAccountNo,
		ECA.CombineCode AS CustAccNoDesc,
		ECA.AccountCCY AS AccountCcy,
		EC.BankName AS BankName,
		OL.VIRTUAL_ACC AS VirtualAccount,
		OL.LET_CREATED_DT AS LetCreatedDate,
		OL.[SUBJECT] AS Subject,
		OL.SUBJECT_ENG AS SubjectEng,
		OL.ATTENTION AS Attention,
		OL.LOCATION AS Location,
		OL.[ADDRESS] AS Address,
		OL.REMARKS AS Remarks,
		OL.SUBMIT_DT AS SubmitDate,
		OL.SUBMIT_BY AS SubmitBy,
		OL.SUBMIT_METHOD AS SubmitMethod,
		OL.LETTER_STS AS LetterStatus,
		OL.APPROVAL_STS AS ApprovalStatus,
		OL.IS_DRAFT AS IsPendingEdit,
		OL.IS_ACTIVE AS IsActive,
		OL.CREATED_BY AS CreatedBy,
		OL.CREATED_DT AS CreatedDate,
		OL.CREATED_HOST AS CreatedHost,
		OL.MODIFIED_BY AS ModifiedBy,
		OL.MODIFIED_DT AS ModifiedDate,
		OL.MODIFIED_HOST AS ModifiedHost,
		OL.DEST_DT AS DestroyedDate,
		OL.DEST_BY AS DestroyedBy,
		OL.DEST_WITNESS AS DestroyedWitness
	FROM TR_OUTGOING_LETTER AS OL
	LEFT JOIN [dbo].[SN_BTMUDirectory_Department] DEPT ON DEPT.SHORTNAME = OL.CREATOR_DEPT 
	LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON OL.CUST_CD = EC.CustomerCode
	LEFT JOIN VW_EASYACCOUNTNO ECA  ON OL.CUST_CD = ECA.CustomerCode
	WHERE OL.UID = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_OUTGOINGLETTEREXPORT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_OUTGOINGLETTEREXPORT]
	-- Add the parameters for the stored procedure here
	@UserDept VARCHAR(100) = '',
	@SearchByString NVARCHAR(100) = '',
	@AttentionString NVARCHAR(2000) = '',
	@AuthorString NVARCHAR(2000) = '',
	@TypeString NVARCHAR(2000) = '',
	@LocationString NVARCHAR(2000) = '',
	@CreatedDateFrom DATETIME = NULL,
	@CreatedDateTo DATETIME = NULL,
	@RegistrationNoString NVARCHAR(100) = '',
	@SubmitDateFrom DATETIME = NULL,
	@SubmitDateTo DATETIME = NULL,
	@RequestDateFrom DATETIME = NULL,
	@RequestDateTo DATETIME = NULL,
	@SubmitMethodString NVARCHAR(2000) = '',
	@SubmitNameString NVARCHAR(2000) = '',
	@FullSearchString NVARCHAR(100) = '',
	@SortOrder NVARCHAR(100) = '',
	@ViewByString NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT OL.UID AS Id,
		isnull(OL.REGIS_NO, '') AS RegistrationNo,
		isnull(OL.CREATOR, '') AS Creator,
		isnull(OL.CREATOR_DEPT, '') AS CreatorDepartment,
		COALESCE(OL.CREATOR_DEPT,'') + ' - ' + ISNULL(DEPT.LONGNAME,'') AS CreatorDepartmentDesc,
		isnull(OL.CREATOR_ROLE, '') AS CreatorRole,
		OL.REQUEST_DT AS RequestDate,
		isnull(OL.BRANCH_CD, '') AS BranchCode,
		COALESCE(OL.BRANCH_CD,'') + ' - ' + ISNULL(MP.VALUE,'') AS BranchCodeDesc,
		OL.LET_CREATED_DT AS LetCreatedDate,
		isnull(OL.LETTER_TYPE, '') AS LetterType,
		EC.LegalName AS CustomerCodeDesc,
		isnull(OL.SUBJECT,'') AS Subject,
		isnull(OL.ATTENTION,'') AS Attention,
		isnull(OL.LOCATION,'') AS Location,
		isnull(OL.SUBMIT_DT,'') AS SubmitDate,
		OL.SUBMIT_DT AS SubmitDate,
		isnull(OL.SUBMIT_BY,'') AS SubmitBy,
		isnull(OL.SUBMIT_METHOD,'') AS SubmitMethod,
		isnull(OL.LETTER_STS,'') AS LetterStatus,
		isnull(OL.APPROVAL_STS, '') AS ApprovalStatus,
		isnull(OL.IS_ACTIVE, '') AS IsActive,
		isnull(OL.CREATED_BY, '') AS CreatedBy,
		OL.CREATED_DT AS CreatedDate,
		isnull(OL.CREATED_HOST, '') AS CreatedHost,
		isnull(OL.MODIFIED_BY, '') AS ModifiedBy,
		OL.MODIFIED_DT AS ModifiedDate,
		isnull(OL.MODIFIED_HOST, '') AS ModifiedHost,
		CASE  
			WHEN DH.WFSTEP IS NULL THEN 'Approval UH' 
			WHEN DH.WFSTEP = 0 THEN 'Ask for Revise'
			WHEN DH.WFSTEP = 1 THEN 'Approval DH'
			WHEN DH.WFSTEP = 2 Then 'Approved' 
		END AS Staging
	FROM dbo.TR_OUTGOING_LETTER AS OL
	LEFT JOIN MS_PARAMETER MP ON MP.[GROUP] = 'BRANCH_CODE' AND MP.KEYFIELD = OL.BRANCH_CD
	LEFT JOIN SN_BTMUDIRECTORY_DEPARTMENT DEPT ON DEPT.SHORTNAME = OL.CREATOR_DEPT
	LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON OL.CUST_CD = EC.CustomerCode
	INNER JOIN TR_DOCUMENT_HISTORY DH
	ON OL.UID = DH.REF_ID
	INNER JOIN ( 
			SELECT MAX(UID) UID, REF_ID 
			FROM TR_DOCUMENT_HISTORY 
			WHERE HIST_TYPE = 'OUT_LETTER'
			GROUP BY REF_ID ) LASTDH
	ON DH.UID = LASTDH.UID
	WHERE OL.IS_ACTIVE = 1 AND IS_DRAFT = 0
	AND (
				@UserDept = OL.CREATOR_DEPT
				OR @UserDept = 'SKAI'
		)
	AND (
				(
					CASE 
						WHEN ISNULL(@ViewByString, '') != ''
							AND @ViewByString != 'ALL'
							THEN OL.LETTER_STS
						END
					) LIKE '%' + @ViewByString + '%'
				OR (@ViewByString = 'ALL')
				)
		AND (
				(
					(
					CASE 
						WHEN @SearchByString = 'Attention'
							AND ISNULL(@AttentionString,'') <> ''
							THEN OL.ATTENTION
						END
					) LIKE '%' + @AttentionString + '%'
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Author'
							AND ISNULL(@AuthorString,'') <> ''
							THEN OL.CREATOR
						END
					) LIKE '%' + @AuthorString + '%'
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Created Date'
							AND @CreatedDateFrom IS NOT NULL
							AND @CreatedDateTo IS NOT NULL
							THEN OL.LET_CREATED_DT
						END
					)  BETWEEN @CreatedDateFrom
					AND @CreatedDateTo
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Letter Type'
							AND ISNULL(@TypeString,'') <> ''
							THEN OL.LETTER_TYPE
						END
					) LIKE '%' + @TypeString + '%'
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Location'
							AND ISNULL(@LocationString,'') <> ''
							THEN OL.LOCATION
						END
					) LIKE '%'+ @LocationString +'%'
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Registration Number'
							AND ISNULL(@RegistrationNoString,'') <> ''
							THEN Ol.REGIS_NO
						END
					) LIKE '%' + @RegistrationNoString + '%'
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Request Date'
							AND @RequestDateFrom IS NOT NULL
							AND @RequestDateTo IS NOT NULL
							THEN OL.REQUEST_DT
						END
					) BETWEEN @RequestDateFrom
					AND @RequestDateTo
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Submit Date'
							AND @SubmitDateFrom IS NOT NULL
							AND @SubmitDateTo IS NOT NULL
							THEN OL.SUBMIT_DT
						END
					) BETWEEN @SubmitDateFrom
					AND @SubmitDateTo
				)
			OR (
				(
					CASE 
						WHEN @SearchByString = 'Submit Method'
							AND ISNULL(@SubmitMethodString,'') <> ''
							AND @SubmitMethodString IS NOT NULL
							THEN OL.SUBMIT_METHOD
						END
					)IN(
						SELECT Name
					FROM dbo.splitstring(@SubmitMethodString, ',')
					)
				)
			OR(
				(
					CASE
						WHEN @SearchByString = 'Submit Name'
							AND ISNULL(@SubmitNameString,'') <> ''
							AND @SubmitNameString IS NOT NULL
							THEN OL.SUBMIT_BY
					END
				) LIKE '%' +@SubmitNameString + '%'	
				)
			OR (
				(
					CASE 
						WHEN ISNULL(@SearchByString,'') <> ''
							AND ISNULL(@AttentionString,'') = ''
							AND ISNULL(@AuthorString,'') = ''
							AND @CreatedDateFrom IS NULL
							AND @CreatedDateTo IS NULL
							AND ISNULL(@TypeString, '') = ''
							AND ISNULL(@LocationString,'') = ''
							AND ISNULL(@RegistrationNoString, '') = ''
							AND @RequestDateFrom IS NULL
							AND @RequestDateTo IS NULL
							AND @SubmitDateFrom IS NULL
							AND @SubmitDateTo IS NULL
							AND ISNULL(@SubmitMethodString,'') = ''
							AND ISNULL(@SubmitNameString,'') = ''

							THEN 1
						END
					) = 1
				)
			OR ISNULL(@SearchByString,'') = ''
			)
		AND (
			ISNULL(@FullSearchString,'') = ''
			OR OL.BRANCH_CD LIKE '%' + @FullSearchString+ '%'
			OR OL.CREATOR_DEPT LIKE '%' + @FullSearchString + '%'
			OR OL.REGIS_NO LIKE '%' + @FullSearchString + '%'
			OR OL.CREATOR LIKE '%' + @FullSearchString + '%'
			OR OL.REQUEST_DT LIKE '%' + @FullSearchString + '%'
			OR OL.LET_CREATED_DT LIKE '%' + @FullSearchString + '%'
			OR OL.CUST_CD LIKE '%' +@FullSearchString+ '%'
			OR EC.LegalName LIKE '%' +@FullSearchString+ '%'
			OR OL.SUBJECT LIKE '%' + @FullSearchString+ '%'
			OR OL.ATTENTION LIKE '%' + @FullSearchString+ '%'
			OR OL.LOCATION LIKE '%' + @FullSearchString+ '%'
			OR OL.SUBMIT_BY LIKE '%' + @FullSearchString + '%'
			OR OL.SUBMIT_DT LIKE '%' + @FullSearchString + '%'
			OR OL.SUBMIT_METHOD LIKE '%' + @FullSearchString + '%'
			OR OL.LETTER_STS LIKE '%' + @FullSearchString + '%'
			OR CASE 
				WHEN DH.WFSTEP IS NULL THEN 'Approval UH'
				WHEN DH.WFSTEP = 0 THEN 'Ask for Revise'
				WHEN DH.WFSTEP = 1 THEN 'Approval DH'
				WHEN DH.WFSTEP = 2 THEN 'Approved'
				END LIKE '%' + @FullSearchString + '%'
			)
			ORDER BY CASE 
				WHEN (@SortOrder = 'Branch')
					THEN OL.BRANCH_CD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Branch_desc')
					THEN OL.BRANCH_CD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Dept')
					THEN OL.CREATOR_DEPT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Dept_desc')
					THEN OL.CREATOR_DEPT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo')
					THEN OL.REGIS_NO
				END ASC,
			CASE 
				WHEN (@SortOrder = 'RegistrationNo_desc')
					THEN OL.REGIS_NO
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Creator')
					THEN OL.CREATOR
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Creator_desc')
					THEN OL.CREATOR
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Created')
					THEN OL.REQUEST_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Created_desc')
					THEN OL.REQUEST_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Letter')
					THEN OL.LETTER_TYPE
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Letter_desc')
					THEN OL.LETTER_TYPE
				END DESC,
			CASE 
				WHEN (@SortOrder = 'CreatedDate')
					THEN OL.LET_CREATED_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'CreatedDate_desc')
					THEN OL.LET_CREATED_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Customer')
					THEN OL.CUST_CD
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Customer_desc')
					THEN OL.CUST_CD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Subject')
					THEN OL.SUBJECT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Subject_desc')
					THEN OL.SUBJECT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Attention')
					THEN OL.ATTENTION
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Attention_desc')
					THEN OL.ATTENTION
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Location')
					THEN OL.LOCATION
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Location_desc')
					THEN OL.LOCATION
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Date')
					THEN OL.SUBMIT_DT
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Date_desc')
					THEN OL.SUBMIT_DT
				END DESC,
			CASE 
				WHEN (@SortOrder = 'By')
					THEN OL.SUBMIT_BY
				END ASC,
			CASE 
				WHEN (@SortOrder = 'By_desc')
					THEN OL.SUBMIT_BY
				END DESC,
			CASE
				WHEN (@SortOrder = 'Method')
					THEN OL.SUBMIT_METHOD
				END ASC,
			CASE
				WHEN (@SortOrder = 'Method_Desc')
					THEN OL.SUBMIT_METHOD
				END DESC,
			CASE 
				WHEN (@SortOrder = 'Staging')
					THEN OL.LETTER_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Staging_desc')
					THEN OL.LETTER_STS
				END DESC,
				CASE 
				WHEN (@SortOrder = 'Status')
					THEN OL.APPROVAL_STS
				END ASC,
			CASE 
				WHEN (@SortOrder = 'Status_desc')
					THEN OL.APPROVAL_STS
				END DESC,
			CASE 
				WHEN (
						@SortOrder = ''
						OR @SortOrder IS NULL
						)
					THEN OL.UID
				END DESC 
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_OUTGOINGLETTERPRINT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_OUTGOINGLETTERPRINT]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT OL.UID AS Id,
		OL.REGIS_NO AS RegistrationNo,
		OL.CREATOR AS Creator,
		OL.CREATOR_DEPT AS CreatorDepartment,
		OL.CREATOR_UNIT AS CreatorUnit,
		OL.CREATOR_ROLE AS CreatorRole,
		OL.REQUEST_DT AS RequestDate,
		OL.ON_BEHALF_OF AS OnBehalfOf,
		OL.BRANCH_CD AS BranchCode,
		ISNULL(OL.BRANCH_CD,'')+ isnull(' - '+ MP.VALUE,'') AS BranchCodeDesc,
		OL.LETTER_TYPE AS LetterType,
		OL.CUST_CD AS CustCode,
		EC.LegalName AS CustomerCodeDesc,
		OL.CUST_ACC_NO AS CustAccNo,
		--ECA.CombineCode AS CustAccNoDesc,
		OL.VIRTUAL_ACC AS VirtualAccount,
		OL.LET_CREATED_DT AS LetCreatedDate,
		OL.[SUBJECT] AS Subject,
		OL.SUBJECT_ENG AS SubjectEng,
		OL.ATTENTION AS Attention,
		OL.LOCATION AS Location,
		OL.[ADDRESS] AS Address,
		OL.REMARKS AS Remarks,
		OL.SUBMIT_DT AS SubmitDate,
		OL.SUBMIT_BY AS SubmitBy,
		OL.SUBMIT_METHOD AS SubmitMethod,
		OL.LETTER_STS AS LetterStatus,
		OL.APPROVAL_STS AS ApprovalStatus,
		OL.IS_DRAFT AS IsPendingEdit,
		OL.IS_ACTIVE AS IsActive,
		OL.CREATED_BY AS CreatedBy,
		OL.CREATED_DT AS CreatedDate,
		OL.CREATED_HOST AS CreatedHost,
		OL.MODIFIED_BY AS ModifiedBy,
		OL.MODIFIED_DT AS ModifiedDate,
		OL.MODIFIED_HOST AS ModifiedHost,
		OL.DEST_DT AS DestroyedDate,
		OL.DEST_BY AS DestroyedBy,
		OL.DEST_WITNESS AS DestroyedWitness
	FROM TR_OUTGOING_LETTER AS OL
	LEFT JOIN MS_PARAMETER MP WITH(NOLOCK) ON MP.[GROUP] = 'BRANCH_CODE' AND MP.KEYFIELD =  OL.BRANCH_CD 
		LEFT JOIN VW_EASYCUSTOMERCODE EC WITH (NOLOCK) ON OL.CUST_CD = EC.CustomerCode
		--LEFT JOIN VW_EASYACCOUNTNO ECA  ON OL.CUST_CD = ECA.CustomerCode
	WHERE OL.UID = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERBYGROUP]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_PARAMETERBYGROUP]
	-- Add the parameters for the stored procedure here
	@GROUP NVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT mp.UID AS Id
		,mp.[GROUP] AS [Group]
		,mp.KEYFIELD AS [Keyfield]
		,mp.VALUE AS [Value]
		,mp.DESCS AS [Description]
		,mp.SEQ AS [Sequence]
		,mp.CREATED_BY AS CreatedBy
		,mp.CREATED_DT AS CreatedDate
		,mp.CREATED_HOST AS CreatedHost
		,mp.MODIFIED_BY AS ModifedBy
		,mp.MODIFIED_DT AS ModifiedDate
		,mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_PARAMETER AS mp
	WHERE mp.[GROUP] = @GROUP
	ORDER BY MP.[SEQ] ASC
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERBYGROUPKEYFIELD]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_PARAMETERBYGROUPKEYFIELD]
	-- Add the parameters for the stored procedure here
	@GROUP NVARCHAR(100),
	@Keyfield NVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT mp.UID AS Id
		,mp.[GROUP] AS [Group]
		,mp.KEYFIELD AS [Keyfield]
		,mp.VALUE AS [Value]
		,mp.DESCS AS [Description]
		,mp.SEQ AS [Sequence]
		,mp.CREATED_BY AS CreatedBy
		,mp.CREATED_DT AS CreatedDate
		,mp.CREATED_HOST AS CreatedHost
		,mp.MODIFIED_BY AS ModifedBy
		,mp.MODIFIED_DT AS ModifiedDate
		,mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_PARAMETER AS mp
	WHERE mp.[GROUP] = @GROUP
	AND mp.KEYFIELD = @Keyfield
	ORDER BY MP.[SEQ] ASC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERBYGROUPVALUE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_PARAMETERBYGROUPVALUE]
	-- Add the parameters for the stored procedure here
	@GROUP NVARCHAR(100),
	@VALUE NVARCHAR(3000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT mp.UID AS Id
		,mp.[GROUP] AS [Group]
		,mp.VALUE AS [Value]
		,mp.DESCS AS [Description]
		,mp.SEQ AS [Sequence]
		,mp.CREATED_BY AS CreatedBy
		,mp.CREATED_DT AS CreatedDate
		,mp.CREATED_HOST AS CreatedHost
		,mp.MODIFIED_BY AS ModifedBy
		,mp.MODIFIED_DT AS ModifiedDate
		,mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_PARAMETER AS mp
	WHERE mp.[GROUP] = @GROUP
	AND mp.VALUE = @VALUE
	ORDER BY MP.[SEQ] ASC
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_PARAMETERDETAIL]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT mp.UID AS Id
		,mp.[GROUP] AS [Group]
		,mp.KEYFIELD AS [Keyfield]
		,mp.VALUE AS [Value]
		,mp.DESCS AS [Description]
		,mp.SEQ AS [Sequence]
		,mp.CREATED_BY AS CreatedBy
		,mp.CREATED_DT AS CreatedDate
		,mp.CREATED_HOST AS CreatedHost
		,mp.MODIFIED_BY AS ModifedBy
		,mp.MODIFIED_DT AS ModifiedDate
		,mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_PARAMETER AS mp
	WHERE mp.UID = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERS_PARAM]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_PARAMETERS_PARAM]
	-- Add the parameters for the stored procedure here
	@SearchByString NVARCHAR(100) = '',
	@KeywordString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		SELECT *
		FROM MS_PARAMETER
		WHERE 1 = 1
			AND (
				@KeywordString = ''
				OR (
					CASE @SearchByString
						WHEN 'Group'
							THEN [GROUP]
						WHEN 'Key Value'
							THEN [KEYFIELD]
						WHEN 'Value'
							THEN [VALUE]
						WHEN 'Description'
							THEN [DESCS]
						END
					) LIKE '%' + @KeywordString + '%'
				)
		ORDER BY CASE 
				WHEN @SortOrder = 'Group'
					THEN [GROUP]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Group_desc'
					THEN [GROUP]
				END DESC,
			CASE 
				WHEN @SortOrder = 'KeyValue'
					THEN [KEYFIELD]
				END ASC,
			CASE 
				WHEN @SortOrder = 'KeyValue_desc'
					THEN [KEYFIELD]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Value'
					THEN [VALUE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Value_desc'
					THEN [VALUE]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Description'
					THEN [DESCS]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Description_desc'
					THEN [DESCS]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Sequence'
					THEN [SEQ]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Sequence_desc'
					THEN [SEQ]
				END DESC,
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM MS_PARAMETER
		WHERE 1 = 1
			AND (
				@KeywordString = ''
				OR (
					CASE @SearchByString
						WHEN 'Group'
							THEN [GROUP]
						WHEN 'Key Value'
							THEN [KEYFIELD]
						WHEN 'Value'
							THEN [VALUE]
						WHEN 'Description'
							THEN [DESCS]
						END
					) LIKE '%' + @KeywordString + '%'
				)
		)
	SELECT TotalRows,
		mp.UID AS Id,
		mp.[GROUP] AS [Group],
		mp.KEYFIELD AS [Keyfield],
		mp.VALUE AS [Value],
		mp.DESCS AS [Description],
		mp.SEQ AS [Sequence],
		mp.CREATED_BY AS CreatedBy,
		mp.CREATED_DT AS CreatedDate,
		mp.CREATED_HOST AS CreatedHost,
		mp.MODIFIED_BY AS ModifedBy,
		mp.MODIFIED_DT AS ModifiedDate,
		mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_PARAMETER AS mp,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = mp.UID
			)
	OPTION (RECOMPILE)
END





GO
/****** Object:  StoredProcedure [dbo].[UDPS_RECEIVENOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_RECEIVENOTICE]
	-- Add the parameters for the stored procedure here
	@ReqDateStart varchar(15),
	@ReqDateEnd varchar(15),
	@RegisNo varchar(50)=NULL,
	@varForm varchar(50)=NULL,
	@varCust varchar(50)=NULL,
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortField NVARCHAR(100) = 'UID',
	@SortOrder NVARCHAR(100) = 'DESC'
	,@allPage varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @sql nvarchar(MAX);
	DECLARE @offset nvarchar(10);
	DECLARE @newsize nvarchar(10);

	SET @PageNo=@PageNo-1;
	if (@PageNo<1)
	BEGIN
		SET @offset=0
		SET @newsize=@PageSize
	END
	ELSE
	BEGIN
		SET @offset=@PageNo*@PageSize
		SET @newsize=@PageSize-1
	END

	SET @sql='SELECT rn.[UID] as Id
      ,rn.[DOC_ID] as DocId
      ,rn.[REGIS_NO] as RegistrationNo
      ,rn.[CREATOR] as Creator
      ,rn.[CREATOR_DEPT] as CreatorDept
      ,rn.[CREATOR_UNIT] as CreatorUnit
      ,rn.[CREATOR_ROLE] as CreatorRole
	  ,FORMAT(rn.[REQUEST_DT],''dd/MMM/yyyy HH:mm:ss'') as RequestDate
	  ,FORMAT(rn.[RCV_DT],''dd/MMM/yyyy HH:mm:ss'') as ReceivedDate
      ,rn.[RCV_METHOD] as ReceivedMethod
      ,rn.[RCV_BY] as ReceivedBy
      ,rn.[REMARKS] as Remarks 
      ,rn.[APPROVAL_STS] as ApprovalStatus 
      ,rn.[IS_DRAFT] as IsDraft 
      ,rn.[IS_ACTIVE] as IsActive
      ,rn.[CREATED_BY] as CreatedBy 
	  ,FORMAT(rn.[CREATED_DT],''dd/MMM/yyyy HH:mm:ss'') as CreatedDate 
      ,rn.[CREATED_HOST] as CreatedHost
      ,rn.[MODIFIED_BY] as ModifiedBy 
	  ,FORMAT(rn.[MODIFIED_DT],''dd/MMM/yyyy HH:mm:ss'') as ModifiedDate
      ,rn.[MODIFIED_HOST] as ModifiedHost
	  ,book.RECEIVED_METHOD
	  ,book.BRANCH_CD 
	  ,(SELECT ISNULL(NULLIF(Value, ''''),Value) as Value  FROM dbo.[MS_PARAMETER] WHERE [KEYFIELD]=book.[BRANCH_CD] AND [GROUP]=''BRANCH_CODE'') as [LONG_BRANCH]
	  ,book.BOOK_TYPE 
	  ,book.TYPE 
	  ,cust.LegalName as [CUSTOMER_CD]
	  ,(SELECT ISNULL(NULLIF(LongName, ''''),LongName) as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as CreatorDeptDesc 
	FROM [TR_BOOK_RECEIVED_NOTICE] as rn 
	LEFT JOIN (SELECT [UID] as [BOOK_UID],[BRANCH_CD],[REGIS_NO] as [BOOK_REGIS_NO],[RECEIVED_METHOD],[BOOK_TYPE],[CUSTOMER_CD],[TYPE] FROM [TR_BOOK]) as book ON book.BOOK_UID=rn.DOC_ID
	LEFT JOIN (SELECT CustomerCode,LegalName FROM EASY_Customer_Code ) as cust ON cust.CustomerCode=book.CUSTOMER_CD
	WHERE 1=1 AND rn.IS_ACTIVE=1
	';
	
	if (@ReqDateStart IS NOT NULL OR @ReqDateStart !='')
	BEGIN
		SET @sql=@sql + ' AND rn.[REQUEST_DT] >= CONVERT(datetime,'''+@ReqDateStart +''')';
	END

	if ( @ReqDateEnd ='')
	BEGIN
		SET @sql=@sql
	END
	ELSE
	BEGIN
		if (@ReqDateEnd IS NOT NULL OR @ReqDateEnd !='' )
		BEGIN
			SET @sql=@sql + ' AND rn.[REQUEST_DT] <= CONVERT(datetime,'''+@ReqDateEnd +''')';
		END
	END
	

	if (@RegisNo IS NOT NULL OR @RegisNo !='')
	BEGIN
		SET @sql=@sql + ' AND rn.[REGIS_NO] LIKE ''%'+@RegisNo +'%'' ';
	END

	if (@varCust IS NOT NULL OR @varCust !='')
	BEGIN
		SET @sql=@sql + ' AND cust.LegalName LIKE ''%'+@varCust +'%'' ';
	END

	
	if (@varForm IS NOT NULL OR @varForm !='')
	BEGIN
		SET @sql=@sql + ' AND book.[TYPE] LIKE ''%'+@varForm +'%'' ';
	END

	/*
	

	if CHARINDEX(',', @cr)>0
		BEGIN
		
		if @fi='CREATOR_DEPT' 
		
		
			SET @sql=@sql +' AND 
			CASE 
			WHEN (charindex('''+@key+''', [CREATOR_DEPT])-1)>1 THEN 
				LEFT([CREATOR_DEPT],charindex('''+@key+''', [CREATOR_DEPT])-1) 
			ELSE [CREATOR_DEPT] END IN ('+@cr+') ORDER BY UID DESC';
			
			--SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ORDER BY UID DESC';

		
		ELSE
		
			SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ORDER BY UID DESC';
		END
	ELSE
		BEGIN
		--SET @cr = Replace(@cr, '%[^0-9a-zA-Z]%', '')
		SET @sql=@sql + ' AND ['+@fi+'] LIKE ''%'+@cr +'%''  ORDER BY UID DESC';
	END
	SET @sql=@sql + ' ORDER BY UID DESC';
	EXECUTE( @sql );
	*/

	if (@SortOrder='ASC')
	BEGIN
		SET @SortOrder='ASC';
	END
	ELSE
	BEGIN
		SET @SortOrder='DESC';
	END
	SET @sql=@sql+' ORDER BY '+@SortField+' '+@SortOrder; 
	
	if (@allPage IS NOT NULL OR @allPage !='')
		BEGIN
		SET @sql=@sql;
		END
	ELSE
		BEGIN
		SET @sql=@sql + ' OFFSET '+@offset+' ROWS FETCH NEXT '+@newsize+' ROWS ONLY';
		END
	
	EXECUTE( @sql );

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_RECEIVENOTICE_DET]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_RECEIVENOTICE_DET]
	@Id INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT [UID] as Id
      ,[DOC_ID] as DocId
      ,[REGIS_NO] as RegistrationNo
      ,[CREATOR] as Creator
      ,[CREATOR_DEPT] as CreatorDept
      ,[CREATOR_UNIT] as CreatorUnit
      ,[CREATOR_ROLE] as CreatorRole
      ,[REQUEST_DT] as RequestDate
      ,[RCV_DT] as ReceivedDate
      ,[RCV_METHOD] as ReceivedMethod
      ,[RCV_BY] as ReceivedBy
      ,[RCV_BY] + ' - ' + us.RealName as ReceivedByRealName
      ,[REMARKS] as Remarks
      ,[APPROVAL_STS] as ApprovalStatus
      ,[IS_DRAFT] as IsDraft
      ,[IS_ACTIVE] as IsActive
      ,[CREATED_BY] as CreatedBy
      ,[CREATED_DT] as CreatedDate
      ,[CREATED_HOST] as CreatedHost
      ,[MODIFIED_BY] as ModifiedBy
      ,[MODIFIED_DT] as ModifiedDate
      ,[MODIFIED_HOST] as ModifiedHost
	  ,(SELECT ISNULL(NULLIF(LongName, ''),LongName) as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as LongDeptDesc 
  FROM [TR_BOOK_RECEIVED_NOTICE] RN
	LEFT JOIN DBO.SN_BTMUDirectory_User us ON RN.RCV_BY = us.UserName
  WHERE UID = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDER_LATESTDATE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REMINDER_LATESTDATE]
	--DECLARE
	@DocId BIGINT,
	@RebookType VARCHAR(100),
	@CurrentDate DATE = NULL,
	@EmailTo VARCHAR(100)
AS
BEGIN
	--SET @DocId=38
	--SET @RebookType='BOOK_CLAIM'
	--SET @CurrentDate='2022-01-26'
	IF (@CurrentDate IS NULL)
		SET @CurrentDate = GETDATE()

	DECLARE @RecurAprv INT
	DECLARE @LastSendOn DATE,
		@NextSendOn DATE

	SELECT @RecurAprv = CAST(VALUE AS INT)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'RECURRING_DAYS_APPROVAL'

	SELECT @LastSendOn = MAX(SENT_ON)
	FROM TR_REMINDER_LOG
	WHERE DOC_ID = @DocId
		AND TRX_TYPE = @RebookType
		AND SENT_TO = @EmailTo

	SET @NextSendOn = DATEADD(D, @RecurAprv, @LastSendOn)

	--SELECT @LastSendOn, @NextSendOn,@CurrentDate,@RecurAprv,DATEDIFF(D,@CurrentDate,@NextSendOn)
	IF (@NextSendOn IS NULL)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		IF (DATEDIFF(D, @CurrentDate, @NextSendOn) <= 0)
			SELECT 1
		ELSE
			SELECT 0
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERLISTAPPROVAL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REMINDERLISTAPPROVAL]
AS
	select WF.UID as WfId, WF.DOCID as DocId, WF.REGIS_NO as RegistrationNo, WF.WFTASK as RebookType, WF.WFSTEP as WfStep, case WF.WFSTEP when 0 then 'Revise' else 'Approve' end as WfStatus
	from TR_WORKFLOWPROCESS WF
	where WF.ISACTIVE = 1 --and wf.uid=74


GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERLISTAPPROVAL_DETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REMINDERLISTAPPROVAL_DETAIL] @WfId BIGINT
AS
BEGIN
	DECLARE @EmailSubject VARCHAR(500)

	SELECT @EmailSubject = VALUE
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQ_APRV_SCH'
		AND KEYFIELD = 'SUBJECT'

	SELECT WF.UID AS WfId,
		WF.DOCID AS DocId,
		WF.WFTASK AS RebookType,
		WF.REGIS_NO AS RegistrationNo,
		WF.WFSTEP AS WfStep,
		'' AS WfStatus,
		WF.WFTASK AS WfTask,
		@EmailSubject AS EmailSubject,
		dbo.fn_EMAILREQUESTAPPROVALDEPTDH_REMINDER(WF.DOCID, WF.WFTASK, (
				CASE 
					WHEN WF.WFTASK IN (
							'BOOK_CLAIM',
							'BOOK_NOTICE',
							'BOOK_REQUEST'
							)
						THEN (
								SELECT CREATOR_DEPT
								FROM DBO.TR_BOOK
								WHERE UID = WF.DOCID
								)
					WHEN WF.WFTASK IN ('ACTION_TAKEN')
						THEN (
								SELECT CREATOR_DEPT
								FROM DBO.TR_BOOK_ACTION_TAKEN
								WHERE UID = WF.DOCID
								)
					WHEN WF.WFTASK IN ('RECIVED_NOTICE')
						THEN (
								SELECT CREATOR_DEPT
								FROM DBO.TR_BOOK_RECEIVED_NOTICE
								WHERE UID = WF.DOCID
								)
					WHEN WF.WFTASK IN ('IMPORT_ARTICLE')
						THEN (
								SELECT CREATOR_DEPT
								FROM DBO.TR_IMPORTANT_ART
								WHERE UID = WF.DOCID
								)
					WHEN WF.WFTASK IN ('IMPORT_CORRECTION')
						THEN (
								SELECT CREATOR_DEPT
								FROM DBO.TR_IMPORTANT_ART_ACTIVITY
								WHERE UID = WF.DOCID
								)
					WHEN WF.WFTASK IN ('OUT_LETTER')
						THEN (
								SELECT CREATOR_DEPT
								FROM DBO.TR_OUTGOING_LETTER
								WHERE UID = WF.DOCID
								)
					END
				), WFd.SN, WF.WFSTEP) AS EmailBody,
		US.Email AS EmailTo
	FROM TR_WORKFLOWPROCESS WF
	JOIN TR_WORKFLOWPROCESS_DETAIL WFD ON WF.UID = WFD.WFID
	JOIN dbo.vw_UserByDepartment US ON WFD.ACTION_BY = US.UserName
	WHERE WF.ISACTIVE = 1
		AND WF.UID = @WfId
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERLOGDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_REMINDERLOGDETAIL]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT mp.UID AS Id
		,mp.[REGIS_NO] AS [RegistrationNo]
		,mp.[TRX_TYPE] AS [RebookType]
		,mp.[SENT_ON] AS [SentOn]
		,mp.[SENT_TO] AS [SentTo]
		,mp.[SENT_CC] AS [CC]
		,mp.[LAST_STATUS] AS [LastStatus]
		,mp.CREATED_BY AS CreatedBy
		,mp.CREATED_DT AS CreatedDate
		,mp.CREATED_HOST AS CreatedHost
		,mp.MODIFIED_BY AS ModifedBy
		,mp.MODIFIED_DT AS ModifiedDate
		,mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_REMINDER_LOG AS mp
	WHERE mp.UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERLOGLIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REMINDERLOGLIST]
	-- Add the parameters for the stored procedure here
	@SearchByString NVARCHAR(100) = '',
	@KeywordString NVARCHAR(100) = '',
	@DateFrom DATETIME = NULL,
	@DateTo DATETIME = NULL,
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH CTE_Result
	AS (
		SELECT *
		FROM TR_REMINDER_LOG
		WHERE 1 = 1
			AND (
				@KeywordString = ''
				OR (
					CASE @SearchByString
						WHEN 'Rebook Type'
							THEN TRX_TYPE
						WHEN 'Registration No'
							THEN REGIS_NO
						WHEN 'Sent To'
							THEN SENT_TO
						WHEN 'CC'
							THEN SENT_CC
						WHEN 'Last Status'
							THEN LAST_STATUS
						END
					) LIKE '%' + @KeywordString + '%'
				)
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM TR_REMINDER_LOG
		WHERE 1 = 1
			AND (
				@KeywordString = ''
				OR (
					CASE @SearchByString
						WHEN 'Rebook Type'
							THEN TRX_TYPE
						WHEN 'Registration No'
							THEN REGIS_NO
						WHEN 'Sent To'
							THEN SENT_TO
						WHEN 'CC'
							THEN SENT_CC
						WHEN 'Last Status'
							THEN LAST_STATUS
						END
					) LIKE '%' + @KeywordString + '%'
				)
		)
	SELECT TotalRows,
		rl.UID AS Id,
		rl.TRX_TYPE AS RebookType,
		rl.REGIS_NO AS RegistrationNo,
		rl.SENT_ON AS SentOn,
		rl.SENT_TO AS SentTo,
		rl.SENT_CC AS Cc,
		rl.LAST_STATUS AS LastStatus,
		rl.CREATED_BY AS CreatedBy,
		rl.CREATED_DT AS CreatedDate,
		rl.CREATED_HOST AS CreatedHost,
		rl.MODIFIED_BY AS ModifedBy,
		rl.MODIFIED_DT AS ModifiedDate,
		rl.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_REMINDER_LOG AS rl,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = rl.UID
			)
		ORDER BY CASE 
				WHEN @SortOrder = 'RebookType'
					THEN TRX_TYPE
				END ASC,
			CASE 
				WHEN @SortOrder = 'RebookType_desc'
					THEN TRX_TYPE
				END DESC,
			CASE 
				WHEN @SortOrder = 'RegistrationNo'
					THEN REGIS_NO
				END ASC,
			CASE 
				WHEN @SortOrder = 'RegistrationNo_desc'
					THEN REGIS_NO
				END DESC,
			CASE 
				WHEN @SortOrder = 'SentOn'
					THEN SENT_ON
				END ASC,
			CASE 
				WHEN @SortOrder = 'SentOn_desc'
					THEN SENT_ON
				END DESC,
			CASE 
				WHEN @SortOrder = 'SentTo'
					THEN SENT_TO
				END ASC,
			CASE 
				WHEN @SortOrder = 'SentTo_desc'
					THEN SENT_TO
				END DESC,
			CASE 
				WHEN @SortOrder = 'Cc'
					THEN SENT_CC
				END ASC,
			CASE 
				WHEN @SortOrder = 'Cc_desc'
					THEN SENT_CC
				END DESC,
			CASE 
				WHEN @SortOrder = 'LastStatus'
					THEN LAST_STATUS
				END ASC,
			CASE 
				WHEN @SortOrder = 'LastStatus_desc'
					THEN LAST_STATUS
				END DESC,
			CASE 
				WHEN @SortOrder = ''
					THEN SENT_ON 
				END DESC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
	OPTION (RECOMPILE)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_STOCK_PROFILEDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_STOCK_PROFILEDETAIL]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT SP.UID AS [UID]
		,SP.ARTICLE_ID AS [ARTICLE_ID]
		,SP.DEPT_OF_ARTICLE AS [DEPT_OF_ARTICLE]
		,SP.STOCK_IN_VAULT AS [STOCK_IN_VAULT]
		,SP.STOCK_IN_HAND AS [STOCK_IN_HAND]
		,SP.USAGE AS [USAGE]
		,SP.INCOMING_STOCK AS [INCOMING_STOCK]
		,SP.CORRECTION_IN_VAULT AS [CORRECTION_IN_VAULT]
		,SP.CORRECTION_IN_HAND AS [CORRECTION_IN_HAND]
		,SP.CORRECTION_IN_USAGE AS [CORRECTION_IN_USAGE]
		,SP.BALANCE_IN_HAND AS [BALANCE_IN_HAND]
		,SP.CREATED_BY AS CREATED_BY
		,SP.CREATED_DT AS CREATED_DT
		,SP.CREATED_HOST AS CREATED_HOST
		,SP.MODIFIED_BY AS MODIFIED_BY
		,SP.MODIFIED_DT AS MODIFIED_DT
		,SP.MODIFIED_HOST AS MODIFIED_HOST
	FROM dbo.MS_STOCK_PROFILE AS SP
	WHERE SP.UID = @Id
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_STOCKIDCHECK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_STOCKIDCHECK]
	-- Add the parameters for the stored procedure here
	-- @Id INT,
	@ARTICLE_ID VARCHAR (255),
	@DEPT_OF_ARTICLE VARCHAR (255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT SP.UID AS [UID]
		,SP.ARTICLE_ID AS [ARTICLE_ID]
		,SP.DEPT_OF_ARTICLE AS [DEPT_OF_ARTICLE]
		,SP.CREATED_BY AS [CREATED_BY]
		,SP.CREATED_DT AS [CREATED_DT]
		,SP.CREATED_HOST AS [CREATED_HOST]
		,SP.MODIFIED_BY AS [MODIFIED_BY]
		,SP.MODIFIED_DT AS [MODIFIED_DT]
		,SP.MODIFIED_HOST AS [MODIFIED_HOST]
	FROM dbo.MS_STOCK_PROFILE AS SP
	WHERE (SP.ARTICLE_ID = @ARTICLE_ID)
	AND SP.DEPT_OF_ARTICLE = @DEPT_OF_ARTICLE 

END

--	IF EXISTS (
--			SELECT 1
--			FROM MS_STOCK_PROFILE
--			WHERE 1 = 1
--				AND ARTICLE_ID = @ARTICLE_ID
--				OR DEPT_OF_ARTICLE = @DEPT_OF_ARTICLE
--			)
--	BEGIN
--		SELECT 1
--	END
--	ELSE
--	BEGIN
--		SELECT 0
--	END
--END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_STOCKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_STOCKPROFILE]
	-- Add the parameters for the stored procedure here
	@UserDept VARCHAR(200) = '',
	@SearchArticleId NVARCHAR(100) = '',
	@SearchDepartment NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	WITH CTE_Result
	AS (
		SELECT *
		FROM MS_STOCK_PROFILE AS SP
		WHERE 
		
		(ISNULL(@SearchArticleId,'') = ''
			OR SP.ARTICLE_ID LIKE '%' + @SearchArticleId + '%'
		)
		AND(ISNULL(@SearchDepartment,'') = ''
			OR SP.DEPT_OF_ARTICLE LIKE '%' + @SearchDepartment + '%'
		)
		AND (
				SP.DEPT_OF_ARTICLE  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		ORDER BY CASE 
				WHEN @SortOrder = 'ArticleID'
					THEN [ARTICLE_ID]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ArticleID_desc'
					THEN [ARTICLE_ID]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Department'
					THEN [DEPT_OF_ARTICLE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Department_desc'
					THEN [DEPT_OF_ARTICLE]
				END DESC,
			CASE 
				WHEN @SortOrder = 'StockVault'
					THEN [STOCK_IN_VAULT]
				END ASC,
			CASE 
				WHEN @SortOrder = 'StockVault_desc'
					THEN [STOCK_IN_VAULT]
				END DESC,
			CASE 
				WHEN @SortOrder = 'StockHand'
					THEN [STOCK_IN_HAND]
				END ASC,
			CASE 
				WHEN @SortOrder = 'StockHand_desc'
					THEN [STOCK_IN_HAND]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Usage'
					THEN [USAGE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Usage_desc'
					THEN [USAGE]
				END DESC,
			CASE 
				WHEN @SortOrder = 'IncomingStock'
					THEN [INCOMING_STOCK]
				END ASC,
			CASE 
				WHEN @SortOrder = 'IncomingStock_desc'
					THEN [INCOMING_STOCK]
				END DESC,
			CASE 
				WHEN @SortOrder = 'CinVault'
					THEN [CORRECTION_IN_VAULT]
				END ASC,
			CASE 
				WHEN @SortOrder = 'CinVault_desc'
					THEN [CORRECTION_IN_VAULT]
				END DESC,
			CASE 
				WHEN @SortOrder = 'CinHand'
					THEN [CORRECTION_IN_HAND]
				END ASC,
			CASE 
				WHEN @SortOrder = 'CinHand_desc'
					THEN [CORRECTION_IN_HAND]
				END DESC,
			CASE 
				WHEN @SortOrder = 'CinUsage'
					THEN [CORRECTION_IN_USAGE]
				END ASC,
			CASE 
				WHEN @SortOrder = 'CinUsage_desc'
					THEN [CORRECTION_IN_USAGE]
				END DESC,
			CASE 
				WHEN @SortOrder = 'BalanceHand'
					THEN [BALANCE_IN_HAND]
				END ASC,
			CASE 
				WHEN @SortOrder = 'BalanceHand_desc'
					THEN [BALANCE_IN_HAND]
				END DESC,

			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM MS_STOCK_PROFILE AS SP
		WHERE 
		
		(ISNULL(@SearchArticleId,'') = ''
			OR SP.ARTICLE_ID LIKE '%' + @SearchArticleId + '%'
		)
		AND(ISNULL(@SearchDepartment,'') = ''
			OR SP.DEPT_OF_ARTICLE LIKE '%' + @SearchDepartment + '%'
		)
		AND (
				SP.DEPT_OF_ARTICLE  LIKE '%' + @UserDept + '%'
				OR @UserDept IN ('SKAI','ITSO') 
		)
		)
	SELECT TotalRows,
		SP.UID AS [UID],
		SP.[ARTICLE_ID] AS [ARTICLE_ID],
		SP.DEPT_OF_ARTICLE AS [DEPT_OF_ARTICLE],
		SP.STOCK_IN_VAULT AS [STOCK_IN_VAULT],
		SP.STOCK_IN_VAULT AS [STOCK_IN_VAULT],
		SP.STOCK_IN_HAND AS [STOCK_IN_HAND],
		SP.USAGE AS [USAGE],
		SP.INCOMING_STOCK AS [INCOMING_STOCK],
		SP.CORRECTION_IN_VAULT AS [CORRECTION_IN_VAULT],
		SP.CORRECTION_IN_HAND AS [CORRECTION_IN_HAND],
		SP.CORRECTION_IN_USAGE AS [CORRECTION_IN_USAGE],
		SP.BALANCE_IN_HAND AS [BALANCE_IN_HAND],
		SP.CREATED_BY AS CreatedBy,
		SP.CREATED_DT AS CreatedDate,
		SP.CREATED_HOST AS CreatedHost,
		SP.MODIFIED_BY AS ModifedBy,
		SP.MODIFIED_DT AS ModifiedDate,
		SP.MODIFIED_HOST AS ModifiedHost
	FROM dbo.MS_STOCK_PROFILE AS SP,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = SP.UID
			)
	OPTION (RECOMPILE)
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_SUBMITTEDTASK_PARAM]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_SUBMITTEDTASK_PARAM]
	-- add more stored procedure parameters here
	@SubmittedBy VARCHAR(50),
	@TaskCategoryByString NVARCHAR(100) = '',
	@CustomerString NVARCHAR(100) = '',
	@RegistrationNoString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	declare @realnm VARCHAR(1000)
	select @realnm = RealName from SN_BTMUDirectory_User where username=@SubmittedBy;

	-- body of the stored procedure
	WITH CTE_Result
	AS (
		SELECT Id, Task
		FROM VW_SUBMITTEDLIST 
		WHERE 1 = 1
			AND (RequestedBy = @realnm)
			AND IsActive = 1
			--AND TaskName = 'Approve'
			AND 			
			(
				(
					ISNULL(@TaskCategoryByString,'') <> ''
					AND TaskCategoryDesc = CASE 
							WHEN @TaskCategoryByString = 'Claim' THEN 'CLAIM' 
							WHEN @TaskCategoryByString = 'Notice' THEN 'NOTICE' 
							WHEN @TaskCategoryByString = 'Request' THEN 'REQUEST' 
							WHEN @TaskCategoryByString = 'Important Article' THEN 'IMPORT_ARTICLE' 
							WHEN @TaskCategoryByString = 'Outgoing Letter' THEN 'OUT_LETTER' 
							WHEN @TaskCategoryByString = 'All' THEN TaskCategoryDesc
						END
				) OR ISNULL(@TaskCategoryByString,'') = ''
			)
			AND (
				(ISNULL(@CustomerString,'') <> '' AND CustomerAttentionDesc LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
				)
			AND (
				(ISNULL(@RegistrationNoString,'') <> '' AND RegistrationNo LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
				)
		),
	CTE_TotalRows
	AS (
		SELECT count(Id) AS TotalRows
		FROM VW_SUBMITTEDLIST AS tw
		WHERE 1 = 1
			AND (RequestedBy = @realnm)
			AND IsActive = 1
			--AND TaskName = 'Approve'
			AND 			
			(
				(
					ISNULL(@TaskCategoryByString,'') <> ''
					AND TaskCategoryDesc = CASE 
							WHEN @TaskCategoryByString = 'Claim' THEN 'CLAIM' 
							WHEN @TaskCategoryByString = 'Notice' THEN 'NOTICE' 
							WHEN @TaskCategoryByString = 'Request' THEN 'REQUEST' 
							WHEN @TaskCategoryByString = 'Important Article' THEN 'IMPORT_ARTICLE' 
							WHEN @TaskCategoryByString = 'Outgoing Letter' THEN 'OUT_LETTER' 
							WHEN @TaskCategoryByString = 'All' THEN TaskCategoryDesc
						END
				) OR ISNULL(@TaskCategoryByString,'') = ''
			)
			AND (
				(ISNULL(@CustomerString,'') <> '' AND CustomerAttentionDesc LIKE '%' + @CustomerString + '%') OR ISNULL(@CustomerString,'') = ''
				)
			AND (
				(ISNULL(@RegistrationNoString,'') <> '' AND RegistrationNo LIKE '%' + @RegistrationNoString + '%') OR ISNULL(@RegistrationNoString,'') = ''
				)
		)
	SELECT TotalRows, Id, TaskCategory,CustomerAttentionDesc,Task,
		TaskName, RegistrationNo, [Action],
		RequestedBy,
		RequestedDate,
		ProcessId,
		SerialNumber, [URL], [UrlFlow]
	FROM VW_SUBMITTEDLIST AS tw, CTE_TotalRows
	WHERE 1 = 1
		AND (RequestedBy = @realnm)
		AND IsActive = 1
		--AND TaskName = 'Approve'
		AND EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.Id = tw.Id AND CTE_Result.Task = tw.Task
			)
		ORDER BY CASE 
				WHEN @SortOrder = 'TaskCategory'
					THEN TaskCategory
				END ASC,
			CASE 
				WHEN @SortOrder = 'TaskCategory_desc'
					THEN TaskCategory
				END DESC,
			CASE 
				WHEN @SortOrder = 'TaskName'
					THEN TaskName
				END ASC,
			CASE 
				WHEN @SortOrder = 'TaskName_desc'
					THEN TaskName
				END DESC,
			CASE 
				WHEN @SortOrder = 'RegistrationNo'
					THEN RegistrationNo
				END ASC,
			CASE 
				WHEN @SortOrder = 'RegistrationNo_desc'
					THEN RegistrationNo
				END DESC,
			CASE 
				WHEN @SortOrder = 'Customer'
					THEN CustomerAttentionDesc
				END ASC,
			CASE 
				WHEN @SortOrder = 'Customer_desc'
					THEN CustomerAttentionDesc
				END DESC,
			CASE 
				WHEN @SortOrder = 'Action'
					THEN [Action]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Action_desc'
					THEN [Action]
				END DESC,
			CASE 
				WHEN @SortOrder = 'SubmittedBy'
					THEN RequestedBy
				END ASC,
			CASE 
				WHEN @SortOrder = 'SubmittedBy_desc'
					THEN RequestedBy
				END DESC,
			CASE 
				WHEN @SortOrder = 'SubmittedDate'
					THEN RequestedDate
				END ASC,
			CASE 
				WHEN @SortOrder = 'SubmittedDate_desc'
					THEN RequestedDate
				END DESC,
			CASE 
				WHEN @SortOrder = ''
					THEN RequestedDate
				END DESC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
	OPTION (RECOMPILE)
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERACTIVITYLOG_PARAM]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_USERACTIVITYLOG_PARAM]
	-- Add the ReminderLog for the stored procedure here
	@SearchByDate DATETIME = NULL,
	@SearchByDateTo DATETIME = NULL,
	@SearchByString NVARCHAR(100) = '',
	@KeywordString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	WITH CTE_Result
	AS (
		SELECT *
		FROM TR_USER_ACTIVITY_LOG_HDR
		WHERE 1 = 1
			--AND CONVERT(VARCHAR(10), ACT_DT, 111) = convert(VARCHAR(10), @SearchByDate, 111)
			AND (
				 CAST(ACT_DT AS DATE) BETWEEN  @SearchByDate AND @SearchByDateTo 
					OR ISNULL(@SearchByDate,'') = '' AND ISNULL(@SearchByDateTo,'') = ''
				)
AND(CONCAT(ACT_RESULT,ACT_USER,ACT_DEPT,ACT_CD,ACT_DESC,ACT_VAL) LIKE '%' +@KeywordString+ '%' OR @KeywordString = '')
			AND (
			@SearchByDate = '' AND @SearchByDateTo = ''
			OR
				@KeywordString = ''
				OR @SearchByString = ''
				OR (
					CASE @SearchByString
						WHEN 'Result'
							THEN [ACT_RESULT]
						WHEN 'User Name'
							THEN [ACT_USER]
						WHEN 'User Departement'
							THEN [ACT_DEPT]
						WHEN 'Activity Code'
							THEN [ACT_CD]
						WHEN 'Activity Description'
							THEN [ACT_DESC]
						WHEN 'Activity Value'
							THEN [ACT_VAL]
						END
					) LIKE '%' + @KeywordString + '%'
				)
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM TR_USER_ACTIVITY_LOG_HDR
		WHERE 1 = 1
			--AND CONVERT(VARCHAR(10), ACT_DT, 111) = convert(VARCHAR(10), @SearchByDate, 111)
			AND (
					CAST(ACT_DT AS DATE) BETWEEN  @SearchByDate AND @SearchByDateTo 
					OR ISNULL(@SearchByDate,'') = '' AND ISNULL(@SearchByDateTo,'') = ''
				)
			AND(CONCAT(ACT_RESULT,ACT_USER,ACT_DEPT,ACT_CD,ACT_DESC,ACT_VAL) LIKE '%' +@KeywordString+ '%' OR @KeywordString = '')
			AND (
				@SearchByDate = '' AND @SearchByDateTo = ''
				OR
				@KeywordString = ''
				OR @SearchByString = ''
				OR (
					CASE @SearchByString
						WHEN 'Result'
							THEN [ACT_RESULT]
						WHEN 'User Name'
							THEN [ACT_USER]
						WHEN 'User Departement'
							THEN [ACT_DEPT]
						WHEN 'Activity Code'
							THEN [ACT_CD]
						WHEN 'Activity Description'
							THEN [ACT_DESC]
						WHEN 'Activity Value'
							THEN [ACT_VAL]
						END
					) LIKE '%' + @KeywordString + '%'
				)
		)
	SELECT TotalRows,
		mp.UID AS Id,
		mp.ACT_CD AS [ActivityCode],
		mp.ACT_DESC AS [ActivityDescription],
		mp.ACT_VAL AS [ActivityValue],
		mp.ACT_RESULT AS [ActivityResult],
		mp.ACT_USER AS [ActivityUserName],
		mp.ACT_DEPT AS [ActivityDepartment],
		mp.ACT_DT AS [ActivityDate],
		MP.ACT_WORKSTATION AS [ActivityWorkstation],
		mp.CREATED_BY AS CreatedBy,
		mp.CREATED_DT AS CreatedDate,
		mp.CREATED_HOST AS CreatedHost,
		mp.MODIFIED_BY AS ModifedBy,
		mp.MODIFIED_DT AS ModifiedDate,
		mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_USER_ACTIVITY_LOG_HDR AS mp,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = mp.UID
			)
			ORDER BY CASE 
				WHEN @SortOrder = 'Result'
					THEN [ACT_RESULT]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Result_desc'
					THEN [ACT_RESULT]
				END DESC,
			CASE 
				WHEN @SortOrder = 'User'
					THEN [ACT_USER]
				END ASC,
			CASE 
				WHEN @SortOrder = 'User_desc'
					THEN [ACT_USER]
				END DESC,
			CASE 
				WHEN @SortOrder = 'Dept'
					THEN [ACT_DEPT]
				END ASC,
			CASE 
				WHEN @SortOrder = 'Dept_desc'
					THEN [ACT_DEPT]
				END DESC,
			CASE 
				WHEN @SortOrder = 'ActivityCode'
					THEN [ACT_CD]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ActivityCode_desc'
					THEN [ACT_CD]
				END DESC,
			CASE 
				WHEN @SortOrder = 'ActivityDate'
					THEN [ACT_DT]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ActivityDate_desc'
					THEN [ACT_DT]
				END DESC,
			CASE 
				WHEN @SortOrder = 'ActivtyDescription'
					THEN [ACT_DESC]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ActivityDescription_desc'
					THEN [ACT_DESC]
				END DESC,
			CASE 
				WHEN @SortOrder = 'ActivityValue'
					THEN [ACT_VAL]
				END ASC,
			CASE 
				WHEN @SortOrder = 'ActivityValue_desc'
					THEN [ACT_VAL]
				END DESC,
			CASE 
				WHEN @SortOrder = ''
					THEN [UID]
				END DESC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
	OPTION (RECOMPILE)
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERACTIVITYLOGDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_USERACTIVITYLOGDETAIL]
	-- Add the ReminderLog for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT UID AS Id,
		ACT_CD AS ActivityCode,
		FIELD_NM AS FieldName,
		-- OLD_VALUE AS OldValue,
		OldValue = CASE 
			WHEN OLD_VALUE = '(null)'
				THEN ''
			ELSE CASE 
					WHEN ISNUMERIC(OLD_VALUE) = 1
						THEN OLD_VALUE
					WHEN ISDATE(OLD_VALUE) = 1
						THEN FORMAT(CONVERT(DATE, OLD_VALUE), 'dd/MMM/yyyy')
					WHEN CHARINDEX('1/1/0001', OLD_VALUE) = 1
						THEN ''
					ELSE OLD_VALUE
					END
			END,
		-- NEW_VALUE AS NewValue,
		NewValue = CASE 
			WHEN NEW_VALUE = '(null)'
				THEN ''
			ELSE CASE 
					WHEN ISNUMERIC(NEW_VALUE) = 1
						THEN NEW_VALUE
					WHEN len(new_value) <= 4000
						AND ISDATE(NEW_VALUE) = 1
						THEN FORMAT(CONVERT(DATE, NEW_VALUE), 'dd/MMM/yyyy')
					WHEN CHARINDEX('1/1/0001', OLD_VALUE) = 1
						THEN ''
					ELSE NEW_VALUE
					END
			END,
		CREATED_BY AS CreatedBy,
		CREATED_DT AS CreatedDate,
		CREATED_HOST AS CreatedHost,
		MODIFIED_BY AS ModifiedBy,
		MODIFIED_DT AS MODIFIED_DT,
		MODIFIED_HOST AS MODIFIED_HOST,
		DOC_ID AS DocumentId
	FROM TR_USER_ACTIVITY_LOG_DTL
	WHERE [DOC_ID] = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERACTIVITYLOGHEADER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_USERACTIVITYLOGHEADER]
	-- Add the ReminderLog for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT mp.UID AS Id
		,mp.ACT_CD AS [ActivityCode]
		,mp.ACT_DESC AS [ActivityDescription]
		,mp.ACT_VAL AS [ActivityValue]
		,mp.ACT_RESULT AS [ActivityResult]
		,mp.ACT_USER AS [ActivityUserName]
		,mp.ACT_DEPT AS [ActivityDepartment]
		,FORMAT(mp.ACT_DT, 'dd/MMM/yyyy HH:mm:ss') AS [ActivityDate]
		,mp.ACT_WORKSTATION AS [ActivityWorkstation]
		,mp.CREATED_BY AS CreatedBy
		,mp.CREATED_DT AS CreatedDate
		,mp.CREATED_HOST AS CreatedHost
		,mp.MODIFIED_BY AS ModifedBy
		,mp.MODIFIED_DT AS ModifiedDate
		,mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_USER_ACTIVITY_LOG_HDR AS mp 
	WHERE mp.UID = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_USRAPPDH]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_USRAPPDH]
	-- add more stored procedure parameters here
	@Originator NVARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	DECLARE @DivID NVARCHAR(5)
	DECLARE @DeptID NVARCHAR(5)

	SELECT @DivID = DivisionID,
		@DeptID = DepartmentID
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

	SELECT Username,
		RealName,
		Email
	FROM [vw_UserByDepartment]
	WHERE DepartmentID = @DeptID
		AND RoleShortName IN ('DH')
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_USRAPPUH]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_USRAPPUH]
	-- add more stored procedure parameters here
	@Originator NVARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	DECLARE @DivID NVARCHAR(5)
	DECLARE @DeptID NVARCHAR(5)

	SELECT @DivID = DivisionID,
		@DeptID = DepartmentID
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

	SELECT Username,
		RealName,
		Email
	FROM [vw_UserByDepartment]
	WHERE DepartmentID = @DeptID
		AND RoleShortName IN ('UH')
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_WF_IMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_WF_IMPORTANTART]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID
      ,REGIS_NO
      ,CREATOR
      ,CREATOR_DEPT
      ,CREATOR_UNIT
      ,CREATOR_ROLE
      ,REQUEST_DT
      ,CUST_BRANCH
      ,ARTICLE_NAME
      ,REGIS_TYPE
      ,BALANCE_IN_HAND
      ,VALUE
      ,DATE
      ,REF_NO
      ,REMARKS
      ,ART_STATUS
      ,APPROVAL_STS
      ,IS_DRAFT
      ,IS_ACTIVE
      ,CREATED_BY
      ,CREATED_DT
      ,CREATED_HOST
      ,MODIFIED_BY
      ,MODIFIED_DT
      ,MODIFIED_HOST,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_IMPORTANT_ART
	WHERE 1 = 1
		AND UID = @RefID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_WF_IMPORTANTART_CORR]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_WF_IMPORTANTART_CORR]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID
		,DOC_ID
      ,REGIS_NO
      ,ACTIVITY_CD
      ,CREATOR
      ,CREATOR_DEPT
      ,CREATOR_UNIT
      ,CREATOR_ROLE
      ,REQUEST_DT
      ,CORR_BALANCE_IN_HAND
      ,CORR_VALUE
      ,CORR_REF_NO
      ,CORR_REMARKS
      ,CAN_REASON
      ,INSP_DP
      ,INSP_BY
      ,APPROVAL_STS
      ,IS_DRAFT
      ,IS_ACTIVE
      ,CREATED_BY
      ,CREATED_DT
      ,CREATED_HOST
      ,MODIFIED_BY
      ,MODIFIED_DT
      ,MODIFIED_HOST
      ,DESTROYED_DT
      ,DESTROYED_BY
      ,WITNESSED_BY,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_IMPORTANT_ART_ACTIVITY
	WHERE 1 = 1
		AND UID = @RefID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_WF_OUTGOINGLETTER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_WF_OUTGOINGLETTER]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID,
		REGIS_NO
      ,CREATOR
      ,CREATOR_DEPT
      ,CREATOR_UNIT
      ,CREATOR_ROLE
      ,REQUEST_DT
      ,ON_BEHALF_OF
      ,BRANCH_CD
      ,LETTER_TYPE
      ,CUST_CD
      ,CUST_ACC_NO
      ,LET_CREATED_DT
      ,SUBJECT
      ,ATTENTION
      ,LOCATION
      ,ADDRESS
      ,REMARKS
      ,SUBMIT_DT
      ,SUBMIT_BY
      ,SUBMIT_METHOD
      ,LETTER_STS
      ,APPROVAL_STS
      ,IS_DRAFT
      ,IS_ACTIVE
      ,CREATED_BY
      ,CREATED_DT
      ,CREATED_HOST
      ,MODIFIED_BY
      ,MODIFIED_DT
      ,MODIFIED_HOST,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_OUTGOING_LETTER
	WHERE 1 = 1
		AND UID = @RefID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_ACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPU_ACTIONTAKEN]
	-- Add the parameters for the stored procedure here
	@Id int
	--,@DOC_ID varchar (50)
    --,@REGIS_NO varchar (50)
    --,@CREATOR varchar (50)
    --,@CREATOR_DEPT varchar (50)
    --,@CREATOR_UNIT varchar (50)
    --,@CREATOR_ROLE varchar (50)
    --,@REQUEST_DT varchar (50)
    ,@ACT_DT varchar (50)
    ,@ACT_ACTION varchar (255)
    ,@ACTOR varchar (100)
    ,@REMARKS varchar (8000)
    ,@RESULT varchar (8000)
    ,@APPROVAL_STS varchar (50)
    ,@IS_DRAFT varchar (50)
    ,@IS_ACTIVE varchar (50)
    --,@CREATED_BY varchar (50)
    --,@CREATED_DT varchar (50)
    --,@CREATED_HOST varchar (50)
    ,@MODIFIED_BY varchar (50)
    --,@MODIFIED_DT varchar (50)
    ,@MODIFIED_HOST varchar (50)
AS
BEGIN

	UPDATE TR_BOOK_ACTION_TAKEN SET
	   --[DOC_ID]=@DOC_ID
       --,[REGIS_NO]=@REGIS_NO
       --,[CREATOR]=@CREATOR
       --,[CREATOR_DEPT]=@CREATOR_DEPT
       --,[CREATOR_UNIT]=@CREATOR_UNIT
       --,[CREATOR_ROLE]=@CREATOR_ROLE
       --,[REQUEST_DT]=@REQUEST_DT
       [ACT_DT]=@ACT_DT
       ,[ACT_ACTION]=@ACT_ACTION
       ,[ACTOR]=@ACTOR
       ,[REMARKS]=@REMARKS
       ,[RESULT]=@RESULT
       ,[APPROVAL_STS]=@APPROVAL_STS
       ,[IS_DRAFT]=@IS_DRAFT
       ,[IS_ACTIVE]=@IS_ACTIVE
       --,[CREATED_BY]=@DOC_ID
       --,[CREATED_DT]=@DOC_ID
       --,[CREATED_HOST]=@DOC_ID
       ,[MODIFIED_BY]=@MODIFIED_BY
       ,[MODIFIED_DT]=GETDATE() 
       ,[MODIFIED_HOST]=@MODIFIED_HOST
	WHERE UID = @Id

	SELECT @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_ARTICLEPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPU_ARTICLEPROFILE]
	-- Add the parameters for the stored procedure here
	@UID INT,
	@ARTICLE_ID VARCHAR (3000),
	@NAME VARCHAR (3000),
	@DEPT_ARTICLE VARCHAR (255),
	@IS_STOCK_IN_VAULT BIT,
	@IS_STOCK_IN_HAND BIT,
	@IS_USAGE BIT,
	@IS_CORRECTION BIT,
	@IS_ACTIVE BIT,
	@MODIFIED_BY VARCHAR(50),
	@MODIFIED_DT DATETIME,
	@MODIFIED_HOST VARCHAR(20)
AS
BEGIN
   SET NOCOUNT ON;
    -- Insert statements for procedure here
	UPDATE dbo.MS_ARTICLE_PROFILE
	SET ARTICLE_ID = @ARTICLE_ID,
		NAME = @NAME,
		DEPT_ARTICLE = @DEPT_ARTICLE,
		IS_STOCK_IN_VAULT = @IS_STOCK_IN_VAULT,
		IS_STOCK_IN_HAND = @IS_STOCK_IN_HAND,
		IS_USAGE = @IS_USAGE,
		IS_CORRECTION = @IS_CORRECTION,
		IS_ACTIVE = @IS_ACTIVE,
		MODIFIED_BY = @MODIFIED_BY,
		MODIFIED_DT = @MODIFIED_DT,
		MODIFIED_HOST = @MODIFIED_HOST
	
	WHERE UID = @UID

	SELECT @UID

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_BOOK_ACTIONTAKEN_APPSTATUS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_BOOK_ACTIONTAKEN_APPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_BOOK_ACTION_TAKEN
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_BOOK_RECEIVEDNOTICE_APPSTATUS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UDPU_BOOK_RECEIVEDNOTICE_APPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_BOOK_RECEIVED_NOTICE
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_BOOKAPPSTATUS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_BOOKAPPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_BOOK
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_BOOKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPU_BOOKPROFILE]
	-- Add the parameters for the stored procedure here
	@UID INT,
	@BOOK_ID VARCHAR (3000),
	@BOOK_NAME VARCHAR (3000),
	@DEPT_BOOK VARCHAR (255),
	@BOOK_SEQ INT,
	@BEFORE_OFFSET_DAYS INT,
	@AFTER_OFFSET_DAYS INT,
	@IS_ACTIVE BIT,
	@MODIFIED_BY VARCHAR(50),
	@MODIFIED_DT DATETIME,
	@MODIFIED_HOST VARCHAR(20)
AS
BEGIN
SET NOCOUNT ON;
    -- Insert statements for procedure here
	UPDATE dbo.MS_BOOK_PROFILE 
	SET BOOK_ID  = @BOOK_ID,
		BOOK_NAME = @BOOK_NAME,
		DEPT_BOOK = @DEPT_BOOK,
		BOOK_SEQ = @BOOK_SEQ,
		BEFORE_OFFSET_DAYS = @BEFORE_OFFSET_DAYS,
		AFTER_OFFSET_DAYS = @AFTER_OFFSET_DAYS,
		IS_ACTIVE = @IS_ACTIVE,
		MODIFIED_BY = @MODIFIED_BY,
		MODIFIED_DT = @MODIFIED_DT,
		MODIFIED_HOST = @MODIFIED_HOST
	
	WHERE UID = @UID

	SELECT @UID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_DEACTIVEWF]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_DEACTIVEWF]
	-- add more stored procedure parameters here
	@WFID BIGINT
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_WORKFLOWPROCESS
	SET ISACTIVE = 0,
		MODIFIED_BY = 'SYSTEM',
		MODIFIED_DT = GETDATE()
	WHERE [UID] = @WFID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_DELETEDACTIONTAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DELETEDACTIONTAKEN]
@UID BIGINT
AS
BEGIN
	UPDATE TR_BOOK_ACTION_TAKEN
	SET IS_ACTIVE = 1
	WHERE UID = @UID



	SELECT REGIS_NO FROM TR_BOOK_ACTION_TAKEN
	WHERE UID = @UID

END

GO
/****** Object:  StoredProcedure [dbo].[UDPU_DELETEDIMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DELETEDIMPORTANTART]
@UID BIGINT
,@IsCorrection bit = 0
,@ModifiedBy varchar(255)
,@ModifiedDate DATETIME
,@ModifiedHost varchar(255)
AS
BEGIN

	IF (@IsCorrection = 0)
	BEGIN
		UPDATE TR_IMPORTANT_ART
		SET IS_ACTIVE = 1
		,MODIFIED_BY = @ModifiedBy
		,MODIFIED_DT = @ModifiedDate
		,MODIFIED_HOST = @ModifiedHost
		WHERE UID = @UID

		SELECT REGIS_NO FROM TR_IMPORTANT_ART
		WHERE UID = @UID
	END
	ELSE
	BEGIN
		UPDATE TR_IMPORTANT_ART_ACTIVITY
		SET IS_ACTIVE = 1
		,MODIFIED_BY = @ModifiedBy
		,MODIFIED_DT = @ModifiedDate
		,MODIFIED_HOST = @ModifiedHost
		WHERE UID = @UID

		SELECT REGIS_NO FROM TR_IMPORTANT_ART_ACTIVITY
		WHERE UID = @UID
	END

END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_DELETEIMPORTANTARTDRAFT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DELETEIMPORTANTARTDRAFT]
@ArticleID bigint
,@ModifiedBy varchar(255)
,@ModifiedDate DATETIME
,@ModifiedHost VARCHAR(255)
AS
BEGIN

	UPDATE TR_IMPORTANT_ART 
	SET IS_ACTIVE = 0
	,MODIFIED_BY = @ModifiedBy
	,MODIFIED_DT = @ModifiedDate
	,MODIFIED_HOST = @ModifiedHost
	WHERE IS_DRAFT = 1
	AND UID = @ArticleID

	SELECT REGIS_NO FROM TR_IMPORTANT_ART 
	WHERE UID = @ArticleID

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_DESTROYIMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_DESTROYIMPORTANTART]
@UID bigint
AS
BEGIN
	UPDATE TR_IMPORTANT_ART 
	SET ART_STATUS = 'DESTROYED'
	WHERE UID = @UID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_DRAFTIMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_DRAFTIMPORTANTART]
 @UID [bigint]
,@REGIS_NO [varchar](50)
,@CREATOR [varchar](50)
,@CREATOR_DEPT [varchar](30)
,@CREATOR_UNIT [varchar](30)
,@CREATOR_ROLE [varchar](30)
,@REQUEST_DT [date]
,@CUST_BRANCH [varchar](255)
,@ARTICLE_NAME [varchar](255)
,@REGIS_TYPE [varchar](255)
,@BALANCE_IN_HAND [numeric]
,@VALUE [numeric]
,@DATE [date]
,@REF_NO [varchar](255)
,@REMARKS [varchar](8000)
,@ART_STATUS [varchar](8000)
,@APPROVAL_STS [varchar](50)
,@IS_DRAFT [bit]
,@IS_ACTIVE [bit]
,@MODIFIED_BY [varchar](255)
,@MODIFIED_DT [datetime]
,@MODIFIED_HOST [varchar](255)

,@ARTICLE_ID [varchar](255) = ''
,@BALANCE_IN_VAULT [numeric] = null
,@SP_DATE [datetime] = null
,@PL_DATE [datetime] = null

,@PURPOSE [varchar](8000) = ''
,@CUSTOMER [varchar](255) = ''
AS
BEGIN
	UPDATE TR_IMPORTANT_ART
	SET
	REGIS_NO = @REGIS_NO
	--,CREATOR = @CREATOR
	--,CREATOR_DEPT = @CREATOR_DEPT
	--,CREATOR_UNIT = @CREATOR_UNIT
	--,CREATOR_ROLE = @CREATOR_ROLE
	,REQUEST_DT = @REQUEST_DT
	--,CUST_BRANCH = @CUST_BRANCH
	--,ARTICLE_NAME = @ARTICLE_NAME
	--,REGIS_TYPE = @REGIS_TYPE
	,BALANCE_IN_HAND = @BALANCE_IN_HAND
	,BALANCE_IN_VAULT = @BALANCE_IN_VAULT
	,VALUE = @VALUE
	,[DATE] = @DATE
	,[PURPOSE] = @PURPOSE
	,[CUSTOMER] = @CUSTOMER
	,[SP_DATE] = @SP_DATE
	,[PL_DATE] = @PL_DATE
	,REF_NO = @REF_NO
	,REMARKS = @REMARKS
	,ART_STATUS = @ART_STATUS
	,APPROVAL_STS = @APPROVAL_STS
	,IS_DRAFT = @IS_DRAFT
	--,IS_ACTIVE = @IS_ACTIVE
	,MODIFIED_BY = @MODIFIED_BY
	,MODIFIED_DT = @MODIFIED_DT
	,MODIFIED_HOST = @MODIFIED_HOST

	WHERE UID = @UID

	SELECT @UID
END




GO
/****** Object:  StoredProcedure [dbo].[UDPU_DRAFTIMPORTANTARTACTIVITY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_DRAFTIMPORTANTARTACTIVITY]
 @UID int
,@DOC_ID int
,@REGIS_NO varchar(255)
,@ACTIVITY_CD varchar(255)
,@CREATOR varchar(255)
,@CREATOR_DEPT varchar(255)
,@CREATOR_UNIT varchar(255)
,@CREATOR_ROLE varchar(255)
,@REQUEST_DT DATETIME
,@CORR_BALANCE_IN_HAND decimal
,@CORR_VALUE decimal
,@CORR_DATE date
,@CORR_REF_NO varchar(255)
,@CORR_REMARKS varchar(8000)
,@CAN_REASON varchar(8000)
,@INSP_DP DATE
,@INSP_BY varchar(255)
,@APPROVAL_STS varchar(255)
,@IS_DRAFT bit
,@IS_ACTIVE bit
,@MODIFIED_BY varchar(255)
,@MODIFIED_DT DATETIME
,@MODIFIED_HOST varchar(255) 
AS
BEGIN

	-- Only Correction has a function for edit/update 
	UPDATE TR_IMPORTANT_ART_ACTIVITY
	SET
	 --DOC_ID = @DOC_ID
	--,REGIS_NO = @REGIS_NO
	--,ACTIVITY_CD = @ACTIVITY_CD
	--,CREATOR = @CREATOR
	--,CREATOR_DEPT = @CREATOR_DEPT
	--,CREATOR_UNIT = @CREATOR_UNIT
	--,CREATOR_ROLE = @CREATOR_ROLE
	--,REQUEST_DT = @REQUEST_DT
	 CORR_BALANCE_IN_HAND = @CORR_BALANCE_IN_HAND
	,CORR_VALUE = @CORR_VALUE
	,CORR_DATE = @CORR_DATE
	,CORR_REF_NO = @CORR_REF_NO
	,CORR_REMARKS = @CORR_REMARKS
	--,CAN_REASON = @CAN_REASON
	--,INSP_DP = @INSP_DP
	--,INSP_BY = @INSP_BY
	,APPROVAL_STS = @APPROVAL_STS
	,IS_DRAFT = @IS_DRAFT
	--,IS_ACTIVE = @IS_ACTIVE
	,MODIFIED_BY = @MODIFIED_BY
	,MODIFIED_DT = @MODIFIED_DT
	,MODIFIED_HOST = @MODIFIED_HOST

	WHERE UID = @UID


	SELECT @UID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_EDITIMPORTANTART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_EDITIMPORTANTART]
 @UID [bigint]
,@REGIS_NO [varchar](50)
,@CREATOR [varchar](50)
,@CREATOR_DEPT [varchar](30)
,@CREATOR_UNIT [varchar](30)
,@CREATOR_ROLE [varchar](30)
,@REQUEST_DT [date]
,@CUST_BRANCH [varchar](255)
,@ARTICLE_NAME [varchar](255)
,@REGIS_TYPE [varchar](255)
,@BALANCE_IN_HAND [numeric]
,@VALUE [numeric]
,@DATE [date]
,@REF_NO [varchar](255)
,@REMARKS [varchar](8000)
,@ART_STATUS [varchar](8000)
,@APPROVAL_STS [varchar](50)
,@IS_DRAFT [bit]
,@IS_ACTIVE [bit]
,@MODIFIED_BY [varchar](255)
,@MODIFIED_DT [datetime]
,@MODIFIED_HOST [varchar](255)
AS
BEGIN
	UPDATE TR_IMPORTANT_ART
	SET
	-- REGIS_NO = @REGIS_NO
	--,CREATOR = @CREATOR
	--,CREATOR_DEPT = @CREATOR_DEPT
	--,CREATOR_UNIT = @CREATOR_UNIT
	--,CREATOR_ROLE = @CREATOR_ROLE
	REQUEST_DT = @REQUEST_DT
	--,CUST_BRANCH = @CUST_BRANCH
	--,ARTICLE_NAME = @ARTICLE_NAME
	--,REGIS_TYPE = @REGIS_TYPE
	,BALANCE_IN_HAND = @BALANCE_IN_HAND
	,VALUE = @VALUE
	,[DATE] = @DATE
	,REF_NO = @REF_NO
	,REMARKS = @REMARKS
	,ART_STATUS = @ART_STATUS
	--,APPROVAL_STS = @APPROVAL_STS
	--,IS_DRAFT = @IS_DRAFT
	--,IS_ACTIVE = @IS_ACTIVE
	,MODIFIED_BY = @MODIFIED_BY
	,MODIFIED_DT = @MODIFIED_DT
	,MODIFIED_HOST = @MODIFIED_HOST
	WHERE UID = @UID

	SELECT @UID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_EDITIMPORTANTARTICLEACTIVITY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_EDITIMPORTANTARTICLEACTIVITY]
 @UID int
,@DOC_ID int
,@REGIS_NO varchar(255)
,@ACTIVITY_CD varchar(255)
,@CREATOR varchar(255)
,@CREATOR_DEPT varchar(255)
,@CREATOR_UNIT varchar(255)
,@CREATOR_ROLE varchar(255)
,@REQUEST_DT DATETIME
,@CORR_BALANCE_IN_HAND decimal
,@CORR_VALUE decimal
,@CORR_DATE date
,@CORR_REF_NO varchar(255)
,@CORR_REMARKS varchar(8000)
,@CAN_REASON varchar(8000)
,@INSP_DP DATE
,@INSP_BY varchar(255)
,@APPROVAL_STS varchar(255)
,@IS_DRAFT bit
,@IS_ACTIVE bit
,@MODIFIED_BY varchar(255)
,@MODIFIED_DT DATETIME
,@MODIFIED_HOST varchar(255) 
AS
BEGIN

	-- Only Correction has a function for edit/update 
	UPDATE TR_IMPORTANT_ART_ACTIVITY
	SET
	 --DOC_ID = @DOC_ID
	--,REGIS_NO = @REGIS_NO
	--,ACTIVITY_CD = @ACTIVITY_CD
	--,CREATOR = @CREATOR
	--,CREATOR_DEPT = @CREATOR_DEPT
	--,CREATOR_UNIT = @CREATOR_UNIT
	--,CREATOR_ROLE = @CREATOR_ROLE
	--,REQUEST_DT = @REQUEST_DT
	 CORR_BALANCE_IN_HAND = @CORR_BALANCE_IN_HAND
	,CORR_VALUE = @CORR_VALUE
	,CORR_DATE = @CORR_DATE
	,CORR_REF_NO = @CORR_REF_NO
	,CORR_REMARKS = @CORR_REMARKS
	--,CAN_REASON = @CAN_REASON
	--,INSP_DP = @INSP_DP
	--,INSP_BY = @INSP_BY
	,APPROVAL_STS = @APPROVAL_STS
	,IS_DRAFT = @IS_DRAFT
	--,IS_ACTIVE = @IS_ACTIVE
	,MODIFIED_BY = @MODIFIED_BY
	,MODIFIED_DT = @MODIFIED_DT
	,MODIFIED_HOST = @MODIFIED_HOST

	WHERE UID = @UID


	SELECT @UID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_IMPORTANTART_STAGE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_IMPORTANTART_STAGE]
@UID INT,
@ART_STATUS VARCHAR(255),
@MODIFIED_BY VARCHAR(255),
@MODIFIED_DT DATETIME,	
@MODIFIED_HOST VARCHAR(255)
AS
BEGIN
	UPDATE A
	SET A.ART_STATUS = @ART_STATUS,
	A.MODIFIED_BY = @MODIFIED_BY,
	A.MODIFIED_DT = @MODIFIED_DT,
	A.MODIFIED_HOST = @MODIFIED_HOST
	FROM TR_IMPORTANT_ART A
	WHERE UID = @UID

	SELECT REGIS_NO 
	FROM TR_IMPORTANT_ART
	WHERE UID = @UID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_LETTERPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPU_LETTERPROFILE]
	-- Add the parameters for the stored procedure here
	@UID INT
	,@LETTER_TYPE VARCHAR(255)
	,@DESCS VARCHAR(255)
	,@DEPT_LETTER VARCHAR(8000)
	,@IS_ACTIVE BIT
	,@Modified_By VARCHAR(50)
	,@Modified_DT DATETIME
	,@Modified_Host VARCHAR(20)
AS
BEGIN
SET NOCOUNT ON;
    -- Insert statements for procedure here


    -- Insert statements for procedure here
	UPDATE dbo.MS_LETTER_PROFILE SET
	
	    [LETTER_TYPE] = @LETTER_TYPE,
		[DESCS] = @DESCS,
		[DEPT_LETTER] = @DEPT_LETTER,
		[IS_ACTIVE] = @IS_ACTIVE,
		[MODIFIED_BY] = @MODIFIED_BY,
		[MODIFIED_DT] = @MODIFIED_DT,
		[MODIFIED_HOST] = @MODIFIED_HOST
	
	WHERE [UID] = @UID

		SELECT @UID


END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_OUTGOINGLETTER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_OUTGOINGLETTER]
	@Id INT,
	@RegistrationNo VARCHAR(2555),
	@Creator VARCHAR(50),
	@CreatorDept VARCHAR(30),
	@CreatorUnit VARCHAR(100),
	@CreatorRole VARCHAR(30),
	@RequestDate DATETIME,
	@OnBehalfOf VARCHAR(50),
	@BranchCode VARCHAR(50),
	@LetterType VARCHAR(255),
	@CustomerCode VARCHAR(255),
	@AccountNo VARCHAR(255),
	@VirtualAccount VARCHAR(255),
	@LetCreatedDate DATETIME,
	@Subject VARCHAR(255),
	@SubjectEng VARCHAR(255),
	@Attention VARCHAR(255),
	@Location VARCHAR(8000),
	@Address VARCHAR(MAX),
	@Remarks VARCHAR(MAX),
	@SubmitDate DATETIME,
	@SubmitBy VARCHAR(50),
	@SubmitMethod VARCHAR(50),
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;


	UPDATE dbo.TR_OUTGOING_LETTER
	SET REGIS_NO = @RegistrationNo,
		CREATOR = @Creator,
		CREATOR_DEPT = @CreatorDept,
		CREATOR_UNIT = @CreatorUnit,
		CREATOR_ROLE = @CreatorRole,
		REQUEST_DT = @RequestDate,
		ON_BEHALF_OF = @OnBehalfOf,
		BRANCH_CD = @BranchCode,
		LETTER_TYPE = 'Others',
		CUST_CD = @CustomerCode,
		CUST_ACC_NO = @AccountNo,
		VIRTUAL_ACC = @VirtualAccount,
		LET_CREATED_DT = @LetCreatedDate,
		SUBJECT = @Subject,
		SUBJECT_ENG = @SubjectEng,
		ATTENTION = @Attention,
		LOCATION = @Location,
		ADDRESS = @Address,
		REMARKS = @Remarks,
		SUBMIT_BY = @SubmitBy,
		SUBMIT_DT = @SubmitDate,
		SUBMIT_METHOD = @SubmitMethod,
		LETTER_STS = 'Active',
		APPROVAL_STS='Awaiting Approval',
		IS_DRAFT=CASE WHEN @SubmitType='Save' THEN 1 ELSE 0 END,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE [UID] = @Id

	SELECT @Id

END

GO
/****** Object:  StoredProcedure [dbo].[UDPU_OUTGOINGLETTER_APPSTATUS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_OUTGOINGLETTER_APPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_OUTGOING_LETTER
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_PARAMETER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_PARAMETER]
	-- Add the parameters for the stored procedure here
	@Id INT
	,@Group VARCHAR(3000)
	,@Keyfield VARCHAR(30)
	,@Value VARCHAR(3000)
	,@Description VARCHAR(3000)
	,@Sequence INT
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	UPDATE dbo.MS_PARAMETER
	SET [GROUP] = @Group
		,KEYFIELD = @Keyfield
		,VALUE = @Value
		,DESCS = @Description
		,SEQ = @Sequence
		,MODIFIED_BY = @ModifiedBy
		,MODIFIED_DT = @ModifiedDate
		,MODIFIED_HOST = @ModifiedHost
	WHERE [UID] = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_POLICEREPORT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_POLICEREPORT]
	@Id INT,
	@RegistrationNo VARCHAR(2555),
	@PolrReceivedDate DATETIME,
	@PolrReceivedBy VARCHAR(8000),
	@PolrReceivedRemarks VARCHAR(MAX),
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.TR_BOOK
	SET 
		POLR_RECEIVED_DT = @PolrReceivedDate,
		POLR_RECEIVED_BY = @PolrReceivedBy,
		POLR_RECEIVED_REMARKS = @PolrReceivedRemarks,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE [UID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_RECEIVENOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_RECEIVENOTICE]
	@Id INT,
	@DocId INT,
	@RegistrationNo VARCHAR(2555),
	@Creator VARCHAR(50),
	@CreatorDept VARCHAR(30),
	@CreatorUnit VARCHAR(100),
	@CreatorRole VARCHAR(30),
	@RequestDate DATETIME,
    @ReceivedDate DATETIME,
	@ReceivedMethod VARCHAR(30),
	@ReceivedBy VARCHAR(30),
	@Remarks VARCHAR(8000),
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
	BEGIN
	
	-- Insert statements for procedure here
	UPDATE [TR_BOOK_RECEIVED_NOTICE] SET
		[DOC_ID]=@DocId,
		[REGIS_NO]=@RegistrationNo,
		CREATOR = @Creator,
		CREATOR_DEPT = @CreatorDept,
		CREATOR_UNIT = @CreatorUnit,
		CREATOR_ROLE = @CreatorRole,
		REQUEST_DT = @RequestDate,
		[REMARKS]=@Remarks,
		APPROVAL_STS='Awaiting Approval',
		IS_DRAFT=CASE WHEN @SubmitType='Save' THEN 1 ELSE 0 END,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE UID = @Id

	SELECT @Id
		
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_REGISTRATION]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_REGISTRATION]
	@Id INT,
	@RegistrationNo VARCHAR(2555),
	@BookType VARCHAR(50),
	@Creator VARCHAR(50),
	@CreatorDept VARCHAR(30),
	@CreatorUnit VARCHAR(100),
	@CreatorRole VARCHAR(30),
	@RequestDate DATETIME,
	@BranchCode VARCHAR(50),
	@ReportedDate DATETIME,
	@Type VARCHAR(50),
	@ReceivedMethod VARCHAR(30),
	@CustomerCode VARCHAR(255),
	@AccountNo VARCHAR(255),
	@HasAmount BIT,
	@AmountCurrency VARCHAR(255),
	@Amount NUMERIC(18,2),
	@Remarks VARCHAR(8000),
	@PolrReceivedDate DATETIME,
	@PolrReceivedBy VARCHAR(8000),
	@PolrReceivedRemarks VARCHAR(MAX),
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	--IF(@SubmitType='Submit' AND ISNULL(@RegistrationNo,'') = '')
	--BEGIN
	--	EXEC UDPS_GETNEWREGISNO @RegistrationNo out, @BookType, @CreatorDept
	--END

	UPDATE dbo.TR_BOOK
	SET REGIS_NO = @RegistrationNo,
		BOOK_TYPE = @BookType,
		CREATOR = @Creator,
		CREATOR_DEPT = @CreatorDept,
		CREATOR_UNIT = @CreatorUnit,
		CREATOR_ROLE = @CreatorRole,
		REQUEST_DT = @RequestDate,
		BRANCH_CD = @BranchCode,
		REPORTED_DT = @ReportedDate,
		TYPE = @Type,
		RECEIVED_METHOD = @ReceivedMethod,
		CUSTOMER_CD = @CustomerCode,
		ACCOUNT_NO = @AccountNo,
		HAS_AMOUNT = @HasAmount,
		AMOUNT_CURR = @AmountCurrency,
		AMOUNT = @Amount,
		REMARKS = @Remarks,
		POLR_RECEIVED_DT = @PolrReceivedDate,
		POLR_RECEIVED_BY = @PolrReceivedBy,
		POLR_RECEIVED_REMARKS = @PolrReceivedRemarks,
		APPROVAL_STS='Awaiting Approval',
		IS_DRAFT=CASE WHEN @SubmitType='Save' THEN 1 ELSE 0 END,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE [UID] = @Id

	SELECT @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_RESTOREBOOK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_RESTOREBOOK]
	@Id INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.TR_BOOK
	SET IS_ACTIVE = 1,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_RESTORERECEIVEDNOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_RESTORERECEIVEDNOTICE]
	@Id INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.TR_BOOK_RECEIVED_NOTICE
	SET IS_ACTIVE = 1,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE UID = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[UDPU_RESTORERECEIVENOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_RESTORERECEIVENOTICE]
	@Id INT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.TR_BOOK_RECEIVED_NOTICE
	SET IS_ACTIVE = 1,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE UID = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[UDPU_STOCKPROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPU_STOCKPROFILE]
	-- Add the parameters for the stored procedure here
	@UID INT
	,@ARTICLE_ID VARCHAR(255)
	,@DEPT_OF_ARTICLE VARCHAR(255)
	,@STOCK_IN_VAULT NUMERIC (18,2)
	,@STOCK_IN_HAND NUMERIC (18,2)
	,@USAGE NUMERIC (18,2)
	,@INCOMING_STOCK NUMERIC (18,2) 
	,@CORRECTION_IN_VAULT NUMERIC (18,2)
	,@CORRECTION_IN_HAND NUMERIC (18,2)
	,@CORRECTION_IN_USAGE NUMERIC (18,2)
	,@BALANCE_IN_HAND NUMERIC (18,2)
	,@MODIFIED_BY VARCHAR(50)
	,@MODIFIED_DT DATETIME
	,@MODIFIED_HOST VARCHAR(20)
AS
BEGIN
SET NOCOUNT ON;
	UPDATE dbo.MS_STOCK_PROFILE SET
	
	     ARTICLE_ID  = @ARTICLE_ID,
		 DEPT_OF_ARTICLE  = @DEPT_OF_ARTICLE,
		 STOCK_IN_VAULT  = @STOCK_IN_VAULT,
		 STOCK_IN_HAND  = @STOCK_IN_HAND,
		 USAGE  = @USAGE,
		 INCOMING_STOCK  = @INCOMING_STOCK,
		 CORRECTION_IN_VAULT  = @CORRECTION_IN_VAULT,
		 CORRECTION_IN_HAND  = @CORRECTION_IN_HAND,
		 CORRECTION_IN_USAGE  = @CORRECTION_IN_USAGE,
		 BALANCE_IN_HAND  = @BALANCE_IN_HAND,
		 MODIFIED_BY  = @MODIFIED_BY,
		 MODIFIED_DT  = @MODIFIED_DT,
		 MODIFIED_HOST  = @MODIFIED_HOST
	
	WHERE UID = @UID

	SELECT @UID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_STOCKPROFILEVALUE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_STOCKPROFILEVALUE]
 @DocID int
,@ArticleID varchar(255)
,@RegistrationType varchar(255)
,@Value decimal = 0
,@IsCorrection bit = 0
,@IsCancellation bit = 0
,@IsRejected bit = 0
,@DeptProfile varchar(50) = 'SKAI'
AS
BEGIN
	IF (@IsRejected = 1)
	BEGIN
		SET @Value = @Value * -1;
	END

	IF (@RegistrationType = 'Usage')
	BEGIN
		IF (@IsCorrection = 1)
		BEGIN
			UPDATE MS_STOCK_PROFILE
			SET BALANCE_IN_HAND = BALANCE_IN_HAND - @Value,
			CORRECTION_IN_USAGE = CORRECTION_IN_USAGE + @Value
			WHERE ARTICLE_ID = @ArticleID
			AND DEPT_OF_ARTICLE = @DeptProfile
		END
		ELSE IF (@IsCancellation = 1)
		BEGIN
			UPDATE MS_STOCK_PROFILE
			SET BALANCE_IN_HAND = BALANCE_IN_HAND + @Value,
			CORRECTION_IN_USAGE = CORRECTION_IN_USAGE - @Value
			--,USAGE = USAGE - @Value
			WHERE ARTICLE_ID = @ArticleID
			AND DEPT_OF_ARTICLE = @DeptProfile
		END
		ELSE
		BEGIN
			UPDATE MS_STOCK_PROFILE
			SET BALANCE_IN_HAND = BALANCE_IN_HAND - @Value,
			STOCK_IN_HAND = IIF((@Value >= 0 AND @IsRejected = 0) OR (@Value <= 0 AND @IsRejected = 1), STOCK_IN_HAND - @Value, STOCK_IN_HAND),
			USAGE = USAGE + @Value
			WHERE ARTICLE_ID = @ArticleID
			AND DEPT_OF_ARTICLE = @DeptProfile
		END 
		
	END
	ELSE IF (@RegistrationType = 'Stock In Vault')
	BEGIN
		IF (@IsCorrection = 1)
		BEGIN
			UPDATE MS_STOCK_PROFILE
			SET CORRECTION_IN_VAULT = CORRECTION_IN_VAULT + @Value
			WHERE ARTICLE_ID = @ArticleID
			AND DEPT_OF_ARTICLE = @DeptProfile
		END
		ELSE
		BEGIN
			UPDATE MS_STOCK_PROFILE
			SET STOCK_IN_VAULT = STOCK_IN_VAULT + @Value,
			INCOMING_STOCK = INCOMING_STOCK + @Value
			WHERE ARTICLE_ID = @ArticleID
			AND DEPT_OF_ARTICLE = @DeptProfile
		END
	END
	ELSE IF (@RegistrationType = 'Stock In Hand')
	BEGIN
		IF (@IsCorrection = 1)
		BEGIN
			UPDATE MS_STOCK_PROFILE
			SET BALANCE_IN_HAND = BALANCE_IN_HAND + @Value,
			CORRECTION_IN_HAND = CORRECTION_IN_HAND + @Value
			-- Confirm Mba eva, Di Document Salah Ketik.
			--CORRECTION_IN_USAGE = CORRECTION_IN_USAGE + @Value
			WHERE ARTICLE_ID = @ArticleID
			AND DEPT_OF_ARTICLE = @DeptProfile
		END
		ELSE
		BEGIN
			UPDATE MS_STOCK_PROFILE
			SET STOCK_IN_HAND = STOCK_IN_HAND + @Value,
			STOCK_IN_VAULT = STOCK_IN_VAULT - @Value
			WHERE ARTICLE_ID = @ArticleID
			AND DEPT_OF_ARTICLE = @DeptProfile
		END
	END
	
	SELECT UID FROM MS_STOCK_PROFILE 
	WHERE ARTICLE_ID = @ArticleID
	AND DEPT_OF_ARTICLE = @DeptProfile
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_SUBMITINFO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_SUBMITINFO]
	@Id INT,
	@RegistrationNo VARCHAR(2555),
	@SubmitDate DATETIME,
	@SubmitBy VARCHAR(200),
	@SubmitMethod VARCHAR(200),
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20),
	@SubmitType VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.TR_OUTGOING_LETTER
	SET 
		SUBMIT_DT = @SubmitDate,
		SUBMIT_BY = @SubmitBy,
		SUBMIT_METHOD = @SubmitMethod,
		MODIFIED_BY = @ModifiedBy,
		MODIFIED_DT = @ModifiedDate,
		MODIFIED_HOST = @ModifiedHost
	WHERE [UID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_WF_IMPORTANTART_APPSTATUS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_WF_IMPORTANTART_APPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_IMPORTANT_ART
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_WF_IMPORTANTART_CORR_APPSTATUS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_WF_IMPORTANTART_CORR_APPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_IMPORTANT_ART_ACTIVITY
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID

	--IF (@AppStatus = 'Approval Completed')
	--BEGIN

	--	UPDATE A
	--	SET 
	--	A.BALANCE_IN_HAND = B.CORR_BALANCE_IN_HAND,
	--	A.VALUE = B.CORR_VALUE,
	--	A.REF_NO = B.CORR_REF_NO,
	--	A.REMARKS = B.CORR_REMARKS,
	--	A.MODIFIED_BY = B.CREATED_BY,
	--	A.MODIFIED_DT = B.CREATED_DT,
	--	A.MODIFIED_HOST = B.CREATED_HOST
	--	FROM TR_IMPORTANT_ART A
	--	INNER JOIN TR_IMPORTANT_ART_ACTIVITY B
	--	ON A.UID = B.DOC_ID
	--	WHERE B.UID = @DOCID

	--END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_WF_OUTGOINGLETTER_APPSTATUS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_WF_OUTGOINGLETTER_APPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_OUTGOING_LETTER
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_WFPROCDETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_WFPROCDETAIL]
	-- add more stored procedure parameters here
	@UserList VARCHAR(8000),
	@WFStep INT,
	@WFID INT,
	@SN VARCHAR(20)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @TabUserList TABLE (
		ID INT IDENTITY(1, 1) NOT NULL,
		UserName VARCHAR(50)
		)
	DECLARE @CountUser INT

    IF charindex('JAKARTA01\', @UserList) > 0
		SET @UserList = replace(@UserList, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @UserList) > 0
		SET @UserList = replace(@UserList, 'jakarta01\', '')

	INSERT INTO @TabUserList (UserName)
	SELECT Name
	FROM dbo.splitstring(@UserList, ';')

	-- delete wf detail jika pindah step
	IF NOT EXISTS (
			SELECT 1
			FROM TR_WORKFLOWPROCESS_DETAIL
			WHERE WFID = @WFID
				AND WFSTEP = @WFStep
			)
	BEGIN
		DELETE TR_WORKFLOWPROCESS_DETAIL
		WHERE WFID = @WFID

		INSERT INTO TR_WORKFLOWPROCESS_DETAIL (
			WFID,
			WFSTEP,
			SN,
			ACTION_BY,
			CREATED_BY,
			CREATED_DT,
			CREATED_HOST
			)
		SELECT @WFID,
			@WFStep,
			'0',
			UserName,
			'SYSTEM',
			GETDATE(),
			NULL
		FROM @TabUserList
	END

	SELECT @CountUser = count(uid) + 1
	FROM TR_WORKFLOWPROCESS_DETAIL
	WHERE WFID = @WFID
		AND WFSTEP = @WFStep
		AND SN <> '0'

	UPDATE twd
	SET twd.SN = @SN
	FROM TR_WORKFLOWPROCESS_DETAIL AS twd
	JOIN @TabUserList AS tul ON twd.ACTION_BY = tul.UserName
	WHERE tul.ID = @CountUser AND twd.WFID = @WFID
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_WFPROCIDTASK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_WFPROCIDTASK]
	-- add more stored procedure parameters here
	@WFID BIGINT,
	@ProcID VARCHAR(59),
	@WFTASK VARCHAR(50),
	@WFSTEP VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_WORKFLOWPROCESS
	SET PROCID = @ProcID,
		WFTASKNAME = @WFTASK,
		WFSTEP = @WFSTEP,
		[MODIFIED_BY] = 'SYSTEM',
		MODIFIED_DT = GETDATE()
	WHERE [UID] = @WFID
END


GO
/****** Object:  Synonym [dbo].[EASY_Account_No]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[EASY_Account_No] FOR [JKTSVRDEV06].[EASY_Rebook].[dbo].[Mst_Account]
GO
/****** Object:  Synonym [dbo].[EASY_Customer_Code]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[EASY_Customer_Code] FOR [JKTSVRDEV06].[EASY_Rebook].[dbo].[Mst_Customer]
GO
/****** Object:  Synonym [dbo].[EASY_DOCUMENT_DETAIL]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[EASY_DOCUMENT_DETAIL] FOR [JKTSVRDEV06].[EASY_Rebook].[dbo].[mst_DetailDocument]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Department]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Department] FOR [vw_BTMUDirectory_Department]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Department_All]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Department_All] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Department]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Role]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Role] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Role]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Role_Parent]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Role_Parent] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Role_Parent]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Unit]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Unit] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Unit]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_User]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_User] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[User]
GO
/****** Object:  Synonym [dbo].[SN_CONE_Cone_User]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_CONE_Cone_User] FOR [JKTSVRDEV03].[Cone].[dbo].[Cone_User]
GO
/****** Object:  Synonym [dbo].[SN_EASY_Address]    Script Date: 16/03/2022 20:28:16 ******/
CREATE SYNONYM [dbo].[SN_EASY_Address] FOR [JKTSVRDEV06].[EASY_Rebook].[dbo].[mst_DetailAddress]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_EMAILREQUESTAPPROVALDEPTDH_REMINDER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_EMAILREQUESTAPPROVALDEPTDH_REMINDER] (
	@DocId BIGINT,
	@HistType VARCHAR(100),
	@Dept VARCHAR(50),
	@SN VARCHAR(50),
	@STEP VARCHAR(10)
	)
RETURNS VARCHAR(8000)
AS
BEGIN
	DECLARE @EmailBody VARCHAR(8000)
	DECLARE @PrevApprover VARCHAR(100) = ''
	DECLARE @CommentPrevApprover VARCHAR(8000) = ''
	DECLARE @DateAssignment VARCHAR(100)
	DECLARE @Path VARCHAR(100)
	DECLARE @UrlLink VARCHAR(100)
	DECLARE @CurrentApprover VARCHAR(2000) = ''

	SELECT @Path = VALUE
	FROM DBO.MS_PARAMETER
	WHERE [GROUP] = 'APP_URL'

	SELECT @UrlLink = CONCAT (
			@Path,
			CASE @HistType
				WHEN 'BOOK_CLAIM'
					THEN 'Claim'
				WHEN 'BOOK_NOTICE'
					THEN 'Claim'
				WHEN 'BOOK_REQUEST'
					THEN 'Claim'
				WHEN 'ACTION_TAKEN'
					THEN 'ActionTaken'
				WHEN 'RECEIVED_NOTICE'
					THEN 'ReceivedNotice'
				WHEN 'IMPORT_ARTICLE'
					THEN 'ImportantArticle'
				WHEN 'IMPORT_CORRECTION'
					THEN 'ImportantArticleCorrection'
				WHEN 'OUT_LETTER'
					THEN 'OutgoingLetter'
				END
			)

	SET @DateAssignment = convert(VARCHAR, getdate(), 107) + ' at ' + convert(VARCHAR, getdate(), 108)

	SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY, ''),
		@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') + ')<br/>' + COMMENT + '<br/><br/>', '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND REF_ID = @DOCID
	ORDER BY SEQ_NO DESC

	IF @STEP = 0
	BEGIN
		SELECT @UrlLink = @UrlLink + '/Revise?id=' + cast(@DOCID AS VARCHAR(5)) + '&sn=' + @SN + '&step=' + @STEP

		SELECT @EmailBody = REPLACE(REPLACE(REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover), '{{ ReviseDate }}', @DateAssignment), '{{ UrlLink }}', @UrlLink)
		FROM MS_PARAMETER
		WHERE [GROUP] = 'EMAIL_REQ_REV_SCH'
			AND KEYFIELD = 'CONTENT'
	END
	ELSE
	BEGIN
		SELECT @CurrentApprover = ISNULL(STUFF((
						SELECT DISTINCT ',' + RealName
						FROM vw_UserByDepartment
						WHERE DepartmentCode = @DEPT
							AND RoleShortName = 'DH'
						FOR XML PATH('')
						), 1, 1, ''), '')

		SELECT @UrlLink = @UrlLink + '/Approval?id=' + cast(@DOCID AS VARCHAR(5)) + '&sn=' + @SN + '&step=' + @STEP

		SELECT TOP 1 @PrevApprover = ISNULL(ACTION_BY, ''),
			@CommentPrevApprover = ISNULL('Comment from the previous approver (' + ISNULL(ACTION_BY, '') + ')<br/>' + COMMENT + '<br/><br/>', '')
		FROM TR_DOCUMENT_HISTORY
		WHERE HIST_TYPE = @HISTTYPE
			AND REF_ID = @DOCID
		ORDER BY SEQ_NO DESC

		SELECT @EmailBody = REPLACE(REPLACE(REPLACE(REPLACE(VALUE, '{{ CommentPrevApprover }}', @CommentPrevApprover), '{{ DateAssignment }}', @DateAssignment), '{{ CurrentApprover }}', @CurrentApprover), '{{ UrlLink }}', @UrlLink)
		FROM MS_PARAMETER
		WHERE [GROUP] = 'EMAIL_REQ_APRV_SCH'
			AND KEYFIELD = 'CONTENT'
	END

	RETURN @EmailBody
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_enamkar]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_enamkar](@str VARCHAR(50),@len INT,@char VARCHAR(10))
	RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @ret VARCHAR(50)

	IF(@len > LEN(@str))
		SET @ret = REPLICATE(@char, @len - LEN(@str)) + @str
	ELSE
		SET @ret = @str

	RETURN @ret
END


GO
/****** Object:  UserDefinedFunction [dbo].[splitstring]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[splitstring] (@stringToSplit VARCHAR(MAX), @stringDelimiter NVARCHAR(100))
RETURNS @returnList TABLE ([Name] [nvarchar](500))
AS
BEGIN
	DECLARE @name NVARCHAR(255)
	DECLARE @pos INT

	WHILE CHARINDEX(@stringDelimiter, @stringToSplit) > 0
	BEGIN
		SELECT @pos = CHARINDEX(@stringDelimiter, @stringToSplit)

		SELECT @name = SUBSTRING(@stringToSplit, 1, @pos - 1)

		INSERT INTO @returnList
		SELECT @name

		SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos + 1, LEN(@stringToSplit) - @pos)
	END

	INSERT INTO @returnList
	SELECT @stringToSplit

	RETURN
END



GO
/****** Object:  Table [dbo].[MS_ARTICLE_PROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_ARTICLE_PROFILE](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[ARTICLE_ID] [varchar](3000) NOT NULL,
	[NAME] [varchar](3000) NOT NULL,
	[DEPT_ARTICLE] [varchar](255) NOT NULL,
	[IS_STOCK_IN_VAULT] [bit] NOT NULL,
	[IS_STOCK_IN_HAND] [bit] NOT NULL,
	[IS_USAGE] [bit] NOT NULL,
	[IS_CORRECTION] [bit] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_MS_ARTICLE_PROFILE] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_BOOK_PROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_BOOK_PROFILE](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[BOOK_ID] [varchar](3000) NOT NULL,
	[BOOK_NAME] [varchar](3000) NOT NULL,
	[DEPT_BOOK] [varchar](255) NOT NULL,
	[BOOK_SEQ] [int] NOT NULL,
	[BEFORE_OFFSET_DAYS] [int] NULL,
	[AFTER_OFFSET_DAYS] [int] NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_MS_BOOK_PROFILE] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_LETTER_PROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_LETTER_PROFILE](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[LETTER_TYPE] [varchar](255) NOT NULL,
	[DESCS] [varchar](255) NOT NULL,
	[DEPT_LETTER] [varchar](255) NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_MS_LETTER_PROFILE] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_PARAMETER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_PARAMETER](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[GROUP] [varchar](3000) NULL,
	[KEYFIELD] [varchar](30) NULL,
	[VALUE] [varchar](3000) NULL,
	[DESCS] [varchar](3000) NULL,
	[SEQ] [int] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_MS_PARAMETER] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_PARAMETER_20220208]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_PARAMETER_20220208](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[GROUP] [varchar](3000) NULL,
	[KEYFIELD] [varchar](30) NULL,
	[VALUE] [varchar](3000) NULL,
	[DESCS] [varchar](3000) NULL,
	[SEQ] [int] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_REMINDER_SETTING]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_REMINDER_SETTING](
	[UID] [smallint] IDENTITY(1,1) NOT NULL,
	[FWD_REMINDER] [bit] NULL,
	[FWD_RECURR_DAYS] [int] NULL,
	[BWD_REMINDER] [bit] NULL,
	[BWD_STARTED_FROM] [int] NULL,
	[BWD_RECURR_DAYS] [int] NULL,
	[BWD_REMIND_DUE] [bit] NULL,
	[BWD_REMIND_DUE_MIN_ONE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[APV_REMINDER] [bit] NULL,
	[APV_RECURR_DAYS] [int] NULL,
 CONSTRAINT [PK_MS_REMINDER_SETTING] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_STOCK_PROFILE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_STOCK_PROFILE](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[ARTICLE_ID] [varchar](255) NOT NULL,
	[DEPT_OF_ARTICLE] [varchar](255) NOT NULL,
	[STOCK_IN_VAULT] [numeric](18, 2) NULL,
	[STOCK_IN_HAND] [numeric](18, 2) NULL,
	[USAGE] [numeric](18, 2) NULL,
	[INCOMING_STOCK] [numeric](18, 2) NULL,
	[CORRECTION_IN_VAULT] [numeric](18, 2) NULL,
	[CORRECTION_IN_HAND] [numeric](18, 2) NULL,
	[CORRECTION_IN_USAGE] [numeric](18, 2) NULL,
	[BALANCE_IN_HAND] [numeric](18, 2) NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_MS_STOCK_PROFILE] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_SYSTEM]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_SYSTEM](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[SYS_GROUP] [varchar](30) NOT NULL,
	[SYS_KEY] [varchar](30) NOT NULL,
	[SYS_VALUE] [varchar](5000) NOT NULL,
	[SYS_DESCS] [varchar](8000) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_MS_SYSTEM] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UX_MS_SYSTEM] UNIQUE NONCLUSTERED 
(
	[SYS_GROUP] ASC,
	[SYS_KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PR_GENREGNO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PR_GENREGNO](
	[BOOK_TYPE] [varchar](50) NOT NULL,
	[DEPT_CD] [varchar](50) NOT NULL,
	[PR_YEAR] [int] NOT NULL,
	[PR_DATE] [smalldatetime] NULL,
	[PR_SEQ] [int] NULL,
	[PR_FLAG] [bit] NULL,
 CONSTRAINT [PK_PR_GENREGNO] PRIMARY KEY CLUSTERED 
(
	[BOOK_TYPE] ASC,
	[DEPT_CD] ASC,
	[PR_YEAR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_ATTACHMENT]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_ATTACHMENT](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[REGIS_NO] [varchar](50) NULL,
	[FILE_TYPE] [varchar](20) NULL,
	[FILE_NM] [varchar](255) NULL,
	[SAVE_FILE_NM] [varchar](80) NULL,
	[FILE_URL] [varchar](200) NULL,
	[REF_ID] [bigint] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](100) NULL,
 CONSTRAINT [PK_TR_ATTACHMENT] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_BOOK]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_BOOK](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[REGIS_NO] [varchar](50) NOT NULL,
	[BOOK_TYPE] [varchar](20) NOT NULL,
	[CREATOR] [varchar](50) NOT NULL,
	[CREATOR_DEPT] [varchar](30) NOT NULL,
	[CREATOR_UNIT] [varchar](200) NULL,
	[CREATOR_ROLE] [varchar](30) NULL,
	[REQUEST_DT] [datetime] NOT NULL,
	[BRANCH_CD] [varchar](50) NOT NULL,
	[REPORTED_DT] [datetime] NOT NULL,
	[TYPE] [varchar](255) NOT NULL,
	[RECEIVED_METHOD] [varchar](255) NOT NULL,
	[CUSTOMER_CD] [varchar](255) NOT NULL,
	[ACCOUNT_NO] [varchar](255) NULL,
	[HAS_AMOUNT] [bit] NULL,
	[AMOUNT_CURR] [varchar](255) NULL,
	[AMOUNT] [numeric](18, 2) NULL,
	[REMARKS] [varchar](8000) NULL,
	[POLR_RECEIVED_DT] [date] NULL,
	[POLR_RECEIVED_BY] [varchar](50) NULL,
	[POLR_RECEIVED_REMARKS] [varchar](8000) NULL,
	[APPROVAL_STS] [varchar](20) NULL,
	[IS_DRAFT] [bit] NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[L_UNIV_ID] [varchar](50) NULL,
 CONSTRAINT [PK_TR_BOOK] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_BOOK_ACTION_TAKEN]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_BOOK_ACTION_TAKEN](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[REGIS_NO] [varchar](50) NOT NULL,
	[CREATOR] [varchar](50) NOT NULL,
	[CREATOR_DEPT] [varchar](30) NOT NULL,
	[CREATOR_UNIT] [varchar](200) NULL,
	[CREATOR_ROLE] [varchar](30) NULL,
	[REQUEST_DT] [date] NOT NULL,
	[ACT_DT] [datetime] NOT NULL,
	[ACT_ACTION] [varchar](255) NOT NULL,
	[ACTOR] [varchar](100) NOT NULL,
	[REMARKS] [varchar](8000) NULL,
	[RESULT] [varchar](8000) NOT NULL,
	[APPROVAL_STS] [varchar](50) NOT NULL,
	[IS_DRAFT] [bit] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[PARENT_DOC_ID] [varchar](50) NULL,
 CONSTRAINT [PK_TR_BOOK_ACTION_TAKEN] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_BOOK_RECEIVED_NOTICE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_BOOK_RECEIVED_NOTICE](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[REGIS_NO] [varchar](50) NOT NULL,
	[CREATOR] [varchar](50) NOT NULL,
	[CREATOR_DEPT] [varchar](30) NOT NULL,
	[CREATOR_UNIT] [varchar](200) NULL,
	[CREATOR_ROLE] [varchar](30) NULL,
	[REQUEST_DT] [date] NOT NULL,
	[RCV_DT] [datetime] NOT NULL,
	[RCV_METHOD] [varchar](255) NOT NULL,
	[RCV_BY] [varchar](100) NOT NULL,
	[REMARKS] [varchar](8000) NULL,
	[APPROVAL_STS] [varchar](50) NOT NULL,
	[IS_DRAFT] [bit] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[PARENT_DOC_ID] [varchar](50) NULL,
	[L_DOC_ID] [varchar](50) NULL,
 CONSTRAINT [PK_TR_BOOK_RECEIVED_NOTICE] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_DOCUMENT_HISTORY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_DOCUMENT_HISTORY](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[SEQ_NO] [int] NOT NULL,
	[HIST_TYPE] [varchar](20) NOT NULL,
	[REGIS_NO] [varchar](255) NOT NULL,
	[REF_ID] [bigint] NULL,
	[ACTION_BY] [varchar](50) NOT NULL,
	[ACTION_DT] [datetime] NOT NULL,
	[STATUS] [varchar](20) NOT NULL,
	[COMMENT] [varchar](2000) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[WFSTEP] [int] NULL,
	[L_DOC_ID] [varchar](50) NULL,
 CONSTRAINT [PK_TR_DOCUMENT_HISTORY] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_IMPORTANT_ART]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_IMPORTANT_ART](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[REGIS_NO] [varchar](50) NOT NULL,
	[CREATOR] [varchar](50) NOT NULL,
	[CREATOR_DEPT] [varchar](30) NOT NULL,
	[CREATOR_UNIT] [varchar](200) NULL,
	[CREATOR_ROLE] [varchar](30) NULL,
	[REQUEST_DT] [date] NOT NULL,
	[CUST_BRANCH] [varchar](255) NOT NULL,
	[ARTICLE_NAME] [varchar](255) NOT NULL,
	[REGIS_TYPE] [varchar](255) NOT NULL,
	[BALANCE_IN_HAND] [numeric](18, 2) NULL,
	[VALUE] [numeric](18, 0) NOT NULL,
	[DATE] [date] NOT NULL,
	[REF_NO] [varchar](255) NOT NULL,
	[REMARKS] [varchar](8000) NULL,
	[ART_STATUS] [varchar](8000) NOT NULL,
	[APPROVAL_STS] [varchar](50) NOT NULL,
	[IS_DRAFT] [bit] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[BALANCE_IN_VAULT] [numeric](18, 0) NULL,
	[ARTICLE_ID] [varchar](255) NULL,
	[SP_DATE] [datetime] NULL,
	[PL_DATE] [datetime] NULL,
	[CUSTOMER] [varchar](255) NULL,
	[PURPOSE] [varchar](8000) NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[PARENT_DOC_ID] [varchar](50) NULL,
 CONSTRAINT [PK_TR_IMPORTANT_ART] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_IMPORTANT_ART_ACTIVITY]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_IMPORTANT_ART_ACTIVITY](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[REGIS_NO] [varchar](50) NOT NULL,
	[ACTIVITY_CD] [varchar](20) NOT NULL,
	[CREATOR] [varchar](50) NOT NULL,
	[CREATOR_DEPT] [varchar](30) NOT NULL,
	[CREATOR_UNIT] [varchar](200) NULL,
	[CREATOR_ROLE] [varchar](30) NULL,
	[REQUEST_DT] [date] NOT NULL,
	[CORR_BALANCE_IN_HAND] [numeric](18, 2) NULL,
	[CORR_VALUE] [numeric](18, 2) NULL,
	[CORR_REF_NO] [varchar](255) NULL,
	[CORR_REMARKS] [varchar](8000) NULL,
	[CAN_REASON] [varchar](8000) NULL,
	[INSP_DP] [date] NULL,
	[INSP_BY] [varchar](255) NULL,
	[APPROVAL_STS] [varchar](50) NOT NULL,
	[IS_DRAFT] [bit] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[DESTROYED_DT] [datetime] NULL,
	[DESTROYED_BY] [varchar](50) NULL,
	[WITNESSED_BY] [varchar](50) NULL,
	[CORR_DATE] [date] NULL,
	[PARENT_DOC_ID] [varchar](50) NULL,
	[L_DOC_ID] [varchar](50) NULL,
 CONSTRAINT [PK_TR_IMPORTANT_ART_ACTIVITY] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_OUTGOING_LETTER]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_OUTGOING_LETTER](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[REGIS_NO] [varchar](50) NOT NULL,
	[CREATOR] [varchar](50) NOT NULL,
	[CREATOR_DEPT] [varchar](30) NOT NULL,
	[CREATOR_UNIT] [varchar](50) NULL,
	[CREATOR_ROLE] [varchar](30) NULL,
	[REQUEST_DT] [date] NOT NULL,
	[ON_BEHALF_OF] [varchar](50) NOT NULL,
	[BRANCH_CD] [varchar](50) NOT NULL,
	[LETTER_TYPE] [varchar](50) NULL,
	[CUST_CD] [varchar](100) NOT NULL,
	[CUST_ACC_NO] [varchar](100) NULL,
	[LET_CREATED_DT] [date] NOT NULL,
	[SUBJECT] [varchar](800) NULL,
	[ATTENTION] [varchar](800) NULL,
	[LOCATION] [varchar](255) NULL,
	[ADDRESS] [varchar](8000) NULL,
	[REMARKS] [varchar](8000) NULL,
	[SUBMIT_DT] [datetime] NULL,
	[SUBMIT_BY] [varchar](255) NULL,
	[SUBMIT_METHOD] [varchar](255) NULL,
	[LETTER_STS] [varchar](255) NOT NULL,
	[APPROVAL_STS] [varchar](50) NOT NULL,
	[IS_DRAFT] [bit] NOT NULL,
	[IS_ACTIVE] [bit] NOT NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NOT NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[DEST_DT] [datetime] NULL,
	[DEST_BY] [varchar](255) NULL,
	[DEST_WITNESS] [varchar](255) NULL,
	[SUBJECT_ENG] [varchar](255) NULL,
	[VIRTUAL_ACC] [varchar](100) NULL,
	[PARENT_DOC_ID] [varchar](50) NULL,
	[L_DOC_ID] [varchar](50) NULL,
 CONSTRAINT [PK_TR_OUTGOING_LETTER] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_REMINDER_LOG]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_REMINDER_LOG](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NOT NULL,
	[REGIS_NO] [varchar](255) NOT NULL,
	[TRX_TYPE] [varchar](30) NOT NULL,
	[SENT_ON] [datetime] NULL,
	[SENT_TO] [varchar](500) NOT NULL,
	[SENT_CC] [varchar](500) NULL,
	[MAIL_SUBJECT] [varchar](8000) NULL,
	[MAIL_BODY] [varchar](8000) NULL,
	[LAST_STATUS] [varchar](30) NULL,
	[CREATED_BY] [varchar](50) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_TR_REMINDER_LOG] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_USER_ACTIVITY_LOG_DTL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_USER_ACTIVITY_LOG_DTL](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[ACT_CD] [varchar](50) NULL,
	[FIELD_NM] [varchar](80) NULL,
	[OLD_VALUE] [varchar](8000) NULL,
	[NEW_VALUE] [varchar](8000) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[DOC_ID] [bigint] NULL,
 CONSTRAINT [PK_TR_USER_ACTIVITY_LOG_DTL] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_USER_ACTIVITY_LOG_HDR]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_USER_ACTIVITY_LOG_HDR](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[ACT_CD] [varchar](50) NULL,
	[ACT_DESC] [varchar](100) NULL,
	[ACT_VAL] [varchar](500) NULL,
	[ACT_RESULT] [varchar](50) NULL,
	[ACT_USER] [varchar](50) NULL,
	[ACT_DEPT] [varchar](100) NULL,
	[ACT_DT] [datetime] NULL,
	[ACT_WORKSTATION] [varchar](30) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[WFGUID] [uniqueidentifier] NULL,
 CONSTRAINT [PK_TR_USER_ACTIVITY_LOG_HDR] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_WORKFLOWPROCESS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_WORKFLOWPROCESS](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[DOCID] [bigint] NULL,
	[REFID] [bigint] NULL,
	[PROCID] [nvarchar](50) NULL,
	[SN] [nvarchar](50) NULL,
	[WFTASK] [nvarchar](50) NULL,
	[WFTASKNAME] [nvarchar](100) NULL,
	[WFACTION] [nvarchar](50) NULL,
	[WFSTEP] [nvarchar](100) NULL,
	[REGIS_NO] [nvarchar](50) NULL,
	[CREATED_BY] [nvarchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [nvarchar](50) NULL,
	[MODIFIED_BY] [nvarchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [nvarchar](50) NULL,
	[ISACTIVE] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TR_WORKFLOWPROCESS_DETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_WORKFLOWPROCESS_DETAIL](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[WFID] [bigint] NOT NULL,
	[WFSTEP] [int] NULL,
	[SN] [varchar](100) NULL,
	[ACTION_BY] [varchar](255) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_TR_WORKFLOWPROCESS_DETAIL] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[VW_EASYCUSTOMERCODE]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[VW_EASYCUSTOMERCODE]
AS
	SELECT OfficeCode AS OfficeCode, CustomerCode AS CustomerCode, AOCode AS AOCode, AliasName AS AliasName, LegalName AS LegalName, ActiveStatus AS ActiveStatus, OtherBankName AS BankName, 
	otherbankcurrency AS Currency, MASTERCIFCUSTCODE AS CIF
	FROM DBO.EASY_Customer_Code
	WHERE ActiveStatus = 'A'





GO
/****** Object:  View [dbo].[VW_DRAFTLIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_DRAFTLIST]
AS	
	SELECT BK.UID AS Id,
		BK.UID AS DocId,
		isnull(BK.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS TaskCategory,
		isnull(FORM.VALUE, '') AS TaskCategoryDesc,
		isnull(BK.CUSTOMER_CD, '') AS CustomerAttention,
		isnull(CC.LegalName, '-') AS CustomerAttentionDesc,
		isnull(BK.CREATOR, '') AS DraftBy,
		BK.CREATED_DT AS DraftDate,
		NULL AS TaskCode,
		NULL AS TaskName,
		NULL AS ActionCode,
		NULL AS ActionName,
		NULL AS RequestBy,
		NULL AS RequestDate,
		NULL AS SubmittedBy,
		NULL AS SubmittedDate
		, BK.CREATOR_DEPT AS CreatorDept
		, appurl.[VALUE] + 'Claim/Draft/' + CAST(BK.UID AS VARCHAR(10)) AS [UrlDetail]
		, appurl.[VALUE] + 'Claim/Delete/' + CAST(BK.UID AS VARCHAR(10)) AS [UrlDelete]
		, 'Claim' AS ControllerName
	FROM dbo.TR_BOOK AS BK
		JOIN dbo.MS_PARAMETER FORM ON FORM.[GROUP]='BOOK_TYPE' AND FORM.KEYFIELD=BK.BOOK_TYPE
		LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUSTOMER_CD = CC.CustomerCode
		LEFT JOIN MS_PARAMETER appurl ON appurl.[GROUP] = 'APP_URL'
	WHERE BK.IS_ACTIVE = 1 AND BK.IS_DRAFT = 1
	UNION ALL
	SELECT AT.UID AS Id,
		AT.DOC_ID AS DocId,
		isnull(AT.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS TaskCategory,
		'Action Taken' AS TaskCategoryDesc,
		isnull(BK.CUSTOMER_CD, '') AS CustomerAttention,
		isnull(CC.LegalName, '-') AS CustomerAttentionDesc,
		isnull(AT.CREATOR, '') AS DraftBy,
		AT.CREATED_DT AS DraftDate,
		NULL AS TaskCode,
		NULL AS TaskName,
		NULL AS ActionCode,
		NULL AS ActionName,
		NULL AS RequestBy,
		NULL AS RequestDate,
		NULL AS SubmittedBy,
		NULL AS SubmittedDate
		, AT.CREATOR_DEPT AS CreatorDept
		, appurl.[VALUE] + 'ActionTaken/Draft/' + CAST(AT.UID AS VARCHAR(10)) AS [UrlDetail]
		, appurl.[VALUE] + 'ActionTaken/Delete/' + CAST(AT.UID AS VARCHAR(10)) AS [UrlDelete]
		, 'ActionTaken' AS ControllerName
	FROM dbo.TR_BOOK_ACTION_TAKEN AS AT
		JOIN TR_BOOK BK ON BK.UID = AT.DOC_ID
		LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUSTOMER_CD = CC.CustomerCode
		LEFT JOIN MS_PARAMETER appurl ON appurl.[GROUP] = 'APP_URL'
	WHERE AT.IS_ACTIVE = 1 AND AT.IS_DRAFT = 1
	UNION ALL
	SELECT RN.UID AS Id,
		RN.DOC_ID AS DocId,
		isnull(RN.REGIS_NO, '') AS RegistrationNo,
		isnull(BK.BOOK_TYPE, '') AS TaskCategory,
		'Received Notice ' AS TaskCategoryDesc,
		isnull(BK.CUSTOMER_CD, '') AS CustomerAttention,
		isnull(CC.LegalName, '-') AS CustomerAttentionDesc,
		isnull(RN.CREATOR, '') AS DraftBy,
		RN.CREATED_DT AS DraftDate,
		NULL AS TaskCode,
		NULL AS TaskName,
		NULL AS ActionCode,
		NULL AS ActionName,
		NULL AS RequestBy,
		NULL AS RequestDate,
		NULL AS SubmittedBy,
		NULL AS SubmittedDate
		, RN.CREATOR_DEPT AS CreatorDept
		, appurl.[VALUE] + 'ReceivedNotice/Draft/' + CAST(RN.UID AS VARCHAR(10)) AS [UrlDetail]
		, appurl.[VALUE] + 'ReceivedNotice/Delete/' + CAST(RN.UID AS VARCHAR(10)) AS [UrlDelete]
		, 'ReceivedNotice' AS ControllerName
	FROM dbo.TR_BOOK_RECEIVED_NOTICE AS RN
		JOIN TR_BOOK BK ON BK.UID = RN.DOC_ID
		LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUSTOMER_CD = CC.CustomerCode
		LEFT JOIN MS_PARAMETER appurl ON appurl.[GROUP] = 'APP_URL'
	WHERE RN.IS_ACTIVE = 1 AND RN.IS_DRAFT = 1
	UNION ALL
	SELECT IA.UID AS Id,
		IA.UID AS DocId,
		isnull(IA.REGIS_NO, '') AS RegistrationNo,
		'IMPORT_ARTICLE' AS TaskCategory,
		'Important Article' AS TaskCategoryDesc,
		ISNULL(IA.CUSTOMER,'-') AS CustomerAttention,
		ISNULL(IA.CUSTOMER,'-') AS CustomerAttentionDesc,
		isnull(IA.CREATOR, '') AS DraftBy,
		IA.CREATED_DT AS DraftDate,
		NULL AS TaskCode,
		NULL AS TaskName,
		NULL AS ActionCode,
		NULL AS ActionName,
		NULL AS RequestBy,
		NULL AS RequestDate,
		NULL AS SubmittedBy,
		NULL AS SubmittedDate
		, IA.CREATOR_DEPT AS CreatorDept
		, appurl.[VALUE] + 'ImportantArticle/Draft/' + CAST(IA.UID AS VARCHAR(10)) AS [UrlDetail]
		, appurl.[VALUE] + 'ImportantArticle/Delete/' + CAST(IA.UID AS VARCHAR(10)) AS [UrlDelete]
		, 'ImportantArticle' AS ControllerName
	FROM dbo.TR_IMPORTANT_ART AS IA
		LEFT JOIN MS_PARAMETER appurl ON appurl.[GROUP] = 'APP_URL'
	WHERE IA.IS_ACTIVE = 1 AND IA.IS_DRAFT = 1
	UNION ALL
	SELECT IAA.UID AS Id,
		IAA.DOC_ID AS DocId,
		isnull(IAA.REGIS_NO, '') AS RegistrationNo,
		'IMPORT_ARTICLE' AS TaskCategory,
		'Important Article Correction' AS TaskCategoryDesc,
		ISNULL(IA.CUSTOMER,'-') AS CustomerAttention,
		ISNULL(IA.CUSTOMER,'-') AS CustomerAttentionDesc,
		isnull(IAA.CREATOR, '') AS DraftBy,
		IAA.CREATED_DT AS DraftDate,
		NULL AS TaskCode,
		NULL AS TaskName,
		NULL AS ActionCode,
		NULL AS ActionName,
		NULL AS RequestBy,
		NULL AS RequestDate,
		NULL AS SubmittedBy,
		NULL AS SubmittedDate
		, IAA.CREATOR_DEPT AS CreatorDept
		, appurl.[VALUE] + 'ImportantArticleCorrection/Draft/' + CAST(IA.UID AS VARCHAR(10)) AS [UrlDetail]
		, appurl.[VALUE] + 'ImportantArticleCorrection/Delete/' + CAST(IA.UID AS VARCHAR(10)) AS [UrlDelete]
		, 'ImportantArticleCorrection' AS ControllerName
	FROM dbo.TR_IMPORTANT_ART_ACTIVITY AS IAA
		JOIN dbo.TR_IMPORTANT_ART IA ON IA.UID = IAA.DOC_ID
		LEFT JOIN MS_PARAMETER appurl ON appurl.[GROUP] = 'APP_URL'
	WHERE IAA.IS_ACTIVE = 1 AND IAA.IS_DRAFT = 1
	UNION ALL
	SELECT OL.UID AS Id,
		OL.UID AS DocId,
		isnull(OL.REGIS_NO, '') AS RegistrationNo,
		'OUT_LETTER' AS TaskCategory,
		'Outgoing Letter' AS TaskCategoryDesc,
		isnull(OL.CUST_CD, '') AS CustomerAttention,
		isnull(CC.LegalName, '-') AS CustomerAttentionDesc,
		isnull(OL.CREATOR, '') AS DraftBy,
		OL.CREATED_DT AS DraftDate,
		NULL AS TaskCode,
		NULL AS TaskName,
		NULL AS ActionCode,
		NULL AS ActionName,
		NULL AS RequestBy,
		NULL AS RequestDate,
		NULL AS SubmittedBy,
		NULL AS SubmittedDate
		, OL.CREATOR_DEPT AS CreatorDept
		, appurl.[VALUE] + 'OutgoingLetter/Draft/' + CAST(OL.UID AS VARCHAR(10)) AS [UrlDetail]
		, appurl.[VALUE] + 'OutgoingLetter/Delete/' + CAST(OL.UID AS VARCHAR(10)) AS [UrlDelete]
		, 'OutgoingLetter' AS ControllerName
	FROM dbo.TR_OUTGOING_LETTER AS OL
		LEFT JOIN VW_EASYCUSTOMERCODE CC ON OL.CUST_CD = CC.CustomerCode
		LEFT JOIN MS_PARAMETER appurl ON appurl.[GROUP] = 'APP_URL'
	WHERE OL.IS_ACTIVE = 1 AND OL.IS_DRAFT = 1



GO
/****** Object:  View [dbo].[VW_SUBMITTEDLIST]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_SUBMITTEDLIST]
AS
SELECT BK.[UID] AS Id,
	FORM.VALUE AS TaskCategory,
	BK.BOOK_TYPE AS TaskCategoryDesc,
	tw.WFTASKNAME AS TaskName,
	tw.WFTASK AS Task,
	BK.CUSTOMER_CD AS Customer,
	CC.LegalName AS CustomerAttentionDesc,
	tw.REGIS_NO AS RegistrationNo,
	tw.WFACTION AS [Action],
	tw.CREATED_BY AS RequestedBy,
	tw.CREATED_DT AS RequestedDate,
	tw.PROCID AS ProcessId,
	tw.[SN] AS SerialNumber,
	mp.[VALUE] + 'Claim/Detail/' + CAST(tw.DOCID AS VARCHAR(10)) AS [URL],
	mp2.[VALUE] + CAST(isnull(tw.PROCID, 0) AS VARCHAR(10)) AS [UrlFlow],
	BK.IS_ACTIVE IsActive,
	bk.APPROVAL_STS
FROM TR_WORKFLOWPROCESS AS tw
JOIN dbo.TR_BOOK BK ON TW.DOCID = BK.UID
	AND TW.WFTASK = 'BOOK_' + BK.BOOK_TYPE
JOIN dbo.MS_PARAMETER FORM ON FORM.[GROUP] = 'BOOK_TYPE'
	AND FORM.KEYFIELD = BK.BOOK_TYPE
LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_URL'
LEFT JOIN MS_PARAMETER AS mp2 ON mp2.[GROUP] = 'K2FLOW_URL'
LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUSTOMER_CD = CC.CustomerCode
WHERE BK.IS_ACTIVE = 1
	AND BK.IS_DRAFT = 0
	AND tw.ISACTIVE = 1

UNION ALL

SELECT BK.[UID] AS Id,
	'Action Taken' AS TaskCategory,
	TB.BOOK_TYPE AS TaskCategoryDesc,
	tw.WFTASKNAME AS TaskName,
	TW.WFTASK AS Task,
	BK.ACTOR AS Customer,
	CC.LegalName AS CustomerAttentionDesc,
	tw.REGIS_NO AS RegistrationNo,
	tw.WFACTION AS [Action],
	tw.CREATED_BY AS RequestedBy,
	tw.CREATED_DT AS RequestedDate,
	tw.PROCID AS ProcessId,
	tw.[SN] AS SerialNumber,
	mp.[VALUE] + 'ActionTaken/Detail/' + CAST(tw.DOCID AS VARCHAR(10)) AS [URL],
	mp2.[VALUE] + CAST(isnull(tw.PROCID, 0) AS VARCHAR(10)) AS [UrlFlow],
	BK.IS_ACTIVE IsActive,
	bk.APPROVAL_STS
FROM TR_WORKFLOWPROCESS AS tw
JOIN dbo.TR_BOOK_ACTION_TAKEN BK ON TW.DOCID = BK.UID
	AND TW.WFTASK = 'ACTION_TAKEN'
JOIN dbo.TR_BOOK TB ON BK.DOC_ID = TB.UID
LEFT JOIN VW_EASYCUSTOMERCODE CC ON TB.CUSTOMER_CD = CC.CustomerCode
LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_URL'
LEFT JOIN MS_PARAMETER AS mp2 ON mp2.[GROUP] = 'K2FLOW_URL'
WHERE BK.IS_ACTIVE = 1
	AND BK.IS_DRAFT = 0
    AND tw.ISACTIVE = 1

UNION ALL

SELECT BK.[UID] AS Id,
	'Received Notice' AS TaskCategory,
	TB.BOOK_TYPE AS TaskCategoryDesc,
	tw.WFTASKNAME AS TaskName,
	TW.WFTASK AS Task,
	TB.CUSTOMER_CD AS Customer,
	CC.LegalName AS CustomerAttentionDesc,
	tw.REGIS_NO AS RegistrationNo,
	tw.WFACTION AS [Action],
	tw.CREATED_BY AS RequestedBy,
	tw.CREATED_DT AS RequestedDate,
	tw.PROCID AS ProcessId,
	tw.[SN] AS SerialNumber,
	mp.[VALUE] + 'ReceivedNotice/Detail/' + CAST(tw.DOCID AS VARCHAR(10)) AS [URL],
	mp2.[VALUE] + CAST(isnull(tw.PROCID, 0) AS VARCHAR(10)) AS [UrlFlow],
	BK.IS_ACTIVE IsActive,
	bk.APPROVAL_STS
FROM TR_WORKFLOWPROCESS AS tw
JOIN dbo.TR_BOOK_RECEIVED_NOTICE BK ON TW.DOCID = BK.UID
	AND TW.WFTASK = 'RECEIVED_NOTICE'
JOIN dbo.TR_BOOK TB ON BK.DOC_ID = TB.UID
LEFT JOIN VW_EASYCUSTOMERCODE CC ON TB.CUSTOMER_CD = CC.CustomerCode
LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_URL'
LEFT JOIN MS_PARAMETER AS mp2 ON mp2.[GROUP] = 'K2FLOW_URL'
WHERE BK.IS_ACTIVE = 1
	AND BK.IS_DRAFT = 0
    AND tw.ISACTIVE = 1

UNION ALL

SELECT BK.[UID] AS Id,
	'Important Article' AS TaskCategory,
	'IMPORT_ARTICLE' AS TaskCategoryDesc,
	tw.WFTASKNAME AS TaskName,
	TW.WFTASK AS Task,
	BK.CREATOR AS Customer,
	COALESCE(BK.CUSTOMER, '-') AS CustomerAttentionDesc,
	tw.REGIS_NO AS RegistrationNo,
	tw.WFACTION AS [Action],
	tw.CREATED_BY AS RequestedBy,
	tw.CREATED_DT AS RequestedDate,
	tw.PROCID AS ProcessId,
	tw.[SN] AS SerialNumber,
	mp.[VALUE] + 'ImportantArticle/Detail/' + CAST(tw.DOCID AS VARCHAR(10)) AS [URL],
	mp2.[VALUE] + CAST(isnull(tw.PROCID, 0) AS VARCHAR(10)) AS [UrlFlow],
	BK.IS_ACTIVE IsActive,
	bk.APPROVAL_STS
FROM TR_WORKFLOWPROCESS AS tw
JOIN dbo.TR_IMPORTANT_ART BK ON TW.DOCID = BK.UID
	AND TW.WFTASK = 'IMPORT_ARTICLE'
LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_URL'
LEFT JOIN MS_PARAMETER AS mp2 ON mp2.[GROUP] = 'K2FLOW_URL'
WHERE BK.IS_ACTIVE = 1
	AND BK.IS_DRAFT = 0
    AND tw.ISACTIVE = 1

UNION ALL

SELECT BK.[UID] AS Id,
	'Important Article Correction' AS TaskCategory,
	'IMPORT_ARTICLE' AS TaskCategoryDesc,
	tw.WFTASKNAME AS TaskName,
	TW.WFTASK AS Task,
	BK.CREATOR AS Customer,
	COALESCE(BK1.CUSTOMER, '-') AS CustomerAttentionDesc,
	tw.REGIS_NO AS RegistrationNo,
	tw.WFACTION AS [Action],
	tw.CREATED_BY AS RequestedBy,
	tw.CREATED_DT AS RequestedDate,
	tw.PROCID AS ProcessId,
	tw.[SN] AS SerialNumber,
	mp.[VALUE] + 'ImportantArticleCorrection/Detail/' + CAST(tw.DOCID AS VARCHAR(10)) AS [URL],
	mp2.[VALUE] + CAST(isnull(tw.PROCID, 0) AS VARCHAR(10)) AS [UrlFlow],
	BK.IS_ACTIVE IsActive,
	bk.APPROVAL_STS
FROM TR_WORKFLOWPROCESS AS tw
JOIN dbo.TR_IMPORTANT_ART_ACTIVITY BK ON TW.DOCID = BK.UID
	AND TW.WFTASK = 'IMPORT_CORRECTION'
LEFT JOIN dbo.TR_IMPORTANT_ART BK1 ON BK.DOC_ID = BK1.UID
LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_URL'
LEFT JOIN MS_PARAMETER AS mp2 ON mp2.[GROUP] = 'K2FLOW_URL'
WHERE BK.IS_ACTIVE = 1
	AND BK.IS_DRAFT = 0
    AND tw.ISACTIVE = 1

UNION ALL

SELECT BK.[UID] AS Id,
	'Outgoing Letter' AS TaskCategory,
	'OUT_LETTER' AS TaskCategoryDesc,
	tw.WFTASKNAME AS TaskName,
	TW.WFTASK AS Task,
	BK.CUST_CD AS Customer,
	CC.LegalName AS CustomerAttentionDesc,
	tw.REGIS_NO AS RegistrationNo,
	tw.WFACTION AS [Action],
	tw.CREATED_BY AS RequestedBy,
	tw.CREATED_DT AS RequestedDate,
	tw.PROCID AS ProcessId,
	tw.[SN] AS SerialNumber,
	mp.[VALUE] + 'OutgoingLetter/Detail/' + CAST(tw.DOCID AS VARCHAR(10)) AS [URL],
	mp2.[VALUE] + CAST(isnull(tw.PROCID, 0) AS VARCHAR(10)) AS [UrlFlow],
	BK.IS_ACTIVE IsActive,
	bk.APPROVAL_STS
FROM TR_WORKFLOWPROCESS AS tw
JOIN dbo.TR_OUTGOING_LETTER BK ON TW.DOCID = BK.UID
	AND TW.WFTASK = 'OUT_LETTER'
LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_URL'
LEFT JOIN MS_PARAMETER AS mp2 ON mp2.[GROUP] = 'K2FLOW_URL'
LEFT JOIN VW_EASYCUSTOMERCODE CC ON BK.CUST_CD = CC.CustomerCode
WHERE BK.IS_ACTIVE = 1
	AND BK.IS_DRAFT = 0
    AND tw.ISACTIVE = 1
GO
/****** Object:  View [dbo].[vw_BTMUDirectory_Department]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_BTMUDirectory_Department]
AS
SELECT ID,
	ShortName,
	LongName,
	DivisionID,
	LastUpdated,
	UpdatedBy,
	CreatedAt,
	CreatedBy
FROM [JKTSVRDEV03].[BTMUDirectory].[dbo].[Department]
WHERE 1 = 1
	AND (
		1 = 1
		AND (
			CASE 
				WHEN ShortName = 'LCBD'
					THEN LongName
				END
			) LIKE 'Local Corporate Banking Department'
		OR ShortName != 'LCBD'
		)


GO
/****** Object:  View [dbo].[VW_DOCUMENT_DETAIL]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VW_DOCUMENT_DETAIL]
AS
	SELECT *
	FROM [EASY_DOCUMENT_DETAIL]



GO
/****** Object:  View [dbo].[VW_EASYACCOUNTNO]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_EASYACCOUNTNO]
AS
	SELECT TOP 100 OfficeCode AS OfficeCode, AccountNo AS AccountNo, CustomerCode AS CustomerCode, AccountCCY AS AccountCCY, GLCode AS GLCode, AccountStatus AS AccountStatus
		, '3' + OfficeCode + '-' + AccountCCY + '-CUA-' + AccountNo AS CombineCode
	FROM DBO.EASY_Account_No
	WHERE AccountStatus = 'A'


GO
/****** Object:  View [dbo].[VW_EASYADDRESS]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_EASYADDRESS]
AS
	SELECT * FROM SN_EASY_ADDRESS

GO
/****** Object:  View [dbo].[vw_UserByDepartment]    Script Date: 16/03/2022 20:28:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_UserByDepartment]
AS
SELECT a.Username,
	RealName = CASE 
		WHEN a.Realname = ''
			THEN a.Username
		ELSE a.Realname
		END,
	c.Email,
	c.DepartmentName,
	RoleParentID,
	OfficeCode,
	CustomerCode,
	b.UnitID,
	b.GroupID,
	D.ShortName AS DepartmentCode,
	b.DepartmentID,
	b.DivisionID,
	f.ShortName AS RoleShortName,
	b.RoleID
FROM dbo.SN_BTMUDirectory_User AS a
INNER JOIN dbo.SN_BTMUDirectory_Role_Parent AS b ON a.Roleparentid = b.ID
INNER JOIN dbo.SN_CONE_Cone_User AS c ON a.Username = c.Username
INNER JOIN dbo.SN_BTMUDirectory_Department_All AS D ON D.ID = b.DepartmentID
INNER JOIN dbo.SN_BTMUDirectory_Role AS f ON f.ID = b.RoleID
WHERE (isdeleted = 0) -- AND (c.Email <> '')


GO
USE [master]
GO
ALTER DATABASE [REBOOK_ITA] SET  READ_WRITE 
GO
