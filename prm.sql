USE [VENDORCONTROL_ITA]
GO
SET IDENTITY_INSERT [dbo].[MS_PARAMETER] ON 

INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (1, N'FORM_TYPE_LIST', N'WAIVER', N'Form 3. WAIVER APPLICATION', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (2, N'FORM_TYPE_LIST', N'PERIODICAL_REVIEW', N'Form 4. CHECKLIST FOR PERIODICAL REVIEW', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (3, N'FORM_TYPE_LIST', N'CONTINUING_VENDOR', N'Form 5. CONTINUING VENDOR CHECK SHEET', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (4, N'FORM_TYPE_LIST', N'LEGAL_REQUEST', N'Form 6. LEGAL REQUEST', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (5, N'FORM_TYPE_DDL', N'REGISTRATION', N'Vendor Registration', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (6, N'FORM_TYPE_DDL', N'PERIODICAL_REVIEW', N'Periodical Review', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (7, N'FORM_TYPE_DDL', N'CONTINUING_VENDOR', N'Continuing Vendor', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (8, N'FORM_TYPE_DDL', N'LEGAL_REQUEST', N'Legal Request', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (9, N'DOCUMENT_STATUS', N'Awaiting Approval', N'Awaiting Approval', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (10, N'DOCUMENT_STATUS', N'Approval Completed', N'Approval Completed', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (11, N'DOCUMENT_STATUS', N'Reject', N'Reject', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (12, N'DOCUMENT_STATUS', N'Revise', N'Revise', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (13, N'DOCUMENT_STATUS', N'Terminated', N'Terminated', 5, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (14, N'ANNUAL_BILLING', N'1', N'Less than $10,000 per year average', 1, NULL, NULL, NULL, N'Guestisd313', CAST(0x0000ADF6017D84AA AS DateTime), N'10.70.178.116')
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (15, N'ANNUAL_BILLING', N'2', N'More than $10,000 up to $50,000 per year average', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (16, N'ANNUAL_BILLING', N'3', N'More than $50,000 per year average', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (17, N'SPECIAL_RELATED', N'1', N'Not Classified as Special Related Company', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (18, N'SPECIAL_RELATED', N'2', N'Classified as Special Related Company', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (19, N'INTERNAL_ACCESS', N'2', N'NO (Low Risk Vendor Factor)', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (20, N'INTERNAL_ACCESS', N'1', N'YES (Medium and High Risk Vendor Factor)', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (21, N'RISK_CATEGORY', N'High Risk Vendor', N'High Risk Vendor', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (22, N'RISK_CATEGORY', N'Medium Risk Vendor', N'Medium Risk Vendor', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (23, N'RISK_CATEGORY', N'Low Risk Vendor', N'Low Risk Vendor', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (24, N'LEGAL_STATUS', N'1', N'PT', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (25, N'LEGAL_STATUS', N'2', N'CV', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (26, N'LEGAL_STATUS', N'3', N'FIRM', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (27, N'LEGAL_STATUS', N'4', N'OTHERS', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (28, N'MAP_RISK_CATEGORY', N'3', N'High Risk Vendor', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (29, N'MAP_RISK_CATEGORY', N'2', N'Medium Risk Vendor', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (30, N'MAP_RISK_CATEGORY', N'1', N'Low Risk Vendor', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (31, N'PAYMENT_DETAIL', N'1', N'Cash, Reason', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (32, N'PAYMENT_DETAIL', N'2', N'Bank Transfer', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (33, N'APPROVAL_STATUS', N'Reject', N'Reject', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (34, N'APPROVAL_STATUS', N'Revise', N'Revise', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (35, N'APPROVAL_STATUS', N'Awaiting Approval', N'Awaiting Approval', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (36, N'APPROVAL_STATUS', N'Approval Completed', N'Approval Completed', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (37, N'WAIVER_ASS_RESULT', N'Y', N'Accept the waiver', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (38, N'WAIVER_ASS_RESULT', N'N', N'Reject the waiver', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (39, N'EXCEL_PASSWORD', N'Report Password', N'mufgbatm', NULL, NULL, NULL, NULL, N'Guestisd275', CAST(0x0000AE1401326352 AS DateTime), N'::1')
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (40, N'EMAIL_CONFIG', N'Email From', N'no-reply@controlvendor.id.mufg.jp', NULL, N'SYSTEM', CAST(0x0000ADEC00CC058C AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (41, N'EMAIL_CONFIG', N'Email Host', N'jdcexguat01', NULL, N'SYSTEM', CAST(0x0000ADEC00CC058C AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (42, N'EMAIL_CONFIG', N'Email Port', N'25', NULL, N'SYSTEM', CAST(0x0000ADEC00CC058C AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (43, N'EMAIL_PERIODICAL_REVIEW', N'SUBJECT', N'CONTROL OF VENDOR INFORM - List of Periodical Review of Vendor(s)', NULL, N'SYSTEM', CAST(0x0000ADEC00C37FB8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (44, N'EMAIL_PERIODICAL_REVIEW', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
</head>
<body>
	<div>
		<table>
			<tr>
				<td>Dear All,</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>There is {{ CountRowsVendor }} vendor that needs to be periodical reviewed.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Below is the detail:</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<table border="1">
						<tr>
							<th>VENDOR ID</th>
							<th>VENDOR NAME</th>
							<th>START DATE</th>
							<th>DOC LINK</th>
						</tr>
					{{{ GenerateTable }}}
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Click on the link to open the specific document.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Thank you.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<pre style="color: red">** <i>Please DO NOT REPLY this email since it is generated by system automatically</i> **</pre>
				</td>
			</tr>
		</table>
	</div>
</body>
<html>
', NULL, N'SYSTEM', CAST(0x0000ADEC00C37FB8 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (45, N'EMAIL_CONTINUING_VENDOR', N'SUBJECT', N'CONTROL OF VENDOR INFORM - List of Expired Contract of Vendor(s)', NULL, N'SYSTEM', CAST(0x0000ADEC00D791F1 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (46, N'EMAIL_CONTINUING_VENDOR', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
</head>
<body>
	<div>
		<table>
			<tr>
				<td>Dear All,</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>There is {{ CountRowsVendor }} vendor that has been expired.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Below is the detail:</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<table border="1">
						<tr>
							<th>VENDOR ID</th>
							<th>VENDOR NAME</th>
							<th>END DATE</th>
							<th>DOC LINK</th>
						</tr>
					{{{ GenerateTable }}}
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Click on the link to open the specific document.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>Thank you.</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
					<pre style="color: red">** <i>Please DO NOT REPLY this email since it is generated by system automatically</i> **</pre>
				</td>
			</tr>
		</table>
	</div>
</body>
<html>
', NULL, N'SYSTEM', CAST(0x0000ADEC00D791F1 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (49, N'EMAIL_CREATOR_APP', N'SUBJECT', N'CONTROL OF VENDOR INFORM you had been chosen to decide approve, reject, revise about a workflowed document.', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (50, N'EMAIL_CREATOR_APP', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table style="width:100%;word-wrap:break-word;table-layout:fixed;">
				<tr>
					<td>Please be informed you had been choosen to make a decide approve, reject, revise about a workflowed document</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Comment from the creator ({{ Creator }})</td>
				</tr>
				<tr>
					<td>{{ Comment }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Date of assignment: {{ AssignmentDate }}</td>
				</tr>
				<tr>
					<td>List of current approver(s):</td>
				</tr>
				<tr>
					<td>{{ CurrentApprover }}</td>
				</tr>
				<tr>
					<td>If you would like to see/view the content of the workflowed document, please click on the following link</td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (51, N'EMAIL_NEXT_APP', N'SUBJECT', N'CONTROL OF VENDOR INFORM you had been chosen to decide approve, reject, revise about a workflowed document.', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (52, N'EMAIL_NEXT_APP', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table style="width:100%;word-wrap:break-word;table-layout:fixed;">
				<tr>
					<td>Please be informed that the workflowed document has been approved since {{ PreviousApprovedDate }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Comment from the previous approver ({{ PreviousApprover }})</td>
				</tr>
				<tr>
					<td>{{ Comment }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>List of current approver(s):</td>
				</tr>
				<tr>
					<td>{{ CurrentApprover }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Decision time limit: {{ DecisionTimeLimit }}day(s).</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>If you would like to see/view the content of the workflowed document, please click on the following link</td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (53, N'EMAIL_REQUEST_REVISE', N'SUBJECT', N'CONTROL OF VENDOR INFORM you that a workflowed document has been revised', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (54, N'EMAIL_REQUEST_REVISE', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table style="width:100%;word-wrap:break-word;table-layout:fixed;">
				<tr>
					<td>Please be informed that the workflowed document has been revise on {{ ReviseDate }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Comment from the previous approver ({{ PreviousApprover }})</td>
				</tr>
				<tr>
					<td>{{ Comment }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Please click on the following link to see/view the revise workflowed document</td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (55, N'EMAIL_REQUEST_APPROVE', N'SUBJECT', N'CONTROL OF VENDOR INFORM you that a workflowed document has been approved', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (56, N'EMAIL_REQUEST_APPROVE', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table style="width:100%;word-wrap:break-word;table-layout:fixed;">
				<tr>
					<td>Please be informed that the workflowed document has been approved on {{ ApproveDate }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Comment from the previous approver ({{ PreviousApprover }})</td>
				</tr>
				<tr>
					<td>{{ Comment }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Please click on the following link to see/view the approved workflowed document <a href="{{ UrlLink }}">link</a></td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (57, N'EMAIL_REQUEST_REJECT', N'SUBJECT', N'CONTROL OF VENDOR INFORM you that a workflowed document has been rejected', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (58, N'EMAIL_REQUEST_REJECT', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table>
				<tr>
					<td>Please be informed that the workflowed document has been rejected since {{ RejectDate }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Please click on the following link to open the workflowed document <a href="{{ UrlLink }}">link</a></td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (59, N'APP_PATH', N'http://JKTSVRDEV02/VENDORCONTROL_ITA/', N'Application Path', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (97, N'Group1', N'Group1_12', N'Group1_123', NULL, N'Guestisd313', CAST(0x0000ADFA0181DDC7 AS DateTime), N'10.70.178.136', NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (98, N'Group_A', N'Group_AB', N'Group_ABC', 0, N'Guestisd313', CAST(0x0000ADFA0183F1F2 AS DateTime), N'10.70.178.136', NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (99, N'Group_1', N'Group ABC', N'Group ABCDEF', 0, N'Guestisd313', CAST(0x0000ADFD00F0D893 AS DateTime), N'10.70.178.4', N'Guestisd313', CAST(0x0000ADFD00F1057C AS DateTime), N'10.70.178.4')
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (100, N'Group123', N'Group123', N'Group123', 1, N'Guestisd313', CAST(0x0000ADFE01797851 AS DateTime), N'10.70.179.47', NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (101, N'group test Edit', N'value test', N'desccc', 1, N'Guestisd275', CAST(0x0000ADFE01850BF3 AS DateTime), N'::1', N'Guestisd275', CAST(0x0000ADFE01876C6B AS DateTime), N'::1')
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (102, N'Group_ABC', N'Group_Test', N'Group_Testing', 0, N'Guestisd313', CAST(0x0000ADFF00C50430 AS DateTime), N'10.70.179.44', NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (103, N'APPROVAL_STATUS', N'Terminated', N'Terminated', 5, N'Guestisd275', CAST(0x0000ADFF016BADD7 AS DateTime), N'::1', NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (104, N'K2FLOW_URL', N'http://jktk2app02dev:81/ViewFlow/ViewFlow.aspx?ProcessID=', N'http://jktk2app02dev:81/ViewFlow/ViewFlow.aspx?ProcessID=', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (105, N'K2_CONFIG', N'K2Server', N'jktk2app02dev', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (106, N'K2_CONFIG', N'K2User', N'K2System', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (107, N'K2_CONFIG', N'K2Pass', N'P@ssw0rd2013', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (108, N'K2_CONFIG', N'K2Domain', N'JAKARTA01', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (109, N'K2_CONFIG', N'K2Label', N'K2', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (110, N'K2_CONFIG', N'K2WFPort', N'5252', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (111, N'K2_CONFIG', N'K2SmOPort', N'5555', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (112, N'K2_CONFIG', N'K2Folder', N'VENDORCONTROL', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (118, N'EMAIL_REQUEST_REVISE_SCH', N'SUBJECT', N'CONTROL OF VENDOR INFORM you that a workflowed document has been revised', NULL, N'SYSTEM', CAST(0x0000AE1200DF4E0A AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (119, N'EMAIL_REQUEST_REVISE_SCH', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table style="width:100%;word-wrap:break-word;table-layout:fixed;">
				<tr>
					<td>Please be informed that the workflowed document has been revise on {{ ReviseDate }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Comment from the approver ({{ PreviousApprover }})</td>
				</tr>
				<tr>
					<td>{{ Comment }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Please click on the following link to see/view the revise workflowed document <a href="{{ DocLink }}">link</a></td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, N'SYSTEM', CAST(0x0000AE1200DF4E0A AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (122, N'EMAIL_CREATOR_APP_SCH', N'SUBJECT', N'CONTROL OF VENDOR INFORM you had been chosen to decide approve, reject, revise about a workflowed document.', NULL, N'SYSTEM', CAST(0x0000AE1200E09B9E AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (123, N'EMAIL_CREATOR_APP_SCH', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table style="width:100%;word-wrap:break-word;table-layout:fixed;">
				<tr>
					<td>Please be informed you had been choosen to make a decide approve, reject, revise about a workflowed document</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Comment from the creator ({{ Creator }})</td>
				</tr>
				<tr>
					<td>{{ Comment }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Date of assignment: {{ AssignmentDate }}</td>
				</tr>
				<tr>
					<td>List of current approver(s):</td>
				</tr>
				<tr>
					<td>{{ CurrentApprover }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>If you would like to see/view the content of the workflowed document, please click on the following link <a href="{{ DocLink }}">link</a></td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, N'SYSTEM', CAST(0x0000AE1200E09B9E AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (124, N'EMAIL_NEXT_APP_SCH', N'SUBJECT', N'CONTROL OF VENDOR INFORM you had been chosen to decide approve, reject, revise about a workflowed document.', NULL, N'SYSTEM', CAST(0x0000AE1200E3B455 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (125, N'EMAIL_NEXT_APP_SCH', N'CONTENT', N'<!DOCTYPE html>
<html>
	<head>
		<style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>
	</head>
	<body>
		<div>
			<table style="width:100%;word-wrap:break-word;table-layout:fixed;">
				<tr>
					<td>Please be informed you had been choosen to make a decide approve, reject, revise about a workflowed document</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Comment from the previous approver ({{ PreviousApprover }})</td>
				</tr>
				<tr>
					<td>{{ Comment }}</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>List of current approver(s):</td>
				</tr>
				<tr>
					<td>{{ CurrentApprover }}</td>
				</tr>
				<tr>
					<td>Decision time limit: {{ DecisionTimeLimit }}day(s).</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Please click on the following link to open the workflowed document in order to make your decision : <a href="{{ DocLink }}">link</a></td>
				</tr>
			</table>
		</div>
	</body>
</html>', NULL, N'SYSTEM', CAST(0x0000AE1200E3B456 AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (126, N'K2_CONFIGOS', N'K2Server', N'jktk2app01dev', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (127, N'K2_CONFIGOS', N'K2User', N'K2System', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (128, N'K2_CONFIGOS', N'K2Pass', N'P@ssw0rd2013', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (129, N'K2_CONFIGOS', N'K2Domain', N'JAKARTA01', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (130, N'K2_CONFIGOS', N'K2Label', N'K2', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (131, N'K2_CONFIGOS', N'K2WFPort', N'5252', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (132, N'K2_CONFIGOS', N'K2SmOPort', N'5555', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (133, N'K2_CONFIGOS', N'K2Folder', N'OUTSOURCINGCONTROLSYSTEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (134, N'K2FLOWOS_URL', N'http://jktk2app01dev:81/ViewFlow/ViewFlow.aspx?ProcessID=', N'http://jktk2app01dev:81/ViewFlow/ViewFlow.aspx?ProcessID=', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (151, N'STEP_CONTINUING_REVIEW', N'0', N'Creator', 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (152, N'STEP_CONTINUING_REVIEW', N'1', N'Creator Dept Head', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (153, N'STEP_CONTINUING_REVIEW', N'2', N'GAD PIC', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (154, N'STEP_CONTINUING_REVIEW', N'3', N'GAD Dept Head', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (155, N'STEP_CONTINUING_REVIEW', N'4', N'Creator DGM, GAD DGM', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (156, N'STEP_CONTINUING_REVIEW', N'5', N'GM', 5, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (157, N'STEP_LEGAL_REVIEW', N'0', N'Creator', 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (158, N'STEP_LEGAL_REVIEW', N'1', N'Creator Dept Head/Creator Unit Head', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (159, N'STEP_LEGAL_REVIEW', N'2', N'Legal PIC', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (160, N'STEP_LEGAL_REVIEW', N'3', N'Legal Staff', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (161, N'STEP_PERIODICAL_REVIEW', N'0', N'Creator', 0, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (162, N'STEP_PERIODICAL_REVIEW', N'1', N'Creator Dept Head', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (163, N'STEP_PERIODICAL_REVIEW', N'10', N'Creator DGM, FCD DGM, RMD DGM, GAD DGM', 10, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (164, N'STEP_PERIODICAL_REVIEW', N'11', N'GM', 11, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (165, N'STEP_PERIODICAL_REVIEW', N'2', N'GAD PIC', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (166, N'STEP_PERIODICAL_REVIEW', N'3', N'FCD PIC', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (167, N'STEP_PERIODICAL_REVIEW', N'4', N'FCD DH', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (168, N'STEP_PERIODICAL_REVIEW', N'5', N'CPC PIC', 5, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (169, N'STEP_PERIODICAL_REVIEW', N'6', N'RMD PIC', 6, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (170, N'STEP_PERIODICAL_REVIEW', N'7', N'RMD Dept Head', 7, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (171, N'STEP_PERIODICAL_REVIEW', N'8', N'GAD Dept Head', 8, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (172, N'STEP_PERIODICAL_REVIEW', N'9', N'Creator DGM, CPC COMDIR, FCD DGM, RMD DGM, GAD DGM', 9, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (173, N'STEP_REGISTRATION', N'0', N'Creator', 0, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (174, N'STEP_REGISTRATION', N'1', N'Creator Dept Head', 1, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (175, N'STEP_REGISTRATION', N'10', N'Creator DGM, FCD DGM, RMD DGM, GAD DGM', 10, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (176, N'STEP_REGISTRATION', N'11', N'GM', 11, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (177, N'STEP_REGISTRATION', N'2', N'GAD PIC', 2, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (178, N'STEP_REGISTRATION', N'20', N'GAD', 20, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (179, N'STEP_REGISTRATION', N'21', N'GAD DH', 21, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (180, N'STEP_REGISTRATION', N'22', N'GAD DGM', 22, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (181, N'STEP_REGISTRATION', N'23', N'GM', 23, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (182, N'STEP_REGISTRATION', N'3', N'FCD PIC', 3, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (183, N'STEP_REGISTRATION', N'4', N'FCD DH', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (184, N'STEP_REGISTRATION', N'5', N'CPC PIC', 5, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (185, N'STEP_REGISTRATION', N'6', N'RMD PIC', 6, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (186, N'STEP_REGISTRATION', N'7', N'RMD Dept Head', 7, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (187, N'STEP_REGISTRATION', N'8', N'GAD Dept Head', 8, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (188, N'STEP_REGISTRATION', N'9', N'Creator DGM, CPC COMDIR, FCD DGM, RMD DGM, GAD DGM', 9, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (192, N'Test123', N'Test-234', N'Test juga-edit', NULL, N'RiestikaP', CAST(0x0000AE2000950A96 AS DateTime), N'150.14.81.113', N'RiestikaP', CAST(0x0000AE2000999ABD AS DateTime), N'150.14.81.113')
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (195, N'GuestTest', N'GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39 GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 Guesti', N'GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39 GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 GuestISD39GuestISD39 Guesti', 1, N'guestisd313', CAST(0x0000AE2000C80441 AS DateTime), N'150.14.10.160', NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (200, N'EMAIL_NOTICE_ITSO', N'SUBJECT', N'[Vendor Control System] approver not found on K2 workflow', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (201, N'EMAIL_NOTICE_ITSO', N'CONTENT', N'  <!DOCTYPE html>  <html>  <head>  <style>  body {    font-family: "Times New Roman";      }  p{   line-height: 30%;  }  </style>  </head>  </head>  <body>  <div>  <pre> Vendor Control System K2 workflow not found the approver with instance id : {{ instanceID }} </pre>  </body>  <html>  ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MS_PARAMETER] ([UID], [GROUP], [VALUE], [DESCS], [SEQ], [CREATED_BY], [CREATED_DT], [CREATED_HOST], [MODIFIED_BY], [MODIFIED_DT], [MODIFIED_HOST]) VALUES (202, N'EMAIL_ADMIN', N'guestisd313_guestisd313@tmp.id.mufg.jp', N'Send email to admin when approval is not exists', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[MS_PARAMETER] OFF
