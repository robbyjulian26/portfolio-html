USE [master]
GO
/****** Object:  Database [VENDORCONTROL_ITA]    Script Date: 16/03/2022 21:02:57 ******/
CREATE DATABASE [VENDORCONTROL_ITA]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VENDORCONTROL_ITA', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VENDORCONTROL_ITA.mdf' , SIZE = 23552KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'VENDORCONTROL_ITA_log', FILENAME = N'd:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\VENDORCONTROL_ITA_log.ldf' , SIZE = 6272KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VENDORCONTROL_ITA].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET ARITHABORT OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET RECOVERY FULL 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET  MULTI_USER 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [VENDORCONTROL_ITA]
GO
/****** Object:  User [sa_linkserver]    Script Date: 16/03/2022 21:02:57 ******/
CREATE USER [sa_linkserver] FOR LOGIN [sa_linkserver] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [JAKARTA01\Domain Admins]    Script Date: 16/03/2022 21:02:57 ******/
CREATE USER [JAKARTA01\Domain Admins] FOR LOGIN [JAKARTA01\Domain Admins]
GO
ALTER ROLE [db_datareader] ADD MEMBER [sa_linkserver]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [sa_linkserver]
GO
ALTER ROLE [db_owner] ADD MEMBER [JAKARTA01\Domain Admins]
GO
/****** Object:  StoredProcedure [dbo].[GenerateProperty]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GenerateProperty] 
@TableName varchar(250)
AS
BEGIN
	SELECT 
	tb.name As [TableName], 
	cl.name as [FieldName],
	cl.is_nullable As [Nullable],
	tp.name As [SqlDataType],
	cl.max_length AS [Length],
	CONCAT('public'
		,CASE tp.name
		WHEN 'bigint' THEN ' int '
		WHEN 'binary' THEN ' bool '
		WHEN 'bit' THEN ' bool '
		WHEN 'char' THEN ' char '
		WHEN 'date' THEN ' DateTime '
		WHEN 'datetime' THEN ' DateTime '
		WHEN 'datetime2' THEN ' DateTime '
		WHEN 'datetimeoffset' THEN ' Object '
		WHEN 'decimal' THEN ' decimal '
		WHEN 'float' THEN ' float '
		WHEN 'geography' THEN ' Object  '
		WHEN 'geometry' THEN 'Object '
		WHEN 'hierarchyid' THEN ' Object '
		WHEN 'image' THEN ' Object '
		WHEN 'int' THEN ' int '
		WHEN 'money' THEN ' decimal '
		WHEN 'nchar' THEN ' char '
		WHEN 'ntext' THEN ' string '
		WHEN 'numeric' THEN ' decimal '
		WHEN 'nvarchar' THEN ' string '
		WHEN 'real' THEN ' decimal '
		WHEN 'smalldatetime' THEN ' DateTime '
		WHEN 'smallint' THEN ' small '
		WHEN 'smallmoney' THEN ' decimal '
		WHEN 'sql_variant' THEN ' Object '
		WHEN 'sysname' THEN ' Object '
		WHEN 'text' THEN ' string '
		WHEN 'time' THEN ' DateTime '
		WHEN 'timestamp' THEN ' Object '
		WHEN 'tinyint' THEN ' small '
		WHEN 'uniqueidentifier' THEN ' Object ' 
		WHEN 'varbinary' THEN ' Object '
		WHEN 'varchar' THEN ' string '
		WHEN 'xml' THEN ' Object '
		ELSE ' Object '
		END
		,'/*'+ tp.name + CASE WHEN cl.is_nullable = 0 THEN ' Not Null' ELSE '' END + '*/ '
		,cl.name
		,' { get; set; } //'
		,cast(cl.max_length as varchar)
	) AS [Property],
	CONCAT('param.Add("@',cl.name,'", request.',cl.name,');') AS [AddParamCode],
	CONCAT(',@',cl.name,' [',tp.name,']',(CASE WHEN tp.name LIKE '%char%' THEN '('+CAST(cl.max_length AS VARCHAR)+')' ELSE '' END)) AS [AddParamSql],
	CONCAT(cl.name,' = fromModel.',cl.name,';') AS [ModelMapper]
	FROM sys.tables tb 
	JOIN sys.all_columns cl
		ON tb.object_id = cl.object_id
	JOIN sys.types tp
		ON cl.system_type_id = tp.system_type_id
	WHERE tb.name = @TableName
	ORDER BY cl.column_id
END

GO
/****** Object:  StoredProcedure [dbo].[GetUser]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUser]
@UserId varchar(50) = 'ALL',
@DepartmentID varchar(10) = '0',
@RoleID varchar(10) = '0',
@DivisionID varchar(10) = '0',
@UnitID varchar(10) = '0',
@GroupID varchar(10) = '0'
AS 
BEGIN
SELECT 
	 Usr.UserName AS UserId
	,Usr.Realname AS RealName
	,Dp.ID AS DepartmentID
	,Dp.ShortName AS DepartmentShortName
	,Dp.LongName AS DepartmentLongName
	,R.ID AS RoleID
	,R.ShortName AS RoleShortName
	,R.LongName AS RoleLongName
	,Dv.ID AS DivisionID
	,Dv.ShortName AS DivisionShortName
	,Dv.LongName AS DivisionLongName
	,Un.ID AS UnitID
	,Un.ShortName AS UnitShortName
	,Un.LongName AS UnitLongName
	,G.ID AS GroupID
	,G.GroupName AS Groupname

FROM SN_BTMUDirectory_User Usr
	JOIN SN_BTMUDirectory_RoleParent Rp ON
	Usr.RoleParentID = Rp.ID
	LEFT JOIN SN_BTMUDirectory_Department Dp ON
	Rp.DepartmentID = Dp.ID
	LEFT JOIN SN_BTMUDirectory_Role R ON
	Rp.RoleID = R.ID
	LEFT JOIN SN_BTMUDirectory_Division Dv ON
	RP.DivisionID = Dv.ID
	LEFT JOIN SN_BTMUDirectory_Unit Un ON
	Rp.UnitID = Un.ID
	LEFT JOIN SN_BTMUDirectory_Group G ON
	Rp.GroupID = G.ID
WHERE Usr.isDeleted = 0 AND
(Usr.UserName = @UserId OR @UserId = 'ALL') AND
(Rp.DepartmentID = @DepartmentID OR @DepartmentID = '0') AND
(Rp.RoleID = @RoleID OR @RoleID = '0') AND
(Rp.DivisionID = @DivisionID OR @DivisionID = '0') AND
(Rp.UnitID = @UnitID OR @UnitID = '0') AND
(Rp.GroupID = @GroupID OR @GroupID = '0')
END


GO
/****** Object:  StoredProcedure [dbo].[sp_retention]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--sp retention
CREATE PROCEDURE [dbo].[sp_retention]
AS
BEGIN
	IF OBJECT_ID(N'tempdb..#RETENTION_VENDOR') IS NOT NULL
	BEGIN
		DROP TABLE #RETENTION_VENDOR
	END

	IF OBJECT_ID(N'tempdb..#RETENTION_VENDOR_ACTIVITY') IS NOT NULL
	BEGIN
		DROP TABLE #RETENTION_VENDOR_ACTIVITY
	END

	DECLARE @RETENTION_YEAR INT = 10
	DECLARE @DB_RETENTION VARCHAR(50) = 'VENDORCONTROL_ARCH'
	DECLARE @DB_SOURCE VARCHAR(50) = 'VENDORCONTROL_ITA'
	DECLARE @CURRENT_DT VARCHAR(10) = '2032-03-31'
	DECLARE @sql NVARCHAR(max)
	DECLARE @ctr INT
	DECLARE @ctrAll INT
	DECLARE @colname VARCHAR(2000)
	DECLARE @TBL VARCHAR(100)
	DECLARE @TBL_JOIN VARCHAR(2000)

	CREATE TABLE #RETENTION_VENDOR (
		[TMP_UID] INT IDENTITY(1, 1),
		TMP_DOC_ID BIGINT,
		TMP_VENDOR_ID VARCHAR(50)
		)

	CREATE TABLE #RETENTION_VENDOR_ACTIVITY (
		[TMP_UID] INT IDENTITY(1, 1),
		TMP_DOC_ID BIGINT,
		TMP_VENDOR_ID VARCHAR(50),
		TMP_FORM_TYPE VARCHAR(30)
		)

	DECLARE @TABLE_REGISTRATION TABLE (
		[UID] INT,
		TABLE_NAME VARCHAR(50),
		JOIN_COND VARCHAR(2000)
	)

	DECLARE @TABLE_ACTIVITY TABLE (
		[UID] INT,
		TABLE_NAME VARCHAR(50),
		JOIN_COND VARCHAR(2000)
	)

	DECLARE @TABLE_OTHER TABLE (
		[UID] INT,
		TABLE_NAME VARCHAR(50),
		JOIN_COND VARCHAR(2000)
	)
	
	DECLARE @TABLE_ALL TABLE (
		[UID] INT IDENTITY(1,1),
		TABLE_NAME VARCHAR(50)
	)

	INSERT INTO @TABLE_REGISTRATION ([UID], TABLE_NAME, JOIN_COND)
	SELECT 1, 'TR_VENDOR_CONTROL', 'RV.TMP_DOC_ID = VC.UID' UNION
	SELECT 2, 'TR_VC_CONF_DOC_CHECKLIST', 'RV.TMP_DOC_ID = VC.DOC_ID' UNION
	SELECT 3, 'TR_VC_EXAM_COMPARISON', 'RV.TMP_DOC_ID = VC.DOC_ID' UNION
	SELECT 4, 'TR_VC_RISK_ASSESSMENT', 'RV.TMP_DOC_ID = VC.DOC_ID' UNION
	SELECT 5, 'TR_VC_RISK_MANAGEMENT_COMMENT', 'RV.TMP_DOC_ID = VC.DOC_ID' UNION
	SELECT 6, 'TR_VC_ASC_CHECKSHEET', 'RV.TMP_DOC_ID = VC.DOC_ID' UNION
	SELECT 7, 'TR_ATTACHMENT', 'RV.TMP_DOC_ID = VC.DOC_ID AND VC.FILE_TYPE = ''REGISTRATION''' UNION
	SELECT 8, 'TR_DOCUMENT_HISTORY', 'RV.TMP_DOC_ID = VC.DOC_ID AND VC.HIST_TYPE = ''REGISTRATION''' 

	INSERT INTO @TABLE_ACTIVITY ([UID], TABLE_NAME, JOIN_COND)
	SELECT 1, 'TR_VENDOR_CONTROL_ACTIVITY', 'RVA.TMP_DOC_ID = VCA.UID' UNION
	SELECT 2, 'TR_VC_ACTIVITY_REV_EXAMINED', 'RVA.TMP_DOC_ID = VCA.DOC_ID' UNION
	SELECT 3, 'TR_VC_ACTIVITY_REV_AML', 'RVA.TMP_DOC_ID = VCA.DOC_ID' UNION
	SELECT 4, 'TR_DOCUMENT_HISTORY', 'RVA.TMP_DOC_ID = VCA.DOC_ID AND VCA.HIST_TYPE = CASE WHEN RVA.TMP_FORM_TYPE = ''CONTINUING_VENDOR'' THEN ''CONTINUING_REVIEW'' WHEN RVA.TMP_FORM_TYPE = ''LEGAL_REQUEST'' THEN ''LEGAL_REVIEW'' WHEN RVA.TMP_FORM_TYPE = ''PERIODICAL_REVIEW'' THEN ''PERIODICAL_REVIEW'' END'

	INSERT INTO @TABLE_OTHER ([UID], TABLE_NAME, JOIN_COND)
	SELECT 1, 'TR_USER_ACTIVITY_LOG_HDR', 'AND DATEDIFF(hour, CREATED_DT, '''+@CURRENT_DT+''') / 8766.0 >= '+CONVERT(VARCHAR, @RETENTION_YEAR)+'' UNION
	SELECT 2, 'TR_USER_ACTIVITY_LOG_DTL', 'AND DATEDIFF(hour, CREATED_DT, '''+@CURRENT_DT+''') / 8766.0 >= '+CONVERT(VARCHAR, @RETENTION_YEAR)+'' UNION
	SELECT 3, 'TR_REMINDER_LOG', 'AND DATEDIFF(hour, CREATED_DT, '''+@CURRENT_DT+''') / 8766.0 >= '+CONVERT(VARCHAR, @RETENTION_YEAR)+''

	-- ============================================================
	-- CREATE TABLE IN DB RETENTION IF TABLE NOT EXISTS
	-- ============================================================
	INSERT INTO @TABLE_ALL (TABLE_NAME)
	SELECT TABLE_NAME FROM @TABLE_REGISTRATION UNION
	SELECT TABLE_NAME FROM @TABLE_ACTIVITY UNION
	SELECT TABLE_NAME FROM @TABLE_OTHER

	DECLARE @COUNT_ALL INT = 1
	DECLARE @COUNT_ALL_TOTAL INT = 1

	SELECT @COUNT_ALL_TOTAL = COUNT([UID])
	FROM @TABLE_ALL

	WHILE @COUNT_ALL <= @COUNT_ALL_TOTAL
	BEGIN
		SELECT @TBL = TABLE_NAME
		FROM @TABLE_ALL
		WHERE [UID] = @COUNT_ALL

		SET @sql = 'select @ctrAll = count(1) from ' + @DB_RETENTION + '.sys.objects where type = ''U'' and name = ''' + @TBL + ''''

		EXEC sp_executesql @sql,
			N' @ctrAll int OUTPUT',
			@ctrAll OUTPUT

		IF @ctrAll = 0
		BEGIN
			SET @sql = N'
			select * into ' + @DB_RETENTION + '.dbo.' + @TBL + ' from ' + @DB_SOURCE + '.dbo.' + @TBL + ' where 1 = 2;
			'

			EXEC sp_executesql @sql
		END

		SET @COUNT_ALL += 1
	END


	-- ============================================================
	-- GET LIST VENDOR ALREADY TERMINATE MORE THAN RETENTION YEAR
	-- ============================================================
	SET @sql = N'
	INSERT INTO #RETENTION_VENDOR (
		TMP_DOC_ID,
		TMP_VENDOR_ID
		)
	SELECT [UID],
		VENDOR_ID
	FROM ' + @DB_SOURCE + '.DBO.TR_VENDOR_CONTROL
	WHERE 1 = 1
		AND APPROVAL_STS = ''Terminated''
		AND TERMINATION_DT IS NOT NULL
		AND DATEDIFF(hour, TERMINATION_DT, ''' + @CURRENT_DT + ''') / 8766.0 >= ' + CONVERT(VARCHAR, @RETENTION_YEAR)

	EXEC sp_executesql @sql

	-- ============================================================
	-- VENDOR REGISTRATION
	-- ============================================================
	DECLARE @COUNT_REGIS INT = 1
	DECLARE @COUNT_REGIS_TOTAL INT = 1

	SELECT @COUNT_REGIS_TOTAL = COUNT([UID])
	FROM @TABLE_REGISTRATION

	WHILE @COUNT_REGIS <= @COUNT_REGIS_TOTAL
	BEGIN
		SELECT @TBL = TABLE_NAME,
			@TBL_JOIN = JOIN_COND
		FROM @TABLE_REGISTRATION
		WHERE [UID] = @COUNT_REGIS

		SET @sql = 'set @colname = STUFF((select '','' + COLUMN_NAME from ' + @DB_RETENTION + '.INFORMATION_SCHEMA.COLUMNS where table_name = ''' + @TBL + ''' FOR XML PATH('''')), 1, 1, '''')'

		EXEC sp_executesql @sql,
			N' @colname varchar(2000) OUTPUT',
			@colname OUTPUT

		SET @sql = 'SET IDENTITY_INSERT ' + @DB_RETENTION + '.dbo.' + @tbl + ' on; insert into ' + @DB_RETENTION + '.dbo.' + @TBL + ' (' + @colname + ')  select ' + @colname + ' from ' + @DB_SOURCE + '.dbo.' + @TBL + ' AS VC JOIN #RETENTION_VENDOR AS RV ON ' + @TBL_JOIN + ''

		EXEC sp_executesql @sql
		PRINT @sql

		-- DELETE DATA RETENTION
		SET @sql = 'delete VC from ' + @DB_SOURCE + '.dbo.' + @tbl + ' AS VC JOIN #RETENTION_VENDOR AS RV ON ' + @TBL_JOIN + ''
		-- EXEC sp_executesql @sql
		PRINT @sql

		SET @COUNT_REGIS += 1
	END


	-- ============================================================
	-- VENDOR ACTIVITY
	-- ============================================================
	SET @sql = N'
	INSERT INTO #RETENTION_VENDOR_ACTIVITY (
		TMP_DOC_ID,
		TMP_VENDOR_ID,
		TMP_FORM_TYPE
		)
	SELECT VCA.UID,
		VCA.VENDOR_ID,
		VCA.FORM_TYPE
	FROM ' + @DB_SOURCE + '.DBO.TR_VENDOR_CONTROL_ACTIVITY AS VCA
	JOIN #RETENTION_VENDOR AS RV ON RV.TMP_DOC_ID = VCA.DOC_ID'

	EXEC sp_executesql @sql

	DECLARE @COUNT_ACTIVITY INT = 1
	DECLARE @COUNT_ACTIVITY_TOTAL INT = 1

	SELECT @COUNT_ACTIVITY_TOTAL = COUNT([UID])
	FROM @TABLE_ACTIVITY

	WHILE @COUNT_ACTIVITY <= @COUNT_ACTIVITY_TOTAL
	BEGIN
		SELECT @TBL = TABLE_NAME,
			@TBL_JOIN = JOIN_COND
		FROM @TABLE_ACTIVITY
		WHERE [UID] = @COUNT_ACTIVITY

		SET @sql = 'set @colname = STUFF((select '','' + COLUMN_NAME from ' + @DB_RETENTION + '.INFORMATION_SCHEMA.COLUMNS where table_name = ''' + @TBL + ''' FOR XML PATH('''')), 1, 1, '''')'

		EXEC sp_executesql @sql,
			N' @colname varchar(2000) OUTPUT',
			@colname OUTPUT

		SET @sql = 'SET IDENTITY_INSERT ' + @DB_RETENTION + '.dbo.' + @tbl + ' on; insert into ' + @DB_RETENTION + '.dbo.' + @TBL + ' (' + @colname + ')  select ' + @colname + ' from ' + @DB_SOURCE + '.dbo.' + @TBL + ' AS VCA JOIN #RETENTION_VENDOR_ACTIVITY AS RVA ON ' + @TBL_JOIN + ''

		EXEC sp_executesql @sql
		PRINT @sql

		-- DELETE DATA RETENTION
		SET @sql = 'delete VCA from ' + @DB_SOURCE + '.dbo.' + @tbl + ' AS VCA JOIN #RETENTION_VENDOR_ACTIVITY AS RVA ON ' + @TBL_JOIN + ''
		-- EXEC sp_executesql @sql
		PRINT @sql

		SET @COUNT_ACTIVITY += 1
	END

	-- ============================================================
	-- OTHER
	-- ============================================================
	DECLARE @COUNT_OTHER INT = 1
	DECLARE @COUNT_OTHER_TOTAL INT = 1

	SELECT @COUNT_OTHER_TOTAL = COUNT([UID])
	FROM @TABLE_OTHER

	WHILE @COUNT_OTHER <= @COUNT_OTHER_TOTAL
	BEGIN
		SELECT @TBL = TABLE_NAME,
			@TBL_JOIN = JOIN_COND
		FROM @TABLE_OTHER
		WHERE [UID] = @COUNT_OTHER

		SET @sql = 'set @colname = STUFF((select '','' + COLUMN_NAME from ' + @DB_RETENTION + '.INFORMATION_SCHEMA.COLUMNS where table_name = ''' + @TBL + ''' FOR XML PATH('''')), 1, 1, '''')'

		EXEC sp_executesql @sql,
			N' @colname varchar(2000) OUTPUT',
			@colname OUTPUT

		SET @sql = 'SET IDENTITY_INSERT ' + @DB_RETENTION + '.dbo.' + @tbl + ' on; insert into ' + @DB_RETENTION + '.dbo.' + @TBL + ' (' + @colname + ')  select ' + @colname + ' from ' + @DB_SOURCE + '.dbo.' + @TBL + ' WHERE 1 = 1 ' + @TBL_JOIN + ''

		EXEC sp_executesql @sql
		PRINT @sql

		-- DELETE DATA RETENTION
		SET @sql = 'delete ' + @DB_SOURCE + '.dbo.' + @tbl + ' WHERE 1 = 1 ' + @TBL_JOIN + ''
		-- EXEC sp_executesql @sql
		PRINT @sql

		SET @COUNT_OTHER += 1
	END


	IF OBJECT_ID(N'tempdb..#RETENTION_VENDOR') IS NOT NULL
	BEGIN
		DROP TABLE #RETENTION_VENDOR
	END

	IF OBJECT_ID(N'tempdb..#RETENTION_VENDOR_ACTIVITY') IS NOT NULL
	BEGIN
		DROP TABLE #RETENTION_VENDOR_ACTIVITY
	END

END

GO
/****** Object:  StoredProcedure [dbo].[UDPD_ATTACHMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_ATTACHMENT]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.TR_ATTACHMENT
	WHERE [UID] = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPD_ATTACHMENTTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_ATTACHMENTTEMP]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE dbo.TR_ATTACHMENT_TEMP
	WHERE [UID] = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_DEPARTMENTPIC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPD_DEPARTMENTPIC]
	-- Add the parameters for the stored procedure here
	@Id INT
	,@Dept VARCHAR(50)
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
		DELETE MS_DEPT_PIC_CONFIG_DTL
		WHERE DEPT_CD = @Dept
		--WHERE (DEPT_CD = LTRIM(RTRIM(LEFT(@Dept,CHARINDEX('-',@Dept)-1))))
		DELETE MS_DEPT_PIC_CONFIG_HDR
		WHERE DEPT_CD = @Dept
		--WHERE (DEPT_CD = LTRIM(RTRIM(LEFT(@Dept,CHARINDEX('-',@Dept)-1))))
		AND UID = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPD_DRAFTLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPD_DRAFTLIST]
	-- add more stored procedure parameters here
	@ID INT = 0,
	@Form VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	IF @form = 'Vendor Registration'
	BEGIN
		UPDATE TR_VENDOR_CONTROL
		SET IS_ACTIVE = 0
		WHERE [UID] = @ID
	END
	ELSE IF @form = 'Edit Registration'
	BEGIN
		DECLARE @REFID BIGINT = 0

		SELECT @REFID = REFID
		FROM TR_VENDOR_CONTROL_TEMP
		WHERE [UID] = @ID

		UPDATE TR_VENDOR_CONTROL_TEMP
		SET IS_ACTIVE = 0
		WHERE [UID] = @ID

		UPDATE TR_VENDOR_CONTROL
		SET APPROVAL_STS = 'Approval Completed'
		WHERE [UID] = @REFID
	END
	ELSE
	BEGIN
		UPDATE TR_VENDOR_CONTROL_ACTIVITY
		SET IS_ACTIVE = 0
		WHERE [UID] = @ID
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_PARAMETER]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPD_PARAMETER]

	-- Add the parameters for the stored procedure here

	@Id INT

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	DELETE dbo.MS_PARAMETER

	WHERE [UID] = @Id

END




GO
/****** Object:  StoredProcedure [dbo].[UDPD_REVOKESUBMITTED]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPD_REVOKESUBMITTED]
	-- add more stored procedure parameters here
	@TaskCategory VARCHAR(20),
	@ProcessId INT
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DocID INT

	SELECT @DocID = DOCID
	FROM TR_WORKFLOWPROCESS
	WHERE PROCID = @ProcessId

	IF @TaskCategory = 'Vendor Registration'
	BEGIN
		UPDATE TR_VENDOR_CONTROL
		SET STATUS_FLOW = 'DRAFT'
		WHERE [UID] = @DocID
	END
	ELSE IF @TaskCategory = 'Legal Request'
	BEGIN
		UPDATE TR_VENDOR_CONTROL_ACTIVITY
		SET IS_DRAFT = 1
		WHERE FORM_TYPE = 'LEGAL_REQUEST'
			AND UID = @DocID
	END
	ELSE IF @TaskCategory = 'Periodical Review'
	BEGIN
		UPDATE TR_VENDOR_CONTROL_ACTIVITY
		SET IS_DRAFT = 1
		WHERE FORM_TYPE = 'PERIODICAL_REVIEW'
			AND UID = @DocID
	END
	ELSE IF @TaskCategory = 'Continuing Vendor'
	BEGIN
		UPDATE TR_VENDOR_CONTROL_ACTIVITY
		SET IS_DRAFT = 1
		WHERE FORM_TYPE = 'CONTINUING_VENDOR'
			AND UID = @DocID
	END
    ELSE IF @TaskCategory = 'Edit Registration'
	BEGIN
		UPDATE TR_VENDOR_CONTROL_TEMP
		SET STATUS_FLOW = 'DRAFT'
		WHERE UID = @DocID
	END

	UPDATE TR_WORKFLOWPROCESS
	SET ISACTIVE = 0
	WHERE PROCID = @ProcessId
END

GO
/****** Object:  StoredProcedure [dbo].[UDPD_VC_RISK_ASSESSMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VC_RISK_ASSESSMENT]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN

	DELETE [TR_VC_RISK_ASSESSMENT]
	WHERE [UID]=@id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_VENDORLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VENDORLIST]
	-- Add the parameters for the stored procedure here
	@Id INT ,@VENDOR_ID VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE TR_VENDOR_CONTROL
	WHERE VENDOR_ID = @VENDOR_ID
	AND UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_VENDORLIST_ATTACH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VENDORLIST_ATTACH]
	-- Add the parameters for the stored procedure here
	@Id INT 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE [TR_ATTACHMENT]
	WHERE UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_VENDORLIST_CHECKDOC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VENDORLIST_CHECKDOC]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN

	DELETE [TR_VC_CONF_DOC_CHECKLIST]
	WHERE [UID]=@id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_VENDORLIST_CHECKSHEET]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VENDORLIST_CHECKSHEET]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN

	DELETE [TR_VC_ASC_CHECKSHEET]
	WHERE [UID]=@id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_VENDORLIST_EXAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VENDORLIST_EXAM]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN

	DELETE [TR_VC_EXAM_COMPARISON]
	WHERE [UID]=@id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_VENDORLIST_PARAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VENDORLIST_PARAM]
	-- Add the parameters for the stored procedure here
	@Id INT 
	,@VENDOR_ID VARCHAR(50)
	,@VENDOR_NAME VARCHAR(255)
	,@CREATOR VARCHAR(255)
	,@CREATOR_DEPT VARCHAR(50)
	,@CONF_CONTRACTOR_NM VARCHAR(255)
	,@TERMINATION VARCHAR(50)
	,@REG_ACC_NO VARCHAR(50)
	,@APPROVAL_STS VARCHAR(50)
	,@page INT 
	,@size INT
	,@sort nVARCHAR(50)
	,@totalrow int  OUTPUT 
AS
BEGIN

	DECLARE @offset INT
	DECLARE @newsize INT
	DECLARE @sql nvarchar(MAX)

	if (@page=0)
	BEGIN
		SET @offset=@page
		SET @newsize=@size

	END
	ELSE
	BEGIN
		SET @offset=@page*@size
		SET @newsize=@size-1
	END

	SET NOCOUNT ON;

	SELECT [UID] 
		,[VENDOR_ID]
		,[CONF_CONTRACTOR_NM]
		,[VENDOR_NAME]
		,'' AS CATEGORY
		,[CREATOR]
		,[APPROVAL_STS]
	FROM [TR_VENDOR_CONTROL]
	WHERE [VENDOR_ID] LIKE '%'+@VENDOR_ID+'%' 
		AND [VENDOR_NAME] LIKE '%'+@VENDOR_NAME+'%'
		AND [CREATOR] LIKE '%'+@CREATOR+'%'
		AND [APPROVAL_STS] LIKE '%'+@APPROVAL_STS+'%'
	ORDER BY [VENDOR_ID] ASC 
	OFFSET @offset ROWS  
	FETCH NEXT @newsize ROWS ONLY;

END


GO
/****** Object:  StoredProcedure [dbo].[UDPD_VENDORLIST_TMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPD_VENDORLIST_TMP]
	-- Add the parameters for the stored procedure here
	@Id INT ,@VENDOR_ID VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE TR_VENDOR_CONTROL_TMP 
	WHERE VENDOR_ID = @VENDOR_ID
	AND UID = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_ADDCONTINUINGVENDOR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ADDCONTINUINGVENDOR]
@DocID INT,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@ContDescPerformance varchar(8000),
@ContBiddingResult varchar(8000),
@ContReasonUtility varchar(8000),
@ContRelExplain varchar(8000),
@ContChangeForm4b varchar(8000),
@ContIntvCreatorDgm varchar(8000),
@ContIntvGadDgm varchar(8000),
@ContGadRecommendation varchar(8000),
@ApprovalStatus varchar(50),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20),
@IsDraft bit,
@ContComment varchar(8000)
AS
BEGIN
	DECLARE @ContinuingId int = -1
	
	SELECT 
	 @FormType = 'CONTINUING_VENDOR'
	,@VendorId = COALESCE(VENDOR_ID, '') 
	,@ApprovalStatus = 'Awaiting Approval'
	,@IsDraft = 0
	,@IsActive = 1
	FROM TR_VENDOR_CONTROL WHERE UID = @DocID

	IF @FormType = 'CONTINUING_VENDOR'
	BEGIN

	INSERT INTO [TR_VENDOR_CONTROL_ACTIVITY]
	(
		 [DOC_ID]
		,[FORM_TYPE]
		,[CREATOR]
		,[CREATOR_DEPT]
		,[CREATOR_SECTION]
		,[CREATOR_ROLE]
		,[REQUEST_DT]
		,[VENDOR_ID]
		,[DOC_DATE_FORM5]
		,[CONT_DESC_PERFORMANCE]
        ,[CONT_BIDDING_RESULT]
        ,[CONT_REASON_UTILITY]
        ,[CONT_REL_EXPLAIN]
        ,[CONT_CHANGE_FORM4B]
        ,[CONT_INTV_CREATOR_DGM]
        ,[CONT_INTV_GAD_DGM]
        ,[CONT_GAD_RECOMMENDATION]
		,[APPROVAL_STS]
		,[REGISTRATION_DT]
		,[IS_ACTIVE]
		,[CREATED_BY]
		,[CREATED_DT]
		,[CREATED_HOST] 
		,[IS_DRAFT]
	)
	SELECT
		@DocID
		,@FormType
		,@Creator
		,@CreatorDepartment
		,@CreatorSection
		,@CreatorRole
		,@RequestDate
		,@VendorId
		,@DocDate
		,@ContDescPerformance 
		,@ContBiddingResult 
		,@ContReasonUtility 
		,@ContRelExplain 
		,@ContChangeForm4b 
		,@ContIntvCreatorDgm 
		,@ContIntvGadDgm 
		,@ContGadRecommendation
		,@ApprovalStatus
		,@DocDate
		,@IsActive
		,@CreatedBy
		,@CreatedDate
		,@CreatedHost
		,@IsDraft

	--SET @ContinuingId = @@IDENTITY

	--DECLARE @SeqNo int
	--SELECT @SeqNo = COALESCE(COUNT(1),0)
	--FROM TR_DOCUMENT_HISTORY
	--WHERE DOC_ID = @DocID AND HIST_TYPE = 'CONTINUING_REVIEW'

	--INSERT INTO TR_DOCUMENT_HISTORY
	--(DOC_ID, HIST_TYPE, SEQ_NO, VENDOR_ID, 
	--ACTION_BY, ACTION_DT, STATUS, COMMENT, 
	--CREATED_BY, CREATED_DT, CREATED_HOST)

	--SELECT @DocID, 'CONTINUING_REVIEW', @SeqNo+1, @VendorId,
	--@Creator, @RequestDate, 'Submit', @ContComment,
	--@CreatedBy, @CreatedDate, @CreatedHost
	END

	SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_ADDLEGALREQUEST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ADDLEGALREQUEST]
@DocID INT,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@LegalReqDeptCode varchar(50),
@LegalSubject varchar(255),
@LegalFacts varchar(8000),
@LegalSupportingDoc varchar(8000),
@LegalRequest varchar(8000),
@LegalDeadline varchar(8000),
@ApprovalStatus varchar(50),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20),
@IsDraft bit,
@LegalComment varchar(8000)
AS
BEGIN
	DECLARE @LegalId int = -1

	SELECT 
	 @FormType = 'LEGAL_REQUEST'
	,@VendorId = COALESCE(VENDOR_ID, '') 
	,@ApprovalStatus = 'Awaiting Approval'
	,@IsDraft = 0
	,@IsActive = 1
	FROM TR_VENDOR_CONTROL WHERE UID = @DocID

	IF @FormType = 'LEGAL_REQUEST'
	BEGIN
		INSERT INTO [TR_VENDOR_CONTROL_ACTIVITY]
		(
			 [DOC_ID]
			,[FORM_TYPE]
			,[CREATOR]
			,[CREATOR_DEPT]
			,[CREATOR_SECTION]
			,[CREATOR_ROLE]
			,[REQUEST_DT]
			,[VENDOR_ID]
			,[DOC_DATE_FORM6]
			,[LGL_REQ_DEPT_CD]
			,[LGL_SUBJECT]
			,[LGL_FACTS]
			,[LGL_SUPPORTING_DOC]
			,[LGL_REQUEST]
			,[LGL_DEADLINE]
			,[APPROVAL_STS]
			,[REGISTRATION_DT]
			,[IS_ACTIVE]
			,[CREATED_BY]
			,[CREATED_DT]
			,[CREATED_HOST] 
			,[IS_DRAFT]
		)
		SELECT
			@DocID
			,@FormType
			,@Creator
			,@CreatorDepartment
			,@CreatorSection
			,@CreatorRole
			,@RequestDate
			,@VendorId
			,@DocDate
			,@LegalReqDeptCode
			,@LegalSubject
			,@LegalFacts
			,@LegalSupportingDoc
			,@LegalRequest
			,@LegalDeadline
			,@ApprovalStatus
			,@DocDate
			,@IsActive
			,@CreatedBy
			,@CreatedDate
			,@CreatedHost
			,@IsDraft

		--DECLARE @SeqNo int
		--SET @LegalId = @@IDENTITY

		--SELECT 
		--	@SeqNo = COALESCE(COUNT(1),0)
		--FROM TR_DOCUMENT_HISTORY
		--WHERE VENDOR_ID = @VendorId 
		--AND HIST_TYPE = 'LEGAL_REVIEW'

		--INSERT INTO TR_DOCUMENT_HISTORY
		--(DOC_ID, HIST_TYPE, SEQ_NO, VENDOR_ID, 
		--ACTION_BY, ACTION_DT, STATUS, COMMENT, 
		--CREATED_BY, CREATED_DT, CREATED_HOST)

		--SELECT @LegalId, 'LEGAL_REVIEW', @SeqNo+1, @VendorId,
		--@Creator, @RequestDate, 'Submit', @LegalComment,
		--@CreatedBy, @CreatedDate, @CreatedHost
	END

	SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_ADDPERIODICALREVIEW]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ADDPERIODICALREVIEW]
@DocId INT,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(50),
@DocDate date,
@NextPeriodicalReviewDate varchar(255),

@GADRecommendation varchar(8000),
@RMDComment varchar(8000),
@CommentBox varchar(255),

@ApprovalStatus varchar(50),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20),
@IsDraft bit
AS
BEGIN
	DECLARE @PeriodicalId int = -1

	SELECT 
	 @FormType = 'PERIODICAL_REVIEW'
	,@VendorId = COALESCE(VENDOR_ID, '') 
	,@ApprovalStatus = 'Awaiting Approval'
	,@IsDraft = 0
	FROM TR_VENDOR_CONTROL WHERE UID = @DocID

	IF @FormType = 'PERIODICAL_REVIEW'
	BEGIN
		INSERT INTO [TR_VENDOR_CONTROL_ACTIVITY]
		(
			 [DOC_ID]
			,[FORM_TYPE]
			,[CREATOR]
			,[CREATOR_DEPT]
			,[CREATOR_SECTION]
			,[CREATOR_ROLE]
			,[VENDOR_ID]
			,[REQUEST_DT]
			,[DOC_DATE_FORM4]
			,[REV_REVIEW_DT]
			,[REV_GAD_RECOMMENDATION]
			,[REV_RMD_COMMENT]
			,[APPROVAL_STS]
			,[IS_ACTIVE]
			,[CREATED_BY]
			,[CREATED_DT]
			,[CREATED_HOST] 
		)
		SELECT
			@DocID,
			@FormType,
			@Creator,
			@CreatorDepartment,
			@CreatorSection,
			@CreatorRole,
			@VendorId,
			@RequestDate,
			@DocDate,
			@NextPeriodicalReviewDate,
			@GADRecommendation,
			@RMDComment,
			@ApprovalStatus,
			@IsActive,
			@CreatedBy ,
			@CreatedDate,
			@CreatedHost
		
		
	END

	SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_ADDPERIODICALREVIEW_AML]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ADDPERIODICALREVIEW_AML]
	@DocId int,
	@SequenceNumber int,
	@Name varchar(255),
	@Title varchar(255),
	@IsActive bit,
	@CreatedBy varchar(50),
	@CreatedDate datetime,
	@CreatedHost varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[TR_VC_ACTIVITY_REV_AML] 
	(
       [DOC_ID]
      ,[SEQ_NO]
      ,[NAME]
      ,[TITLE]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
	  ,[CREATED_HOST]

	)
	VALUES
	(	
	@DocId,
	@SequenceNumber,
	@Name,
	@Title,
	@IsActive,
	@CreatedBy,
	@CreatedDate,
	@CreatedHost 

	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_ADDPERIODICALREVIEW_EXAMINED]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_ADDPERIODICALREVIEW_EXAMINED]
	@DocId int,
	@SequenceNumber int,
	@Point varchar(255),
	@Result bit,
	@ResultComment varchar(255),
	@IsActive bit,
	@CreatedBy varchar(50),
	@CreatedDate datetime,
	@CreatedHost varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[TR_VC_ACTIVITY_REV_EXAMINED] 
	(
       [DOC_ID]
      ,[SEQ_NO]
      ,[POINT]
      ,[RESULT]
      ,[COMMENT]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
	  ,[CREATED_HOST]

	)
	VALUES
	(	
	@DocId,
	@SequenceNumber,
	@Point,
	@Result,
	@ResultComment,
	@IsActive,
	@CreatedBy,
	@CreatedDate,
	@CreatedHost 

	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_ATTACHMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPI_ATTACHMENT]
	-- add more stored procedure parameters here
	@DocumentId BIGINT,
	@FileType VARCHAR(20),
	@FileName VARCHAR(80),
	@SaveFileName VARCHAR(80),
	@FileUrl VARCHAR(200),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20)
AS
BEGIN
	-- body of the stored procedure
	INSERT INTO TR_ATTACHMENT (
		DOC_ID,
		FILE_TYPE,
		FILE_NM,
		SAVE_FILE_NM,
		FILE_URL,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
		)
	VALUES (
		@DocumentId,
		@FileType,
		@FileName,
		@SaveFileName,
		@FileUrl,
		@CreatedBy,
		@CreatedDate,
		@CreatedHost
		)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_ATTACHMENTTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPI_ATTACHMENTTEMP]
	-- add more stored procedure parameters here
	@DocumentId BIGINT,
	@FileType VARCHAR(20),
	@FileName VARCHAR(80),
	@SaveFileName VARCHAR(80),
	@FileUrl VARCHAR(200),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20)
AS
BEGIN
	-- body of the stored procedure
	INSERT INTO TR_ATTACHMENT_TEMP (
		DOC_ID,
		FILE_TYPE,
		FILE_NM,
		SAVE_FILE_NM,
		FILE_URL,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
		)
	VALUES (
		@DocumentId,
		@FileType,
		@FileName,
		@SaveFileName,
		@FileUrl,
		@CreatedBy,
		@CreatedDate,
		@CreatedHost
		)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_CONTROLAGREEMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPI_CONTROLAGREEMENT]

	-- Add the parameters for the stored procedure here
	@Id int
	,@AgreeStratDate DATETIME
	,@AgreeEndDate DATETIME
	,@AgreeCreatedBy VARCHAR(50)
	,@AgreeCreatedDate DATETIME
	,@AgreeModifiedBy VARCHAR(50)
	,@AgreeModifiedDate DATETIME
	
AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;

	-- Insert statements for procedure here

	UPDATE dbo.TR_VENDOR_CONTROL
		SET AGREE_START_DATE = @AgreeStratDate
			,AGREE_END_DATE = @AgreeEndDate
			,AGREE_CREATED_BY = @AgreeCreatedBy
			,AGREE_CREATED_DT = @AgreeCreatedDate
			,AGREE_MODIFIED_BY = @AgreeModifiedBy
			,AGREE_MODIFIED_DT = @AgreeModifiedDate
	WHERE UID = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_DEPARTMENTPIC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DEPARTMENTPIC]
	-- Add the parameters for the stored procedure here
	@DepartmentCode VARCHAR(50)
	,@CreatedBy VARCHAR(50)
	,@CreatedDate DATETIME
	,@CreatedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     --Insert statements for procedure here
	INSERT INTO dbo.MS_DEPT_PIC_CONFIG_HDR
	(
		DEPT_CD,
	    CREATED_BY,
	    CREATED_DT,
	    CREATED_HOST
	)
	VALUES
	(	@DepartmentCode,
		@CreatedBy,        -- CREATED_BY - varchar(50)
	    @CreatedDate, -- CREATED_DT - datetime
	    @CreatedHost        -- CREATED_HOST - varchar(20)
	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_DEPARTMENTPICDTL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DEPARTMENTPICDTL]
	-- Add the parameters for the stored procedure here
	@DepartmentCode VARCHAR(50)= 0
	,@MemberCode VARCHAR(2000)
	,@CreatedBy VARCHAR(50)
	,@CreatedDate DATETIME
	,@CreatedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN

	DECLARE  @DepartmentPicDetailList TABLE(
		id INT IDENTITY (1,1),
		nama VARCHAR(200))

	IF (@MemberCode != '')
		BEGIN
			INSERT INTO @DepartmentPicDetailList(nama)
			SELECT *
			FROM dbo.splitstring(@MemberCode, ',')

			INSERT INTO dbo.MS_DEPT_PIC_CONFIG_DTL(
				DEPT_CD,
				MEMBER_CD,
				CREATED_BY,
				CREATED_DT,
				CREATED_HOST
				)
			SELECT @DepartmentCode,
				rc.nama,
				@CreatedBy,
				@CreatedDate,
				@CreatedHost
				FROM @DepartmentPicDetailList AS rc
		END
	IF @@ERROR > 0
		ROLLBACK TRAN
	ELSE
		COMMIT TRAN 

		SELECT @DepartmentCode = @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_DOCUMENTHISTORY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_DOCUMENTHISTORY]
	-- Add the parameters for the stored procedure here
	@HistoryType VARCHAR(20),
	@DocId BIGINT,
	@VenId VARCHAR(50) = 0,
	@User VARCHAR(50),
	@CreatedBy VARCHAR(50) = '',
	@ModifiedBy VARCHAR(50) = '',
	@Status VARCHAR(20),
	@Comment VARCHAR(2000) = '',
	@Step INT = 0
AS
BEGIN
	DECLARE @SeqNo INT = 1

	SELECT @SeqNo = max(isnull(seq_no, 0)) + 1
	FROM TR_DOCUMENT_HISTORY
	WHERE 1 = 1
		AND HIST_TYPE = @HistoryType
		AND DOC_ID = @DocId

	SET @SeqNo = ISNULL(@SeqNo, 1)

	IF charindex('JAKARTA01\', @User) > 0
		SET @User = replace(@User, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @User) > 0
		SET @User = replace(@User, 'jakarta01\', '')

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	INSERT INTO TR_DOCUMENT_HISTORY (
		SEQ_NO,
		DOC_ID,
		HIST_TYPE,
		VENDOR_ID,
		STATUS,
		COMMENT,
        STEP,
		ACTION_BY,
		ACTION_DT,
		CREATED_BY,
		CREATED_DT,
		MODIFIED_BY,
		MODIFIED_DT
		)
	VALUES (
		@SeqNo,
		@DocId,
		@HistoryType,
		@VenId,
		@Status,
		@Comment,
        @Step,
		@User,
		GETDATE(),
		@CreatedBy,
		GETDATE(),
		@ModifiedBy,
		GETDATE()
		)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_DRAFTCONTINUINGVENDOR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DRAFTCONTINUINGVENDOR]
@DocID INT,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@ContDescPerformance varchar(8000),
@ContBiddingResult varchar(8000),
@ContReasonUtility varchar(8000),
@ContRelExplain varchar(8000),
@ContChangeForm4b varchar(8000),
@ContIntvCreatorDgm varchar(8000),
@ContIntvGadDgm varchar(8000),
@ContGadRecommendation varchar(8000),
@ApprovalStatus varchar(50),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20),
@IsDraft bit = 1,
@ContComment varchar(8000) = ''
AS
BEGIN
	
	
	SELECT 
	 @FormType = 'CONTINUING_VENDOR'
	,@VendorId = COALESCE(VENDOR_ID, '') 
	,@ApprovalStatus = NULL
	,@IsDraft = 1
	,@IsActive = 1 
	FROM TR_VENDOR_CONTROL WHERE UID = @DocID

	INSERT INTO [TR_VENDOR_CONTROL_ACTIVITY]
	(
		 [DOC_ID]
		,[FORM_TYPE]
		,[CREATOR]
		,[CREATOR_DEPT]
		,[CREATOR_SECTION]
		,[CREATOR_ROLE]
		,[REQUEST_DT]
		,[VENDOR_ID]
		,[DOC_DATE_FORM5]
		,[CONT_DESC_PERFORMANCE]
        ,[CONT_BIDDING_RESULT]
        ,[CONT_REASON_UTILITY]
        ,[CONT_REL_EXPLAIN]
        ,[CONT_CHANGE_FORM4B]
        ,[CONT_INTV_CREATOR_DGM]
        ,[CONT_INTV_GAD_DGM]
        ,[CONT_GAD_RECOMMENDATION]
		,[APPROVAL_STS]
		,[IS_ACTIVE]
		,[CREATED_BY]
		,[CREATED_DT]
		,[CREATED_HOST]
		,[IS_DRAFT] 
	)
	SELECT
		@DocID
		,@FormType
		,@Creator
		,@CreatorDepartment
		,@CreatorSection
		,@CreatorRole
		,@RequestDate
		,@VendorId
		,@DocDate
		,@ContDescPerformance 
		,@ContBiddingResult 
		,@ContReasonUtility 
		,@ContRelExplain 
		,@ContChangeForm4b 
		,@ContIntvCreatorDgm 
		,@ContIntvGadDgm 
		,@ContGadRecommendation
		,@ApprovalStatus
		,@IsActive
		,@CreatedBy
		,@CreatedDate
		,@CreatedHost
		,@IsDraft

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_DRAFTLEGALREQUEST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DRAFTLEGALREQUEST]
@DocID INT,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@LegalReqDeptCode varchar(50),
@LegalSubject varchar(255),
@LegalFacts varchar(8000),
@LegalSupportingDoc varchar(8000),
@LegalRequest varchar(8000),
@LegalDeadline varchar(8000),
@ApprovalStatus varchar(50),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20),
@IsDraft bit = 1,
@LegalComment varchar(8000) = ''
AS
BEGIN

	SELECT 
	 @FormType = 'LEGAL_REQUEST'
	,@VendorId = COALESCE(VENDOR_ID, '') 
	,@ApprovalStatus = NULL
	,@IsDraft = 1
	,@IsActive = 1
	FROM [TR_VENDOR_CONTROL] WHERE UID = @DocID

	INSERT INTO [TR_VENDOR_CONTROL_ACTIVITY]
	(
		 [DOC_ID]
		,[FORM_TYPE]
		,[CREATOR]
		,[CREATOR_DEPT]
		,[CREATOR_SECTION]
		,[CREATOR_ROLE]
		,[REQUEST_DT]
		,[VENDOR_ID]
		,[DOC_DATE_FORM6]
		,[LGL_REQ_DEPT_CD]
		,[LGL_SUBJECT]
		,[LGL_FACTS]
		,[LGL_SUPPORTING_DOC]
		,[LGL_REQUEST]
		,[LGL_DEADLINE]
		,[APPROVAL_STS]
		,[IS_ACTIVE]
		,[CREATED_BY]
		,[CREATED_DT]
		,[CREATED_HOST] 
		,[IS_DRAFT]
	)
	SELECT
		@DocID
		,@FormType
		,@Creator
		,@CreatorDepartment
		,@CreatorSection
		,@CreatorRole
		,@RequestDate
		,@VendorId
		,@DocDate
		,@LegalReqDeptCode
		,@LegalSubject
		,@LegalFacts
		,@LegalSupportingDoc
		,@LegalRequest
		,@LegalDeadline
		,@ApprovalStatus
		,@IsActive
		,@CreatedBy
		,@CreatedDate
		,@CreatedHost
		,@IsDraft

	SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_DRAFTPERIODICALREVIEW]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DRAFTPERIODICALREVIEW]
@DocID int,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@NextPeriodicalReviewDate varchar(255),
@GADRecommendation varchar(255),
@RMDComment varchar(255),
@CommentBox varchar(255),
@ApprovalStatus varchar(50),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20),
@IsDraft bit = 1
AS
BEGIN

	SELECT 
	 @FormType = 'PERIODICAL_REVIEW'
	,@VendorId = COALESCE(VENDOR_ID, '') 
	,@ApprovalStatus = NULL
	,@IsDraft = 1
	,@IsActive = 1
	FROM [TR_VENDOR_CONTROL] WHERE UID = @DocID

		INSERT INTO [TR_VENDOR_CONTROL_ACTIVITY]
		(
			 [DOC_ID]
			,[FORM_TYPE]
			,[CREATOR]
			,[CREATOR_DEPT]
			,[CREATOR_SECTION]
			,[CREATOR_ROLE]
			,[REQUEST_DT]
			,[VENDOR_ID]
			,[DOC_DATE_FORM4]
			,[REV_REVIEW_DT]
			,[REV_GAD_RECOMMENDATION]
			,[REV_RMD_COMMENT]
			,[APPROVAL_STS]
			,[IS_ACTIVE]
			,[CREATED_BY]
			,[CREATED_DT]
			,[CREATED_HOST] 
			,[IS_DRAFT] 
		)
		SELECT
			@DocID,
			@FormType,
			@Creator,
			@CreatorDepartment,
			@CreatorSection,
			@CreatorRole,
			@RequestDate,
			@VendorId,
			@DocDate,
			@NextPeriodicalReviewDate,
			@GADRecommendation,
			@RMDComment,
			@ApprovalStatus,
			@IsActive,
			@CreatedBy ,
			@CreatedDate,
			@CreatedHost,
			@IsDraft

		SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_DRAFTPERIODICALREVIEW_AML]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DRAFTPERIODICALREVIEW_AML]
@DocID int,
@SequenceNumber int,
@Name varchar(255),
@Title varchar(255),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20)

AS
BEGIN

		INSERT INTO [TR_VC_ACTIVITY_REV_AML]
		(
			 [DOC_ID]
			,[SEQ_NO]
			,[NAME]
			,[TITLE]
			,[IS_ACTIVE]
			,[CREATED_BY]
			,[CREATED_DT]
			,[CREATED_HOST] 
		)
		SELECT
			@DocID,
			@SequenceNumber,
			@Name,
			@Title,
			@IsActive,
			@CreatedBy ,
			@CreatedDate,
			@CreatedHost

		SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_DRAFTPERIODICALREVIEW_EXAMINED]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_DRAFTPERIODICALREVIEW_EXAMINED]
@DocID int,
@SequenceNumber int,
@Point varchar(255),
@Result bit,
@ResultComment varchar(255),
@IsActive bit,
@CreatedBy varchar(50),
@CreatedDate datetime,
@CreatedHost varchar(20)

AS
BEGIN

		INSERT INTO [TR_VC_ACTIVITY_REV_EXAMINED]
		(
			 [DOC_ID]
			,[SEQ_NO]
			,[POINT]
			,[RESULT]
			,[COMMENT]
			,[IS_ACTIVE]
			,[CREATED_BY]
			,[CREATED_DT]
			,[CREATED_HOST] 
		)
		SELECT
			@DocID,
			@SequenceNumber,
			@Point,
			@Result,
			@ResultComment,
			@IsActive,
			@CreatedBy ,
			@CreatedDate,
			@CreatedHost

		SELECT @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[UDPI_PARAMETER]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPI_PARAMETER]

	-- Add the parameters for the stored procedure here

	@Group VARCHAR(30),

	@Value VARCHAR(5000),

	@Description VARCHAR(8000),

	@Sequence INT,

	@CreatedBy VARCHAR(50),

	@CreatedDate DATETIME,

	@CreatedHost VARCHAR(20)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



    -- Insert statements for procedure here

	INSERT INTO dbo.MS_PARAMETER

	(

	    [GROUP],

	    VALUE,

	    DESCS,

	    SEQ,

	    CREATED_BY,

	    CREATED_DT,

	    CREATED_HOST

	)

	VALUES

	(   @Group,        -- GROUP - varchar(30)

	    @Value,        -- VALUE - varchar(5000)

	    @Description,        -- DESCS - varchar(8000)

	    @Sequence,         -- SEQ - int

	    @CreatedBy,        -- CREATED_BY - varchar(50)

	    @CreatedDate, -- CREATED_DT - datetime

	    @CreatedHost        -- CREATED_HOST - varchar(20)

	)



	SELECT @@IDENTITY

END




GO
/****** Object:  StoredProcedure [dbo].[UDPI_REMINDERLOG]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211123>
-- Description:	<Insert Reminder Log>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_REMINDERLOG]
	@ReminderSource VARCHAR(200)
	,@DepartmentCode varchar(255)
	,@VendorId varchar(255)
	,@SentTo VARCHAR(MAX)
	,@SentCC VARCHAR(MAX)
	,@SentDate DATETIME
	,@MailSubject VARCHAR(MAX)
	,@MailBody VARCHAR(MAX)
	,@MailToEmail VARCHAR(MAX)
	,@MailCCEmail VARCHAR(MAX)
	,@LastStatus VARCHAR(200)
	,@CreatedBy VARCHAR(50)
	,@CreatedDate DATETIME
	,@CreatedHost VARCHAR(20)
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO dbo.TR_REMINDER_LOG
	(
	    RMD_SOURCE,
	    DEPT_CD,
	    VENDOR_ID,
		SENT_TO,
		SENT_CC,
	    SENT_DT,
		MAIL_SUBJECT,
		MAIL_BODY,
		MAIL_TO_EMAIL,
		MAIL_CC_EMAIL,
		LAST_STATUS,	
	    CREATED_BY,
	    CREATED_DT,
	    CREATED_HOST
		--,
		--MODIFIED_BY,
	 --   MODIFIED_DT,
	 --   MODIFIED_HOST
	)
	VALUES
	(	@ReminderSource
		,@DepartmentCode
		,@VendorId
		,@SentTo
		,@SentCC 
		,@SentDate
		,@MailSubject
		,@MailBody
		,@MailToEmail
		,@MailCCEmail
		,@LastStatus 
		,@CreatedBy 
		,@CreatedDate
		,@CreatedHost
		--,@ModifiedBy 
		--,@ModifiedDate
		--,@ModifiedHost
	)

	SELECT @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_SETTING]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_SETTING]
	-- Add the parameters for the stored procedure here
	@RevCC VARCHAR(8000)
	,@RevBwdReminder BIT
	,@RevBwdStartedFrom INT
	,@RevBwdRecurringDays INT
	,@RevFwdReminder BIT
	,@RevFwdRecurringDays INT
	,@ContCC VARCHAR(8000)
	,@ContBwdReminder BIT
	,@ContBwdStartedFrom INT
	,@ContBwdRecurringDays INT
	,@ContFwdReminder BIT
	,@ContFwdRecurringDays INT
	,@ApvRmdReminder BIT
	,@ApvRmdRecurringDays INT
	,@GadDeptmentName VARCHAR(50)
	,@GadPicName VARCHAR(8000)
	,@RmdDeptmentName VARCHAR(50)
	,@RmdStaffName VARCHAR(8000)
	,@CpcDeptmentName VARCHAR(50)
	,@CpcStaffName VARCHAR(8000)
	,@FcdDeptmentName VARCHAR(50)
	,@FcdPicName VARCHAR(8000)
	,@LglDeptmentName VARCHAR(50)
	,@LglPicName VARCHAR(8000)
	,@CreatedBy VARCHAR(50)
	,@CreatedDate DATETIME
	,@CreatedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.MS_SETTING
	(
	    REV_CC,
	    REV_BWD_ACTIVE,
	    REV_BWD_START_FROM,
	    REV_BWD_RECUR_DAY,
		REV_FWD_ACTIVE,
		REV_FWD_RECUR_DAY,
		CONT_CC,
		CONT_BWD_ACTIVE,
		CONT_BWD_START_FROM,
		CONT_BWD_RECUR_DAY,
		CONT_FWD_ACTIVE,
		CONT_FWD_RECUR_DAY,
		APV_RMD_ACTIVE,
		APV_RMD_RECUR_DAY,
		GAD_DEPT_CD,
		GAD_PIC,
		RMD_DEPT_CD,
		RMD_STAFF,
		CPC_DEPT_CD,
		CPC_STAFF,
		FCD_DEPT_CD,
		FCD_PIC,
		LGL_DEPT_CD,
		LGL_PIC,
	    CREATED_BY,
	    CREATED_DT,
	    CREATED_HOST
	)
	VALUES
	(	@RevCC,
		@RevBwdReminder,
		@RevBwdStartedFrom,
		@RevBwdRecurringDays,
		@RevFwdReminder,
		@RevFwdRecurringDays,
		@ContCC,
		@ContBwdReminder,
		@ContBwdStartedFrom,
		@ContBwdRecurringDays,
		@ContFwdReminder,
		@ContFwdRecurringDays,
		@ApvRmdReminder,
		@ApvRmdRecurringDays,
		@GadDeptmentName,
		@GadPicName,
		@RmdDeptmentName,
		@RmdStaffName,
		@CpcDeptmentName,
		@CpcStaffName,
		@FcdDeptmentName,
		@FcdPicName,
		@LglDeptmentName,
		@LglPicName,
	    @CreatedBy,        -- CREATED_BY - varchar(50)
	    @CreatedDate, -- CREATED_DT - datetime
	    @CreatedHost        -- CREATED_HOST - varchar(20)
	)

	SELECT @@IDENTITY
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_USERACTIVITYLOGDTL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_USERACTIVITYLOGDTL]
	-- Add the parameters for the stored procedure here
	@DocumentId INT,
	@ActivityCode VARCHAR(50),
	@FieldName VARCHAR(80),
	@OldValue VARCHAR(8000),
	@NewValue VARCHAR(8000),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@OldValue = '(null)')
		SET @OldValue = ''

	IF (@NewValue = '(null)')
		SET @NewValue = ''

	-- Insert statements for procedure here
	INSERT INTO dbo.TR_USER_ACTIVITY_LOG_DTL (
		DOC_ID,
		FIELD_NM,
		OLD_VALUE,
		NEW_VALUE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
		)
	VALUES (
		@DocumentId, -- DOC_ID - bigint
		@FieldName, -- FIELD_NM - varchar(80)
		@OldValue, -- OLD_VALUE - varchar(8000)
		@NewValue, -- NEW_VALUE - varchar(8000)
		@CreatedBy, -- CREATED_BY - varchar(50)
		@CreatedDate, -- CREATED_DT - datetime
		@CreatedHost -- CREATED_HOST - varchar(20)
		)
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_USERACTIVITYLOGHDR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPI_USERACTIVITYLOGHDR]
	-- Add the parameters for the stored procedure here
	@ActivityCode VARCHAR(50),
	@ActivityDescription VARCHAR(100),
	@ActivityValue VARCHAR(500),
	@ActivityResult VARCHAR(50),
	@ActivityUserName VARCHAR(50),
	@ActivityDepartment VARCHAR(100),
	@ActivityDate DATETIME,
	@ActivityWorkstation VARCHAR(30),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@CreatedHost VARCHAR(20),
	@WFGUID UNIQUEIDENTIFIER
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DocumentID BIGINT

	-- Insert statements for procedure here
	INSERT INTO dbo.TR_USER_ACTIVITY_LOG_HDR (
		ACT_CD,
		ACT_DESC,
		ACT_VAL,
		ACT_RESULT,
		ACT_USER,
		ACT_DEPT,
		ACT_DT,
		ACT_WORKSTATION,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		WFGUID
		)
	VALUES (
		@ActivityCode, -- ACT_CD - varchar(50)
		@ActivityDescription, -- ACT_DESC - varchar(100)
		@ActivityValue, -- ACT_VAL - varchar(500)
		'Success', -- ACT_RESULT - varchar(50)
		@ActivityUserName, -- ACT_USER - varchar(50)
		@ActivityDepartment, -- ACT_DEPT - varchar(30)
		@ActivityDate, -- ACT_DT - datetime
		@ActivityWorkstation, -- ACT_WORKSTATION - varchar(30)
		@CreatedBy, -- CREATED_BY - varchar(50)
		@CreatedDate, -- CREATED_DT - datetime
		@CreatedHost, -- CREATED_HOST - varchar(20)
		@WFGUID
		)

	SELECT @DocumentID = @@IDENTITY

	SELECT @DocumentID
	--DECLARE @DICFollowUpID VARCHAR(80)
	--DECLARE @IssueNumber VARCHAR(50)

	--IF (UPPER(@ActivityCode) = 'REGIS')
	--BEGIN
	--	--AF Issue Num : {ISSUE_NUMBER}
	--	SELECT 1
	--END
	--ELSE IF (UPPER(@ActivityCode) = 'DICFU')
	--BEGIN
	--	--AF Issue Num : {ISSUE_NUMBER}; DICFU's ID  : {DIC_FOL_ID}
	--	IF (UPPER(@ActivityDescription) = 'DICFU-WKF-SUBMIT NEW')
	--	BEGIN
	--		SELECT @DICFollowUpID = tafa.DIC_FOL_UP_ID,
	--			@IssueNumber = taf.ISSUE_NO
	--		FROM TR_AUDIT_FINDING_ACTIVITY AS tafa
	--		JOIN TR_WORKFLOWPROCESS AS tw ON tw.DOCID = tafa.UID
	--		JOIN TR_AUDIT_FINDING AS taf ON taf.UID = tafa.DOC_ID
	--		WHERE tw.WFGUID = @WFGUID

	--		UPDATE TR_USER_ACTIVITY_LOG_HDR
	--		SET ACT_VAL = 'AF Issue Num : ' + @IssueNumber + '; DICFU''s ID  : ' + @DICFollowUpID
	--		WHERE WFGUID = @WFGUID
	--	END
	--END
	--ELSE IF (UPPER(@ActivityCode) = 'DDE')
	--BEGIN
	--	--AF Issue Num : {ISSUE_NUMBER}; DICFU's ID  : {DIC_FOL_ID};  DDE's New Due Date : {NEW_DUE_DATE}
	--	SELECT 1
	--END
	--ELSE IF (UPPER(@ActivityCode) = 'SET')
	--BEGIN
	--	--AF Issue Num : {ISSUE_NUMBER}
	--	SELECT 1
	--END
	--ELSE IF (UPPER(@ActivityCode) = 'PAR')
	--BEGIN
	--	--Group: {GROUP}
	--	SELECT 1
	--END

	--SELECT @DocumentID
END




GO
/****** Object:  StoredProcedure [dbo].[UDPI_VC_RISK_ASSESSMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_VC_RISK_ASSESSMENT]
	-- Add the parameters for the stored procedure here
@DOC_ID VARCHAR(50)
      ,@SEQ_NO VARCHAR(50)
      ,@PARAMETER VARCHAR(80)
      ,@ASSESS_VENDOR_A VARCHAR(255)
      ,@ASSESS_VENDOR_B VARCHAR(255)
      ,@REMARK VARCHAR(8000)
      ,@IS_ACTIVE VARCHAR(50)
      ,@CREATED_BY VARCHAR(50)
      ,@CREATED_HOST VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT [UID] FROM dbo.TR_VC_RISK_ASSESSMENT 
                   WHERE [PARAMETER] = @PARAMETER
                   AND [DOC_ID] = @DOC_ID)
	BEGIN
		 --Insert statements for procedure here
		INSERT INTO dbo.TR_VC_RISK_ASSESSMENT 
		(
		  [DOC_ID]
		  ,[SEQ_NO]
		  ,[PARAMETER]
		  ,[ASSESS_VENDOR_A]
		  ,[ASSESS_VENDOR_B]
		  ,[REMARK]
		  ,[IS_ACTIVE]
		  ,[CREATED_BY]
		  ,[CREATED_DT]
		  ,[CREATED_HOST]

		)
		VALUES
		(	
		  @DOC_ID 
		  ,@SEQ_NO 
		  ,@PARAMETER 
		  ,@ASSESS_VENDOR_A 
		  ,@ASSESS_VENDOR_B 
		  ,@REMARK 
		  ,@IS_ACTIVE 
		  ,@CREATED_BY 
		  ,GETDATE()  
		  ,@CREATED_HOST 
    
		)

		SELECT @@IDENTITY
	END
	ELSE
	BEGIN
		SELECT '0';
	END

END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPI_VENDORDETAIL]
	-- add more stored procedure parameters here
	@VENDOR_ID VARCHAR(50) = NULL,
	@CREATOR VARCHAR(255) = NULL,
	@CREATOR_DEPT VARCHAR(255) = NULL,
	@CREATOR_UNIT VARCHAR(255) = NULL,
	@CREATOR_ROLE VARCHAR(255) = NULL,
	@REQUEST_DT DATE = NULL,
	@DOC_DATE_FORM1 DATE = NULL,
	@CONF_CONTRACTOR_NM VARCHAR(255) = NULL,
	@CONF_OPERATION_TO_OUTSOURCE VARCHAR(255) = NULL,
	@CONF_CATEGORY VARCHAR(255) = NULL,
	@CONF_DEPT_CD VARCHAR(50) = NULL,
	@CONF_BRIEF_EXPLAIN_CONTRACT VARCHAR(8000) = NULL,
	@ITEM_1 VARCHAR(255) = NULL,
	@SUBMITTED_1 BIT = NULL,
	@REASON_1 VARCHAR(8000) = NULL,
	@CHECKED_1 BIT = NULL,
    @REASON_CHECKED_1 VARCHAR(8000) = NULL,
	@ITEM_2 VARCHAR(255) = NULL,
	@SUBMITTED_2 BIT = NULL,
	@REASON_2 VARCHAR(8000) = NULL,
	@CHECKED_2 BIT = NULL,
    @REASON_CHECKED_2 VARCHAR(8000) = NULL,
	@ITEM_3 VARCHAR(255) = NULL,
	@SUBMITTED_3 BIT = NULL,
	@REASON_3 VARCHAR(8000) = NULL,
	@CHECKED_3 BIT = NULL,
    @REASON_CHECKED_3 VARCHAR(8000) = NULL,
	@ITEM_4 VARCHAR(255) = NULL,
	@SUBMITTED_4 BIT = NULL,
	@REASON_4 VARCHAR(8000) = NULL,
	@CHECKED_4 BIT = NULL,
    @REASON_CHECKED_4 VARCHAR(8000) = NULL,
	@ITEM_5 VARCHAR(255) = NULL,
	@SUBMITTED_5 BIT = NULL,
	@REASON_5 VARCHAR(8000) = NULL,
	@CHECKED_5 BIT = NULL,
    @REASON_CHECKED_5 VARCHAR(8000) = NULL,
	@ITEM_6 VARCHAR(255) = NULL,
	@SUBMITTED_6 BIT = NULL,
	@REASON_6 VARCHAR(8000) = NULL,
	@CHECKED_6 BIT = NULL,
    @REASON_CHECKED_6 VARCHAR(8000) = NULL,
	@ITEM_7 VARCHAR(255) = NULL,
	@SUBMITTED_7 BIT = NULL,
	@REASON_7 VARCHAR(8000) = NULL,
	@CHECKED_7 BIT = NULL,
    @REASON_CHECKED_7 VARCHAR(8000) = NULL,
	@ITEM_8 VARCHAR(255) = NULL,
	@SUBMITTED_8 BIT = NULL,
	@REASON_8 VARCHAR(8000) = NULL,
	@CHECKED_8 BIT = NULL,
    @REASON_CHECKED_8 VARCHAR(8000) = NULL,
	@ITEM_9 VARCHAR(255) = NULL,
	@SUBMITTED_9 BIT = NULL,
	@REASON_9 VARCHAR(8000) = NULL,
	@CHECKED_9 BIT = NULL,
    @REASON_CHECKED_9 VARCHAR(8000) = NULL,
	@ITEM_10 VARCHAR(255) = NULL,
	@SUBMITTED_10 BIT = NULL,
	@REASON_10 VARCHAR(8000) = NULL,
	@CHECKED_10 BIT = NULL,
    @REASON_CHECKED_10 VARCHAR(8000) = NULL,
	@ITEM_11 VARCHAR(255) = NULL,
	@SUBMITTED_11 BIT = NULL,
	@REASON_11 VARCHAR(8000) = NULL,
	@CHECKED_11 BIT = NULL,
    @REASON_CHECKED_11 VARCHAR(8000) = NULL,
	@ITEM_12 VARCHAR(255) = NULL,
	@SUBMITTED_12 BIT = NULL,
	@REASON_12 VARCHAR(8000) = NULL,
	@CHECKED_12 BIT = NULL,
    @REASON_CHECKED_12 VARCHAR(8000) = NULL,
	@ITEM_13 VARCHAR(255) = NULL,
	@SUBMITTED_13 BIT = NULL,
	@REASON_13 VARCHAR(8000) = NULL,
	@CHECKED_13 BIT = NULL,
    @REASON_CHECKED_13 VARCHAR(8000) = NULL,
	@ITEM_14 VARCHAR(255) = NULL,
	@SUBMITTED_14 BIT = NULL,
	@REASON_14 VARCHAR(8000) = NULL,
	@CHECKED_14 BIT = NULL,
    @REASON_CHECKED_14 VARCHAR(8000) = NULL,
	@ITEM_15 VARCHAR(255) = NULL,
	@SUBMITTED_15 BIT = NULL,
	@REASON_15 VARCHAR(8000) = NULL,
	@CHECKED_15 BIT = NULL,
    @REASON_CHECKED_15 VARCHAR(8000) = NULL,
	@CONF_DECISION BIT = NULL,
	@CONF_GAD_COMMENT VARCHAR(8000) = NULL,
	@DOC_DATE_FORM2 DATE = NULL,
	@EXAM_NAME_OF_GROUP VARCHAR(255) = NULL,
	@EXAM_CORE_BUSINESS VARCHAR(255) = NULL,
	@EXAM_REL_VENDOR BIT = NULL,
	@EXAM_REL_VENDOR_REMARK VARCHAR(8000) = NULL,
	@EXAM_HAS_ENGAGED_BEFORE BIT = NULL,
	@EXAM_HAS_ENGAGED_SINCE DATE = NULL,
	@EXAM_BACKGROUND VARCHAR(8000) = NULL,
	@EXAM_VENDOR_A VARCHAR(255) = NULL,
	@EXAM_VENDOR_B VARCHAR(255) = NULL,
	@EXAM_ITEM_1 VARCHAR(200) = NULL,
	@EXAM_UID_1 INT = NULL,
	@COMPARE_VENDOR_A_1 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_1 VARCHAR(800) = NULL,
	@EXAM_REMARK_1 VARCHAR(8000) = NULL,
	@EXAM_ITEM_2 VARCHAR(200) = NULL,
	@EXAM_UID_2 INT = NULL,
	@COMPARE_VENDOR_A_2 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_2 VARCHAR(800) = NULL,
	@EXAM_REMARK_2 VARCHAR(8000) = NULL,
	@EXAM_ITEM_3 VARCHAR(200) = NULL,
	@EXAM_UID_3 INT = NULL,
	@COMPARE_VENDOR_A_3 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_3 VARCHAR(800) = NULL,
	@EXAM_REMARK_3 VARCHAR(8000) = NULL,
	@EXAM_ITEM_4 VARCHAR(200) = NULL,
	@EXAM_UID_4 INT = NULL,
	@COMPARE_VENDOR_A_4 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_4 VARCHAR(800) = NULL,
	@EXAM_REMARK_4 VARCHAR(8000) = NULL,
	@EXAM_OVERALL_CONCLUSION VARCHAR(8000) = NULL,
	@RISK_ANNUAL_BILLING VARCHAR(80) = NULL,
	@RISK_SPECIAL_RELATED BIT = NULL,
	@RISK_CPC_ASSESSMENT VARCHAR(8000) = NULL,
	@RISK_PROVIDE_ACCESS BIT = NULL,
	@RISK_AS_VENDOR_A VARCHAR(255) = NULL,
	@RISK_AS_VENDOR_B VARCHAR(255) = NULL,
	@ASSESS_ITEM_1 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_1 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_1 VARCHAR(255) = NULL,	@ASSESS_REMARK_1 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_2 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_2 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_2 VARCHAR(255) = NULL,
	@ASSESS_REMARK_2 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_3 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_3 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_3 VARCHAR(255) = NULL,
	@ASSESS_REMARK_3 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_4 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_4 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_4 VARCHAR(255) = NULL,
	@ASSESS_REMARK_4 VARCHAR(8000) = NULL,
	@RISK_AML_COMMENT VARCHAR(8000) = NULL,
	@RISK_CATEGORY VARCHAR(50) = NULL,
	@RISK_RMD_RECOMMENDATION VARCHAR(8000) = NULL,
	@RISK_SCREENING_RESULT VARCHAR(8000) = NULL,
	@RISK_MANAGEMENT_COMMENT VARCHAR(8000) = NULL,
	@REG_PROVIDED_SERVICE VARCHAR(8000) = NULL,
	@REG_CONTRACT_NM VARCHAR(255) = NULL,
	@VENDOR_NAME VARCHAR(255) = NULL,
	@REG_LEGAL_STATUS VARCHAR(50) = NULL,
	@REG_NPWP_NO VARCHAR(255) = NULL,
	@REG_ADDRESS VARCHAR(255) = NULL,
	@REG_CITY VARCHAR(255) = NULL,
	@REG_POSTAL VARCHAR(255) = NULL,
	@REG_WEBSITE VARCHAR(255) = NULL,
	@REG_EMAIL VARCHAR(255) = NULL,
	@REG_PHONE_1 VARCHAR(255) = NULL,
	@REG_PHONE_EXT_1 VARCHAR(255) = NULL,
	@REG_PHONE_2 VARCHAR(255) = NULL,
	@REG_PHONE_EXT_2 VARCHAR(255) = NULL,
	@REG_FAX_1 VARCHAR(255) = NULL,
	@REG_FAX_2 VARCHAR(255) = NULL,
	@REG_CP_NM_1 VARCHAR(255) = NULL,
	@REG_CP_POSITION_1 VARCHAR(255) = NULL,
	@REG_CP_NM_2 VARCHAR(255) = NULL,
	@REG_CP_POSITION_2 VARCHAR(255) = NULL,
	@REG_PAYMENT_TYPE VARCHAR(50) = NULL,
	@REG_REASON_CASH VARCHAR(8000) = NULL,
	@REG_ACC_HOLDER_NM VARCHAR(255) = NULL,
	@REG_ACC_NO VARCHAR(255) = NULL,
	@REG_ACC_BANK VARCHAR(255) = NULL,
	@DOC_DATE_FORM4A DATE = NULL,
	@SHEET_ID_1 INT = NULL,
	@SHEET_POSITION_1 VARCHAR(50) = NULL,
	@SHEET_NAME_1 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_1 VARCHAR(255) = NULL,
	@SHEET_SHARE_1 DECIMAL(36,8) = NULL,
	@SHEET_ID_2 INT = NULL,
	@SHEET_POSITION_2 VARCHAR(50) = NULL,
	@SHEET_NAME_2 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_2 VARCHAR(255) = NULL,
	@SHEET_SHARE_2 DECIMAL(36,8) = NULL,
	@SHEET_ID_3 INT = NULL,
	@SHEET_POSITION_3 VARCHAR(50) = NULL,
	@SHEET_NAME_3 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_3 VARCHAR(255) = NULL,
	@SHEET_SHARE_3 DECIMAL(36,8) = NULL,
	@SHEET_ID_4 INT = NULL,
	@SHEET_POSITION_4 VARCHAR(50) = NULL,
	@SHEET_NAME_4 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_4 VARCHAR(255) = NULL,
	@SHEET_SHARE_4 DECIMAL(36,8) = NULL,
	@SHEET_ID_5 INT = NULL,
	@SHEET_POSITION_5 VARCHAR(50) = NULL,
	@SHEET_NAME_5 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_5 VARCHAR(255) = NULL,
	@SHEET_SHARE_5 DECIMAL(36,8) = NULL,
	@SHEET_ID_6 INT = NULL,
	@SHEET_POSITION_6 VARCHAR(50) = NULL,
	@SHEET_NAME_6 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_6 VARCHAR(255) = NULL,
	@SHEET_SHARE_6 DECIMAL(36,8) = NULL,
	@SHEET_ID_7 INT = NULL,
	@SHEET_POSITION_7 VARCHAR(50) = NULL,
	@SHEET_NAME_7 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_7 VARCHAR(255) = NULL,
	@SHEET_SHARE_7 DECIMAL(36,8) = NULL,
	@SHEET_ID_8 INT = NULL,
	@SHEET_POSITION_8 VARCHAR(50) = NULL,
	@SHEET_NAME_8 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_8 VARCHAR(255) = NULL,
	@SHEET_SHARE_8 DECIMAL(36,8) = NULL,
	@SHEET_ID_9 INT = NULL,
	@SHEET_POSITION_9 VARCHAR(50) = NULL,
	@SHEET_NAME_9 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_9 VARCHAR(255) = NULL,
	@SHEET_SHARE_9 DECIMAL(36,8) = NULL,
	@SHEET_ID_10 INT = NULL,
	@SHEET_POSITION_10 VARCHAR(50) = NULL,
	@SHEET_NAME_10 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_10 VARCHAR(255) = NULL,
	@SHEET_SHARE_10 DECIMAL(36,8) = NULL,
	@ASC_CONTRACTOR_JAPANESE BIT = NULL,
	@ASC_SUBCONTRACT_OPERATION BIT = NULL,
	@APPROVAL_STS VARCHAR(50) = NULL,
	@IS_ACTIVE BIT = NULL,
	@CREATED_BY VARCHAR(50) = NULL,
	@CREATED_DT DATETIME = NULL,
	@CREATED_HOST VARCHAR(20),
	@STATUS_FLOW VARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @VendorUID BIGINT

	INSERT INTO TR_VENDOR_CONTROL (
		VENDOR_ID,
		VENDOR_NAME,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		DOC_DATE_FORM1,
		CONF_CONTRACTOR_NM,
		CONF_OPERATION_TO_OUTSOURCE,
		CONF_CATEGORY,
		CONF_DEPT_CD,
		CONF_BRIEF_EXPLAIN_CONTRACT,
		CONF_DECISION,
		CONF_GAD_COMMENT,
		DOC_DATE_FORM2,
		EXAM_NAME_OF_GROUP,
		EXAM_CORE_BUSINESS,
		EXAM_REL_VENDOR,
		EXAM_REL_VENDOR_REMARK,
		EXAM_HAS_ENGAGED_BEFORE,
		EXAM_HAS_ENGAGED_SINCE,
		EXAM_BACKGROUND,
		EXAM_VENDOR_A,
		EXAM_VENDOR_B,
		EXAM_OVERALL_CONCLUSION,
		RISK_ANNUAL_BILLING,
		RISK_SPECIAL_RELATED,
		RISK_CPC_ASSESSMENT,
		RISK_PROVIDE_ACCESS,
		RISK_AS_VENDOR_A,
		RISK_AS_VENDOR_B,
		RISK_AML_COMMENT,
		RISK_CATEGORY,
		RISK_RMD_RECOMMENDATION,
		RISK_SCREENING_RESULT,
		REG_PROVIDED_SERVICE,
		REG_CONTRACT_NM,
		REG_LEGAL_STATUS,
		REG_NPWP_NO,
		REG_ADDRESS,
		REG_CITY,
		REG_POSTAL,
		REG_WEBSITE,
		REG_EMAIL,
		REG_PHONE_1,
		REG_PHONE_EXT_1,
		REG_PHONE_2,
		REG_PHONE_EXT_2,
		REG_FAX_1,
		REG_FAX_2,
		REG_CP_NM_1,
		REG_CP_POSITION_1,
		REG_CP_NM_2,
		REG_CP_POSITION_2,
		REG_PAYMENT_TYPE,
		REG_REASON_CASH,
		REG_ACC_HOLDER_NM,
		REG_ACC_NO,
		REG_ACC_BANK,
		DOC_DATE_FORM4A,
		ASC_CONTRACTOR_JAPANESE,
		ASC_SUBCONTRACT_OPERATION,
		APPROVAL_STS,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		STATUS_FLOW
		)
	VALUES (
		@VENDOR_ID,
		@VENDOR_NAME,
		@CREATOR,
		@CREATOR_DEPT,
		@CREATOR_UNIT,
		@CREATOR_ROLE,
		@REQUEST_DT,
		@DOC_DATE_FORM1,
		@CONF_CONTRACTOR_NM,
		@CONF_OPERATION_TO_OUTSOURCE,
		@CONF_CATEGORY,
		@CONF_DEPT_CD,
		@CONF_BRIEF_EXPLAIN_CONTRACT,
		@CONF_DECISION,
		@CONF_GAD_COMMENT,
		@DOC_DATE_FORM2,
		@EXAM_NAME_OF_GROUP,
		@EXAM_CORE_BUSINESS,
		@EXAM_REL_VENDOR,
		@EXAM_REL_VENDOR_REMARK,
		@EXAM_HAS_ENGAGED_BEFORE,
		@EXAM_HAS_ENGAGED_SINCE,
		@EXAM_BACKGROUND,
		@EXAM_VENDOR_A,
		@EXAM_VENDOR_B,
		@EXAM_OVERALL_CONCLUSION,
		@RISK_ANNUAL_BILLING,
		@RISK_SPECIAL_RELATED,
		@RISK_CPC_ASSESSMENT,
		@RISK_PROVIDE_ACCESS,
		@RISK_AS_VENDOR_A,
		@RISK_AS_VENDOR_B,
		@RISK_AML_COMMENT,
		@RISK_CATEGORY,
		@RISK_RMD_RECOMMENDATION,
		@RISK_SCREENING_RESULT,
		@REG_PROVIDED_SERVICE,
		@REG_CONTRACT_NM,
		@REG_LEGAL_STATUS,
		@REG_NPWP_NO,
		@REG_ADDRESS,
		@REG_CITY,
		@REG_POSTAL,
		@REG_WEBSITE,
		@REG_EMAIL,
		@REG_PHONE_1,
		@REG_PHONE_EXT_1,
		@REG_PHONE_2,
		@REG_PHONE_EXT_2,
		@REG_FAX_1,
		@REG_FAX_2,
		@REG_CP_NM_1,
		@REG_CP_POSITION_1,
		@REG_CP_NM_2,
		@REG_CP_POSITION_2,
		@REG_PAYMENT_TYPE,
		@REG_REASON_CASH,
		@REG_ACC_HOLDER_NM,
		@REG_ACC_NO,
		@REG_ACC_BANK,
		@DOC_DATE_FORM4A,
		@ASC_CONTRACTOR_JAPANESE,
		@ASC_SUBCONTRACT_OPERATION,
		@APPROVAL_STS,
		@IS_ACTIVE,
		@CREATED_BY,
		@CREATED_DT,
		@CREATED_HOST,
		@STATUS_FLOW
		)

	SELECT @VendorUID = @@IDENTITY

    -- insert conf doc checklist
    INSERT INTO TR_VC_CONF_DOC_CHECKLIST (DOC_ID, SEQ, ITEM, SUBMITTED, REASON, CHECKED, REASON_CHECKED, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @ITEM_1, @SUBMITTED_1, @REASON_1, @CHECKED_1, @REASON_CHECKED_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @ITEM_2, @SUBMITTED_2, @REASON_2, @CHECKED_2, @REASON_CHECKED_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @ITEM_3, @SUBMITTED_3, @REASON_3, @CHECKED_3, @REASON_CHECKED_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @ITEM_4, @SUBMITTED_4, @REASON_4, @CHECKED_4, @REASON_CHECKED_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 5, @ITEM_5, @SUBMITTED_5, @REASON_5, @CHECKED_5, @REASON_CHECKED_5, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 6, @ITEM_6, @SUBMITTED_6, @REASON_6, @CHECKED_6, @REASON_CHECKED_6, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 7, @ITEM_7, @SUBMITTED_7, @REASON_7, @CHECKED_7, @REASON_CHECKED_7, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 8, @ITEM_8, @SUBMITTED_8, @REASON_8, @CHECKED_8, @REASON_CHECKED_8, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 9, @ITEM_9, @SUBMITTED_9, @REASON_9, @CHECKED_9, @REASON_CHECKED_9, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 10, @ITEM_10, @SUBMITTED_10, @REASON_10, @CHECKED_10, @REASON_CHECKED_10, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 11, @ITEM_11, @SUBMITTED_11, @REASON_11, @CHECKED_11, @REASON_CHECKED_11, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 12, @ITEM_12, @SUBMITTED_12, @REASON_12, @CHECKED_12, @REASON_CHECKED_12, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 13, @ITEM_13, @SUBMITTED_13, @REASON_13, @CHECKED_13, @REASON_CHECKED_13, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 14, @ITEM_14, @SUBMITTED_14, @REASON_14, @CHECKED_14, @REASON_CHECKED_14, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 15, @ITEM_15, @SUBMITTED_15, @REASON_15, @CHECKED_15, @REASON_CHECKED_15, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    -- insert exam comparison
    insert into TR_VC_EXAM_COMPARISON (DOC_ID, SEQ_NO, PARAMETER, COMPARE_VENDOR_A, COMPARE_VENDOR_B, REMARK, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @EXAM_ITEM_1, @COMPARE_VENDOR_A_1, @COMPARE_VENDOR_B_1, @EXAM_REMARK_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @EXAM_ITEM_2, @COMPARE_VENDOR_A_2, @COMPARE_VENDOR_B_2, @EXAM_REMARK_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @EXAM_ITEM_3, @COMPARE_VENDOR_A_3, @COMPARE_VENDOR_B_3, @EXAM_REMARK_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @EXAM_ITEM_4, @COMPARE_VENDOR_A_4, @COMPARE_VENDOR_B_4, @EXAM_REMARK_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    -- insert risk assessment
    insert into TR_VC_RISK_ASSESSMENT (DOC_ID, SEQ_NO, PARAMETER, ASSESS_VENDOR_A, ASSESS_VENDOR_B, REMARK, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @ASSESS_ITEM_1, @ASSESS_VENDOR_A_1, @ASSESS_VENDOR_B_1, @ASSESS_REMARK_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @ASSESS_ITEM_2, @ASSESS_VENDOR_A_2, @ASSESS_VENDOR_B_2, @ASSESS_REMARK_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @ASSESS_ITEM_3, @ASSESS_VENDOR_A_3, @ASSESS_VENDOR_B_3, @ASSESS_REMARK_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @ASSESS_ITEM_4, @ASSESS_VENDOR_A_4, @ASSESS_VENDOR_B_4, @ASSESS_REMARK_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    -- insert risk management comment

    -- insert snti social checksheet
    insert into TR_VC_ASC_CHECKSHEET (DOC_ID, SEQ_NO, [POSITION], NAME, NATIONALITY, SHARE, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @SHEET_POSITION_1, @SHEET_NAME_1, @SHEET_NATIONALITY_1, @SHEET_SHARE_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @SHEET_POSITION_2, @SHEET_NAME_2, @SHEET_NATIONALITY_2, @SHEET_SHARE_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @SHEET_POSITION_3, @SHEET_NAME_3, @SHEET_NATIONALITY_3, @SHEET_SHARE_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @SHEET_POSITION_4, @SHEET_NAME_4, @SHEET_NATIONALITY_4, @SHEET_SHARE_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 5, @SHEET_POSITION_5, @SHEET_NAME_5, @SHEET_NATIONALITY_5, @SHEET_SHARE_5, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 6, @SHEET_POSITION_6, @SHEET_NAME_6, @SHEET_NATIONALITY_6, @SHEET_SHARE_6, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 7, @SHEET_POSITION_7, @SHEET_NAME_7, @SHEET_NATIONALITY_7, @SHEET_SHARE_7, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 8, @SHEET_POSITION_8, @SHEET_NAME_8, @SHEET_NATIONALITY_8, @SHEET_SHARE_8, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 9, @SHEET_POSITION_9, @SHEET_NAME_9, @SHEET_NATIONALITY_9, @SHEET_SHARE_9, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 10, @SHEET_POSITION_10, @SHEET_NAME_10, @SHEET_NATIONALITY_10, @SHEET_SHARE_10, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    select @VendorUID
END
GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORDETAILTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPI_VENDORDETAILTEMP]
	-- add more stored procedure parameters here
    @REFID BIGINT = 0,
	@VENDOR_ID VARCHAR(50) = NULL,
    @CREATOR VARCHAR(255) = NULL,
    @CREATOR_DEPT VARCHAR(255) = NULL,
    @CREATOR_UNIT VARCHAR(255) = NULL,
    @CREATOR_ROLE VARCHAR(255) = NULL,
    @REQUEST_DT DATE = NULL,
    @DOC_DATE_FORM1 DATE = NULL,
    @CONF_CONTRACTOR_NM VARCHAR(255) = NULL,
    @CONF_OPERATION_TO_OUTSOURCE VARCHAR(255) = NULL,
    @CONF_CATEGORY VARCHAR(255) = NULL,
    @CONF_DEPT_CD VARCHAR(50) = NULL,
    @CONF_BRIEF_EXPLAIN_CONTRACT VARCHAR(8000) = NULL,
    @ITEM_1 VARCHAR(255) = NULL,
    @SUBMITTED_1 BIT = NULL,
    @REASON_1 VARCHAR(8000) = NULL,
    @CHECKED_1 BIT = NULL,
    @REASON_CHECKED_1 VARCHAR(8000) = NULL,
    @ITEM_2 VARCHAR(255) = NULL,
    @SUBMITTED_2 BIT = NULL,
    @REASON_2 VARCHAR(8000) = NULL,
    @CHECKED_2 BIT = NULL,
    @REASON_CHECKED_2 VARCHAR(8000) = NULL,
    @ITEM_3 VARCHAR(255) = NULL,
    @SUBMITTED_3 BIT = NULL,
    @REASON_3 VARCHAR(8000) = NULL,
    @CHECKED_3 BIT = NULL,
    @REASON_CHECKED_3 VARCHAR(8000) = NULL,
    @ITEM_4 VARCHAR(255) = NULL,
    @SUBMITTED_4 BIT = NULL,
    @REASON_4 VARCHAR(8000) = NULL,
    @CHECKED_4 BIT = NULL,
    @REASON_CHECKED_4 VARCHAR(8000) = NULL,
    @ITEM_5 VARCHAR(255) = NULL,
    @SUBMITTED_5 BIT = NULL,
    @REASON_5 VARCHAR(8000) = NULL,
    @CHECKED_5 BIT = NULL,
    @REASON_CHECKED_5 VARCHAR(8000) = NULL,
    @ITEM_6 VARCHAR(255) = NULL,
    @SUBMITTED_6 BIT = NULL,
    @REASON_6 VARCHAR(8000) = NULL,
    @CHECKED_6 BIT = NULL,
    @REASON_CHECKED_6 VARCHAR(8000) = NULL,
    @ITEM_7 VARCHAR(255) = NULL,
    @SUBMITTED_7 BIT = NULL,
    @REASON_7 VARCHAR(8000) = NULL,
    @CHECKED_7 BIT = NULL,
    @REASON_CHECKED_7 VARCHAR(8000) = NULL,
    @ITEM_8 VARCHAR(255) = NULL,
    @SUBMITTED_8 BIT = NULL,
    @REASON_8 VARCHAR(8000) = NULL,
    @CHECKED_8 BIT = NULL,
    @REASON_CHECKED_8 VARCHAR(8000) = NULL,
    @ITEM_9 VARCHAR(255) = NULL,
    @SUBMITTED_9 BIT = NULL,
    @REASON_9 VARCHAR(8000) = NULL,
    @CHECKED_9 BIT = NULL,
    @REASON_CHECKED_9 VARCHAR(8000) = NULL,
    @ITEM_10 VARCHAR(255) = NULL,
    @SUBMITTED_10 BIT = NULL,
    @REASON_10 VARCHAR(8000) = NULL,
    @CHECKED_10 BIT = NULL,
    @REASON_CHECKED_10 VARCHAR(8000) = NULL,
    @ITEM_11 VARCHAR(255) = NULL,
    @SUBMITTED_11 BIT = NULL,
    @REASON_11 VARCHAR(8000) = NULL,
    @CHECKED_11 BIT = NULL,
    @REASON_CHECKED_11 VARCHAR(8000) = NULL,
    @ITEM_12 VARCHAR(255) = NULL,
    @SUBMITTED_12 BIT = NULL,
    @REASON_12 VARCHAR(8000) = NULL,
    @CHECKED_12 BIT = NULL,
    @REASON_CHECKED_12 VARCHAR(8000) = NULL,
    @ITEM_13 VARCHAR(255) = NULL,
    @SUBMITTED_13 BIT = NULL,
    @REASON_13 VARCHAR(8000) = NULL,
    @CHECKED_13 BIT = NULL,
    @REASON_CHECKED_13 VARCHAR(8000) = NULL,
    @ITEM_14 VARCHAR(255) = NULL,
    @SUBMITTED_14 BIT = NULL,
    @REASON_14 VARCHAR(8000) = NULL,
    @CHECKED_14 BIT = NULL,
    @REASON_CHECKED_14 VARCHAR(8000) = NULL,
    @ITEM_15 VARCHAR(255) = NULL,
    @SUBMITTED_15 BIT = NULL,
    @REASON_15 VARCHAR(8000) = NULL,
    @CHECKED_15 BIT = NULL,
    @REASON_CHECKED_15 VARCHAR(8000) = NULL,
    @CONF_DECISION BIT = NULL,
    @CONF_GAD_COMMENT VARCHAR(8000) = NULL,
    @DOC_DATE_FORM2 DATE = NULL,
    @EXAM_NAME_OF_GROUP VARCHAR(255) = NULL,
    @EXAM_CORE_BUSINESS VARCHAR(255) = NULL,
    @EXAM_REL_VENDOR BIT = NULL,
    @EXAM_REL_VENDOR_REMARK VARCHAR(8000) = NULL,
    @EXAM_HAS_ENGAGED_BEFORE BIT = NULL,
    @EXAM_HAS_ENGAGED_SINCE DATE = NULL,
    @EXAM_BACKGROUND VARCHAR(8000) = NULL,
    @EXAM_VENDOR_A VARCHAR(255) = NULL,
    @EXAM_VENDOR_B VARCHAR(255) = NULL,
    @EXAM_ITEM_1 VARCHAR(200) = NULL,
    @EXAM_UID_1 INT = NULL,
    @COMPARE_VENDOR_A_1 VARCHAR(800) = NULL,
    @COMPARE_VENDOR_B_1 VARCHAR(800) = NULL,
    @EXAM_REMARK_1 VARCHAR(8000) = NULL,
    @EXAM_ITEM_2 VARCHAR(200) = NULL,
    @EXAM_UID_2 INT = NULL,
    @COMPARE_VENDOR_A_2 VARCHAR(800) = NULL,
    @COMPARE_VENDOR_B_2 VARCHAR(800) = NULL,
    @EXAM_REMARK_2 VARCHAR(8000) = NULL,
    @EXAM_ITEM_3 VARCHAR(200) = NULL,
    @EXAM_UID_3 INT = NULL,
    @COMPARE_VENDOR_A_3 VARCHAR(800) = NULL,
    @COMPARE_VENDOR_B_3 VARCHAR(800) = NULL,
    @EXAM_REMARK_3 VARCHAR(8000) = NULL,
    @EXAM_ITEM_4 VARCHAR(200) = NULL,
    @EXAM_UID_4 INT = NULL,
    @COMPARE_VENDOR_A_4 VARCHAR(800) = NULL,
    @COMPARE_VENDOR_B_4 VARCHAR(800) = NULL,
    @EXAM_REMARK_4 VARCHAR(8000) = NULL,
    @EXAM_OVERALL_CONCLUSION VARCHAR(8000) = NULL,
    @RISK_ANNUAL_BILLING VARCHAR(80) = NULL,
    @RISK_SPECIAL_RELATED BIT = NULL,
    @RISK_CPC_ASSESSMENT VARCHAR(8000) = NULL,
    @RISK_PROVIDE_ACCESS BIT = NULL,
    @RISK_AS_VENDOR_A VARCHAR(255) = NULL,
    @RISK_AS_VENDOR_B VARCHAR(255) = NULL,
    @ASSESS_ITEM_1 VARCHAR(80) = NULL,
    @ASSESS_VENDOR_A_1 VARCHAR(255) = NULL,
    @ASSESS_VENDOR_B_1 VARCHAR(255) = NULL,	@ASSESS_REMARK_1 VARCHAR(8000) = NULL,
    @ASSESS_ITEM_2 VARCHAR(80) = NULL,
    @ASSESS_VENDOR_A_2 VARCHAR(255) = NULL,
    @ASSESS_VENDOR_B_2 VARCHAR(255) = NULL,
    @ASSESS_REMARK_2 VARCHAR(8000) = NULL,
    @ASSESS_ITEM_3 VARCHAR(80) = NULL,
    @ASSESS_VENDOR_A_3 VARCHAR(255) = NULL,
    @ASSESS_VENDOR_B_3 VARCHAR(255) = NULL,
    @ASSESS_REMARK_3 VARCHAR(8000) = NULL,
    @ASSESS_ITEM_4 VARCHAR(80) = NULL,
    @ASSESS_VENDOR_A_4 VARCHAR(255) = NULL,
    @ASSESS_VENDOR_B_4 VARCHAR(255) = NULL,
    @ASSESS_REMARK_4 VARCHAR(8000) = NULL,
    @RISK_AML_COMMENT VARCHAR(8000) = NULL,
    @RISK_CATEGORY VARCHAR(50) = NULL,
    @RISK_RMD_RECOMMENDATION VARCHAR(8000) = NULL,
    @RISK_SCREENING_RESULT VARCHAR(8000) = NULL,
    @RISK_MANAGEMENT_COMMENT VARCHAR(8000) = NULL,
    @REG_PROVIDED_SERVICE VARCHAR(8000) = NULL,
    @REG_CONTRACT_NM VARCHAR(255) = NULL,
    @VENDOR_NAME VARCHAR(255) = NULL,
    @REG_LEGAL_STATUS VARCHAR(50) = NULL,
    @REG_NPWP_NO VARCHAR(255) = NULL,
    @REG_ADDRESS VARCHAR(255) = NULL,
    @REG_CITY VARCHAR(255) = NULL,
    @REG_POSTAL VARCHAR(255) = NULL,
    @REG_WEBSITE VARCHAR(255) = NULL,
    @REG_EMAIL VARCHAR(255) = NULL,
    @REG_PHONE_1 VARCHAR(255) = NULL,
    @REG_PHONE_EXT_1 VARCHAR(255) = NULL,
    @REG_PHONE_2 VARCHAR(255) = NULL,
    @REG_PHONE_EXT_2 VARCHAR(255) = NULL,
    @REG_FAX_1 VARCHAR(255) = NULL,
    @REG_FAX_2 VARCHAR(255) = NULL,
    @REG_CP_NM_1 VARCHAR(255) = NULL,
    @REG_CP_POSITION_1 VARCHAR(255) = NULL,
    @REG_CP_NM_2 VARCHAR(255) = NULL,
    @REG_CP_POSITION_2 VARCHAR(255) = NULL,
    @REG_PAYMENT_TYPE VARCHAR(50) = NULL,
    @REG_REASON_CASH VARCHAR(8000) = NULL,
    @REG_ACC_HOLDER_NM VARCHAR(255) = NULL,
    @REG_ACC_NO VARCHAR(255) = NULL,
    @REG_ACC_BANK VARCHAR(255) = NULL,
    @DOC_DATE_FORM4A DATE = NULL,
    @SHEET_ID_1 INT = NULL,
    @SHEET_POSITION_1 VARCHAR(50) = NULL,
    @SHEET_NAME_1 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_1 VARCHAR(255) = NULL,
    @SHEET_SHARE_1 DECIMAL(36,8) = NULL,
    @SHEET_ID_2 INT = NULL,
    @SHEET_POSITION_2 VARCHAR(50) = NULL,
    @SHEET_NAME_2 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_2 VARCHAR(255) = NULL,
    @SHEET_SHARE_2 DECIMAL(36,8) = NULL,
    @SHEET_ID_3 INT = NULL,
    @SHEET_POSITION_3 VARCHAR(50) = NULL,
    @SHEET_NAME_3 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_3 VARCHAR(255) = NULL,
    @SHEET_SHARE_3 DECIMAL(36,8) = NULL,
    @SHEET_ID_4 INT = NULL,
    @SHEET_POSITION_4 VARCHAR(50) = NULL,
    @SHEET_NAME_4 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_4 VARCHAR(255) = NULL,
    @SHEET_SHARE_4 DECIMAL(36,8) = NULL,
    @SHEET_ID_5 INT = NULL,
    @SHEET_POSITION_5 VARCHAR(50) = NULL,
    @SHEET_NAME_5 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_5 VARCHAR(255) = NULL,
    @SHEET_SHARE_5 DECIMAL(36,8) = NULL,
    @SHEET_ID_6 INT = NULL,
    @SHEET_POSITION_6 VARCHAR(50) = NULL,
    @SHEET_NAME_6 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_6 VARCHAR(255) = NULL,
    @SHEET_SHARE_6 DECIMAL(36,8) = NULL,
    @SHEET_ID_7 INT = NULL,
    @SHEET_POSITION_7 VARCHAR(50) = NULL,
    @SHEET_NAME_7 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_7 VARCHAR(255) = NULL,
    @SHEET_SHARE_7 DECIMAL(36,8) = NULL,
    @SHEET_ID_8 INT = NULL,
    @SHEET_POSITION_8 VARCHAR(50) = NULL,
    @SHEET_NAME_8 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_8 VARCHAR(255) = NULL,
    @SHEET_SHARE_8 DECIMAL(36,8) = NULL,
    @SHEET_ID_9 INT = NULL,
    @SHEET_POSITION_9 VARCHAR(50) = NULL,
    @SHEET_NAME_9 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_9 VARCHAR(255) = NULL,
    @SHEET_SHARE_9 DECIMAL(36,8) = NULL,
    @SHEET_ID_10 INT = NULL,
    @SHEET_POSITION_10 VARCHAR(50) = NULL,
    @SHEET_NAME_10 VARCHAR(255) = NULL,
    @SHEET_NATIONALITY_10 VARCHAR(255) = NULL,
    @SHEET_SHARE_10 DECIMAL(36,8) = NULL,
    @ASC_CONTRACTOR_JAPANESE BIT = NULL,
    @ASC_SUBCONTRACT_OPERATION BIT = NULL,
    @APPROVAL_STS VARCHAR(50) = NULL,
    @IS_ACTIVE BIT = NULL,
    @CREATED_BY VARCHAR(50) = NULL,
    @CREATED_DT DATETIME = NULL,
    @CREATED_HOST VARCHAR(20),
    @STATUS_FLOW VARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @VendorUID BIGINT

	INSERT INTO TR_VENDOR_CONTROL_TEMP (        REFID,
		VENDOR_ID,
		VENDOR_NAME,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		DOC_DATE_FORM1,
		CONF_CONTRACTOR_NM,
		CONF_OPERATION_TO_OUTSOURCE,
		CONF_CATEGORY,
		CONF_DEPT_CD,
		CONF_BRIEF_EXPLAIN_CONTRACT,
		CONF_DECISION,
		CONF_GAD_COMMENT,
		DOC_DATE_FORM2,
		EXAM_NAME_OF_GROUP,
		EXAM_CORE_BUSINESS,
		EXAM_REL_VENDOR,
		EXAM_REL_VENDOR_REMARK,
		EXAM_HAS_ENGAGED_BEFORE,
		EXAM_HAS_ENGAGED_SINCE,
		EXAM_BACKGROUND,
		EXAM_VENDOR_A,
		EXAM_VENDOR_B,
		EXAM_OVERALL_CONCLUSION,
		RISK_ANNUAL_BILLING,
		RISK_SPECIAL_RELATED,
		RISK_CPC_ASSESSMENT,
		RISK_PROVIDE_ACCESS,
		RISK_AS_VENDOR_A,
		RISK_AS_VENDOR_B,
		RISK_AML_COMMENT,
		RISK_CATEGORY,
		RISK_RMD_RECOMMENDATION,
		RISK_SCREENING_RESULT,
		REG_PROVIDED_SERVICE,
		REG_CONTRACT_NM,
		REG_LEGAL_STATUS,
		REG_NPWP_NO,
		REG_ADDRESS,
		REG_CITY,
		REG_POSTAL,
		REG_WEBSITE,
		REG_EMAIL,
		REG_PHONE_1,
		REG_PHONE_EXT_1,
		REG_PHONE_2,
		REG_PHONE_EXT_2,
		REG_FAX_1,
		REG_FAX_2,
		REG_CP_NM_1,
		REG_CP_POSITION_1,
		REG_CP_NM_2,
		REG_CP_POSITION_2,
		REG_PAYMENT_TYPE,
		REG_REASON_CASH,
		REG_ACC_HOLDER_NM,
		REG_ACC_NO,
		REG_ACC_BANK,
		DOC_DATE_FORM4A,
		ASC_CONTRACTOR_JAPANESE,
		ASC_SUBCONTRACT_OPERATION,
		APPROVAL_STS,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		STATUS_FLOW
		)
	VALUES (
        @REFID,
		@VENDOR_ID,
		@VENDOR_NAME,
		@CREATOR,
		@CREATOR_DEPT,
		@CREATOR_UNIT,
		@CREATOR_ROLE,
		@REQUEST_DT,
		@DOC_DATE_FORM1,
		@CONF_CONTRACTOR_NM,
		@CONF_OPERATION_TO_OUTSOURCE,
		@CONF_CATEGORY,
		@CONF_DEPT_CD,
		@CONF_BRIEF_EXPLAIN_CONTRACT,
		@CONF_DECISION,
		@CONF_GAD_COMMENT,
		@DOC_DATE_FORM2,
		@EXAM_NAME_OF_GROUP,
		@EXAM_CORE_BUSINESS,
		@EXAM_REL_VENDOR,
		@EXAM_REL_VENDOR_REMARK,
		@EXAM_HAS_ENGAGED_BEFORE,
		@EXAM_HAS_ENGAGED_SINCE,
		@EXAM_BACKGROUND,
		@EXAM_VENDOR_A,
		@EXAM_VENDOR_B,
		@EXAM_OVERALL_CONCLUSION,
		@RISK_ANNUAL_BILLING,
		@RISK_SPECIAL_RELATED,
		@RISK_CPC_ASSESSMENT,
		@RISK_PROVIDE_ACCESS,
		@RISK_AS_VENDOR_A,
		@RISK_AS_VENDOR_B,
		@RISK_AML_COMMENT,
		@RISK_CATEGORY,
		@RISK_RMD_RECOMMENDATION,
		@RISK_SCREENING_RESULT,
		@REG_PROVIDED_SERVICE,
		@REG_CONTRACT_NM,
		@REG_LEGAL_STATUS,
		@REG_NPWP_NO,
		@REG_ADDRESS,
		@REG_CITY,
		@REG_POSTAL,
		@REG_WEBSITE,
		@REG_EMAIL,
		@REG_PHONE_1,
		@REG_PHONE_EXT_1,
		@REG_PHONE_2,
		@REG_PHONE_EXT_2,
		@REG_FAX_1,
		@REG_FAX_2,
		@REG_CP_NM_1,
		@REG_CP_POSITION_1,
		@REG_CP_NM_2,
		@REG_CP_POSITION_2,
		@REG_PAYMENT_TYPE,
		@REG_REASON_CASH,
		@REG_ACC_HOLDER_NM,
		@REG_ACC_NO,
		@REG_ACC_BANK,
		@DOC_DATE_FORM4A,
		@ASC_CONTRACTOR_JAPANESE,
		@ASC_SUBCONTRACT_OPERATION,
		@APPROVAL_STS,
		@IS_ACTIVE,
		@CREATED_BY,
		@CREATED_DT,
		@CREATED_HOST,
		@STATUS_FLOW
		)

	SELECT @VendorUID = @@IDENTITY

    UPDATE vct
    SET vct.AGREE_START_DATE = vc.AGREE_START_DATE,
        vct.AGREE_END_DATE = vc.AGREE_END_DATE,
        vct.AGREE_CREATED_BY = vc.AGREE_CREATED_BY,
        vct.AGREE_CREATED_DT = vc.AGREE_CREATED_DT,
        vct.AGREE_MODIFIED_BY = vc.AGREE_MODIFIED_BY,
        vct.AGREE_MODIFIED_DT = vc.AGREE_MODIFIED_DT,
        vct.TERMINATION = vc.TERMINATION,
        vct.TERMINATION_BY = vc.TERMINATION_BY,
        vct.TERMINATION_DT = vc.TERMINATION_DT,
        vct.REGISTRATION_DT = vc.REGISTRATION_DT,
        vct.MODIFIED_BY = vc.MODIFIED_BY,
        vct.MODIFIED_DT = vc.MODIFIED_DT,
        vct.MODIFIED_HOST = vc.MODIFIED_HOST,
        vct.L_DOC_ID = vc.L_DOC_ID
    FROM TR_VENDOR_CONTROL_TEMP AS vct
    JOIN TR_VENDOR_CONTROL AS vc ON vct.REFID = vc.UID
    WHERE vct.UID = @VendorUID

    -- insert conf doc checklist
    INSERT INTO TR_VC_CONF_DOC_CHECKLIST_TEMP (DOC_ID, SEQ, ITEM, SUBMITTED, REASON, CHECKED, REASON_CHECKED, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @ITEM_1, @SUBMITTED_1, @REASON_1, @CHECKED_1, @REASON_CHECKED_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @ITEM_2, @SUBMITTED_2, @REASON_2, @CHECKED_2, @REASON_CHECKED_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @ITEM_3, @SUBMITTED_3, @REASON_3, @CHECKED_3, @REASON_CHECKED_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @ITEM_4, @SUBMITTED_4, @REASON_4, @CHECKED_4, @REASON_CHECKED_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 5, @ITEM_5, @SUBMITTED_5, @REASON_5, @CHECKED_5, @REASON_CHECKED_5, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 6, @ITEM_6, @SUBMITTED_6, @REASON_6, @CHECKED_6, @REASON_CHECKED_6, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 7, @ITEM_7, @SUBMITTED_7, @REASON_7, @CHECKED_7, @REASON_CHECKED_7, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 8, @ITEM_8, @SUBMITTED_8, @REASON_8, @CHECKED_8, @REASON_CHECKED_8, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 9, @ITEM_9, @SUBMITTED_9, @REASON_9, @CHECKED_9, @REASON_CHECKED_9, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 10, @ITEM_10, @SUBMITTED_10, @REASON_10, @CHECKED_10, @REASON_CHECKED_10, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 11, @ITEM_11, @SUBMITTED_11, @REASON_11, @CHECKED_11, @REASON_CHECKED_11, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 12, @ITEM_12, @SUBMITTED_12, @REASON_12, @CHECKED_12, @REASON_CHECKED_12, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 13, @ITEM_13, @SUBMITTED_13, @REASON_13, @CHECKED_13, @REASON_CHECKED_13, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 14, @ITEM_14, @SUBMITTED_14, @REASON_14, @CHECKED_14, @REASON_CHECKED_14, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 15, @ITEM_15, @SUBMITTED_15, @REASON_15, @CHECKED_15, @REASON_CHECKED_15, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    -- insert exam comparison
    insert into TR_VC_EXAM_COMPARISON_TEMP (DOC_ID, SEQ_NO, PARAMETER, COMPARE_VENDOR_A, COMPARE_VENDOR_B, REMARK, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @EXAM_ITEM_1, @COMPARE_VENDOR_A_1, @COMPARE_VENDOR_B_1, @EXAM_REMARK_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @EXAM_ITEM_2, @COMPARE_VENDOR_A_2, @COMPARE_VENDOR_B_2, @EXAM_REMARK_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @EXAM_ITEM_3, @COMPARE_VENDOR_A_3, @COMPARE_VENDOR_B_3, @EXAM_REMARK_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @EXAM_ITEM_4, @COMPARE_VENDOR_A_4, @COMPARE_VENDOR_B_4, @EXAM_REMARK_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    -- insert risk assessment
    insert into TR_VC_RISK_ASSESSMENT_TEMP (DOC_ID, SEQ_NO, PARAMETER, ASSESS_VENDOR_A, ASSESS_VENDOR_B, REMARK, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @ASSESS_ITEM_1, @ASSESS_VENDOR_A_1, @ASSESS_VENDOR_B_1, @ASSESS_REMARK_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @ASSESS_ITEM_2, @ASSESS_VENDOR_A_2, @ASSESS_VENDOR_B_2, @ASSESS_REMARK_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @ASSESS_ITEM_3, @ASSESS_VENDOR_A_3, @ASSESS_VENDOR_B_3, @ASSESS_REMARK_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @ASSESS_ITEM_4, @ASSESS_VENDOR_A_4, @ASSESS_VENDOR_B_4, @ASSESS_REMARK_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    -- insert risk management comment
	insert into TR_VC_RISK_MANAGEMENT_COMMENT_TEMP (
		DOC_ID,
		USER_APV,
		COMMENT,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
	) select @VendorUID,
		USER_APV,
		COMMENT,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
	from TR_VC_RISK_MANAGEMENT_COMMENT
	where DOC_ID = @REFID

    -- insert snti social checksheet
    insert into TR_VC_ASC_CHECKSHEET_TEMP (DOC_ID, SEQ_NO, [POSITION], NAME, NATIONALITY, SHARE, IS_ACTIVE, CREATED_BY, CREATED_DT, CREATED_HOST)
    select @VendorUID, 1, @SHEET_POSITION_1, @SHEET_NAME_1, @SHEET_NATIONALITY_1, @SHEET_SHARE_1, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 2, @SHEET_POSITION_2, @SHEET_NAME_2, @SHEET_NATIONALITY_2, @SHEET_SHARE_2, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 3, @SHEET_POSITION_3, @SHEET_NAME_3, @SHEET_NATIONALITY_3, @SHEET_SHARE_3, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 4, @SHEET_POSITION_4, @SHEET_NAME_4, @SHEET_NATIONALITY_4, @SHEET_SHARE_4, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 5, @SHEET_POSITION_5, @SHEET_NAME_5, @SHEET_NATIONALITY_5, @SHEET_SHARE_5, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 6, @SHEET_POSITION_6, @SHEET_NAME_6, @SHEET_NATIONALITY_6, @SHEET_SHARE_6, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 7, @SHEET_POSITION_7, @SHEET_NAME_7, @SHEET_NATIONALITY_7, @SHEET_SHARE_7, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 8, @SHEET_POSITION_8, @SHEET_NAME_8, @SHEET_NATIONALITY_8, @SHEET_SHARE_8, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 9, @SHEET_POSITION_9, @SHEET_NAME_9, @SHEET_NATIONALITY_9, @SHEET_SHARE_9, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST UNION
    select @VendorUID, 10, @SHEET_POSITION_10, @SHEET_NAME_10, @SHEET_NATIONALITY_10, @SHEET_SHARE_10, 1, @CREATED_BY, @CREATED_DT, @CREATED_HOST

    select @VendorUID
END
GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_VENDORLIST]
	@VENDOR_ID VARCHAR(50)
	,@VENDOR_NAME VARCHAR(255)
	,@CREATOR VARCHAR(50)
	,@CREATOR_DEPT VARCHAR(50)
	,@CREATOR_UNIT VARCHAR(50)
	,@CREATOR_ROLE VARCHAR(50)
	,@REQUEST_DT VARCHAR(50)
	,@DOC_DATE_FORM1 VARCHAR(50)
	,@CONF_CONTRACTOR_NM VARCHAR(50)
	,@CONF_OPERATION_TO_OUTSOURCE VARCHAR(50)
	,@CONF_CATEGORY VARCHAR(50)
	,@CONF_DEPT_CD VARCHAR(50)
	,@CONF_BRIEF_EXPLAIN_CONTRACT VARCHAR(50)
	,@CONF_DECISION VARCHAR (50)
	,@CONF_GAD_COMMENT VARCHAR(50)
	,@DOC_DATE_FORM2 VARCHAR(50)
	,@EXAM_NAME_OF_GROUP VARCHAR(50)
	,@EXAM_CORE_BUSINESS VARCHAR(50)
	,@EXAM_REL_VENDOR VARCHAR(50)
	,@EXAM_REL_VENDOR_REMARK VARCHAR(50)
	,@EXAM_HAS_ENGAGED_BEFORE VARCHAR(50)
	,@EXAM_HAS_ENGAGED_SINCE VARCHAR(50)
	,@EXAM_BACKGROUND VARCHAR(50)
	,@EXAM_VENDOR_A VARCHAR(50)
	,@EXAM_VENDOR_B VARCHAR(50)
	,@EXAM_OVERALL_CONCLUSION VARCHAR(50)
	,@RISK_ANNUAL_BILLING VARCHAR(50)
	,@RISK_SPECIAL_RELATED VARCHAR(50)
	,@RISK_CPC_ASSESSMENT VARCHAR(50)
	,@RISK_PROVIDE_ACCESS VARCHAR(50)
	,@RISK_AS_VENDOR_A VARCHAR(50)
	,@RISK_AS_VENDOR_B VARCHAR(50)

	,@RISK_AML_COMMENT VARCHAR(50)
	,@RISK_CATEGORY VARCHAR(50)
	,@RISK_RMD_RECOMMENDATION VARCHAR(50)
	,@RISK_SCREENING_RESULT VARCHAR(50)
	,@REG_PROVIDED_SERVICE VARCHAR (50)
	,@REG_CONTRACT_NM VARCHAR (50)
	,@REG_LEGAL_STATUS VARCHAR(50)
	,@REG_NPWP_NO VARCHAR(50)
	,@REG_ADDRESS VARCHAR(255)
	,@REG_CITY VARCHAR(50)
	,@REG_POSTAL VARCHAR(50)
	,@REG_WEBSITE VARCHAR(100)
	,@REG_EMAIL VARCHAR(100)
	,@REG_PHONE_1 VARCHAR (50)
	,@REG_PHONE_EXT_1 VARCHAR(10)
	,@REG_PHONE_2 VARCHAR(50)
	,@REG_PHONE_EXT_2 VARCHAR(10)
	,@REG_FAX_1 VARCHAR(50)
	,@REG_FAX_2 VARCHAR(50)
	,@REG_CP_NM_1 VARCHAR(150)
	,@REG_CP_POSITION_1 VARCHAR(50)
	,@REG_CP_NM_2 VARCHAR (150)
	,@REG_CP_POSITION_2 VARCHAR(50)
	,@REG_PAYMENT_TYPE VARCHAR(50)
	,@REG_REASON_CASH VARCHAR(255)
	,@REG_ACC_HOLDER_NM VARCHAR(150)
	,@REG_ACC_NO VARCHAR (100)
	,@REG_ACC_BANK VARCHAR(100)
	,@DOC_DATE_FORM4A VARCHAR(50)
	,@ASC_CONTRACTOR_JAPANESE VARCHAR(50)
	,@ASC_SUBCONTRACT_OPERATION VARCHAR(50)
	
	,@REGISTRATION_DT VARCHAR(50)
	,@APPROVAL_STS VARCHAR(50)
	,@IS_ACTIVE VARCHAR(50)
	,@CREATED_BY VARCHAR(50)
	,@CREATED_DT VARCHAR(50)
	,@CREATED_HOST VARCHAR (50) 
	,@STATUS_FLOW VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[TR_VENDOR_CONTROL]
	(
	   [VENDOR_ID]
      ,[VENDOR_NAME]
      ,[CREATOR]
      ,[CREATOR_DEPT]
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      ,[REQUEST_DT]

      ,[DOC_DATE_FORM1]
      ,[CONF_CONTRACTOR_NM]
      ,[CONF_OPERATION_TO_OUTSOURCE]
      ,[CONF_CATEGORY]
      ,[CONF_DEPT_CD]
      ,[CONF_BRIEF_EXPLAIN_CONTRACT]
      ,[CONF_DECISION]
      ,[CONF_GAD_COMMENT]

      ,[DOC_DATE_FORM2]
      ,[EXAM_NAME_OF_GROUP]
      ,[EXAM_CORE_BUSINESS]
      ,[EXAM_REL_VENDOR]
      ,[EXAM_REL_VENDOR_REMARK]
      ,[EXAM_HAS_ENGAGED_BEFORE]
      ,[EXAM_HAS_ENGAGED_SINCE]
      ,[EXAM_BACKGROUND]
      ,[EXAM_VENDOR_A]
      ,[EXAM_VENDOR_B]
      ,[EXAM_OVERALL_CONCLUSION]

      ,[RISK_ANNUAL_BILLING]
      ,[RISK_SPECIAL_RELATED]
      ,[RISK_CPC_ASSESSMENT]
      ,[RISK_PROVIDE_ACCESS]
      ,[RISK_AS_VENDOR_A]
      ,[RISK_AS_VENDOR_B]
      ,[RISK_AML_COMMENT]
      ,[RISK_CATEGORY]
      ,[RISK_RMD_RECOMMENDATION]
      ,[RISK_SCREENING_RESULT]

      ,[REG_PROVIDED_SERVICE]
      ,[REG_CONTRACT_NM]
      ,[REG_LEGAL_STATUS]
      ,[REG_NPWP_NO]
      ,[REG_ADDRESS]
      ,[REG_CITY]
      ,[REG_POSTAL]
      ,[REG_WEBSITE]
      ,[REG_EMAIL]
      ,[REG_PHONE_1]
      ,[REG_PHONE_EXT_1]
      ,[REG_PHONE_2]
      ,[REG_PHONE_EXT_2]
      ,[REG_FAX_1]
      ,[REG_FAX_2]
      ,[REG_CP_NM_1]
      ,[REG_CP_POSITION_1]
      ,[REG_CP_NM_2]
      ,[REG_CP_POSITION_2]
      ,[REG_PAYMENT_TYPE]
      ,[REG_REASON_CASH]
      ,[REG_ACC_HOLDER_NM]
      ,[REG_ACC_NO]
      ,[REG_ACC_BANK]

      ,[DOC_DATE_FORM4A]
      ,[ASC_CONTRACTOR_JAPANESE]
      ,[ASC_SUBCONTRACT_OPERATION]
      
      ,[REGISTRATION_DT]
      ,[APPROVAL_STS]
      ,[IS_ACTIVE]

      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]

	  ,[STATUS_FLOW]
	)
	VALUES
	(	
		@VENDOR_ID  
		,@VENDOR_NAME  
		,@CREATOR  
		,@CREATOR_DEPT  
		,@CREATOR_UNIT  
		,@CREATOR_ROLE  
		,@REQUEST_DT  

		,@DOC_DATE_FORM1  
		,@CONF_CONTRACTOR_NM  
		,@CONF_OPERATION_TO_OUTSOURCE  
		,@CONF_CATEGORY  
		,@CONF_DEPT_CD  
		,@CONF_BRIEF_EXPLAIN_CONTRACT  
		,@CONF_DECISION  
		,@CONF_GAD_COMMENT 
		 
		,@DOC_DATE_FORM2  
		,@EXAM_NAME_OF_GROUP  
		,@EXAM_CORE_BUSINESS  
		,@EXAM_REL_VENDOR  
		,@EXAM_REL_VENDOR_REMARK  
		,@EXAM_HAS_ENGAGED_BEFORE  
		,@EXAM_HAS_ENGAGED_SINCE  
		,@EXAM_BACKGROUND  
		,@EXAM_VENDOR_A  
		,@EXAM_VENDOR_B  
		,@EXAM_OVERALL_CONCLUSION  

		,@RISK_ANNUAL_BILLING  
		,@RISK_SPECIAL_RELATED  
		,@RISK_CPC_ASSESSMENT  
		,@RISK_PROVIDE_ACCESS  
		,@RISK_AS_VENDOR_A  
		,@RISK_AS_VENDOR_B  
		,@RISK_AML_COMMENT  
		,@RISK_CATEGORY  
		,@RISK_RMD_RECOMMENDATION  
		,@RISK_SCREENING_RESULT  

		,@REG_PROVIDED_SERVICE  
		,@REG_CONTRACT_NM  
		,@REG_LEGAL_STATUS  
		,@REG_NPWP_NO  
		,@REG_ADDRESS  
		,@REG_CITY  
		,@REG_POSTAL  
		,@REG_WEBSITE  
		,@REG_EMAIL  
		,@REG_PHONE_1  
		,@REG_PHONE_EXT_1  
		,@REG_PHONE_2  
		,@REG_PHONE_EXT_2  
		,@REG_FAX_1  
		,@REG_FAX_2  
		,@REG_CP_NM_1  
		,@REG_CP_POSITION_1  
		,@REG_CP_NM_2  
		,@REG_CP_POSITION_2  
		,@REG_PAYMENT_TYPE  
		,@REG_REASON_CASH  
		,@REG_ACC_HOLDER_NM  
		,@REG_ACC_NO  
		,@REG_ACC_BANK  

		,@DOC_DATE_FORM4A  
		,@ASC_CONTRACTOR_JAPANESE  
		,@ASC_SUBCONTRACT_OPERATION  
		
		,@REGISTRATION_DT  
		,@APPROVAL_STS  
		,@IS_ACTIVE  

		,@CREATED_BY  
		,GETDATE()  
		,@CREATED_HOST  

		,@STATUS_FLOW
	)

	--SELECT @@IDENTITY
	SELECT TOP 1 UID FROM TR_VENDOR_CONTROL  ORDER BY UID DESC ;
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORLIST_ATTACH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_VENDORLIST_ATTACH]
      @DOC_ID VARCHAR(50)
      ,@FILE_TYPE VARCHAR(50)
      ,@FILE_NM VARCHAR(50)
      ,@SAVE_FILE_NM VARCHAR(80)
      ,@FILE_URL VARCHAR(200)
      ,@IS_ACTIVE VARCHAR(50)
      ,@CREATED_BY VARCHAR(50)
      ,@CREATED_HOST VARCHAR(50)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT [UID] FROM dbo.[TR_ATTACHMENT] 
                   WHERE [DOC_ID] = @DOC_ID)
	BEGIN
       INSERT INTO dbo.[TR_ATTACHMENT] 
		(
		  [DOC_ID]
		  ,[FILE_TYPE]
		  ,[FILE_NM]
		  ,[SAVE_FILE_NM]
		  ,[FILE_URL]
		  ,[IS_ACTIVE]
		  ,[CREATED_BY]
		  ,[CREATED_DT]
		  ,[CREATED_HOST]
		 
		)
		VALUES
		(	
		   @DOC_ID 
		  ,@FILE_TYPE 
		  ,@FILE_NM 
		  ,@SAVE_FILE_NM 
		  ,@FILE_URL 
		  ,@IS_ACTIVE 
		  ,@CREATED_BY 
		  ,GETDATE() 
		  ,@CREATED_HOST 
		 
		)
		SELECT @@IDENTITY
	END
	ELSE
	BEGIN
		SELECT '0';
	END
    -- Insert statements for procedure here
	
	
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORLIST_CHECKDOC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_VENDORLIST_CHECKDOC]
@DOC_ID VARCHAR(50)
      ,@SEQ VARCHAR(50)
      ,@ITEM VARCHAR(255)
      ,@SUBMITTED VARCHAR(50)
      ,@REASON VARCHAR(8000)
      ,@CHECKED VARCHAR(50)
      ,@REASON_CHECKED VARCHAR(8000)
      ,@IS_ACTIVE VARCHAR(50)
      ,@CREATED_BY VARCHAR(50)
      ,@CREATED_HOST VARCHAR(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT [UID] FROM dbo.[TR_VC_CONF_DOC_CHECKLIST] 
                   WHERE [ITEM] = @ITEM
                   AND [DOC_ID] = @DOC_ID)
	BEGIN
       INSERT INTO dbo.[TR_VC_CONF_DOC_CHECKLIST] 
		(
		   [DOC_ID]
		  ,[SEQ]
		  ,[ITEM]
		  ,[SUBMITTED]
		  ,[REASON]
		  ,[CHECKED]
		  ,[REASON_CHECKED]
		  ,[IS_ACTIVE]
		  ,[CREATED_BY]
		  ,[CREATED_DT]
		  ,[CREATED_HOST]

		)
		VALUES
		(	
		   @DOC_ID 
		  ,@SEQ 
		  ,@ITEM 
		  ,@SUBMITTED 
		  ,@REASON 
		  ,@CHECKED 
		  ,@REASON_CHECKED 
		  ,@IS_ACTIVE 
		  ,@CREATED_BY 
		  ,GETDATE() 
		  ,@CREATED_HOST 

		)
		SELECT @@IDENTITY
	END
	ELSE
	BEGIN
		SELECT '0';
	END
    -- Insert statements for procedure here
	
	
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORLIST_CHECKSHEET]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_VENDORLIST_CHECKSHEET]
@DOC_ID  VARCHAR(50)
      ,@SEQ_NO  VARCHAR(50)
      ,@POSITION  VARCHAR(50)
      ,@NAME  VARCHAR(255)
      ,@NATIONALITY  VARCHAR(255)
      ,@SHARE  VARCHAR(50)
      ,@IS_ACTIVE  VARCHAR(50)
      ,@CREATED_BY  VARCHAR(50)
      ,@CREATED_HOST  VARCHAR(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT [UID] FROM dbo.[TR_VC_ASC_CHECKSHEET] 
                   WHERE [NAME] = @NAME AND [POSITION]=@POSITION
                   AND [DOC_ID] = @DOC_ID)
	BEGIN
    -- Insert statements for procedure here
		INSERT INTO dbo.[TR_VC_ASC_CHECKSHEET]
		(
		   [DOC_ID]
		  ,[SEQ_NO]
		  ,[POSITION]
		  ,[NAME]
		  ,[NATIONALITY]
		  ,[SHARE]
		  ,[IS_ACTIVE]
		  ,[CREATED_BY]
		  ,[CREATED_DT]
		  ,[CREATED_HOST]

		)
		VALUES
		(	
			@DOC_ID
			,@SEQ_NO
			,@POSITION
			,@NAME
			,@NATIONALITY
			,@SHARE
			,@IS_ACTIVE
			,@CREATED_BY
			,GETDATE() 
			,@CREATED_HOST

		)
		SELECT @@IDENTITY
	END
	ELSE
	BEGIN
		SELECT '0';
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORLIST_EXAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_VENDORLIST_EXAM]
@DOC_ID VARCHAR(50)
      ,@SEQ_NO VARCHAR(50)
      ,@PARAMETER VARCHAR(200)
      ,@COMPARE_VENDOR_A VARCHAR(255)
      ,@COMPARE_VENDOR_B VARCHAR(255)
      ,@REMARK nvarchar(max)
      ,@IS_ACTIVE VARCHAR(50)
      ,@CREATED_BY VARCHAR(50)
      ,@CREATED_HOST VARCHAR(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT [UID] FROM dbo.[TR_VC_EXAM_COMPARISON] 
                   WHERE [PARAMETER] = @PARAMETER
                   AND [DOC_ID] = @DOC_ID)
	BEGIN
    -- Insert statements for procedure here
		INSERT INTO dbo.[TR_VC_EXAM_COMPARISON] 
		(
		   [DOC_ID]
		  ,[SEQ_NO]
		  ,[PARAMETER]
		  ,[COMPARE_VENDOR_A]
		  ,[COMPARE_VENDOR_B]
		  ,[REMARK]
		  ,[IS_ACTIVE]
		  ,[CREATED_BY]
		  ,[CREATED_DT]
		  ,[CREATED_HOST]
      

		)
		VALUES
		(	
		  @DOC_ID 
		  ,@SEQ_NO 
		  ,@PARAMETER 
		  ,@COMPARE_VENDOR_A 
		  ,@COMPARE_VENDOR_B 
		  ,@REMARK 
		  ,@IS_ACTIVE 
		  ,@CREATED_BY 
		  ,GETDATE()  
		  ,@CREATED_HOST 


		)

		SELECT @@IDENTITY
	END
	BEGIN
		SELECT '0';
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORLIST_TMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPI_VENDORLIST_TMP]
	@UID VARCHAR(50)
	,@VENDOR_ID VARCHAR(50)
	,@VENDOR_NAME VARCHAR(255)
	,@CREATOR VARCHAR(50)
	,@CREATOR_DEPT VARCHAR(50)
	,@CREATOR_UNIT VARCHAR(50)
	,@CREATOR_ROLE VARCHAR(50)
	,@REQUEST_DT VARCHAR(50)
	,@DOC_DATE_FORM1 VARCHAR(50)
	,@CONF_CONTRACTOR_NM VARCHAR(50)
	,@CONF_OPERATION_TO_OUTSOURCE VARCHAR(50)
	,@CONF_CATEGORY VARCHAR(50)
	,@CONF_DEPT_CD VARCHAR(50)
	,@CONF_BRIEF_EXPLAIN_CONTRACT VARCHAR(50)
	,@CONF_DECISION VARCHAR (50)
	,@CONF_GAD_COMMENT VARCHAR(50)
	,@DOC_DATE_FORM2 VARCHAR(50)
	,@EXAM_NAME_OF_GROUP VARCHAR(50)
	,@EXAM_CORE_BUSINESS VARCHAR(50)
	,@EXAM_REL_VENDOR VARCHAR(50)
	,@EXAM_REL_VENDOR_REMARK VARCHAR(50)
	,@EXAM_HAS_ENGAGED_BEFORE VARCHAR(50)
	,@EXAM_HAS_ENGAGED_SINCE VARCHAR(50)
	,@EXAM_BACKGROUND VARCHAR(50)
	,@EXAM_VENDOR_A VARCHAR(50)
	,@EXAM_VENDOR_B VARCHAR(50)
	,@EXAM_OVERALL_CONCLUSION VARCHAR(50)
	,@RISK_ANNUAL_BILLING VARCHAR(50)
	,@RISK_SPECIAL_RELATED VARCHAR(50)
	,@RISK_CPC_ASSESSMENT VARCHAR(50)
	,@RISK_PROVIDE_ACCESS VARCHAR(50)
	,@RISK_AS_VENDOR_A VARCHAR(50)
	,@RISK_AS_VENDOR_B VARCHAR(50)

	,@RISK_AML_COMMENT VARCHAR(50)
	,@RISK_CATEGORY VARCHAR(50)
	,@RISK_RMD_RECOMMENDATION VARCHAR(50)
	,@RISK_SCREENING_RESULT VARCHAR(50)
	,@REG_PROVIDED_SERVICE VARCHAR (50)
	,@REG_CONTRACT_NM VARCHAR (50)
	,@REG_LEGAL_STATUS VARCHAR(50)
	,@REG_NPWP_NO VARCHAR(50)
	,@REG_ADDRESS VARCHAR(255)
	,@REG_CITY VARCHAR(50)
	,@REG_POSTAL VARCHAR(50)
	,@REG_WEBSITE VARCHAR(100)
	,@REG_EMAIL VARCHAR(100)
	,@REG_PHONE_1 VARCHAR (50)
	,@REG_PHONE_EXT_1 VARCHAR(10)
	,@REG_PHONE_2 VARCHAR(50)
	,@REG_PHONE_EXT_2 VARCHAR(10)
	,@REG_FAX_1 VARCHAR(50)
	,@REG_FAX_2 VARCHAR(50)
	,@REG_CP_NM_1 VARCHAR(150)
	,@REG_CP_POSITION_1 VARCHAR(50)
	,@REG_CP_NM_2 VARCHAR (150)
	,@REG_CP_POSITION_2 VARCHAR(50)
	,@REG_PAYMENT_TYPE VARCHAR(50)
	,@REG_REASON_CASH VARCHAR(255)
	,@REG_ACC_HOLDER_NM VARCHAR(150)
	,@REG_ACC_NO VARCHAR (100)
	,@REG_ACC_BANK VARCHAR(100)
	,@DOC_DATE_FORM4A VARCHAR(50)
	,@ASC_CONTRACTOR_JAPANESE VARCHAR(50)
	,@ASC_SUBCONTRACT_OPERATION VARCHAR(50)
	
	,@REGISTRATION_DT VARCHAR(50)
	,@APPROVAL_STS VARCHAR(50)
	,@IS_ACTIVE VARCHAR(50)
	,@CREATED_BY VARCHAR(50)
	,@CREATED_DT VARCHAR(50)
	,@CREATED_HOST VARCHAR (50) 
	,@STATUS_FLOW VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.[TR_VENDOR_CONTROL_TMP]
	(
		[UID]
	   ,[VENDOR_ID]
      ,[VENDOR_NAME]
      ,[CREATOR]
      ,[CREATOR_DEPT]
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      ,[REQUEST_DT]

      ,[DOC_DATE_FORM1]
      ,[CONF_CONTRACTOR_NM]
      ,[CONF_OPERATION_TO_OUTSOURCE]
      ,[CONF_CATEGORY]
      ,[CONF_DEPT_CD]
      ,[CONF_BRIEF_EXPLAIN_CONTRACT]
      ,[CONF_DECISION]
      ,[CONF_GAD_COMMENT]

      ,[DOC_DATE_FORM2]
      ,[EXAM_NAME_OF_GROUP]
      ,[EXAM_CORE_BUSINESS]
      ,[EXAM_REL_VENDOR]
      ,[EXAM_REL_VENDOR_REMARK]
      ,[EXAM_HAS_ENGAGED_BEFORE]
      ,[EXAM_HAS_ENGAGED_SINCE]
      ,[EXAM_BACKGROUND]
      ,[EXAM_VENDOR_A]
      ,[EXAM_VENDOR_B]
      ,[EXAM_OVERALL_CONCLUSION]

      ,[RISK_ANNUAL_BILLING]
      ,[RISK_SPECIAL_RELATED]
      ,[RISK_CPC_ASSESSMENT]
      ,[RISK_PROVIDE_ACCESS]
      ,[RISK_AS_VENDOR_A]
      ,[RISK_AS_VENDOR_B]
      ,[RISK_AML_COMMENT]
      ,[RISK_CATEGORY]
      ,[RISK_RMD_RECOMMENDATION]
      ,[RISK_SCREENING_RESULT]

      ,[REG_PROVIDED_SERVICE]
      ,[REG_CONTRACT_NM]
      ,[REG_LEGAL_STATUS]
      ,[REG_NPWP_NO]
      ,[REG_ADDRESS]
      ,[REG_CITY]
      ,[REG_POSTAL]
      ,[REG_WEBSITE]
      ,[REG_EMAIL]
      ,[REG_PHONE_1]
      ,[REG_PHONE_EXT_1]
      ,[REG_PHONE_2]
      ,[REG_PHONE_EXT_2]
      ,[REG_FAX_1]
      ,[REG_FAX_2]
      ,[REG_CP_NM_1]
      ,[REG_CP_POSITION_1]
      ,[REG_CP_NM_2]
      ,[REG_CP_POSITION_2]
      ,[REG_PAYMENT_TYPE]
      ,[REG_REASON_CASH]
      ,[REG_ACC_HOLDER_NM]
      ,[REG_ACC_NO]
      ,[REG_ACC_BANK]

      ,[DOC_DATE_FORM4A]
      ,[ASC_CONTRACTOR_JAPANESE]
      ,[ASC_SUBCONTRACT_OPERATION]
      
      ,[REGISTRATION_DT]
      ,[APPROVAL_STS]
      ,[IS_ACTIVE]

      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]

	  ,[STATUS_FLOW]
	)
	VALUES
	(	
		@UID
		,@VENDOR_ID  
		,@VENDOR_NAME  
		,@CREATOR  
		,@CREATOR_DEPT  
		,@CREATOR_UNIT  
		,@CREATOR_ROLE  
		,@REQUEST_DT  

		,@DOC_DATE_FORM1  
		,@CONF_CONTRACTOR_NM  
		,@CONF_OPERATION_TO_OUTSOURCE  
		,@CONF_CATEGORY  
		,@CONF_DEPT_CD  
		,@CONF_BRIEF_EXPLAIN_CONTRACT  
		,@CONF_DECISION  
		,@CONF_GAD_COMMENT 
		 
		,@DOC_DATE_FORM2  
		,@EXAM_NAME_OF_GROUP  
		,@EXAM_CORE_BUSINESS  
		,@EXAM_REL_VENDOR  
		,@EXAM_REL_VENDOR_REMARK  
		,@EXAM_HAS_ENGAGED_BEFORE  
		,@EXAM_HAS_ENGAGED_SINCE  
		,@EXAM_BACKGROUND  
		,@EXAM_VENDOR_A  
		,@EXAM_VENDOR_B  
		,@EXAM_OVERALL_CONCLUSION  

		,@RISK_ANNUAL_BILLING  
		,@RISK_SPECIAL_RELATED  
		,@RISK_CPC_ASSESSMENT  
		,@RISK_PROVIDE_ACCESS  
		,@RISK_AS_VENDOR_A  
		,@RISK_AS_VENDOR_B  
		,@RISK_AML_COMMENT  
		,@RISK_CATEGORY  
		,@RISK_RMD_RECOMMENDATION  
		,@RISK_SCREENING_RESULT  

		,@REG_PROVIDED_SERVICE  
		,@REG_CONTRACT_NM  
		,@REG_LEGAL_STATUS  
		,@REG_NPWP_NO  
		,@REG_ADDRESS  
		,@REG_CITY  
		,@REG_POSTAL  
		,@REG_WEBSITE  
		,@REG_EMAIL  
		,@REG_PHONE_1  
		,@REG_PHONE_EXT_1  
		,@REG_PHONE_2  
		,@REG_PHONE_EXT_2  
		,@REG_FAX_1  
		,@REG_FAX_2  
		,@REG_CP_NM_1  
		,@REG_CP_POSITION_1  
		,@REG_CP_NM_2  
		,@REG_CP_POSITION_2  
		,@REG_PAYMENT_TYPE  
		,@REG_REASON_CASH  
		,@REG_ACC_HOLDER_NM  
		,@REG_ACC_NO  
		,@REG_ACC_BANK  

		,@DOC_DATE_FORM4A  
		,@ASC_CONTRACTOR_JAPANESE  
		,@ASC_SUBCONTRACT_OPERATION  
		
		,@REGISTRATION_DT  
		,@APPROVAL_STS  
		,@IS_ACTIVE  

		,@CREATED_BY  
		,GETDATE()  
		,@CREATED_HOST  

		,@STATUS_FLOW
	)

	SELECT TOP 1 UID FROM TR_VENDOR_CONTROL_TMP ORDER BY UID DESC;
END



GO
/****** Object:  StoredProcedure [dbo].[UDPI_VENDORMANAGEMENTCOMMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPI_VENDORMANAGEMENTCOMMENT]
	-- add more stored procedure parameters here
	@VendorUID BIGINT,
	@Comment VARCHAR(8000),
	@UserApproval VARCHAR(50),
	@CreatedHost VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	INSERT INTO TR_VC_RISK_MANAGEMENT_COMMENT (
		DOC_ID,
		USER_APV,
		COMMENT,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST
		)
	VALUES (
		@VendorUID,
		@UserApproval,
		@Comment,
		1,
		@UserApproval,
		GETDATE(),
		@CreatedHost
		)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPI_WORKFLOWPROCESS]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPI_WORKFLOWPROCESS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@REFID BIGINT,
	@PROCID VARCHAR(100) = '0',
	@SN VARCHAR(100) = '0',
	@WFTASK VARCHAR(100),
	@WFACTION VARCHAR(100),
	@WFSTEP VARCHAR(200) = '0',
	@VENDORID VARCHAR(100),
	@CREATEDBY VARCHAR(100),
	@CREATEDDT DATETIME,
	@CREATEDHOST VARCHAR(100),
	@WFTASKNAME VARCHAR(200)
AS
BEGIN
	-- body of the stored procedure
	INSERT INTO TR_WORKFLOWPROCESS (
		DOCID,
		REFID,
		PROCID,
		SN,
		WFTASK,
		WFACTION,
		WFSTEP,
		VENDORID,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		WFTASKNAME,
		ISACTIVE
		)
	VALUES (
		@DOCID,
		@REFID,
		@PROCID,
		@SN,
		@WFTASK,
		@WFACTION,
		@WFSTEP,
		@VENDORID,
		@CREATEDBY,
		@CREATEDDT,
		@CREATEDHOST,
		@WFTASKNAME,
		1
		)

	SELECT @@IDENTITY
END


GO
/****** Object:  StoredProcedure [dbo].[UDPP_ATTACHMENTTOTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPP_ATTACHMENTTOTEMP]
	-- add more stored procedure parameters here
	@ParentUID BIGINT,
	@VendorUID BIGINT,
	@DeleteID VARCHAR(1000) = ''
AS
BEGIN
	-- body of the stored procedure
	INSERT INTO TR_ATTACHMENT_TEMP (
		DOC_ID,
		FILE_TYPE,
		FILE_NM,
		SAVE_FILE_NM,
		FILE_URL,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST
		)
	SELECT @VendorUID,
		FILE_TYPE,
		FILE_NM,
		SAVE_FILE_NM,
		FILE_URL,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST
	FROM TR_ATTACHMENT
	WHERE 1 = 1
		AND FILE_TYPE = 'REGISTRATION'
		AND DOC_ID = @ParentUID
		AND [UID] NOT IN (
			SELECT [Name]
			FROM DBO.splitstring(@DeleteID, ',')
			)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPP_LATESTREMINDERDATE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211123>
-- Description:	<UDPP_LASTREMINDERDATE>
-- =============================================
CREATE PROCEDURE [dbo].[UDPP_LATESTREMINDERDATE]
	@Department VARCHAR(100)
	,@TypeDate VARCHAR(100)
	,@LastSentReminderDate DATETIME
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Id int

	IF NOT EXISTS (SELECT 1 FROM DBO.TR_LATEST_REMINDER WHERE DEPT_CD = @Department)
	BEGIN
		INSERT INTO DBO.TR_LATEST_REMINDER (DEPT_CD,CREATED_BY,CREATED_DT) VALUES (@Department,'SYSTEM',GETDATE())

		SELECT @Id = [UID] FROM DBO.TR_LATEST_REMINDER WHERE DEPT_CD = @Department
	END
	ELSE
		SELECT @Id = [UID] FROM DBO.TR_LATEST_REMINDER WHERE DEPT_CD = @Department

	IF(@TypeDate = 'REV_LATEST_SENT_REMINDER_FWD')
	BEGIN
		UPDATE dbo.TR_LATEST_REMINDER
		SET REV_LATEST_SENT_REMINDER_FWD = @LastSentReminderDate
			,MODIFIED_BY = @ModifiedBy
			,MODIFIED_DT = @ModifiedDate
			,MODIFIED_HOST = @ModifiedHost
		WHERE [UID] = @Id
	END
	ELSE IF(@TypeDate = 'REV_LATEST_SENT_REMINDER_BWD')
	BEGIN
		UPDATE dbo.TR_LATEST_REMINDER
		SET REV_LATEST_SENT_REMINDER_BWD = @LastSentReminderDate
			,MODIFIED_BY = @ModifiedBy
			,MODIFIED_DT = @ModifiedDate
			,MODIFIED_HOST = @ModifiedHost
		WHERE [UID] = @Id
	END
	ELSE IF(@TypeDate = 'CONT_LATEST_SENT_REMINDER_FWD')
	BEGIN
		UPDATE dbo.TR_LATEST_REMINDER
		SET CONT_LATEST_SENT_REMINDER_FWD = @LastSentReminderDate
			,MODIFIED_BY = @ModifiedBy
			,MODIFIED_DT = @ModifiedDate
			,MODIFIED_HOST = @ModifiedHost
		WHERE [UID] = @Id
	END
	ELSE IF(@TypeDate = 'CONT_LATEST_SENT_REMINDER_BWD')
	BEGIN
		UPDATE dbo.TR_LATEST_REMINDER
		SET CONT_LATEST_SENT_REMINDER_BWD = @LastSentReminderDate
			,MODIFIED_BY = @ModifiedBy
			,MODIFIED_DT = @ModifiedDate
			,MODIFIED_HOST = @ModifiedHost
		WHERE [UID] = @Id
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPP_LATESTREMINDERDATE_VENDOR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPP_LATESTREMINDERDATE_VENDOR]
	@Uid BIGINT
	,@FormType VARCHAR(100)
	,@AprStatus VARCHAR(100)
	,@LastSentReminderDate DATETIME
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Id int

	IF NOT EXISTS (SELECT 1 FROM DBO.TR_LATEST_REMINDER_VENDOR WHERE DOC_ID = @Uid AND FORM_TYPE = @FormType)
	BEGIN
		INSERT INTO DBO.TR_LATEST_REMINDER_VENDOR (DOC_ID,FORM_TYPE,CREATED_BY,CREATED_DT) VALUES (@Uid,@FormType,'SYSTEM',GETDATE())

		SELECT @Id = [UID] FROM DBO.TR_LATEST_REMINDER_VENDOR WHERE DOC_ID = @Uid AND FORM_TYPE = @FormType
	END
	ELSE
		SELECT @Id = [UID] FROM DBO.TR_LATEST_REMINDER_VENDOR WHERE DOC_ID = @Uid AND FORM_TYPE = @FormType

	IF(@AprStatus = 'REVISE')
	BEGIN
		UPDATE dbo.TR_LATEST_REMINDER_VENDOR
		SET LATEST_SENT_REMINDER_REV = @LastSentReminderDate
			,MODIFIED_BY = @ModifiedBy
			,MODIFIED_DT = @ModifiedDate
			,MODIFIED_HOST = @ModifiedHost
		WHERE [UID] = @Id AND FORM_TYPE = @FormType
	END
	ELSE IF(@AprStatus = 'APPROVAL')
	BEGIN
		UPDATE dbo.TR_LATEST_REMINDER_VENDOR
		SET LATEST_SENT_REMINDER_APP = @LastSentReminderDate
			,MODIFIED_BY = @ModifiedBy
			,MODIFIED_DT = @ModifiedDate
			,MODIFIED_HOST = @ModifiedHost
		WHERE [UID] = @Id AND FORM_TYPE = @FormType
	END

	SELECT @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPP_LATESTREMINDERDATEBYVENDORID]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPP_LATESTREMINDERDATEBYVENDORID]
	-- add more stored procedure parameters here
	@VendorList VARCHAR(8000),
	@TypeDate VARCHAR(100),
	@LastSentReminderDate DATETIME
AS
BEGIN
	-- body of the stored procedure
	IF (@TypeDate = 'REV_LATEST_SENT_REMINDER_FWD')
	BEGIN
		UPDATE TR_VENDOR_CONTROL
		SET REV_LATEST_SENT_REMINDER_FWD = @LastSentReminderDate
		WHERE VENDOR_ID IN (
				SELECT Name
				FROM dbo.splitstring(@VendorList, ',')
				)
	END
	ELSE IF (@TypeDate = 'REV_LATEST_SENT_REMINDER_BWD')
	BEGIN
		UPDATE TR_VENDOR_CONTROL
		SET REV_LATEST_SENT_REMINDER_BWD = @LastSentReminderDate
		WHERE VENDOR_ID IN (
				SELECT Name
				FROM dbo.splitstring(@VendorList, ',')
				)
	END
	ELSE IF (@TypeDate = 'CONT_LATEST_SENT_REMINDER_FWD')
	BEGIN
		UPDATE TR_VENDOR_CONTROL
		SET CONT_LATEST_SENT_REMINDER_FWD = @LastSentReminderDate
		WHERE VENDOR_ID IN (
				SELECT Name
				FROM dbo.splitstring(@VendorList, ',')
				)
	END
	ELSE IF (@TypeDate = 'CONT_LATEST_SENT_REMINDER_BWD')
	BEGIN
		UPDATE TR_VENDOR_CONTROL
		SET CONT_LATEST_SENT_REMINDER_BWD = @LastSentReminderDate
		WHERE VENDOR_ID IN (
				SELECT Name
				FROM dbo.splitstring(@VendorList, ',')
				)
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UDPP_PERIODICALREVIEWAPPROVE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPP_PERIODICALREVIEWAPPROVE]
	-- add more stored procedure parameters here
	@Id INT,
	@WFStep INT,
	@Result5 BIT,
	@ResultComment5 VARCHAR(8000),
	@GADRecommendation VARCHAR(8000),
	@RMDComment VARCHAR(8000)
AS
BEGIN
	-- body of the stored procedure
	IF @WFStep = 2
	BEGIN
		UPDATE TR_VENDOR_CONTROL_ACTIVITY
		SET REV_GAD_RECOMMENDATION = @GADRecommendation
		WHERE UID = @Id
	END

	IF @WFStep = 6
	BEGIN
		UPDATE TR_VENDOR_CONTROL_ACTIVITY
		SET REV_RMD_COMMENT = @RMDComment
		WHERE UID = @Id
	END

	IF @WFStep = 3
	BEGIN
		UPDATE TR_VC_ACTIVITY_REV_EXAMINED
		SET RESULT = @Result5,
			COMMENT = @ResultComment5
		WHERE DOC_ID = @Id
			AND SEQ_NO = 5
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_ANTISOCIALELEMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_ANTISOCIALELEMENT]
@DocID INT
AS
BEGIN
	SELECT * FROM TR_VC_ASC_CHECKSHEET
	WHERE IS_ACTIVE = 1
	AND DOC_ID = @DocID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_APPGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
create PROCEDURE [dbo].[UDPS_APPGM]
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedure
	SELECT Username,
		RealName,
		Email
	FROM [vw_UserByDepartment]
	WHERE RoleParentID IN (
			SELECT RoleParentID
			FROM vw_UserByDepartment
			WHERE RoleID = 70
			)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_ATTACHMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_ATTACHMENT]
@DocID INT
AS
BEGIN
	SELECT * FROM TR_ATTACHMENT
	WHERE DOC_ID = @DocID
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_ATTACHMENTBYID]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ATTACHMENTBYID]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT ta.UID
		,ta.DOC_ID
		,ta.FILE_TYPE
		,ta.FILE_NM
		,ta.SAVE_FILE_NM
		,ta.FILE_URL
		,ta.IS_ACTIVE
		,ta.CREATED_BY
		,ta.CREATED_DT
		,ta.CREATED_HOST
		,ta.MODIFIED_BY
		,ta.MODIFIED_DT
		,ta.MODIFIED_HOST
	FROM dbo.TR_ATTACHMENT AS ta
	WHERE 1 = 1
		AND [ta].[UID] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_ATTACHMENTTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_ATTACHMENTTEMP] @DocID INT
AS
BEGIN
	SELECT *
	FROM TR_ATTACHMENT_TEMP
	WHERE DOC_ID = @DocID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_ATTACHMENTTEMPBYID]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_ATTACHMENTTEMPBYID]
	-- Add the parameters for the stored procedure here
	@Id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT ta.UID
		,ta.DOC_ID
		,ta.FILE_TYPE
		,ta.FILE_NM
		,ta.SAVE_FILE_NM
		,ta.FILE_URL
		,ta.IS_ACTIVE
		,ta.CREATED_BY
		,ta.CREATED_DT
		,ta.CREATED_HOST
		,ta.MODIFIED_BY
		,ta.MODIFIED_DT
		,ta.MODIFIED_HOST
	FROM dbo.TR_ATTACHMENT_TEMP AS ta
	WHERE 1 = 1
		AND [ta].[UID] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_CHECKDEPART]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_CHECKDEPART]
	-- Add the parameters for the stored procedure here
	@DepartmentCode NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	IF EXISTS (
			SELECT 1
			FROM MS_DEPT_PIC_CONFIG_HDR
			WHERE 1 = 1
				AND DEPT_CD = @DepartmentCode
			)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_CONTINUINGDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_CONTINUINGDETAIL]
@Id INT = 0
AS
BEGIN
	SELECT 
		 [UID] AS Id
		,[DOC_ID] AS DocId
		,[FORM_TYPE] AS FormType
		,[CREATOR] AS Creator
		,[CREATOR_DEPT] AS CreatorDepartment
		,[CREATOR_SECTION] AS CreatorSection
		,[CREATOR_ROLE] AS CreatorRole
		,[REQUEST_DT] AS RequestDate
		,[VENDOR_ID] AS VendorId
		,[VENDOR_ID] AS VendID
		,[DOC_DATE_FORM5] AS DocDate
		,[WAIV_PROVIDED_SERVICE] AS WaivProvidedService 
		,[CONT_DESC_PERFORMANCE] AS ContDescPerformance
		,[CONT_BIDDING_RESULT] AS ContBiddingResult
		,[CONT_REASON_UTILITY] AS ContReasonUtility 
		,[CONT_REL_EXPLAIN] AS ContRelExplain
		,[CONT_CHANGE_FORM4B] AS ContChangeForm4b
		,[CONT_INTV_CREATOR_DGM] AS ContIntvCreatorDgm
		,[CONT_INTV_GAD_DGM] AS ContIntvGadDgm
		,[CONT_GAD_RECOMMENDATION] AS ContGadRecommendation
		,[APPROVAL_STS] As ApprovalStatus
		,[IS_ACTIVE] AS IsActive
		,[CREATED_BY] AS CreatedBy
		,[CREATED_DT] AS CreatedDate
		,[CREATED_HOST] AS CreatedHost
		,[MODIFIED_BY] AS ModifiedBy
		,[MODIFIED_DT] AS ModifiedDate
		,[MODIFIED_HOST] AS ModifiedHost
		,[IS_DRAFT] AS IsDraft
	FROM [TR_VENDOR_CONTROL_ACTIVITY] VCA
	WHERE IS_ACTIVE = 1
	AND [FORM_TYPE] = 'CONTINUING_VENDOR'
	AND ([UID] = @Id OR @Id = 0)
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_CONTINUINGVENDOR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_CONTINUINGVENDOR]
@Id INT = 0
AS
BEGIN
	SELECT 
		 VCA.[UID] AS Id
		,VCA.[DOC_ID] AS DocId
		,VCA.[FORM_TYPE] AS FormType
		,VCA.[CREATOR] AS Creator
		,VCA.[CREATOR_DEPT] AS CreatorDepartment
		,VCA.[CREATOR_SECTION] AS CreatorSection
		,VCA.[CREATOR_ROLE] AS CreatorRole
		,VCA.[REQUEST_DT] AS RequestDate
		,vca.[VENDOR_ID] as VendorId
		,A.[VENDOR_ID] AS VendID
		,a.[VENDOR_NAME] as NameOfVendor
		,VCA.[DOC_DATE_FORM5] AS DocDate
		,A.CONF_BRIEF_EXPLAIN_CONTRACT AS ContBriefProvided 
		,VCA.[CONT_DESC_PERFORMANCE] AS ContDescPerformance
		,VCA.[CONT_BIDDING_RESULT] AS ContBiddingResult
		,vca.[CONT_REASON_UTILITY] AS ContReasonUtility 
		,VCA.[CONT_REL_EXPLAIN] AS ContRelExplain
		,VCA.[CONT_CHANGE_FORM4B] AS ContChangeForm4b
		,VCA.[CONT_INTV_CREATOR_DGM] AS ContIntvCreatorDgm
		,VCA.[CONT_INTV_GAD_DGM] AS ContIntvGadDgm
		,VCA.[CONT_GAD_RECOMMENDATION] AS ContGadRecommendation
		,VCA.[APPROVAL_STS] As ApprovalStatus
		,vca.[IS_ACTIVE] AS IsActive
		,VCA.[CREATED_BY] AS CreatedBy
		,VCA.[CREATED_DT] AS CreatedDate
		,VCA.[CREATED_HOST] AS CreatedHost
		,VCA.[MODIFIED_BY] AS ModifiedBy
		,VCA.[MODIFIED_DT] AS ModifiedDate
		,VCA.[MODIFIED_HOST] AS ModifiedHost
		,VCA.[IS_DRAFT] AS IsDraft
		,CONCAT(Dept.ShortName, ' - ', Dept.LongName) AS DepartmentDesc
	FROM [TR_VENDOR_CONTROL_ACTIVITY] VCA
	LEFT JOIN [TR_VENDOR_CONTROL] AS a 
	ON a.UID = VCA.DOC_ID 
	LEFT JOIN SN_BTMUDirectory_Department Dept
	ON Dept.ShortName = VCA.CREATOR_DEPT
	WHERE VCA.IS_ACTIVE = 1
	AND [FORM_TYPE] = 'CONTINUING_VENDOR'
	AND (vca.[UID] = @Id OR @Id = 0)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_CONTINUINGVENDORAPPROVAL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_CONTINUINGVENDORAPPROVAL]
@UID INT,
@ContIntvCreatorDgm VARCHAR(8000),
@ContIntvGadDgm VARCHAR(8000),
@ContGadRecommendation VARCHAR(8000)

--@ModifiedBy VARCHAR(255),
--@ModifiedDate DATETIME,
--@ModifiedHost VARCHAR(50)
AS
BEGIN
	UPDATE A
	SET A.CONT_INTV_CREATOR_DGM = @ContIntvCreatorDgm,
		A.CONT_INTV_GAD_DGM = @ContIntvGadDgm,
		A.CONT_GAD_RECOMMENDATION = @ContGadRecommendation
	FROM TR_VENDOR_CONTROL_ACTIVITY A
	WHERE UID = @UID
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_CONTINUINGVENDORCREATIONINFO]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_CONTINUINGVENDORCREATIONINFO] @VendorID BIGINT
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedure
	SELECT --a.VENDOR_ID AS VendID,
		VC.VENDOR_ID as VendorId,
		VC.VENDOR_ID as VendID,
		VC.VENDOR_NAME AS NameOfVendor,
		VC.CONF_BRIEF_EXPLAIN_CONTRACT AS ContBriefProvided
		FROM [TR_VENDOR_CONTROL] VC
	WHERE [UID] = @VendorID

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_CONTINUINGVENDORDATA]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_CONTINUINGVENDORDATA] @Id INT = 0
AS
BEGIN
	SELECT VCA.[UID] AS Id,
		VCA.[DOC_ID] AS DocId,
		VCA.[FORM_TYPE] AS FormType,
		VCA.[CREATOR] AS Creator,
		VCA.[CREATOR_DEPT] AS CreatorDepartment,
		VCA.[CREATOR_SECTION] AS CreatorSection,
		VCA.[CREATOR_ROLE] AS CreatorRole,
		VCA.[REQUEST_DT] AS RequestDate,
		VCA.[VENDOR_ID] AS VendorId,
		VCA.[VENDOR_ID] AS VendID,
		VC.[VENDOR_NAME] AS NameOfVendor,
		VCA.[DOC_DATE_FORM5] AS DocDate,
		VCA.[WAIV_PROVIDED_SERVICE] AS WaivProvidedService,
		VCA.[CONT_DESC_PERFORMANCE] AS ContDescPerformance,
		VCA.[CONT_BIDDING_RESULT] AS ContBiddingResult,
		VCA.[CONT_REASON_UTILITY] AS ContReasonUtility,
		VCA.[CONT_REL_EXPLAIN] AS ContRelExplain,
		VCA.[CONT_CHANGE_FORM4B] AS ContChangeForm4b,
		VCA.[CONT_INTV_CREATOR_DGM] AS ContIntvCreatorDgm,
		VCA.[CONT_INTV_GAD_DGM] AS ContIntvGadDgm,
		VCA.[CONT_GAD_RECOMMENDATION] AS ContGadRecommendation,
		VCA.[APPROVAL_STS] AS ApprovalStatus,
		VCA.[IS_ACTIVE] AS IsActive,
		VCA.[CREATED_BY] AS CreatedBy,
		VCA.[CREATED_DT] AS CreatedDate,
		VCA.[CREATED_HOST] AS CreatedHost,
		VCA.[MODIFIED_BY] AS ModifiedBy,
		VCA.[MODIFIED_DT] AS ModifiedDate,
		VCA.[MODIFIED_HOST] AS ModifiedHost,
		VCA.[IS_DRAFT] AS IsDraft
	FROM [TR_VENDOR_CONTROL_ACTIVITY] VCA
	INNER JOIN [TR_VENDOR_CONTROL] VC ON 
	VCA.DOC_ID = VC.UID
	WHERE VCA.IS_ACTIVE = 1
		AND VCA.[FORM_TYPE] = 'CONTINUING_VENDOR'
		AND (
			VCA.[UID] = @Id
			OR @Id = 0
			)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_CONTROLAGREEMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_CONTROLAGREEMENT]
	
	@Id int

AS
BEGIN

SELECT [UID]
      ,[VENDOR_ID]
      ,[AGREE_START_DATE]
      ,[AGREE_END_DATE]
      ,[AGREE_CREATED_BY]
      ,[AGREE_CREATED_DT]
      ,[AGREE_MODIFIED_BY]
      ,[AGREE_MODIFIED_DT]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]
      ,[MODIFIED_BY]
      ,[MODIFIED_DT]
      ,[MODIFIED_HOST]
  FROM [TR_VENDOR_CONTROL]
	WHERE IS_ACTIVE = 1
	AND UID = @Id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENINFO]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_DEPARTMENINFO]
	-- Add the parameters for the stored procedure here
	@Id varchar(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here

	--declare @sn varchar(15)
	--set @SN = 'SKAI'
		SELECT  ID,
			SHORTNAME,
			LONGNAME
		FROM [SN_BTMUDirectory_Department]
		WHERE  SHORTNAME = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTPIC_PARAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTPIC_PARAM]	
	@DepartmentString NVARCHAR(2000) = '',
	@KeywordString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON
	 
	 --WITH CTE_Result
	 --AS(
		--SELECT * 
		--FROM MS_DEPT_PIC_CONFIG_HDR
		--WHERE 1 = 1
		--	AND (
		--		@KeywordString = ''
		--		)
		--ORDER BY CASE(
		--	CASE @SortOrder = ''		
		--)

	 --)

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTPICDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTPICDETAIL] @Id INT,
	@Dept VARCHAR(50)
AS
BEGIN
	SELECT dpih.[UID] AS Id,
		dpih.[DEPT_CD] AS DepartmentCode ,
		dpih.[DEPT_CD] + ' - ' + dept.longname AS DepartmentCodeDesc,
		-- dpih.[MEMBER_CD] AS MemberCode,
		MemberCode = ISNULL(STUFF((
					SELECT ',' + dpid.MEMBER_CD
					FROM dbo.MS_DEPT_PIC_CONFIG_DTL AS dpid
					WHERE dpid.DEPT_CD = dpih.DEPT_CD
					FOR XML PATH('')
					), 1, 1, ''), ''),
		dpih.[CREATED_BY] AS CreatedBy,
		dpih.[CREATED_DT] AS CreatedDate,
		dpih.[CREATED_HOST] AS CreatedHost,
		dpih.[MODIFIED_BY] AS ModifiedBy,
		dpih.[MODIFIED_DT] AS ModifiedDate,
		dpih.[MODIFIED_HOST] AS ModifiedHost
	FROM MS_DEPT_PIC_CONFIG_HDR AS dpih
	LEFT JOIN [dbo].[SN_BTMUDirectory_department] dept
	ON dpih.DEPT_CD = dept.ShortName
	-- LEFT JOIN MS_DEPT_PIC_CONFIG_DTL AS dpid ON dpih.DEPT_CD = dpid.DEPT_CD
	WHERE dpih.UID = @Id
		--AND dpih.DEPT_CD = LTRIM(RTRIM(LEFT(@Dept,CHARINDEX('-',@Dept)-1)))
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTPICEXPORT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTPICEXPORT]	
@MemberCode varchar(2000) = 'ALL MEMBER'
AS
BEGIN

SELECT dpih.UID AS Id,
	dpih.DEPT_CD AS DepartmentCode,
	dpih.DEPT_CD + isnull('-'+dept.longname,'') AS DepartmentCodeDesc,
	dpid.MEMBER_CD AS MemberCode,
	dpih.CREATED_BY AS CreatedBy,
	dpih.CREATED_DT AS CreatedDate,
	dpih.CREATED_HOST AS CreatedHost,
	dpih.MODIFIED_BY AS ModifiedBy,
	dpih.MODIFIED_DT AS ModifiedDate,
	dpih.MODIFIED_HOST AS ModifiedHost
	FROM MS_DEPT_PIC_CONFIG_HDR AS dpih
	LEFT JOIN MS_DEPT_PIC_CONFIG_DTL AS dpid
	ON dpid.DEPT_CD = dpih.DEPT_CD
	LEFT JOIN [dbo].[SN_BTMUDirectory_department] dept 
	ON dpih.DEPT_CD = dept.ShortName
	--WHERE DEPT_CD = @DeptId
	where dpid.MEMBER_CD LIKE '%'+@MemberCode+'%' or @MemberCode = 'ALL MEMBER';

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTPICLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTPICLIST]	
		--@DeptId varchar(50)

AS
BEGIN
		
		--DECLARE @DEPTID VARCHAR(50)
		--	SET @DeptId = 'ACC'
		

SELECT UID AS Id,
	pic.DEPT_CD AS DepartmentCode,
	pic.DEPT_CD + isnull('-'+dept.longname,'') AS DepartmentCodeDesc,
	pic.CREATED_BY AS CreatedBy,
	pic.CREATED_DT AS CreatedDate,
	pic.CREATED_HOST AS CreatedHost,
	pic.MODIFIED_BY AS ModifiedBy,
	pic.MODIFIED_DT AS ModifiedDate,
	pic.MODIFIED_HOST AS ModifiedHost
	FROM MS_DEPT_PIC_CONFIG_HDR AS pic
	LEFT JOIN [dbo].[SN_BTMUDirectory_department] dept
	ON pic.DEPT_CD = dept.ShortName
	--WHERE DEPT_CD = @DeptId
		


END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTPICMEMBEREXPORT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTPICMEMBEREXPORT]
	@DeptId varchar(50) = 'ALLDEPT',
	@MemberCode VARCHAR(50) = 'ALLMEMBER'
AS
BEGIN
	SELECT 
	 dpcd.[UID] AS Id
	,dpcd.[DEPT_CD] AS DepartmentCode
	,dpcd.[MEMBER_CD] AS MemberCode
	,dpcd.[CREATED_BY] AS CreatedBy
	,dpcd.[CREATED_DT] AS CreatedDate
	,dpcd.[CREATED_HOST] AS CreatedHost
	,dpcd.[MODIFIED_BY] AS ModifiedBy
	,dpcd.[MODIFIED_DT] AS ModifiedDate
	,dpcd.[MODIFIED_HOST] AS ModifiedHost
	FROM MS_DEPT_PIC_CONFIG_DTL AS dpcd
	WHERE dpcd.DEPT_CD = @DeptId OR @DeptId = 'ALLDEPT'
	--WHERE (dpcd.DEPT_CD= LTRIM(RTRIM(LEFT(@DeptId,CHARINDEX('-',@DeptId)-1))) OR @DeptId = 'ALLDEPT')
	AND (dpcd.MEMBER_CD LIKE '%'+@MemberCode+'%' OR @MemberCode = 'ALLMEMBER')

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTPICMEMBERLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTPICMEMBERLIST]
	@DeptId varchar(50) = 'ALLDEPT',
	@MemberCode VARCHAR(50) = 'ALLMEMBER'
AS
BEGIN
	SELECT 
	 dpcd.[UID] AS Id
	,dpcd.[DEPT_CD] AS DepartmentCode
	,dpcd.[MEMBER_CD] AS MemberCode
	,dpcd.[CREATED_BY] AS CreatedBy
	,dpcd.[CREATED_DT] AS CreatedDate
	,dpcd.[CREATED_HOST] AS CreatedHost
	,dpcd.[MODIFIED_BY] AS ModifiedBy
	,dpcd.[MODIFIED_DT] AS ModifiedDate
	,dpcd.[MODIFIED_HOST] AS ModifiedHost
	FROM MS_DEPT_PIC_CONFIG_DTL AS dpcd
	WHERE (dpcd.DEPT_CD = @DeptId OR ISNULL(@DeptId,'') = '')
	--WHERE (dpcd.DEPT_CD= LTRIM(RTRIM(LEFT(@DeptId,CHARINDEX('-',@DeptId)-1))) OR @DeptId = 'ALLDEPT')
	AND (dpcd.MEMBER_CD LIKE '%'+@MemberCode+'%' OR @MemberCode = 'ALLMEMBER')

	
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTPICPRINT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTPICPRINT] @Id INT,
	@Dept VARCHAR(50)
AS
BEGIN
	SELECT dpih.[UID] AS Id,
		dpih.[DEPT_CD] AS DepartmentCode,
		dpih.DEPT_CD + ' - ' + dpt.longname as DepartmentCodeDesc,
		 dpid.[MEMBER_CD] AS MemberCode,
		--MemberCode = ISNULL(STUFF((
		--			SELECT ',' + dpid.MEMBER_CD
		--			FROM dbo.MS_DEPT_PIC_CONFIG_DTL AS dpid
		--			WHERE dpid.DEPT_CD = dpih.DEPT_CD
		--			FOR XML PATH('')
		--			), 1, 1, ''), ''),
		dpih.[CREATED_BY] AS CreatedBy,
		dpih.[CREATED_DT] AS CreatedDate,
		dpih.[CREATED_HOST] AS CreatedHost,
		dpih.[MODIFIED_BY] AS ModifiedBy,
		dpih.[MODIFIED_DT] AS ModifiedDate,
		dpih.[MODIFIED_HOST] AS ModifiedHost
	FROM MS_DEPT_PIC_CONFIG_HDR AS dpih
	 LEFT JOIN MS_DEPT_PIC_CONFIG_DTL AS dpid ON dpih.DEPT_CD = dpid.DEPT_CD
	 LEFT JOIN SN_BTMUDIRECTORY_DEPARTMENT dpt ON dpt.shortname = dpih.DEPT_CD
	WHERE dpih.UID = @Id
		AND dpih.DEPT_CD = @Dept
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPARTMENTVENDORLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_DEPARTMENTVENDORLIST]
AS
BEGIN
	SELECT 'ALL' AS SHORTNAME, 'ALL - ALL' LONGNAME
	UNION ALL
	SELECT * FROM(
	SELECT DISTINCT 
		DEPT.SHORTNAME,
		DEPT.SHORTNAME + ' - ' + DEPT.LONGNAME AS LONGNAME
		FROM  SN_BTMUDirectory_Department DEPT
		WHERE DEPT.SHORTNAME <> 'ALL'
		--ORDER BY SHORTNAME
	) A

END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPCOMDIR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPCOMDIR]
	-- add more stored procedure parameters here
	@Department VARCHAR(10) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivID NVARCHAR(5)

	IF charindex('-', @Department) > 0
		SET @Department = RTRIM(LTRIM(SUBSTRING(@Department, 0, CHARINDEX('-', @Department))))

	SELECT @DivID = DivisionID
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Department

	-- creator dgm
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept(@Department)
	-- cpc comdir
	
	UNION
	
	SELECT Username,
		RealName,
		Email
	FROM vw_UserByDepartment
	WHERE 1 = 1
		-- AND DepartmentCode = 'CPC'
		AND RoleShortName = 'COMPLIANCE DIRECTOR'
	-- fcd dgm
	
	UNION
	
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept('FCD')
	-- rmd dgm
	
	UNION
	
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept('RMA')
	-- gad dgm
	
	UNION
	
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept('GAD')
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPCREATORDGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPCREATORDGM]
	-- add more stored procedure parameters here
	@Department VARCHAR(10) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivID NVARCHAR(5)

	IF charindex('-', @Department) > 0
		SET @Department = RTRIM(LTRIM(SUBSTRING(@Department, 0, CHARINDEX('-', @Department))))

	SELECT @DivID = DivisionID
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Department

	-- creator dgm
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept(@Department)
	-- fcd dgm
	
	UNION
	
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept('FCD')
	-- rmd dgm
	
	UNION
	
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept('RMA')
	-- gad dgm
	
	UNION
	
	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept('GAD')
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPDGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPDGM]
	-- add more stored procedure parameters here
	@Department VARCHAR(10) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivID NVARCHAR(5)

	IF charindex('-', @Department) > 0
		SET @Department = RTRIM(LTRIM(SUBSTRING(@Department, 0, CHARINDEX('-', @Department))))

	SELECT @DivID = DivisionID
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Department

	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept(@Department)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPDH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPDH]
	-- add more stored procedure parameters here
	@Department VARCHAR(10) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @DivisionID NCHAR(5) = NULL

	IF charindex('-', @Department) > 0
		SET @Department = RTRIM(LTRIM(SUBSTRING(@Department, 0, CHARINDEX('-', @Department))))

    if @Department = 'RMD'
        set @Department = 'RMA'

	-- Insert statements for procedure here
	SELECT Username,
		Realname,
		Email
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Department
		AND RoleShortName = 'DH'
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPPIC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPPIC]
	-- add more stored procedure parameters here
	@Dept VARCHAR(10)
AS
BEGIN
	-- body of the stored procedure
	IF @Dept = 'RMD'
		SET @Dept = 'RMA'

	SELECT Username,
		RealName,
		Email
	FROM dbo.vw_PICMsSetting X
	WHERE X.DepartmentCode = @Dept
		AND X.Username IS NOT NULL
		-- SELECT cnUser.Username,
		-- 	cnUser.RealName,
		-- 	cnUser.Email
		-- FROM MS_DEPT_PIC_CONFIG_DTL AS picdtl
		-- JOIN dbo.SN_CONE_Cone_User AS cnUser ON (
		-- 		picdtl.MEMBER_CD = cnUser.RealName
		-- 		OR picdtl.MEMBER_CD = cnUser.Username
		-- 		)
		-- WHERE 1 = 1
		-- 	AND picdtl.DEPT_CD = @Dept
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTAPPSTAFF]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DEPTAPPSTAFF]
	-- add more stored procedure parameters here
	@Dept VARCHAR(10)
AS
BEGIN
	-- body of the stored procedure
	SELECT Username,
		RealName,
		Email
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Dept
		AND RoleShortName = 'STAFF'
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DEPTINCHARGECODELIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DEPTINCHARGECODELIST]
	-- add more stored procedure parameters here
AS
BEGIN
	SELECT 'ALL' AS SHORTNAME, 'ALL' LONGNAME
	UNION ALL
	SELECT DISTINCT D.SHORTNAME, D.LONGNAME 
	FROM [SN_BTMUDirectory_Department] AS D 
	JOIN [dbo].[SN_BTMUDirectory_RoleParent] AS RP
	ON D.ID = RP.DEPARTMENTID 
	JOIN [dbo].[SN_BTMUDirectory_User] AS U 
	ON U.ROLEPARENTID = RP.ID
	WHERE U.ISDELETED = '0' AND D.SHORTNAME <> 'ALL'

END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_DOCUMENTCHECKLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_DOCUMENTCHECKLIST]
@DocID INT
AS
BEGIN
	SELECT * FROM TR_VC_CONF_DOC_CHECKLIST
	WHERE IS_ACTIVE = 1
	AND DOC_ID = @DocID
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_DOCUMENTHISTORY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_DOCUMENTHISTORY]
	-- Add the parameters for the stored procedure here
	@DocId INT,
	@HistoryType VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT tdh.UID AS Id,
		tdh.SEQ_NO AS SequenceNumber,
		tdh.HIST_TYPE AS HistoryType,
		tdh.DOC_ID AS DocId,
		tdh.VENDOR_ID AS VendorId,
		tdh.ACTION_BY AS ActionBy,
		tdh.ACTION_DT AS ActionDate,
		tdh.STATUS AS STATUS,
		tdh.COMMENT AS Comment,
		tdh.CREATED_BY AS CreatedBy,
		tdh.CREATED_DT AS CreatedDate,
		tdh.CREATED_HOST AS CreatedHost,
		tdh.MODIFIED_BY AS ModifiedBy,
		tdh.MODIFIED_DT AS ModifiedDate,
		tdh.MODIFIED_HOST AS ModifiedHost,
		tdh.STEP AS Step,
		mp.[DESCS] AS StepDescription
	FROM dbo.TR_DOCUMENT_HISTORY AS tdh
	LEFT JOIN (
		SELECT [GROUP],
			[VALUE],
			[DESCS]
		FROM MS_PARAMETER
		WHERE [GROUP] IN (
				'STEP_REGISTRATION',
				'STEP_PERIODICAL_REVIEW',
				'STEP_LEGAL_REVIEW',
				'STEP_CONTINUING_REVIEW'
				)
		) AS mp ON tdh.STEP = cast(mp.[VALUE] AS INT)
		AND mp.[GROUP] = CASE 
			WHEN tdh.HIST_TYPE = 'REGISTRATION'
				THEN 'STEP_REGISTRATION'
			WHEN tdh.HIST_TYPE = 'PERIODICAL_REVIEW'
				THEN 'STEP_PERIODICAL_REVIEW'
			WHEN tdh.HIST_TYPE = 'CONTINUING_REVIEW'
				THEN 'STEP_CONTINUING_REVIEW'
			WHEN tdh.HIST_TYPE = 'LEGAL_REVIEW'
				THEN 'STEP_LEGAL_REVIEW'
			END
	WHERE 1 = 1
		AND tdh.DOC_ID = @DocId
		AND tdh.HIST_TYPE = @HistoryType
	ORDER BY tdh.SEQ_NO DESC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_DRAFTLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_DRAFTLIST]
	-- add more stored procedure parameters here
	@DraftBy VARCHAR(50),
	@FormString NVARCHAR(100) = '',
	@VendorIDString NVARCHAR(100) = '',
	@VendorNameString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	DECLARE @TotalRow INT = 0;

	SELECT @TotalRow = count(1)
	FROM dbo.vw_DraftList AS dl
	WHERE dl.DraftBy = @DraftBy
		AND (
			dl.Form = @FormString
			OR @FormString = ''
			OR @FormString = 'ALL'
			)
		AND (
			dl.VendorID LIKE '%' + @VendorIDString + '%'
			OR @VendorIDString = ''
			)
		AND (
			dl.VendorName LIKE '%' + @VendorNameString + '%'
			OR @VendorNameString = ''
			)

	SELECT @TotalRow AS TotalRows,
		dl.[UID] AS Id,
		dl.[Form],
		dl.[VendorID],
		dl.[VendorName],
		dl.[Action],
		dl.[DraftBy],
		dl.[DraftDate],
		[URL] = CASE 
			WHEN dl.Form = 'Vendor Registration'
				THEN mp.[VALUE] + 'VendorList/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
			WHEN dl.Form = 'Legal Request'
				THEN mp.[VALUE] + 'LegalRequest/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
			WHEN dl.Form = 'Periodical Review'
				THEN mp.[VALUE] + 'PeriodicalReview/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
			WHEN dl.Form = 'Continuing Vendor'
				THEN mp.[VALUE] + 'ContinuingVendor/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
			WHEN dl.Form = 'Edit Registration'
				THEN mp.[VALUE] + 'VendorList/DraftEdit/' + CAST(dl.[UID] AS VARCHAR(10))
			END
	FROM vw_DraftList AS dl
	LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_PATH'
	WHERE dl.DraftBy = @DraftBy
		AND (
			dl.Form = @FormString
			OR @FormString = ''
			OR @FormString = 'ALL'
			)
		AND (
			VendorID LIKE '%' + @VendorIDString + '%'
			OR @VendorIDString = ''
			)
		AND (
			VendorName LIKE '%' + @VendorNameString + '%'
			OR @VendorNameString = ''
			)
	ORDER BY CASE 
			WHEN @SortOrder = 'Form'
				THEN dl.[Form]
			END ASC,
		CASE 
			WHEN @SortOrder = 'Form_desc'
				THEN dl.[Form]
			END DESC,
		CASE 
			WHEN @SortOrder = 'VendorID'
				THEN dl.[VendorID]
			END ASC,
		CASE 
			WHEN @SortOrder = 'VendorID_desc'
				THEN dl.[VendorID]
			END DESC,
		CASE 
			WHEN @SortOrder = 'VendorName'
				THEN dl.[VendorName]
			END ASC,
		CASE 
			WHEN @SortOrder = 'VendorName_desc'
				THEN dl.[VendorName]
			END DESC,
		CASE 
			WHEN @SortOrder = 'Action'
				THEN dl.[Action]
			END ASC,
		CASE 
			WHEN @SortOrder = 'Action_desc'
				THEN dl.[Action]
			END DESC,
		CASE 
			WHEN @SortOrder = 'DraftBy'
				THEN dl.[DraftBy]
			END ASC,
		CASE 
			WHEN @SortOrder = 'DraftBy_desc'
				THEN dl.[DraftBy]
			END DESC,
		CASE 
			WHEN @SortOrder = 'DraftDate'
				THEN dl.[DraftDate]
			END ASC,
		CASE 
			WHEN @SortOrder = 'DraftDate_desc'
				THEN dl.[DraftDate]
			END DESC,
		CASE 
			WHEN @SortOrder = ''
				THEN DraftDate
			END DESC OFFSET @PageSize * (@PageNo - 1) rows

	FETCH NEXT @PageSize rows ONLY
		---- body of the stored procedure
		--WITH CTE_Result
		--AS (
		--	SELECT *
		--	FROM dbo.vw_DraftList AS dl
		--	WHERE 
		--		1 = 1
		--		AND (dl.DraftBy = @DraftBy)
		--		--AND (
		--		--	@FormString = ''
		--		--	OR @FormString = 'ALL'
		--		--	OR @FormString = dl.Form
		--		--	)
		--		AND (
		--			@VendorIDString = ''
		--			OR (VendorID LIKE '%' + @VendorIDString + '%')
		--			)
		--		AND (
		--			@VendorNameString = ''
		--			OR (VendorName LIKE '%' + @VendorNameString + '%')
		--			)
		--	ORDER BY CASE 
		--			WHEN @SortOrder = 'Form'
		--				THEN dl.[Form]
		--			END ASC,
		--		CASE 
		--			WHEN @SortOrder = 'Form_desc'
		--				THEN dl.[Form]
		--			END DESC,
		--		CASE 
		--			WHEN @SortOrder = 'VendorID'
		--				THEN [VendorID]
		--			END ASC,
		--		CASE 
		--			WHEN @SortOrder = 'VendorID_desc'
		--				THEN [VendorID]
		--			END DESC,
		--		CASE 
		--			WHEN @SortOrder = 'VendorName'
		--				THEN [VendorName]
		--			END ASC,
		--		CASE 
		--			WHEN @SortOrder = 'VendorName_desc'
		--				THEN [VendorName]
		--			END DESC,
		--		CASE 
		--			WHEN @SortOrder = 'Action'
		--				THEN [Action]
		--			END ASC,
		--		CASE 
		--			WHEN @SortOrder = 'Action_desc'
		--				THEN [Action]
		--			END DESC,
		--		CASE 
		--			WHEN @SortOrder = 'DraftBy'
		--				THEN [DraftBy]
		--			END ASC,
		--		CASE 
		--			WHEN @SortOrder = 'DraftBy_desc'
		--				THEN [DraftBy]
		--			END DESC,
		--		CASE 
		--			WHEN @SortOrder = 'DraftDate'
		--				THEN [DraftDate]
		--			END ASC,
		--		CASE 
		--			WHEN @SortOrder = 'DraftDate_desc'
		--				THEN [DraftDate]
		--			END DESC,
		--		CASE 
		--			WHEN @SortOrder = ''
		--				THEN [UID]
		--			END DESC 
		--			OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
		--	),
		--CTE_TotalRows
		--AS (
		--	SELECT count(UID) AS TotalRows
		--	FROM dbo.vw_DraftList AS dl
		--	WHERE 1 = 1
		--		AND (dl.DraftBy = @DraftBy)
		--		AND (
		--			@FormString = ''
		--			OR @FormString = 'ALL'
		--			OR @FormString = dl.Form
		--			)
		--		AND (
		--			@VendorIDString = ''
		--			OR (VendorID LIKE '%' + @VendorIDString + '%')
		--			)
		--		AND (
		--			@VendorNameString = ''
		--			OR (VendorName LIKE '%' + @VendorNameString + '%')
		--			)
		--	)
		--SELECT TotalRows,
		--	dl.[UID] AS Id,
		--	dl.[Form],
		--	dl.[VendorID],
		--	dl.[VendorName],
		--	dl.[Action],
		--	dl.[DraftBy],
		--	dl.[DraftDate],
		--	[URL] = CASE 
		--		WHEN dl.Form = 'Vendor Registration'
		--			THEN mp.[VALUE] + 'VendorList/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
		--		WHEN dl.Form = 'Legal Request'
		--			THEN mp.[VALUE] + 'LegalRequest/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
		--		WHEN dl.Form = 'Periodical Review'
		--			THEN mp.[VALUE] + 'PeriodicalReview/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
		--		WHEN dl.Form = 'Continuing Vendor'
		--			THEN mp.[VALUE] + 'ContinuingVendor/Draft/' + CAST(dl.[UID] AS VARCHAR(10))
		--		END
		--FROM vw_DraftList AS dl
		--, CTE_TotalRows
		--LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_PATH'
		--WHERE 1 = 1
		--	AND dl.DraftBy = @DraftBy
		--	AND EXISTS (
		--		SELECT 1
		--		FROM CTE_Result
		--		WHERE CTE_Result.UID = dl.UID
		--		)
		--OPTION (RECOMPILE)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAIL_SUBJECT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAIL_SUBJECT]
	-- add more stored procedure parameters here
	@GROUP VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	SELECT DESCS
	FROM MS_PARAMETER
	WHERE [GROUP] = @GROUP
		AND [VALUE] = 'SUBJECT'
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILNOTICEITSO]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILNOTICEITSO]
	-- add more stored procedure parameters here
	@INSTANCEID VARCHAR(100)
AS
BEGIN
	SELECT REPLACE(DESCS, '{{ instanceID }}', @INSTANCEID)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NOTICE_ITSO'
		AND [VALUE] = 'CONTENT'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALCOMDIR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALCOMDIR]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@DEPT VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''
	DECLARE @CreatorDGM VARCHAR(2000) = ''
	DECLARE @FCDDGM VARCHAR(2000) = ''
	DECLARE @RMDDGM VARCHAR(2000) = ''
	DECLARE @GADDGM VARCHAR(2000) = ''
	DECLARE @CPCCOMDIR VARCHAR(2000) = ''

	IF charindex('-', @DEPT) > 0
		SET @DEPT = RTRIM(LTRIM(SUBSTRING(@DEPT, 0, CHARINDEX('-', @DEPT))))

	IF @DEPT = 'RMD'
		SET @DEPT = 'RMA'

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApprover = dbo.fnGetCOMPDIRByDept(@DEPT)

	-- CREATOR
	SELECT @CreatorDGM = dbo.fnGetDGMByDept(@DEPT)

	-- CPC
	SELECT @CPCCOMDIR = dbo.fnGetCOMPDIRByDept(@DEPT)

	-- FCD DGM
	SELECT @FCDDGM = dbo.fnGetDGMByDept('FCD')

	-- RMD DGM
	SELECT @RMDDGM = dbo.fnGetDGMByDept('RMA')

	-- GAD DGM
	SELECT @GADDGM = dbo.fnGetDGMByDept('GAD')

	SET @CurrentApprover = stuff(coalesce(', ' + nullif(@CreatorDGM, ''), '') + coalesce(', ' + nullif(@CPCCOMDIR, ''), '') + coalesce(', ' + nullif(@FCDDGM, ''), '') + coalesce(', ' + nullif(@RMDDGM, ''), '') + coalesce(', ' + nullif(@GADDGM, ''), ''), 1, 1, '');

	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + Name
					FROM dbo.splitstring(@CurrentApprover, ',')
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
		@PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT replace(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NEXT_APP'
		AND [VALUE] = 'CONTENT'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALCREATORDGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALCREATORDGM]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@ORIGINATOR VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @CreatorDGM VARCHAR(2000) = ''
	DECLARE @FCDDGM VARCHAR(2000) = ''
	DECLARE @RMDDGM VARCHAR(2000) = ''
	DECLARE @GADDGM VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	-- CREATOR DGM
	SELECT @CreatorDGM = dbo.fnGetDGMByOriginator(@ORIGINATOR)

	-- FCD DGM
	SELECT @FCDDGM = dbo.fnGetDGMByDept('FCD')

	-- RMD DGM
	SELECT @RMDDGM = dbo.fnGetDGMByDept('RMA')

	-- GAD DGM
	SELECT @GADDGM = dbo.fnGetDGMByDept('GAD')

	SET @CurrentApprover = stuff(coalesce(', ' + nullif(@CreatorDGM, ''), '') + coalesce(', ' + nullif(@FCDDGM, ''), '') + coalesce(', ' + nullif(@RMDDGM, ''), '') + coalesce(', ' + nullif(@GADDGM, ''), ''), 1, 1, '');
	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + Name
					FROM dbo.splitstring(@CurrentApprover, ',')
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
		@PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NEXT_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALCVCREATORDGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALCVCREATORDGM]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@ORIGINATOR VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @CreatorDGM VARCHAR(2000) = ''
	DECLARE @GADDGM VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	-- CREATOR DGM
	SELECT @CreatorDGM = dbo.fnGetDGMByOriginator(@ORIGINATOR)

	-- GAD DGM
	SELECT @GADDGM = dbo.fnGetDGMByDept('GAD')

	SET @CurrentApprover = stuff(coalesce(', ' + nullif(@CreatorDGM, ''), '') + coalesce(', ' + nullif(@GADDGM, ''), ''), 1, 1, '');
	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + Name
					FROM dbo.splitstring(@CurrentApprover, ',')
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
		@PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NEXT_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDGM]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@DEPT VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''

	IF charindex('-', @DEPT) > 0
		SET @DEPT = RTRIM(LTRIM(SUBSTRING(@DEPT, 0, CHARINDEX('-', @DEPT))))

	IF @DEPT = 'RMD'
		SET @DEPT = 'RMA'

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApprover = dbo.fnGetDGMByDept(@DEPT)

	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + Name
					FROM dbo.splitstring(@CurrentApprover, ',')
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
		@PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = CASE 
			WHEN @HISTTYPE = 'EDITREGISTRATION'
				THEN 'REGISTRATION'
			ELSE @HISTTYPE
			END
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NEXT_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTDH]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@DEPT VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @WFSTEP INT = 1
	DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''

	IF charindex('-', @DEPT) > 0
		SET @DEPT = RTRIM(LTRIM(SUBSTRING(@DEPT, 0, CHARINDEX('-', @DEPT))))

	IF @DEPT = 'RMD'
		SET @DEPT = 'RMA'

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = @DEPT
						AND RoleShortName = 'DH'
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT @WFSTEP = ISNULL(WFSTEP, 1)
	FROM TR_WORKFLOWPROCESS
	WHERE DOCID = @DOCID
		AND WFTASK = CASE 
			WHEN @HISTTYPE = 'LEGAL_REVIEW'
				THEN 'LEGALREQUEST'
			WHEN @HISTTYPE = 'CONTINUING_REVIEW'
				THEN 'CONTVENDOR'
			WHEN @HISTTYPE = 'REGISTRATION'
				THEN 'REGISTRATION'
			WHEN @HISTTYPE = 'EDITREGISTRATION'
				THEN 'EDITREGISTRATION'
			WHEN @HISTTYPE = 'PERIODICAL_REVIEW'
				THEN 'PERIODICALREVIEW'
			END
		AND ISACTIVE = 1

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
		@PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = CASE 
			WHEN @HISTTYPE = 'EDITREGISTRATION'
				THEN 'REGISTRATION'
			ELSE @HISTTYPE
			END
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ Creator }}', @PreviousApprover), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE (
			(
				[GROUP] = 'EMAIL_NEXT_APP'
				AND @WFSTEP <> 1
				)
			OR (
				[GROUP] = 'EMAIL_CREATOR_APP'
				AND @WFSTEP = 1
				)
			)
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTPIC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALDEPTPIC]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@DEPT VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''

	IF charindex('-', @DEPT) > 0
		SET @DEPT = RTRIM(LTRIM(SUBSTRING(@DEPT, 0, CHARINDEX('-', @DEPT))))

	IF @DEPT = 'RMD'
		SET @DEPT = 'RMA'

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + Realname
					FROM dbo.vw_PICMsSetting
					WHERE DepartmentCode = @DEPT
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
		@PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NEXT_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALGM]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	-- GM
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + RealName
					FROM dbo.[SN_BTMUDirectory_User]
					WHERE RoleParentID = (
							SELECT ID
							FROM dbo.SN_BTMUDirectory_RoleParent
							WHERE RoleID = 70
							)
						AND IsDeleted = 0
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
		@PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = CASE 
			WHEN @HISTTYPE = 'EDITREGISTRATION'
				THEN 'REGISTRATION'
			ELSE @HISTTYPE
			END
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NEXT_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALLEGALSTAFF]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALLEGALSTAFF]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
    DECLARE @PrevApprovedDate DATETIME
	DECLARE @CharPrevApprovedDate VARCHAR(50) = ''

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = 'LGL'
						AND RoleShortName = 'STAFF'
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT @DecisionTimeLimit = ISNULL(APV_RMD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, ''),
        @PrevApprovedDate = CREATED_DT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

    SET @CharPrevApprovedDate = convert(VARCHAR, @PrevApprovedDate, 107) + ' at ' + convert(VARCHAR, @PrevApprovedDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ PreviousApprovedDate }}', @CharPrevApprovedDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_NEXT_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALUSRDH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALUSRDH]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@ORIGINATOR VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @ORIGINATOR

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'jakarta01\', '')

	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = (
							SELECT DepartmentCode
							FROM vw_UserByDepartment
							WHERE Username = @StrUsername
							)
						AND RoleShortName = 'DH'
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ Creator }}', @PreviousApprover)

	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_CREATOR_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTAPPROVALUSRUHDH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTAPPROVALUSRUHDH]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@ORIGINATOR VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @ORIGINATOR

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'jakarta01\', '')

	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @CurrentApproverUH VARCHAR(2000) = ''
	DECLARE @CurrentApproverDH VARCHAR(2000) = ''

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApproverUH = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = (
							SELECT DepartmentCode
							FROM vw_UserByDepartment
							WHERE Username = @StrUsername
							)
						AND RoleShortName = 'UH'
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	SELECT @CurrentApproverDH = ISNULL(STUFF((
					SELECT DISTINCT ',<br/>' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = (
							SELECT DepartmentCode
							FROM vw_UserByDepartment
							WHERE Username = @StrUsername
							)
						AND RoleShortName = 'DH'
					FOR XML PATH(''),
						root('MyString'),
						type
					).value('/MyString[1]', 'varchar(max)'), 1, 6, ''), '')

	IF (@CurrentApproverDH != '')
	BEGIN
		SET @CurrentApprover = @CurrentApproverDH
	END

	IF (@CurrentApproverUH != '')
	BEGIN
		IF @CurrentApprover != ''
		BEGIN
			SET @CurrentApprover = @CurrentApprover + ', ' + @CurrentApproverUH
		END
		ELSE
		BEGIN
			SET @CurrentApprover = @CurrentApproverUH
		END
	END

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = isnull(COMMENT, '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ Creator }}', @PreviousApprover)


	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_CREATOR_APP'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTCOMPLETE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTCOMPLETE]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @ApproveDate DATETIME
	DECLARE @UrlLink NVARCHAR(2000)
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @CharAssignmentDate VARCHAR(50) = ''

	IF @HISTTYPE = 'EDITREGISTRATION'
	BEGIN
		SELECT @DOCID = REFID
		FROM TR_WORKFLOWPROCESS
		WHERE WFTASK = 'EDITREGISTRATION'
			AND DOCID = @DOCID
	END

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = CASE 
			WHEN @HISTTYPE = 'EDITREGISTRATION'
				THEN 'REGISTRATION'
			ELSE @HISTTYPE
			END
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SELECT @UrlLink = [VALUE]
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_PATH'

	IF @HISTTYPE = 'REGISTRATION'
		OR @HISTTYPE = 'EDITREGISTRATION'
	BEGIN
		SET @UrlLink = @UrlLink + 'VendorList/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END
	ELSE IF @HISTTYPE = 'PERIODICAL_REVIEW'
	BEGIN
		SET @UrlLink = @UrlLink + 'PeriodicalReview/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END
	ELSE IF @HISTTYPE = 'CONTINUING_REVIEW'
	BEGIN
		SET @UrlLink = @UrlLink + 'ContinuingVendor/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END
	ELSE IF @HISTTYPE = 'LEGAL_REVIEW'
	BEGIN
		SET @UrlLink = @UrlLink + 'LegalRequest/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END

	--{{ RejectDate }}
	--{{ UrlLink }}
	SET @ApproveDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @ApproveDate, 107) + ' at ' + convert(VARCHAR, @ApproveDate, 24)

	SELECT REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ ApproveDate }}', @CharAssignmentDate), '{{ UrlLink }}', @UrlLink), '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQUEST_APPROVE'
		AND [VALUE] = 'CONTENT'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTREJECT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTREJECT]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @RejectDate DATETIME
	DECLARE @UrlLink NVARCHAR(2000)
	DECLARE @CharAssignmentDate VARCHAR(50) = ''

	SELECT @UrlLink = [VALUE]
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_PATH'

	IF @HISTTYPE = 'REGISTRATION'
		OR @HISTTYPE = 'EDITREGISTRATION'
	BEGIN
		SET @UrlLink = @UrlLink + 'VendorList/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END
	ELSE IF @HISTTYPE = 'PERIODICAL_REVIEW'
	BEGIN
		SET @UrlLink = @UrlLink + 'PeriodicalReview/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END
	ELSE IF @HISTTYPE = 'CONTINUING_REVIEW'
	BEGIN
		SET @UrlLink = @UrlLink + 'ContinuingVendor/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END
	ELSE IF @HISTTYPE = 'LEGAL_REVIEW'
	BEGIN
		SET @UrlLink = @UrlLink + 'LegalRequest/Detail/' + CONVERT(VARCHAR(10), @DOCID)
	END

	--{{ RejectDate }}
	--{{ UrlLink }}
	SET @RejectDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @RejectDate, 107) + ' at ' + convert(VARCHAR, @RejectDate, 24)

	SELECT REPLACE(REPLACE(DESCS, '{{ RejectDate }}', @CharAssignmentDate), '{{ UrlLink }}', @UrlLink)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQUEST_REJECT'
		AND [VALUE] = 'CONTENT'
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_EMAILREQUESTREVISE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_EMAILREQUESTREVISE]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @ReviseDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''

	--{{ ReviseDate }}
	--{{ PreviousApprover }}
	--{{ Comment }}
	SET @ReviseDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @ReviseDate, 107) + ' at ' + convert(VARCHAR, @ReviseDate, 24)

    IF @HISTTYPE = 'EDITREGISTRATION'
	BEGIN
		SELECT @DOCID = REFID
		FROM TR_WORKFLOWPROCESS
		WHERE WFTASK = 'EDITREGISTRATION'
			AND DOCID = @DOCID
	END

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = ISNULL(COMMENT, '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = CASE 
			WHEN @HISTTYPE = 'EDITREGISTRATION'
				THEN 'REGISTRATION'
			ELSE @HISTTYPE
			END
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SELECT REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ ReviseDate }}', @CharAssignmentDate)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_REQUEST_REVISE'
		AND [VALUE] = 'CONTENT'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_EXAMCOMPARISON]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_EXAMCOMPARISON]
@DocID INT
AS
BEGIN
	SELECT * FROM TR_VC_EXAM_COMPARISON
	WHERE IS_ACTIVE = 1
	AND DOC_ID = @DocID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_GETUSEREMAILCC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_GETUSEREMAILCC]
	@EmailTo VARCHAR(10),
	@DocId INT,
	@Department VARCHAR(10) = NULL,
	@Originator NVARCHAR(50),
	@WfTask VARCHAR(30),
	@Step INT,
	@WfName NVARCHAR(50) = null
AS
BEGIN
	DECLARE @GADPIC VARCHAR(1000)
	declare @thisWfName VARCHAR(50)
	select @thisWfName = case when @WfTask = 'REGISTRATION' then 'WF_VC_REGISTRATION' 
							when @WfTask = 'PERIODICALREVIEW' then 'WF_VC_PERIODREVIEW' 
							when @WfTask = 'LEGALREQUEST' then 'WF_VC_LGLREQUEST' 
							when @WfTask = 'CONTVENDOR' then 'WF_VC_CONTVENDOR' 
							when @WfTask = 'EDITREGISTRATION' then 'WF_EDT_VC_REGISTRATION' 
						end
						
	if(@Step = 0)
	BEGIN
		if(@EmailTo = 'CC')
		begin
			if(@WfTask = 'LEGALREQUEST')
			BEGIN
				select SNU.Email as EmailCcUser
				from DBO.vw_UserByDepartment SNU where SNU.RoleShortName in ('UH','DH') and SNU.DepartmentCode = @Department
				UNION ALL
				select SNU.Email as EmailCcUser
				from DBO.vw_UserByDepartment SNU where SNU.RoleShortName in ('STAFF','OUTSOURCE') and SNU.DepartmentCode = 'LGL'
			END
			ELSE
			BEGIN
				select SNU.Email as EmailCcUser
				from DBO.vw_UserByDepartment SNU where SNU.RoleShortName in ('UH','DH') and SNU.DepartmentCode = @Department
				UNION ALL
				select SNU.Email as EmailCcUser
				from DBO.vw_UserByDepartment SNU where SNU.RoleShortName in ('DH') and SNU.DepartmentCode = 'GAD'
			END
		end
		else if(@EmailTo = 'TO')
		begin
			if(@WfTask = 'LEGALREQUEST')
			BEGIN
				select SNU.Email as EmailToUser from DBO.TR_WORKFLOWPROCESS WF
					JOIN DBO.TR_WORKFLOWPROCESS_DETAIL WFD on WF.UID = WFD.WFID
					JOIN DBO.vw_UserByDepartment SNU ON WFD.ACTION_BY = SNU.Username
				where WF.DOCID=@DocId AND WF.WFTASK = @WfTask AND WF.WFSTEP = @Step AND WF.ISACTIVE = 1
			END
			ELSE
			BEGIN
				SELECT @GADPIC = GAD_PIC
				FROM MS_SETTING
			
				select SNU.Email as EmailToUser from DBO.TR_WORKFLOWPROCESS WF
					JOIN DBO.TR_WORKFLOWPROCESS_DETAIL WFD on WF.UID = WFD.WFID
					JOIN DBO.vw_UserByDepartment SNU ON WFD.ACTION_BY = SNU.Username
				where WF.DOCID=@DocId AND WF.WFSTEP = @Step AND WF.ISACTIVE = 1
				UNION ALL
				SELECT snu.Email AS EmailToUser
				FROM dbo.splitstring(@gadpic, ',') AS gad
					JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
			END
		end
	END
	else --if(@Step = 1)
	BEGIN
		if(@EmailTo = 'CC')
		BEGIN
			if(@WfTask = 'LEGALREQUEST')
				exec UDPS_MAILCCLEGAL
		END
		else if(@EmailTo = 'TO')
		BEGIN
			select SNU.Email, WFD.SN from DBO.TR_WORKFLOWPROCESS WF
				JOIN DBO.TR_WORKFLOWPROCESS_DETAIL WFD on WF.UID = WFD.WFID
				JOIN DBO.vw_UserByDepartment SNU ON WFD.ACTION_BY = SNU.Username
			where WF.DOCID=@DocId AND WF.WFTASK = @WfTask AND WF.WFSTEP = @Step AND WF.ISACTIVE = 1
		END
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_HASACTIVEVENDORACTIVITY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_HASACTIVEVENDORACTIVITY]
@DOC_ID INT,
@FORM_TYPE VARCHAR(50)
AS
BEGIN
	DECLARE @HasActive BIT = 0
	
	SELECT @HasActive = 1
	FROM TR_VENDOR_CONTROL_ACTIVITY
	WHERE IS_ACTIVE = 1 AND IS_DRAFT = 0
	AND APPROVAL_STS = 'Awaiting Approval'
	AND DOC_ID = @DOC_ID
	AND FORM_TYPE = @FORM_TYPE

	SELECT @HasActive;
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_ISUSERDGMBYDEPARTMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_ISUSERDGMBYDEPARTMENT]
@UserID varchar(255),
@DepartmentCode varchar(255)
AS
BEGIN
	DECLARE @Result bit = 0;

	SELECT @Result = 1
	FROM dbo.fnGetTabDGMByDept(@DepartmentCode)
	WHERE Username = @UserID

	SELECT @Result;
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_LEGALREQUESTDATA]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_LEGALREQUESTDATA] @Id INT = 0
AS
BEGIN
	SELECT 
		VCA.[UID] AS Id,
		VCA.[DOC_ID] AS DocId,
		VCA.[FORM_TYPE] AS FormType,
		VCA.[CREATOR] AS Creator,
		VCA.[CREATOR_DEPT] AS CreatorDepartment,
		VCA.[CREATOR_SECTION] AS CreatorSection,
		VCA.[CREATOR_ROLE] AS CreatorRole,
		VCA.[REQUEST_DT] AS RequestDate,
		VCA.[VENDOR_ID] AS VendorId,
		VC.[VENDOR_NAME] AS VendorName,
		VCA.[DOC_DATE_FORM6] AS DocDate,
		VCA.[LGL_REQ_DEPT_CD] AS LegalReqDeptCode,
		VCA.[LGL_SUBJECT] AS LegalSubject,
		VCA.[LGL_FACTS] AS LegalFacts,
		VCA.[LGL_SUPPORTING_DOC] AS LegalSupportingDoc,
		VCA.[LGL_REQUEST] AS LegalRequest,
		VCA.[LGL_DEADLINE] AS LegalDeadline,
		VCA.[APPROVAL_STS] AS ApprovalStatus,
		VCA.[IS_ACTIVE] AS IsActive,
		VCA.[CREATED_BY] AS CreatedBy,
		VCA.[CREATED_DT] AS CreatedDate,
		VCA.[CREATED_HOST] AS CreatedHost,
		VCA.[MODIFIED_BY] AS ModifiedBy,
		VCA.[MODIFIED_DT] AS ModifiedDate,
		VCA.[MODIFIED_HOST] AS ModifiedHost,
		VCA.[IS_DRAFT] AS IsDraft
	FROM [TR_VENDOR_CONTROL_ACTIVITY] VCA
	LEFT JOIN [TR_VENDOR_CONTROL] VC ON
	VCA.DOC_ID = VC.UID
	WHERE VCA.IS_ACTIVE = 1
		AND VCA.[FORM_TYPE] = 'LEGAL_REQUEST'
		AND (VCA.[UID] = @Id OR @Id = 0)

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_LEGALREQUESTDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_LEGALREQUESTDETAIL]
@Id INT = 0
AS
BEGIN
	SELECT 
		 VCA.[UID] AS Id
		,VCA.[DOC_ID] AS DocId
		,VCA.[FORM_TYPE] AS FormType
		,VCA.[CREATOR] AS Creator
		,VCA.[CREATOR_DEPT] AS CreatorDepartment
		,VCA.[CREATOR_SECTION] AS CreatorSection
		,VCA.[CREATOR_ROLE] AS CreatorRole
		,VCA.[REQUEST_DT] AS RequestDate
		,VCA.[VENDOR_ID] AS VendorId
		,VCA.[DOC_DATE_FORM6] AS DocDate
		,VCA.[LGL_REQ_DEPT_CD] AS LegalReqDeptCode
		,VCA.[LGL_REQ_DEPT_CD] + ISNULL(' - '+dept.LongName,'') AS LegalReqDeptFullName
		,VCA.[LGL_SUBJECT] AS LegalSubject
		,VCA.[LGL_FACTS] AS LegalFacts
		,VCA.[LGL_SUPPORTING_DOC] AS LegalSupportingDoc
		,VCA.[LGL_REQUEST] AS LegalRequest
		,VCA.[LGL_DEADLINE] AS LegalDeadline
		,VCA.[APPROVAL_STS] As ApprovalStatus
		,VCA.[IS_ACTIVE] AS IsActive
		,VCA.[CREATED_BY] AS CreatedBy
		,VCA.[CREATED_DT] AS CreatedDate
		,VCA.[CREATED_HOST] AS CreatedHost
		,VCA.[MODIFIED_BY] AS ModifiedBy
		,VCA.[MODIFIED_DT] AS ModifiedDate
		,VCA.[MODIFIED_HOST] AS ModifiedHost
		,VCA.[IS_DRAFT] AS IsDraft
	FROM [TR_VENDOR_CONTROL_ACTIVITY] VCA
	LEFT JOIN [dbo].[SN_BTMUDirectory_department] dept
	ON VCA.[LGL_REQ_DEPT_CD] = dept.ShortName
	WHERE 
	--VCA.[IS_ACTIVE] = 1
    VCA.[FORM_TYPE] = 'LEGAL_REQUEST'
	AND (VCA.[UID] = @Id OR @Id = 0)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILCCCOMPLETE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILCCCOMPLETE]
	-- add more stored procedure parameters here
	@Department VARCHAR(10) = NULL,
	@ORIGINATOR NVARCHAR(50),
	@STEP INT,
	@WFNAME NVARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50),
		@CreatorDepartment VARCHAR(10)

	IF charindex('-', @Department) > 0
		SET @Department = RTRIM(LTRIM(SUBSTRING(@Department, 0, CHARINDEX('-', @Department))))
	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT @CreatorDepartment = DepartmentCode
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

    -- WF_VC_REGISTRATION
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION

    -- WF_VC_EDT_REGISTRATION
    SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	-- WF_VC_LGLREQUEST
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'LGL'
		AND RoleShortName IN (
			'STAFF',
			'OUTSOURCE'
			)
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	-- WF_VC_CONTVENDOR
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	-- WF_VC_PERIODREVIEW
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILCCLEGAL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILCCLEGAL]
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedur
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'LGL'
		AND RoleShortName = 'OUTSOURCE'
	
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILCCREJECT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILCCREJECT]
	-- add more stored procedure parameters here
	@Department VARCHAR(10) = NULL,
	@ORIGINATOR NVARCHAR(50),
	@STEP INT,
	@WFNAME NVARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50),
		@CreatorDepartment VARCHAR(10)

	IF charindex('-', @Department) > 0
		SET @Department = RTRIM(LTRIM(SUBSTRING(@Department, 0, CHARINDEX('-', @Department))))
	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT @CreatorDepartment = DepartmentCode
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

	-- WF_VC_REGISTRATION
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION

    -- WF_VC_EDT_REGISTRATION
    SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'

    UNION
	
	-- WF_VC_LGLREQUEST
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'LGL'
		AND RoleShortName IN (
			'STAFF',
			'OUTSOURCE'
			)
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	-- WF_VC_CONTVENDOR
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	-- WF_VC_PERIODREVIEW
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILCCREVISE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILCCREVISE]
	-- add more stored procedure parameters here
	@Department VARCHAR(10) = NULL,
	@ORIGINATOR NVARCHAR(50),
	@STEP INT,
	@WFNAME NVARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50),
		@CreatorDepartment VARCHAR(10)

	IF charindex('-', @Department) > 0
		SET @Department = RTRIM(LTRIM(SUBSTRING(@Department, 0, CHARINDEX('-', @Department))))
	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT @CreatorDepartment = DepartmentCode
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

    -- WF_VC_REGISTRATION
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_REGISTRATION'

    UNION

    -- WF_VC_EDT_REGISTRATION
    SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	-- WF_VC_LGLREQUEST
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'LGL'
		AND RoleShortName IN (
			'STAFF',
			'OUTSOURCE'
			)
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	-- WF_VC_CONTVENDOR
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	-- WF_VC_PERIODREVIEW
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @CreatorDepartment
		AND RoleShortName IN (
			'DH',
			'UH'
			)
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
	
	UNION
	
	SELECT Email AS EmailCcUser
	FROM vw_UserByDepartment
	WHERE DepartmentCode = 'GAD'
		AND RoleShortName = 'DH'
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILTOCOMPLETE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILTOCOMPLETE]
	-- add more stored procedure parameters here
	--@Department NCHAR(10) = NULL,
	@ORIGINATOR NVARCHAR(50),
	@STEP INT,
	@WFNAME NVARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	--DECLARE @DEPARTMENT NVARCHAR(50)
	--SET @Department = 'GAD'
	DECLARE @StrUsername NVARCHAR(50)
	DECLARE @GADPIC VARCHAR(1000)

	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT @GADPIC = GAD_PIC
	FROM MS_SETTING

	-- WF_VC_REGISTRATION
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	-- WF_VC_EDT_REGISTRATION
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	-- WF_VC_LGLREQUEST
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	-- WF_VC_CONTVENDOR
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	-- WF_VC_PERIODREVIEW
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_PERIODICALREVIEW'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILTOITSO]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
create PROCEDURE [dbo].[UDPS_MAILTOITSO]
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedure
	--DECLARE @DEPARTMENT NVARCHAR(50)
	--SET @Department = 'GAD'
	SELECT [VALUE] AS EmailAdmin
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_ADMIN'
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILTOREJECT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILTOREJECT]
	-- add more stored procedure parameters here
	--@Department NCHAR(10) = NULL,
	@ORIGINATOR NVARCHAR(50),
	@STEP INT,
	@WFNAME NVARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50)
	DECLARE @GADPIC VARCHAR(1000)

	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT @GADPIC = GAD_PIC
	FROM MS_SETTING

	-- WF_VC_REGISTRATION
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	-- WF_VC_EDT_REGISTRATION
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION

    SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_EDT_REGISTRATION'

    UNION
	
	-- WF_VC_LGLREQUEST
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	-- SELECT snu.Email AS EmailToUser
	-- FROM dbo.splitstring(@gadpic, ',') AS gad
	-- JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	-- WHERE @WFNAME = 'WF_VC_LEGALREQUEST'
	
	-- UNION
	
	-- WF_VC_CONTVENDOR
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	-- WF_VC_PERIODREVIEW
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_PERIODICALREVIEW'
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_MAILTOREVISE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_MAILTOREVISE]
	-- add more stored procedure parameters here
	--@Department NCHAR(10) = NULL,
	@ORIGINATOR NVARCHAR(50),
	@STEP INT,
	@WFNAME NVARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	--DECLARE @DEPARTMENT NVARCHAR(50)
	--SET @Department = 'GAD'
	DECLARE @StrUsername NVARCHAR(50)
	DECLARE @GADPIC VARCHAR(1000)

	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	SELECT @GADPIC = GAD_PIC
	FROM MS_SETTING

	-- WF_VC_REGISTRATION
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_REGISTRATION'
	
	UNION
	
	-- WF_VC_EDT_REGISTRATION
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_EDT_REGISTRATION'
	
	UNION
	
	-- WF_VC_LGLREQUEST
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_LEGALREQUEST'
	
	UNION
	
	-- WF_VC_CONTVENDOR
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_CONTVENDOR'
	
	UNION
	
	-- WF_VC_PERIODREVIEW
	SELECT Email AS EmailToUser
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername
		AND @WFNAME = 'WF_VC_PERIODICALREVIEW'
	
	UNION
	
	SELECT snu.Email AS EmailToUser
	FROM dbo.splitstring(@gadpic, ',') AS gad
	JOIN dbo.vw_UserByDepartment AS snu ON gad.Name = snu.RealName
	WHERE @WFNAME = 'WF_VC_PERIODICALREVIEW'
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERBYGROUP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_PARAMETERBYGROUP]

	-- Add the parameters for the stored procedure here

	@GROUP NVARCHAR(100)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	SELECT mp.UID AS Id

		,mp.[GROUP] AS [Group]

		,mp.VALUE AS [Value]

		,mp.DESCS AS [Description]

		,mp.SEQ AS [Sequence]

		,mp.CREATED_BY AS CreatedBy

		,mp.CREATED_DT AS CreatedDate

		,mp.CREATED_HOST AS CreatedHost

		,mp.MODIFIED_BY AS ModifedBy

		,mp.MODIFIED_DT AS ModifiedDate

		,mp.MODIFIED_HOST AS ModifiedHost

	FROM dbo.MS_PARAMETER AS mp

	WHERE mp.[GROUP] = @GROUP

	ORDER BY MP.[SEQ] ASC

END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERBYGROUPVALUE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_PARAMETERBYGROUPVALUE]

	-- Add the parameters for the stored procedure here

	@GROUP NVARCHAR(100),

	@VALUE NVARCHAR(3000)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	SELECT mp.UID AS Id

		,mp.[GROUP] AS [Group]

		,mp.VALUE AS [Value]

		,mp.DESCS AS [Description]

		,mp.SEQ AS [Sequence]

		,mp.CREATED_BY AS CreatedBy

		,mp.CREATED_DT AS CreatedDate

		,mp.CREATED_HOST AS CreatedHost

		,mp.MODIFIED_BY AS ModifedBy

		,mp.MODIFIED_DT AS ModifiedDate

		,mp.MODIFIED_HOST AS ModifiedHost

	FROM dbo.MS_PARAMETER AS mp

	WHERE mp.[GROUP] = @GROUP

	AND mp.VALUE = @VALUE

	ORDER BY MP.[SEQ] ASC

END






GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_PARAMETERDETAIL]

	-- Add the parameters for the stored procedure here

	@Id INT

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	SELECT mp.UID AS Id

		,mp.[GROUP] AS [Group]

		,mp.VALUE AS [Value]

		,mp.DESCS AS [Description]

		,mp.SEQ AS [Sequence]

		,mp.CREATED_BY AS CreatedBy

		,mp.CREATED_DT AS CreatedDate

		,mp.CREATED_HOST AS CreatedHost

		,mp.MODIFIED_BY AS ModifedBy

		,mp.MODIFIED_DT AS ModifiedDate

		,mp.MODIFIED_HOST AS ModifiedHost

	FROM dbo.MS_PARAMETER AS mp

	WHERE mp.UID = @Id

END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERS]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_PARAMETERS]

	-- Add the parameters for the stored procedure here

	@CONDITION VARCHAR(20),

	@VALUE VARCHAR(500)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	SELECT mp.UID AS Id

		,mp.[GROUP] AS [Group]

		,mp.VALUE AS [Value]

		,mp.DESCS AS [Description]

		,mp.SEQ AS [Sequence]

		,mp.CREATED_BY AS CreatedBy

		,mp.CREATED_DT AS CreatedDate

		,mp.CREATED_HOST AS CreatedHost

		,mp.MODIFIED_BY AS ModifedBy

		,mp.MODIFIED_DT AS ModifiedDate

		,mp.MODIFIED_HOST AS ModifiedHost

	FROM dbo.MS_PARAMETER AS mp

END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_PARAMETERS_PARAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_PARAMETERS_PARAM]

	-- Add the parameters for the stored procedure here

	@SearchByString NVARCHAR(100) = '',

	@KeywordString NVARCHAR(100) = '',

	@PageNo INT = 1,

	@PageSize INT = 10,

	@SortOrder NVARCHAR(100) = ''

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	WITH CTE_Result

	AS (

		SELECT *

		FROM MS_PARAMETER

		WHERE 1 = 1

			AND (

				@KeywordString = ''

				OR (

					CASE @SearchByString

						WHEN 'Group'

							THEN [GROUP]

						WHEN 'Value'

							THEN [VALUE]

						WHEN 'Description'

							THEN [DESCS]

						END

					) LIKE '%' + @KeywordString + '%'

				)

		ORDER BY CASE 

				WHEN @SortOrder = 'Group'

					THEN [GROUP]

				END ASC,

			CASE 

				WHEN @SortOrder = 'Group_desc'

					THEN [GROUP]

				END DESC,

			CASE 

				WHEN @SortOrder = 'Value'

					THEN [VALUE]

				END ASC,

			CASE 

				WHEN @SortOrder = 'Value_desc'

					THEN [VALUE]

				END DESC,

			CASE 

				WHEN @SortOrder = 'Description'

					THEN [DESCS]

				END ASC,

			CASE 

				WHEN @SortOrder = 'Description_desc'

					THEN [DESCS]

				END DESC,

			CASE 

				WHEN @SortOrder = 'Sequence'

					THEN [SEQ]

				END ASC,

			CASE 

				WHEN @SortOrder = 'Sequence_desc'

					THEN [SEQ]

				END DESC,

			CASE 

				WHEN @SortOrder = ''

					THEN [UID]

				END ASC OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY

		),

	CTE_TotalRows

	AS (

		SELECT count(UID) AS TotalRows

		FROM MS_PARAMETER

		WHERE 1 = 1

			AND (

				@KeywordString = ''

				OR (

					CASE @SearchByString

						WHEN 'Group'

							THEN [GROUP]

						WHEN 'Value'

							THEN [VALUE]

						WHEN 'Description'

							THEN [DESCS]

						END

					) LIKE '%' + @KeywordString + '%'

				)

		)

	SELECT TotalRows,

		mp.UID AS Id,

		mp.[GROUP] AS [Group],

		mp.VALUE AS [Value],

		mp.DESCS AS [Description],

		mp.SEQ AS [Sequence],

		mp.CREATED_BY AS CreatedBy,

		mp.CREATED_DT AS CreatedDate,

		mp.CREATED_HOST AS CreatedHost,

		mp.MODIFIED_BY AS ModifedBy,

		mp.MODIFIED_DT AS ModifiedDate,

		mp.MODIFIED_HOST AS ModifiedHost

	FROM dbo.MS_PARAMETER AS mp,

		CTE_TotalRows

	WHERE EXISTS (

			SELECT 1

			FROM CTE_Result

			WHERE CTE_Result.UID = mp.UID

			)

	OPTION (RECOMPILE)

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_PERIDODICALREVIEWCREATIONINFO]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_PERIDODICALREVIEWCREATIONINFO]
	-- add more stored procedure parameters here
	@VendorID BIGINT
AS
BEGIN
	SELECT vc.VENDOR_ID AS VendorId,
		vc.VENDOR_NAME AS vendorName,
		vc.REG_PROVIDED_SERVICE AS TypeOfService,
		-- vc.RISK_CATEGORY AS RiskCategory,
		mp.DESCS AS RiskCategory,
		NextPeriodicalReviewDate = CASE 
			WHEN VCA.DOC_ID IS NULL
				AND VCA2.DOC_ID IS NULL
				THEN CASE 
						WHEN VC.[RISK_CATEGORY] = 3
							THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 2
							THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 1
							THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
						END -- first periodical review
			WHEN VCA.DOC_ID IS NOT NULL
				AND VCA2.DOC_ID IS NULL
				THEN CASE 
						WHEN VC.[RISK_CATEGORY] = 3
							THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 2
							THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 1
							THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
						END -- next periodical review without continuing
			WHEN VCA.DOC_ID IS NULL
				AND VCA2.DOC_ID IS NOT NULL
				THEN CASE 
						WHEN VC.[RISK_CATEGORY] = 3
							THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 2
							THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 1
							THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
						END -- continuing without periodical
			WHEN VCA.DOC_ID IS NOT NULL
				AND VCA2.DOC_ID IS NOT NULL
				THEN CASE 
						WHEN TDH2.CREATED_DT > TDH3.CREATED_DT
							THEN CASE 
									WHEN VC.[RISK_CATEGORY] = 3
										THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 2
										THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 1
										THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
									END -- next periodical review
						WHEN TDH2.CREATED_DT < TDH3.CREATED_DT
							THEN CASE 
									WHEN VC.[RISK_CATEGORY] = 3
										THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 2
										THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 1
										THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
									END -- next periodical review after continuing vendor
						END
			END,
		vca.DOC_ID,
		vca2.DOC_ID
	FROM TR_VENDOR_CONTROL AS vc
	LEFT JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'MAP_RISK_CATEGORY'
		AND mp.[VALUE] = vc.RISK_CATEGORY
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA ON VC.UID = VCA.DOC_ID
		AND VCA.FORM_TYPE = 'PERIODICAL_REVIEW'
		AND VCA.[UID] = (
			SELECT MAX(VCA2.[UID])
			FROM DBO.TR_VENDOR_CONTROL_ACTIVITY VCA2
			WHERE VC.[UID] = VCA2.DOC_ID
				AND VCA2.FORM_TYPE = 'PERIODICAL_REVIEW'
				AND VCA2.APPROVAL_STS = 'Approval Completed'
			)
		AND VCA.IS_ACTIVE = 1
	-- GET LAST CONTINUING VENDOR
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA2 ON VC.UID = VCA2.DOC_ID
		AND VCA2.FORM_TYPE = 'CONTINUING_VENDOR'
		AND VCA2.[UID] = (
			SELECT MAX([UID])
			FROM DBO.TR_VENDOR_CONTROL_ACTIVITY
			WHERE VC.[UID] = DOC_ID
				AND FORM_TYPE = 'CONTINUING_VENDOR'
				AND APPROVAL_STS = 'Approval Completed'
			)
		AND VCA2.IS_ACTIVE = 1
	-- GET LAST APPROVAL REGISTRATION
	LEFT JOIN TR_DOCUMENT_HISTORY TDH ON TDH.DOC_ID = VC.UID
		AND TDH.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VC.UID
				AND HIST_TYPE = 'REGISTRATION'
				AND [STATUS] = 'Approve'
				AND ISNULL(STEP, 0) NOT LIKE '2%'
			)
	-- GET LAST APPROVAL PERIODICAL REVIEW
	LEFT JOIN TR_DOCUMENT_HISTORY TDH2 ON TDH2.DOC_ID = VCA.UID
		AND TDH2.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VCA.UID
				AND HIST_TYPE = 'PERIODICAL_REVIEW'
				AND [STATUS] = 'Approve'
			)
	-- GET LAST APPROVAL CONTINUING VENDOR
	LEFT JOIN TR_DOCUMENT_HISTORY TDH3 ON TDH3.DOC_ID = VCA2.UID
		AND TDH3.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VCA2.UID
				AND HIST_TYPE = 'CONTINUING_REVIEW'
				AND [STATUS] = 'Approve'
			)
	WHERE vc.[UID] = @VendorID
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_PERIODICALREVIEW_AML]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UDPS_PERIODICALREVIEW_AML]

	-- Add the parameters for the stored procedure here

	@DocId int 

AS

BEGIN

select 
UID as Id
,DOC_ID as DocId
,SEQ_NO as SeqNo
,NAME as Name
,TITLE as Title
,IS_ACTIVE
,CREATED_BY
,CREATED_DT
,CREATED_HOST
,MODIFIED_BY
,MODIFIED_DT
,MODIFIED_HOST
 from [TR_VC_ACTIVITY_REV_AML]

where DOC_ID = @DocId



END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_PERIODICALREVIEW_EXAMINED]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_PERIODICALREVIEW_EXAMINED]
	-- Add the parameters for the stored procedure here
	@DocId int 
AS
BEGIN
select 
UID as Id
,DOC_ID as DocId
,SEQ_NO as SeqNo
,POINT as Point
,RESULT as Result
,COMMENT as Comment
,IS_ACTIVE
,CREATED_BY
,CREATED_DT
,CREATED_HOST
,MODIFIED_BY
,MODIFIED_DT
,MODIFIED_HOST
 from [TR_VC_ACTIVITY_REV_EXAMINED]
where DOC_ID = @DocId

END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_PERIODICALREVIEWDATA]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_PERIODICALREVIEWDATA] @Id INT = 0
AS
BEGIN
	SELECT VCA.[UID],
		VCA.[DOC_ID] AS DocId,
		VCA.[FORM_TYPE] AS FormType,
		VCA.[CREATOR] AS Creator,
		VCA.[CREATOR_DEPT] AS CreatorDepartment,
		VCA.[CREATOR_SECTION] AS CreatorSection,
		VCA.[CREATOR_ROLE] AS CreatorRole,
		VCA.[REQUEST_DT] AS RequestDate,
		VCA.[VENDOR_ID] AS VendorId,
		VCA.[DOC_DATE_FORM4] AS DocDate,
		VCA.[REV_REVIEW_DT] AS NextPeriodicalReviewDate,
		VCA.[REV_GAD_RECOMMENDATION] AS GADRecommendation,
		VCA.[REV_RMD_COMMENT] AS RMDComment,
		VCA.[APPROVAL_STS] AS ApprovalStatus,
		VCA.[IS_ACTIVE] AS IsActive,
		VCA.[CREATED_BY] AS CreatedBy,
		VCA.[CREATED_DT] AS CreatedDate,
		VCA.[CREATED_HOST] AS CreatedHost,
		VCA.[MODIFIED_BY] AS ModifiedBy,
		VCA.[MODIFIED_DT] AS ModifiedDate,
		VCA.[MODIFIED_HOST] AS ModifiedHost,
		VCA.[IS_DRAFT] AS IsDraft,
		-- vc.VENDOR_ID AS VendorId,
		vc.VENDOR_NAME AS vendorName,
		vc.REG_PROVIDED_SERVICE AS TypeOfService,
		vc.RISK_CATEGORY AS RiskCategory,
        vc.RISK_SPECIAL_RELATED AS SpecialRelated
	FROM [TR_VENDOR_CONTROL_ACTIVITY] AS VCA
	LEFT JOIN TR_VENDOR_CONTROL AS vc ON vca.DOC_ID = vc.UID
	WHERE VCA.IS_ACTIVE = 1
		AND VCA.[FORM_TYPE] = 'PERIODICAL_REVIEW'
		AND (
			VCA.[UID] = @Id
			OR @Id = 0
			)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_PERIODICALREVIEWDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_PERIODICALREVIEWDETAIL]
@Id INT = 0
AS
BEGIN
	SELECT 
		 VCA.[UID] AS Id
		,VCA.[DOC_ID] AS DocId
		,VCA.[FORM_TYPE] AS FormType
		,VCA.[CREATOR] AS Creator
		,VCA.[CREATOR_DEPT] AS CreatorDepartment
		,VCA.CREATOR_DEPT + ISNULL('-'+dept.longname,'') AS CreatorDepartmentLongName
		,VCA.[CREATOR_SECTION] AS CreatorSection
		,VCA.[CREATOR_ROLE] AS CreatorRole
		,VCA.[REQUEST_DT] AS RequestDate
		,VCA.[VENDOR_ID] AS VendorId
		,VCA.[DOC_DATE_FORM4] AS DocDate
		,VCA.[REV_REVIEW_DT] AS NextPeriodicalReviewDate
		,VCA.[REV_GAD_RECOMMENDATION] AS GADRecommendation
		,VCA.[REV_RMD_COMMENT] AS RMDComment
		,VCA.[APPROVAL_STS] As ApprovalStatus
		,VCA.[IS_ACTIVE] AS IsActive
		,VCA.[CREATED_BY] AS CreatedBy
		,VCA.[CREATED_DT] AS CreatedDate
		,VCA.[CREATED_HOST] AS CreatedHost
		,VCA.[MODIFIED_BY] AS ModifiedBy
		,VCA.[MODIFIED_DT] AS ModifiedDate
		,VCA.[MODIFIED_HOST] AS ModifiedHost
		,VCA.[IS_DRAFT] AS IsDraft
		,vc.VENDOR_ID AS VendorId
		,vc.VENDOR_NAME AS vendorName
		,vc.REG_PROVIDED_SERVICE AS TypeOfService
		,P.DESCS AS RiskCategory

	FROM [TR_VENDOR_CONTROL_ACTIVITY] AS VCA
	LEFT JOIN [TR_VENDOR_CONTROL] as vc on vca.DOC_ID = vc.UID
	LEFT JOIN SN_BTMUDIRECTORY_DEPARTMENT AS dept ON VCA.CREATOR_DEPT = dept.shortname
	LEFT JOIN MS_PARAMETER P ON P.[GROUP] = 'MAP_RISK_CATEGORY' AND VC.RISK_CATEGORY = P.[VALUE]
	WHERE VCA.IS_ACTIVE = 1 AND VCA.[FORM_TYPE] = 'PERIODICAL_REVIEW' 
	AND (VCA.[UID] = @Id OR @Id = 0)
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_PICLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_PICLIST]
	-- add more stored procedure parameters here
	@Department varchar(50)
AS
BEGIN
	-- body of the stored procedure
	--DECLARE @Department VARCHAR(50)
	--SET @Department = 'ACC'

	--SELECT U.USERNAME,U.ROLEPARENTID FROM SN_BTMUDirectory_DEPARTMENT AS D  LEFT JOIN SN_BTMUDirectory_user AS U
	--	ON D.ID=U.ROLEPARENTID
	--		WHERE D.shortname = @Department

			SELECT REALNAME
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Department
END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_REGISTRATIONEXPORT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REGISTRATIONEXPORT]	

AS
BEGIN
		

SELECT vc.UID,
		vc.VENDOR_ID,
		vc.CONF_CONTRACTOR_NM,
		vc.VENDOR_NAME,
		vc.CONF_CATEGORY,
		vc.CREATOR_DEPT,
		vc.CREATOR,
		vc.APPROVAL_STS
	FROM TR_VENDOR_CONTROL vc
		WHERE 1 = 1
		AND vc.IS_ACTIVE = 1
		AND vc.STATUS_FLOW != 'DRAFT'		

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_REGISTRATIONVENDOR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_REGISTRATIONVENDOR]
@DocID INT
AS
BEGIN
	SELECT * FROM TR_VENDOR_CONTROL
	WHERE IS_ACTIVE = 1 
	AND UID = @DocID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDER_LATESTDATE_BYDEPT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211119>
-- Description:	<Get Latest Reminder Date by department>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_REMINDER_LATESTDATE_BYDEPT]
	@Department VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		LR.DEPT_CD AS DepartmentCode,
		LR.REV_LATEST_SENT_REMINDER_FWD AS LatestSentRevReminderFwd,
		LR.REV_LATEST_SENT_REMINDER_BWD AS LatestSentRevReminderBwd,
		LR.CONT_LATEST_SENT_REMINDER_FWD AS LatestSentContReminderFwd,
		LR.CONT_LATEST_SENT_REMINDER_BWD AS LatestSentContReminderBwd
	FROM DBO.TR_LATEST_REMINDER LR
	WHERE LR.DEPT_CD = @Department
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDER_LATESTDATE_BYDOCID]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_REMINDER_LATESTDATE_BYDOCID]
	@DocId BIGINT
	,@FormType VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		LRV.DOC_ID AS DocId,
		LRV.LATEST_SENT_REMINDER_APP AS LatestDateSentAppr,
		LRV.LATEST_SENT_REMINDER_REV AS LatestDateSentRevise
	FROM DBO.TR_LATEST_REMINDER_VENDOR LRV
	WHERE LRV.DOC_ID = @DocId AND FORM_TYPE = @FormType
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERAPPROVAL_TASK]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REMINDERAPPROVAL_TASK]
	@FormType VARCHAR(200)
	, @ApprovalStatus VARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = '' --@ControllerName Varchar(255) = 'VendorList'
	DECLARE @ReviseDate DATE, @Path VARCHAR(100), @GadPic VARCHAR(1000), @RmdStaff VARCHAR(1000), @CpcPic VARCHAR(1000), @FcdPic VARCHAR(1000), @LglPic VARCHAR(1000)
	SELECT @GadPic = GAD_PIC, @RmdStaff = RMD_STAFF, @CpcPic = CPC_STAFF, @FcdPic = FCD_PIC, @LglPic = LGL_PIC FROM DBO.MS_SETTING 
	SELECT @Path = VALUE FROM DBO.MS_PARAMETER WHERE [GROUP]='APP_PATH'

	--SET @ControllerName = CASE @FormType 
	--WHEN 'LEGAL_REVIEW' THEN 'LegalRequest'
	--WHEN 'CONTINUING_REVIEW' THEN 'ContinuingReview'
	--WHEN 'PERIODICAL_REVIEW' THEN 'PeriodicalReview'
	--ELSE 'VendorList' END 

	IF(@ApprovalStatus='REVISE')
	BEGIN
		SELECT 
			VC.UID AS UID
			, VC.VENDOR_ID AS VendorId
			, VC.VENDOR_NAME AS VendorName
			, CONCAT(@Path, 'Revise?id' + cast(VC.UID as varchar(5)) + '&sn=' + WFD.SN)  AS DocLink --belum tau link nya
			, '' AS UserCreator --leave it empty
			, VC.CREATOR_DEPT AS DeptCreator
			--, SUBSTRING(VC.CREATOR_DEPT,1,CHARINDEX(' ',VC.CREATOR_DEPT)-1) AS DeptCreator
			, GETDATE() AS ReviseDate
			, (SELECT TOP 1 ACTION_BY	FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VC.[UID]) AS PreviousApprover
			, (SELECT TOP 1 COMMENT FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VC.[UID]) AS Comment
			, null AS AssignmentDate --leave it empty
			, '' AS CurrentApprover --leave it empty
			, '' AS Creator --leave it empty
			, CAST(0 as BIT) AS IsNextApprover --1 next approver, 0 creator --or leave it empty
			, CONCAT((select username from SN_BTMUDirectory_User where realname in (
				SELECT Name
				FROM dbo.splitstring(@GadPic, ',')
				)), ';',(select username from SN_BTMUDirectory_User where realname=VC.CREATOR)) AS SentTo --GAD PIC, CREATOR			
			, STUFF((SELECT ';' + Username 
					FROM vw_UserByDepartment VU 
					WHERE (VC.CREATOR_DEPT = VU.DepartmentCode AND VU.RoleShortName IN ('DH','UH'))
						OR (VU.DepartmentCode='GAD' AND VU.RoleShortName='DH') 
					FOR XML PATH ('')), 1, 1, '' ) AS CcTo 
		FROM DBO.TR_WORKFLOWPROCESS WF 
			JOIN DBO.TR_VENDOR_CONTROL VC ON VC.UID = WF.DOCID AND WF.WFTASK = 'REGISTRATION' AND WF.WFSTEP = 0
			JOIN DBO.TR_WORKFLOWPROCESS_DETAIL WFD ON WF.UID = WFD.WFID
		WHERE VC.IS_ACTIVE = 1 
			AND VC.APPROVAL_STS = 'REVISE' --registration-revise OR other formtype-revise
		UNION ALL
		SELECT 
			VC.UID AS UID
			, VC.VENDOR_ID AS VendorId
			, VC.VENDOR_NAME AS VendorName
			, CONCAT(@Path, 'Revise?id' + cast(VC.UID as varchar(5)) + '&sn=' + WFD.SN)  AS DocLink --belum tau link nya
			, '' AS UserCreator --leave it empty
			, VC.CREATOR_DEPT AS DeptCreator
			--, SUBSTRING(VC.CREATOR_DEPT,1,CHARINDEX(' ',VC.CREATOR_DEPT)-1) AS DeptCreator
			, GETDATE() AS ReviseDate
			, (SELECT TOP 1 ACTION_BY	FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VC.[UID]) AS PreviousApprover
			, (SELECT TOP 1 COMMENT FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VC.[UID]) AS Comment
			, null AS AssignmentDate --leave it empty
			, '' AS CurrentApprover --leave it empty
			, '' AS Creator --leave it empty
			, CAST(0 as BIT) AS IsNextApprover --1 next approver, 0 creator --or leave it empty
			, CASE 
				WHEN @FormType = 'LEGAL_REVIEW' THEN (select username from SN_BTMUDirectory_User where realname=VC.CREATOR) --Creator
				ELSE CONCAT((select username from SN_BTMUDirectory_User where realname in (
				SELECT Name
				FROM dbo.splitstring(@GadPic, ',')
				)), ';',(select username from SN_BTMUDirectory_User where realname=VC.CREATOR))  --GAD PIC, CREATOR
			END AS SentTo
			, CASE 
				WHEN @FormType = 'LEGAL_REVIEW' THEN STUFF((SELECT ';' + Username 
					FROM vw_UserByDepartment VU 
					WHERE (VC.CREATOR_DEPT = VU.DepartmentCode AND VU.RoleShortName IN ('DH','UH'))
						OR (VU.DepartmentCode='LGL' AND VU.RoleShortName IN ('STAFF','OUTSOURCE')) 
					FOR XML PATH ('')), 1, 1, '' )--DH & UH Creator JKT LGL Staff & Outsource
				ELSE STUFF((SELECT ';' + Username 
					FROM vw_UserByDepartment VU 
					WHERE (VC.CREATOR_DEPT = VU.DepartmentCode AND VU.RoleShortName IN ('DH','UH'))
						OR (VU.DepartmentCode='GAD' AND VU.RoleShortName='DH') 
					FOR XML PATH ('')), 1, 1, '' ) --DH & UH Creator, DH GAD
			END AS CcTo 
		FROM DBO.TR_VENDOR_CONTROL VC
			JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA ON VCA.DOC_ID = VC.[UID]
			JOIN DBO.TR_WORKFLOWPROCESS WF ON VCA.UID = WF.DOCID AND WF.WFTASK = 'REGISTRATION' AND WF.WFSTEP = 0
			JOIN DBO.TR_WORKFLOWPROCESS_DETAIL WFD ON WF.UID = WFD.WFID
		WHERE VC.IS_ACTIVE = 1 AND VCA.IS_DRAFT = 0
			AND VCA.APPROVAL_STS = 'REVISE' --registration-revise OR other formtype-revise
	END
	ELSE IF(@ApprovalStatus = 'APPROVAL')
	BEGIN
		IF(@FormType='REGISTRATION')
		BEGIN
			SELECT 
				VC.UID AS UID
				, VC.VENDOR_ID AS VendorId
				, VC.VENDOR_NAME AS VendorName
				, CONCAT(@Path,'Approval?id' + cast(VC.UID as varchar(5)) + '&sn=' + WFD.SN) AS DocLink --belum tau link nya
				, '' AS UserCreator
				, VC.CREATOR_DEPT AS DeptCreator
				, null AS ReviseDate
				, (SELECT TOP 1 ACTION_BY FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VC.UID ORDER BY SEQ_NO DESC) AS PreviousApprover
				, (SELECT TOP 1 COMMENT FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VC.UID ORDER BY SEQ_NO DESC) AS Comment
				, GETDATE() AS AssignmentDate
				, '' AS CurrentApprover
				, VC.CREATOR AS Creator
				, CAST(CASE WHEN WF.WFSTEP = '0' THEN 0 ELSE 1 END AS BIT) AS IsNextApprover --1 next approver, 0 creator
				, CASE WHEN WF.WFSTEP = '1' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE VU.DepartmentCode = VC.CREATOR_DEPT AND VU.RoleShortName = 'DH'
						FOR XML PATH ('')), 1, 1, '' ) 
					WHEN WF.WFSTEP = '2' THEN @GadPic --GAD PIC
					WHEN WF.WFSTEP = '3' THEN @FcdPic --FCD PIC
					WHEN WF.WFSTEP = '4' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE VU.DepartmentCode = 'FCD' AND VU.RoleShortName = 'DH'
						FOR XML PATH ('')), 1, 1, '' ) --FCD DH
					WHEN WF.WFSTEP = '5' THEN @CpcPic
					WHEN WF.WFSTEP = '6' THEN @RmdStaff
					WHEN WF.WFSTEP = '7' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE VU.DepartmentCode = 'RMD' AND VU.RoleShortName = 'DH'
						FOR XML PATH ('')), 1, 1, '' ) --RMD DH
					WHEN WF.WFSTEP = '8' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE VU.DepartmentCode = 'GAD' AND VU.RoleShortName = 'DH'
						FOR XML PATH ('')), 1, 1, '' ) --RMD DH
					WHEN WF.WFSTEP = '9' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE (VU.DepartmentCode = VC.CREATOR_DEPT AND VU.RoleShortName = 'DGM')
							OR (VU.DepartmentCode = 'CPC' AND VU.RoleShortName = 'COMDIR')
							OR (VU.DepartmentCode = 'FCD' AND VU.RoleShortName = 'DGM')
							OR (VU.DepartmentCode = 'RMD' AND VU.RoleShortName = 'DGM')
							OR (VU.DepartmentCode = 'GAD' AND VU.RoleShortName = 'DGM')
						FOR XML PATH ('')), 1, 1, '' ) 
					WHEN WF.WFSTEP = '10' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE (VU.DepartmentCode = VC.CREATOR_DEPT AND VU.RoleShortName = 'DGM')
							OR (VU.DepartmentCode = 'FCD' AND VU.RoleShortName = 'DGM')
							OR (VU.DepartmentCode = 'RMD' AND VU.RoleShortName = 'DGM')
							OR (VU.DepartmentCode = 'GAD' AND VU.RoleShortName = 'DGM')
						FOR XML PATH ('')), 1, 1, '' ) 
					WHEN WF.WFSTEP = '11' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE VU.RoleShortName = 'GM'
						FOR XML PATH ('')), 1, 1, '' ) --GM
				END AS SentTo
				, NULL AS CcTo  --selain lgl ga di cc
				, 1 AS DecisionTimeLimit
				, @FormType AS FormType
			FROM DBO.TR_VENDOR_CONTROL VC
				JOIN DBO.TR_WORKFLOWPROCESS WF ON WF.VENDORID = VC.VENDOR_ID AND WF.DOCID = VC.[UID] AND WF.WFTASK = @FormType AND WF.ISACTIVE = 1 AND WF.WFTASK = 'REGISTRATION' AND WF.WFTASKNAME='APPROVE'
				JOIN DBO.TR_WORKFLOWPROCESS_DETAIL WFD ON WF.UID = WFD.WFID
				JOIN DBO.MS_PARAMETER MP ON MP.[GROUP] = 'STEP_' + @FormType AND MP.VALUE = WF.WFSTEP
			WHERE VC.IS_ACTIVE = 1
				AND VC.APPROVAL_STS = 'AWAITING APPROVAL'
		END
		ELSE 
		BEGIN
			SELECT 
				VC.UID AS UID
				, VC.VENDOR_ID AS VendorId
				, VC.VENDOR_NAME AS VendorName
				, CONCAT(@Path,'Approval?id' + cast(VC.UID as varchar(5)) + '&sn=' + WFD.SN) AS DocLink --belum tau link nya
				, '' AS UserCreator
				, VC.CREATOR_DEPT AS DeptCreator
				, null AS ReviseDate
				, (SELECT TOP 1 ACTION_BY FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VCA.UID ORDER BY SEQ_NO DESC) AS PreviousApprover
				, (SELECT TOP 1 COMMENT FROM TR_DOCUMENT_HISTORY WHERE HIST_TYPE = @FormType AND DOC_ID = VCA.UID ORDER BY SEQ_NO DESC) AS Comment
				, GETDATE() AS AssignmentDate
				, '' AS CurrentApprover
				, VC.CREATOR AS Creator
				, CAST(CASE WHEN WF.WFSTEP = '0' THEN 0 ELSE 1 END AS BIT) AS IsNextApprover --1 next approver, 0 creator
				, CASE 
					WHEN @FormType = 'EDIT_REGISTRATION' THEN STUFF((SELECT ';' + Username 
							FROM vw_UserByDepartment VU 
							WHERE VU.DepartmentCode = 'GAD' AND VU.RoleShortName IN ('DH','STAFF')
							FOR XML PATH ('')), 1, 1, '' ) 
					ELSE (
						CASE WHEN WF.WFSTEP = '1' THEN STUFF((SELECT ';' + Username 
							FROM vw_UserByDepartment VU 
							WHERE VU.DepartmentCode = VC.CREATOR_DEPT AND VU.RoleShortName = 'DH'
							FOR XML PATH ('')), 1, 1, '' ) 
							WHEN WF.WFSTEP = '2' THEN @GadPic --GAD PIC
							WHEN WF.WFSTEP = '3' THEN @FcdPic --FCD PIC
							WHEN WF.WFSTEP = '4' THEN STUFF((SELECT ';' + Username 
								FROM vw_UserByDepartment VU 
								WHERE VU.DepartmentCode = 'FCD' AND VU.RoleShortName = 'DH'
								FOR XML PATH ('')), 1, 1, '' ) --FCD DH
							WHEN WF.WFSTEP = '5' THEN @CpcPic
							WHEN WF.WFSTEP = '6' THEN @RmdStaff
							WHEN WF.WFSTEP = '7' THEN STUFF((SELECT ';' + Username 
								FROM vw_UserByDepartment VU 
								WHERE VU.DepartmentCode = 'RMD' AND VU.RoleShortName = 'DH'
								FOR XML PATH ('')), 1, 1, '' ) --RMD DH
							WHEN WF.WFSTEP = '8' THEN STUFF((SELECT ';' + Username 
								FROM vw_UserByDepartment VU 
								WHERE VU.DepartmentCode = 'GAD' AND VU.RoleShortName = 'DH'
								FOR XML PATH ('')), 1, 1, '' ) --GAD DH
							WHEN WF.WFSTEP = '9' THEN STUFF((SELECT ';' + Username 
								FROM vw_UserByDepartment VU 
								WHERE (VU.DepartmentCode = VC.CREATOR_DEPT AND VU.RoleShortName = 'DGM')
									OR (VU.DepartmentCode = 'CPC' AND VU.RoleShortName = 'COMDIR')
									OR (VU.DepartmentCode = 'FCD' AND VU.RoleShortName = 'DGM')
									OR (VU.DepartmentCode = 'RMD' AND VU.RoleShortName = 'DGM')
									OR (VU.DepartmentCode = 'GAD' AND VU.RoleShortName = 'DGM')
								FOR XML PATH ('')), 1, 1, '' ) 
							WHEN WF.WFSTEP = '10' THEN STUFF((SELECT ';' + Username 
								FROM vw_UserByDepartment VU 
								WHERE (VU.DepartmentCode = VC.CREATOR_DEPT AND VU.RoleShortName = 'DGM')
									OR (VU.DepartmentCode = 'FCD' AND VU.RoleShortName = 'DGM')
									OR (VU.DepartmentCode = 'RMD' AND VU.RoleShortName = 'DGM')
									OR (VU.DepartmentCode = 'GAD' AND VU.RoleShortName = 'DGM')
								FOR XML PATH ('')), 1, 1, '' ) 
							WHEN WF.WFSTEP = '11' THEN STUFF((SELECT ';' + Username 
								FROM vw_UserByDepartment VU 
								WHERE VU.RoleShortName = 'GM'
								FOR XML PATH ('')), 1, 1, '' ) --GM
						END )
				END AS SentTo
				, CASE 
					WHEN @FormType = 'LEGAL_REVIEW' THEN STUFF((SELECT ';' + Username 
						FROM vw_UserByDepartment VU 
						WHERE VU.DepartmentCode='LGL' AND VU.RoleShortName = 'OUTSOURCE'
						FOR XML PATH ('')), 1, 1, '' )--DH & UH Creator JKT LGL Staff & Outsource
					ELSE null
				END AS CcTo  --selain lgl ga di cc
				, 1 AS DecisionTimeLimit
				, VCA.FORM_TYPE AS FormType
			FROM DBO.TR_VENDOR_CONTROL VC
				JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA ON VCA.DOC_ID = VC.[UID] AND isnull(VCA.IS_DRAFT,0) = 0
				JOIN DBO.TR_WORKFLOWPROCESS WF ON WF.VENDORID = VC.VENDOR_ID AND WF.DOCID = VCA.[UID] 
				--AND CASE @FormType 
				--WHEN 'LEGAL_REVIEW' THEN 'LEGALREQUEST'
				--WHEN 'CONTINUING_REVIEW' THEN 'CONTVENDOR'
				--WHEN 'PERIODICAL_REVIEW' THEN 'PERIODICALREVIEW'
				--ELSE @FormType
				--END = WF.WFTASK  
				AND WF.ISACTIVE = 1 AND WF.WFTASK = 'REGISTRATION' 
				AND WF.WFTASKNAME='APPROVE'
				JOIN DBO.TR_WORKFLOWPROCESS_DETAIL WFD ON WF.UID = WFD.WFID
				JOIN DBO.MS_PARAMETER MP ON MP.[GROUP] = 'STEP_' + @FormType AND MP.VALUE = WF.WFSTEP
			WHERE VC.IS_ACTIVE = 1
				AND VC.APPROVAL_STS = 'APPROVAL COMPLETED'
				AND VCA.APPROVAL_STS = 'AWAITING APPROVAL'
		END
	END
	
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERCONTBWD_TASK]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Author:		<Isti>
-- Create date: <20211124>
-- Description:	<Remider Continuing Vendor, Task List of any active vendor with upcoming expired date (send backward), CURRENT DATE >= CONTROL AGREEMENT END DATE - START FORM && CURRENT DATE <= CONTROL AGREEMENT END DATE>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_REMINDERCONTBWD_TASK] @CurrentDate DATETIME = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @StartFromDate INT

	SELECT @StartFromDate = CONT_BWD_START_FROM
	FROM DBO.MS_SETTING

	DECLARE @UrlLink NVARCHAR(2000)

	SELECT @UrlLink = ISNULL([VALUE], '')
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_PATH'

	DECLARE @RecurringDay INT

	SELECT @RecurringDay = CONT_BWD_RECUR_DAY
	FROM MS_SETTING

	SELECT VC.UID AS VendorControlId,
		VC.VENDOR_ID AS VendorId,
		VC.VENDOR_NAME AS VendorName,
		CAST(VC.AGREE_END_DATE AS DATE) AS StartDate,
		'<a href="' + @UrlLink + 'VendorList/Detail/' + CAST(VC.UID AS VARCHAR(10)) + '">link</a>' AS DocLink,
		VC.CREATOR_DEPT AS CreatorDepartment
	FROM DBO.TR_VENDOR_CONTROL VC
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA ON VC.UID = VCA.DOC_ID
		AND VCA.FORM_TYPE = 'CONTINUING_VENDOR'
		AND VCA.APPROVAL_STS = 'Approval Completed'
	WHERE VC.IS_ACTIVE = 1
		AND VC.APPROVAL_STS = 'Approval Completed'
		AND (
			ISNULL(@CurrentDate, GETDATE()) >= DATEADD(DAY, - @StartFromDate, VC.AGREE_END_DATE)
			AND ISNULL(@CurrentDate, GETDATE()) <= VC.AGREE_END_DATE
			)
		AND (
			VCA.DOC_ID IS NULL
			OR VCA.CREATED_DT > VC.AGREE_END_DATE
			)
		AND (isnull(datediff(d, DATEADD(d, @RecurringDay, CONVERT(VARCHAR(10), VC.CONT_LATEST_SENT_REMINDER_BWD, 120)), @CurrentDate), 0) >= 0)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERCONTFWD_TASK]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Author:		<Isti>
-- Create date: <20211124>
-- Description:	<Remider Continuing Vendor, Task List of any active vendor, currently expired (send forward), CURRENT DATE > CONTROL AGREEMENT END DATE>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_REMINDERCONTFWD_TASK] @CurrentDate DATETIME = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @UrlLink NVARCHAR(2000)

	SELECT @UrlLink = ISNULL([VALUE], '')
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_PATH'

	DECLARE @RecurringDay INT

	SELECT @RecurringDay = CONT_FWD_RECUR_DAY
	FROM MS_SETTING

	SELECT DISTINCT VC.UID AS VendorControlId,
		VC.VENDOR_ID AS VendorId,
		VC.VENDOR_NAME AS VendorName,
		CAST(VC.AGREE_END_DATE AS DATE) AS StartDate,
		'<a href="' + @UrlLink + 'VendorList/Detail/' + CAST(VC.UID AS VARCHAR(10)) + '">link</a>' AS DocLink,
		VC.CREATOR_DEPT AS CreatorDepartment
	FROM DBO.TR_VENDOR_CONTROL VC
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA ON VC.UID = VCA.DOC_ID
		AND VCA.FORM_TYPE = 'CONTINUING_VENDOR'
		AND VCA.APPROVAL_STS = 'Approval Completed'
	WHERE VC.IS_ACTIVE = 1
		AND VC.APPROVAL_STS = 'Approval Completed'
		AND DATEDIFF(D, DATEADD(D, 1, VC.AGREE_END_DATE), ISNULL(@CurrentDate, GETDATE())) >= 0 --end date + plus 1
		AND (
			VCA.DOC_ID IS NULL
			OR VCA.CREATED_DT > VC.AGREE_END_DATE
			) --belum di create atau create terakhir sebelum end date
		AND (isnull(datediff(d, DATEADD(d, @RecurringDay, CONVERT(VARCHAR(10), VC.CONT_LATEST_SENT_REMINDER_FWD, 120)), @CurrentDate), 0) >= 0)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDEREMAILAPPROVALBODY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REMINDEREMAILAPPROVALBODY] @DocId BIGINT,
	@HistType VARCHAR(100),
	@Originator VARCHAR(100),
	@Department VARCHAR(10) = NULL,
	@Step INT,
	@WfTask VARCHAR(50),
	@SN VARCHAR(50)
AS
BEGIN
	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @ReviseDate DATE
	DECLARE @DocLink VARCHAR(500),
		@Path VARCHAR(50)
	DECLARE @AssignmentDate DATETIME
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''
	DECLARE @DecisionTimeLimit INT = 0
	DECLARE @ControllerName VARCHAR(20) = ''

	SELECT @Path = VALUE
	FROM DBO.MS_PARAMETER
	WHERE [GROUP] = 'APP_PATH'

	SELECT TOP 1 @PreviousApprover = ISNULL(ACTION_BY, ''),
		@Comment = isnull(COMMENT, '')
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = CASE 
			WHEN @HistType = 'LEGAL_REQUEST'
				THEN 'LEGAL_REVIEW'
			WHEN @HistType = 'PERIODICAL_REVIEW'
				THEN 'PERIODICAL_REVIEW'
			WHEN @HistType = 'CONTINUING_VENDOR'
				THEN 'CONTINUING_REVIEW'
			WHEN @HistType = 'REGISTRATION'
				THEN 'REGISTRATION'
			END
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	SELECT @DecisionTimeLimit = APV_RMD_RECUR_DAY
	FROM MS_SETTING

	SET @ReviseDate = GETDATE()
	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	IF (@HistType = 'LEGAL_REQUEST')
		SET @ControllerName = 'LegalRequest'
	ELSE IF (@HistType = 'PERIODICAL_REVIEW')
		SET @ControllerName = 'PeriodicalReview'
	ELSE IF (@HistType = 'CONTINUING_VENDOR')
		SET @ControllerName = 'ContinuingVendor'
	ELSE
		SET @ControllerName = 'VendorList'

	IF (@Step = 0)
	BEGIN
		SELECT @DocLink = CONCAT (
				@Path,
				@ControllerName + '/Revise?id=' + cast(@DOCID AS VARCHAR(5)) + '&sn=' + WFD.SN
				)
		FROM TR_WORKFLOWPROCESS WF
		JOIN TR_WORKFLOWPROCESS_DETAIL WFD ON WF.UID = WFD.WFID
		WHERE WF.DOCID = @DOCID
			AND WF.ISACTIVE = 1
			AND WF.WFSTEP = 0

		SELECT REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ ReviseDate }}', @ReviseDate), '{{ DocLink }}', @DocLink)
		FROM MS_PARAMETER
		WHERE [GROUP] = 'EMAIL_REQUEST_REVISE_SCH'
			AND [VALUE] = 'CONTENT'
	END
	ELSE IF (@Step = 1)
	BEGIN
		SELECT @DocLink = CONCAT (
				@Path,
				@ControllerName + '/Approval?id=' + cast(@DOCID AS VARCHAR(5)) + '&sn=' + @SN + '&step=' + CAST(@Step AS VARCHAR(5))
				)

		-- SELECT @CurrentApprover = ISNULL(STUFF((
		-- 				SELECT DISTINCT ',' + MEMBER_CD
		-- 				FROM MS_DEPT_PIC_CONFIG_DTL
		-- 				WHERE DEPT_CD = 'GAD'
		-- 				FOR XML PATH('')
		-- 				), 1, 1, ''), '')
		SELECT @CurrentApprover = ISNULL(STUFF((
						SELECT DISTINCT ',' + vu.RealName
						FROM TR_WORKFLOWPROCESS_DETAIL AS twd
						JOIN TR_WORKFLOWPROCESS AS tw ON twd.WFID = tw.UID
						JOIN vw_UserByDepartment AS vu ON twd.ACTION_BY = vu.Username
						WHERE tw.PROCID = (
								SELECT TOP 1 [Name]
								FROM dbo.splitstring(@SN, '_')
								)
						FOR XML PATH('')
						), 1, 1, ''), '')

		SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DocLink }}', ISNULL(@DocLink, '')), '{{ Creator }}', ISNULL(@Originator, ''))
		FROM MS_PARAMETER
		WHERE [GROUP] = 'EMAIL_CREATOR_APP_SCH'
			AND [VALUE] = 'CONTENT'
	END
	ELSE
	BEGIN
		DECLARE @CreatorDGM VARCHAR(2000) = ''
		DECLARE @FCDDGM VARCHAR(2000) = ''
		DECLARE @RMDDGM VARCHAR(2000) = ''
		DECLARE @GADDGM VARCHAR(2000) = ''

		SELECT @DocLink = CONCAT (
				@Path,
				@ControllerName + '/Approval?id=' + cast(@DOCID AS VARCHAR(5)) + '&sn=' + @SN + '&step=' + CAST(@Step AS VARCHAR(5))
				)

		-- CREATOR DGM
		SELECT @CreatorDGM = dbo.fnGetDGMByOriginator(@ORIGINATOR)

		-- FCD DGM
		SELECT @FCDDGM = dbo.fnGetDGMByDept('FCD')

		-- RMD DGM
		SELECT @RMDDGM = dbo.fnGetDGMByDept('RMD')

		-- GAD DGM
		SELECT @GADDGM = dbo.fnGetDGMByDept('GAD')

		-- SET @CurrentApprover = stuff(coalesce(', ' + nullif(@CreatorDGM, ''), '') + coalesce(', ' + nullif(@FCDDGM, ''), '') + coalesce(', ' + nullif(@RMDDGM, ''), '') + coalesce(', ' + nullif(@GADDGM, ''), ''), 1, 1, '');
		-- RY 20220207, currentapprover ambil dari TR_WORKFLOWPROCESS_DETAIL
		SELECT @CurrentApprover = ISNULL(STUFF((
						SELECT DISTINCT ',' + vu.RealName
						FROM TR_WORKFLOWPROCESS_DETAIL AS twd
						JOIN TR_WORKFLOWPROCESS AS tw ON twd.WFID = tw.UID
						JOIN vw_UserByDepartment AS vu ON twd.ACTION_BY = vu.Username
						WHERE tw.PROCID = (
								SELECT TOP 1 [Name]
								FROM dbo.splitstring(@SN, '_')
								)
						FOR XML PATH('')
						), 1, 1, ''), '')

		SELECT REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover), '{{ DecisionTimeLimit }}', CONVERT(VARCHAR(10), @DecisionTimeLimit)), '{{ DocLink }}', @DocLink)
		FROM MS_PARAMETER
		WHERE [GROUP] = 'EMAIL_NEXT_APP_SCH'
			AND [VALUE] = 'CONTENT'
	END
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERLISTAPPROVAL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_REMINDERLISTAPPROVAL]
AS
	select VC.UID as DocId, VC.VENDOR_ID as VendorId, 'REGISTRATION' as FormType, VC.CREATOR as Originator, VC.CREATOR_DEPT as DepartmentOriginator
		, WF.WFSTEP as WfStep, case WF.WFSTEP when 0 then 'Revise' when 1 then 'Creator' else 'Approve' end as WfStatus
		, WF.WFTASK as WfTask
	from TR_WORKFLOWPROCESS WF
		JOIN TR_VENDOR_CONTROL VC on WF.DOCID = VC.UID
	where WF.ISACTIVE = 1 AND WF.WFTASK = 'REGISTRATION' --AND VC.STATUS_FLOW = 'SUBMIT'
	union all
	select VCA.UID as DocId, VCA.VENDOR_ID as VendorId, VCA.FORM_TYPE as FormType, VCA.CREATOR as Originator, VCA.CREATOR_DEPT as DepartmentOriginator
		, WF.WFSTEP as WfStep, case WF.WFSTEP when 0 then 'Revise' when 1 then 'Creator' else 'Approve' end as WfStatus
		, WF.WFTASK as WfTask
	from TR_WORKFLOWPROCESS WF
		JOIN TR_VENDOR_CONTROL_ACTIVITY VCA on WF.DOCID = VCA.UID
	where WF.ISACTIVE = 1 
		AND WF.WFTASK = (case VCA.FORM_TYPE when 'LEGAL_REQUEST' then 'LEGALREQUEST' when 'PERIODICAL_REVIEW' then 'PERIODICALREVIEW' when 'CONTINUING_VENDOR' then 'CONTVENDOR' when 'EDIT_REGISTRATION' then 'EDITREGISTRATION' end) 
		AND VCA.IS_DRAFT = 0
GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERPERIODICALREVIEW_PICBYDEPT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_REMINDERPERIODICALREVIEW_PICBYDEPT]
	@Department VARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DPDTL.DEPT_CD AS DepartmentCode,
		DPDTL.MEMBER_CD AS MemberCode
	FROM DBO.MS_DEPT_PIC_CONFIG_DTL DPDTL
	WHERE DPDTL.DEPT_CD = @Department
	--ORDER BY DPDTL.MEMBER_CD ASC
	UNION ALL
	SELECT DepartmentCode as DepartmentCode, UserName as MemberCode
	FROM vw_UserByDepartment where DepartmentCode = @Department and RoleShortName = 'DH'
	
END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERREVBWD_TASK]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Isti>
-- Create date: <20211124>
-- Description:	<Remider, Task List of any active vendor with upcoming review date (send backward)
-- , CURRENT DATE >= REVIEW DATE - START FORM && CURRENT DATE <= REVIEW DATE  
-- >>>> NOTES WA 20211124 High Risk PVDate CreatedDate + 1 year, medium + 2 year, low + 3 year, backward: pvdate - startedfrom>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_REMINDERREVBWD_TASK] @CurrentDate DATETIME = NULL
AS
BEGIN
	SET NOCOUNT ON;

	--declare @CurrentDate datetime = '2023-01-28 00:00:00'
	DECLARE @UrlLink NVARCHAR(2000)

	SELECT @UrlLink = ISNULL([VALUE], '')
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_PATH'

	DECLARE @StartFromDate INT

	SELECT @StartFromDate = ISNULL(REV_BWD_START_FROM, 0)
	FROM DBO.MS_SETTING

	DECLARE @RecurringDay INT

	SELECT @RecurringDay = ISNULL(REV_BWD_RECUR_DAY, 0)
	FROM MS_SETTING

	SELECT DATEADD(DAY, - @StartFromDate, DATEADD(D, 1, VCA.REV_REVIEW_DT)),
		VC.UID AS VendorControlId,
		VC.VENDOR_ID AS VendorId,
		VC.VENDOR_NAME AS VendorName,
		-- VCA.REV_REVIEW_DT AS StartDate,
		StartDate = CASE 
			WHEN VCA.DOC_ID IS NULL
				AND VCA2.DOC_ID IS NULL
				THEN CASE 
						WHEN VC.[RISK_CATEGORY] = 3
							THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 2
							THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 1
							THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
						END -- first periodical review
			WHEN VCA.DOC_ID IS NOT NULL
				AND VCA2.DOC_ID IS NULL
				THEN CASE 
						WHEN VC.[RISK_CATEGORY] = 3
							THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 2
							THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 1
							THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
						END -- next periodical review without continuing
			WHEN VCA.DOC_ID IS NULL
				AND VCA2.DOC_ID IS NOT NULL
				THEN CASE 
						WHEN VC.[RISK_CATEGORY] = 3
							THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 2
							THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
						WHEN VC.[RISK_CATEGORY] = 1
							THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
						END -- continuing without periodical
			WHEN VCA.DOC_ID IS NOT NULL
				AND VCA2.DOC_ID IS NOT NULL
				THEN CASE 
						WHEN TDH2.CREATED_DT > TDH3.CREATED_DT
							THEN CASE 
									WHEN VC.[RISK_CATEGORY] = 3
										THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 2
										THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 1
										THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
									END -- next periodical review
						WHEN TDH2.CREATED_DT < TDH3.CREATED_DT
							THEN CASE 
									WHEN VC.[RISK_CATEGORY] = 3
										THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 2
										THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
									WHEN VC.[RISK_CATEGORY] = 1
										THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
									END -- next periodical review after continuing vendor
						END
			END,
		'<a href="' + @UrlLink + 'VendorList/Detail/' + CAST(VC.UID AS VARCHAR(10)) + '">link</a>' AS DocLink,
		VC.CREATOR_DEPT AS CreatorDepartment,
		VC.RISK_CATEGORY AS RiskCategory,
		VCA.UID AS VCAId
	FROM DBO.TR_VENDOR_CONTROL VC
	-- GET LAST PERIODICAL REVIEW
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA ON VC.UID = VCA.DOC_ID
		AND VCA.FORM_TYPE = 'PERIODICAL_REVIEW'
		AND VCA.[UID] = (
			SELECT MAX(VCA2.[UID])
			FROM DBO.TR_VENDOR_CONTROL_ACTIVITY VCA2
			WHERE VC.[UID] = VCA2.DOC_ID
				AND VCA2.FORM_TYPE = 'PERIODICAL_REVIEW'
				AND VCA2.APPROVAL_STS = 'Approval Completed'
			)
		AND VCA.IS_ACTIVE = 1
	-- GET LAST CONTINUING VENDOR
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA2 ON VC.UID = VCA2.DOC_ID
		AND VCA2.FORM_TYPE = 'CONTINUING_VENDOR'
		AND VCA2.[UID] = (
			SELECT MAX([UID])
			FROM DBO.TR_VENDOR_CONTROL_ACTIVITY
			WHERE VC.[UID] = DOC_ID
				AND FORM_TYPE = 'CONTINUING_VENDOR'
				AND APPROVAL_STS = 'Approval Completed'
			)
		AND VCA2.IS_ACTIVE = 1
	-- GET LAST APPROVAL REGISTRATION
	LEFT JOIN TR_DOCUMENT_HISTORY TDH ON TDH.DOC_ID = VC.UID
		AND TDH.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VC.UID
				AND HIST_TYPE = 'REGISTRATION'
				AND [STATUS] = 'Approve'
				AND ISNULL(STEP, 0) NOT LIKE '2%'
			)
	-- GET LAST APPROVAL PERIODICAL REVIEW
	LEFT JOIN TR_DOCUMENT_HISTORY TDH2 ON TDH2.DOC_ID = VCA.UID
		AND TDH2.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VCA.UID
				AND HIST_TYPE = 'PERIODICAL_REVIEW'
				AND [STATUS] = 'Approve'
			)
	-- GET LAST APPROVAL CONTINUING VENDOR
	LEFT JOIN TR_DOCUMENT_HISTORY TDH3 ON TDH3.DOC_ID = VCA2.UID
		AND TDH3.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VCA2.UID
				AND HIST_TYPE = 'CONTINUING_REVIEW'
				AND [STATUS] = 'Approve'
			)
	WHERE VC.IS_ACTIVE = 1 --and vc.VENDOR_ID='3CB9FBB8-5550'
		AND VC.APPROVAL_STS = 'Approval Completed'
		AND (
			(
				ISNULL(@CurrentDate, GETDATE()) >= DATEADD(DAY, - @StartFromDate, DATEADD(D, 0, (
							CASE 
								WHEN VC.[RISK_CATEGORY] = 3
									THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
								WHEN VC.[RISK_CATEGORY] = 2
									THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
								WHEN VC.[RISK_CATEGORY] = 1
									THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
								END
							)))
				AND ISNULL(@CurrentDate, GETDATE()) <= DATEADD(D, 0, (
						CASE 
							WHEN VC.[RISK_CATEGORY] = 3
								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 2
								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 1
								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
							END
						))
				AND VCA.DOC_ID IS NULL --first periodical review
				AND VCA2.DOC_ID IS NULL
				)
			OR (
				ISNULL(@CurrentDate, GETDATE()) >= DATEADD(DAY, - @StartFromDate, DATEADD(D, 0, CONVERT(VARCHAR(10), CASE 
								WHEN VC.[RISK_CATEGORY] = 3
									THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
								WHEN VC.[RISK_CATEGORY] = 2
									THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
								WHEN VC.[RISK_CATEGORY] = 1
									THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
								END, 120)))
				AND ISNULL(@CurrentDate, GETDATE()) <= DATEADD(D, 0, CONVERT(VARCHAR(10), CASE 
							WHEN VC.[RISK_CATEGORY] = 3
								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 2
								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 1
								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
							END, 120))
				AND (
					(
						VCA.DOC_ID IS NOT NULL --next periodical review without continuing
						AND VCA2.DOC_ID IS NULL
						)
					OR (
						VCA.DOC_ID IS NOT NULL
						AND VCA2.DOC_ID IS NOT NULL
						AND TDH2.CREATED_DT > TDH3.CREATED_DT --latest periodical after continuing
						)
					)
				)
			OR (
				ISNULL(@CurrentDate, GETDATE()) >= DATEADD(DAY, - @StartFromDate, DATEADD(D, 0, CONVERT(VARCHAR(10), CASE 
								WHEN VC.[RISK_CATEGORY] = 3
									THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
								WHEN VC.[RISK_CATEGORY] = 2
									THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
								WHEN VC.[RISK_CATEGORY] = 1
									THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
								END, 120)))
				AND ISNULL(@CurrentDate, GETDATE()) <= DATEADD(D, 0, CONVERT(VARCHAR(10), CASE 
							WHEN VC.[RISK_CATEGORY] = 3
								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 2
								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 1
								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
							END, 120))
				AND (
					(
						VCA.DOC_ID IS NULL --continuing without periodical
						AND VCA2.DOC_ID IS NOT NULL
						)
					OR (
						VCA.DOC_ID IS NOT NULL
						AND VCA2.DOC_ID IS NOT NULL
						AND TDH3.CREATED_DT > TDH2.CREATED_DT --latest continuing after periodcal
						)
					)
				)
			)
		AND (isnull(datediff(d, DATEADD(d, @RecurringDay, CONVERT(VARCHAR(10), VC.REV_LATEST_SENT_REMINDER_BWD, 120)), @CurrentDate), 0) >= 0)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_REMINDERREVFWD_TASK]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Author:		<Isti>
-- Create date: <20211124>
-- Description:	<Remider, Task List of any active vendor with overview review date overdue (send forward), CURRENT DATE > REVIEW DATE >>>> NOTES WA 20211124 High Risk PVDate CreatedDate + 1 year, medium + 2 year, low + 3 year, forward: pvdate + 1 day
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_REMINDERREVFWD_TASK] @CurrentDate DATETIME = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @UrlLink NVARCHAR(2000)

	SELECT @UrlLink = ISNULL([VALUE], '')
	FROM MS_PARAMETER
	WHERE [GROUP] = 'APP_PATH'

	DECLARE @RecurringDay INT

	SELECT @RecurringDay = REV_FWD_RECUR_DAY
	FROM MS_SETTING

	SELECT VC.UID AS VendorControlId,
		VC.VENDOR_ID AS VendorId,
		VC.VENDOR_NAME AS VendorName,
		-- VCA.REV_REVIEW_DT AS StartDate,
        VC.AGREE_START_DATE as StartDate, -- confirm eva
		-- StartDate = CASE 
		-- 	WHEN VCA.DOC_ID IS NULL
		-- 		AND VCA2.DOC_ID IS NULL
		-- 		THEN CASE 
		-- 				WHEN VC.[RISK_CATEGORY] = 3
		-- 					THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
		-- 				WHEN VC.[RISK_CATEGORY] = 2
		-- 					THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
		-- 				WHEN VC.[RISK_CATEGORY] = 1
		-- 					THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
		-- 				END -- first periodical review
		-- 	WHEN VCA.DOC_ID IS NOT NULL
		-- 		AND VCA2.DOC_ID IS NULL
		-- 		THEN CASE 
		-- 				WHEN VC.[RISK_CATEGORY] = 3
		-- 					THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
		-- 				WHEN VC.[RISK_CATEGORY] = 2
		-- 					THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
		-- 				WHEN VC.[RISK_CATEGORY] = 1
		-- 					THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
		-- 				END -- next periodical review without continuing
		-- 	WHEN VCA.DOC_ID IS NULL
		-- 		AND VCA2.DOC_ID IS NOT NULL
		-- 		THEN CASE 
		-- 				WHEN VC.[RISK_CATEGORY] = 3
		-- 					THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
		-- 				WHEN VC.[RISK_CATEGORY] = 2
		-- 					THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
		-- 				WHEN VC.[RISK_CATEGORY] = 1
		-- 					THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
		-- 				END -- continuing without periodical
		-- 	WHEN VCA.DOC_ID IS NOT NULL
		-- 		AND VCA2.DOC_ID IS NOT NULL
		-- 		THEN CASE 
		-- 				WHEN TDH2.CREATED_DT > TDH3.CREATED_DT
		-- 					THEN CASE 
		-- 							WHEN VC.[RISK_CATEGORY] = 3
		-- 								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
		-- 							WHEN VC.[RISK_CATEGORY] = 2
		-- 								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
		-- 							WHEN VC.[RISK_CATEGORY] = 1
		-- 								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
		-- 							END -- next periodical review
		-- 				WHEN TDH2.CREATED_DT < TDH3.CREATED_DT
		-- 					THEN CASE 
		-- 							WHEN VC.[RISK_CATEGORY] = 3
		-- 								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
		-- 							WHEN VC.[RISK_CATEGORY] = 2
		-- 								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
		-- 							WHEN VC.[RISK_CATEGORY] = 1
		-- 								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
		-- 							END -- next periodical review after continuing vendor
		-- 				END
		-- 	END,
		'<a href="' + @UrlLink + 'VendorList/Detail/' + CAST(VC.UID AS VARCHAR(10)) + '">link</a>' AS DocLink,
		VC.CREATOR_DEPT AS CreatorDepartment
	FROM DBO.TR_VENDOR_CONTROL VC
	-- GET LAST PERIODICAL REVIEW
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA ON VC.UID = VCA.DOC_ID
		AND VCA.FORM_TYPE = 'PERIODICAL_REVIEW'
		AND VCA.[UID] = (
			SELECT MAX(VCA2.[UID])
			FROM DBO.TR_VENDOR_CONTROL_ACTIVITY VCA2
			WHERE VC.[UID] = VCA2.DOC_ID
				AND VCA2.FORM_TYPE = 'PERIODICAL_REVIEW'
				AND VCA2.APPROVAL_STS = 'Approval Completed'
			)
		AND VCA.IS_ACTIVE = 1
	-- GET LAST CONTINUING VENDOR
	LEFT JOIN DBO.TR_VENDOR_CONTROL_ACTIVITY VCA2 ON VC.UID = VCA2.DOC_ID
		AND VCA2.FORM_TYPE = 'CONTINUING_VENDOR'
		AND VCA2.[UID] = (
			SELECT MAX([UID])
			FROM DBO.TR_VENDOR_CONTROL_ACTIVITY
			WHERE VC.[UID] = DOC_ID
				AND FORM_TYPE = 'CONTINUING_VENDOR'
				AND APPROVAL_STS = 'Approval Completed'
			)
		AND VCA2.IS_ACTIVE = 1
	-- GET LAST APPROVAL REGISTRATION
	LEFT JOIN TR_DOCUMENT_HISTORY TDH ON TDH.DOC_ID = VC.UID
		AND TDH.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VC.UID
				AND HIST_TYPE = 'REGISTRATION'
				AND [STATUS] = 'Approve'
				AND ISNULL(STEP, 0) NOT LIKE '2%'
			)
	-- GET LAST APPROVAL PERIODICAL REVIEW
	LEFT JOIN TR_DOCUMENT_HISTORY TDH2 ON TDH2.DOC_ID = VCA.UID
		AND TDH2.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VCA.UID
				AND HIST_TYPE = 'PERIODICAL_REVIEW'
				AND [STATUS] = 'Approve'
			)
	-- GET LAST APPROVAL CONTINUING VENDOR
	LEFT JOIN TR_DOCUMENT_HISTORY TDH3 ON TDH3.DOC_ID = VCA2.UID
		AND TDH3.UID = (
			SELECT MAX(UID)
			FROM TR_DOCUMENT_HISTORY
			WHERE DOC_ID = VCA2.UID
				AND HIST_TYPE = 'CONTINUING_REVIEW'
				AND [STATUS] = 'Approve'
			)
	JOIN DBO.MS_PARAMETER MP ON MP.[GROUP] = 'MAP_RISK_CATEGORY'
		AND MP.[VALUE] = VC.RISK_CATEGORY
	WHERE VC.IS_ACTIVE = 1
		AND VC.APPROVAL_STS = 'Approval Completed'
		AND (
			(
				ISNULL(@CurrentDate, GETDATE()) > DATEADD(D, 0, (
						CASE 
							WHEN VC.[RISK_CATEGORY] = 3
								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 2
								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 1
								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH.CREATED_DT, 120))
							END
						))
				AND VCA.DOC_ID IS NULL --first periodical review'
				AND VCA2.DOC_ID IS NULL
				)
			OR (
				ISNULL(@CurrentDate, GETDATE()) > DATEADD(D, 0, CONVERT(VARCHAR(10), CASE 
							WHEN VC.[RISK_CATEGORY] = 3
								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 2
								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 1
								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH2.CREATED_DT, 120))
							END, 120))
				AND (
					(
						VCA.DOC_ID IS NOT NULL --next periodical review without continuing
						AND VCA2.DOC_ID IS NULL
						)
					OR (
						VCA.DOC_ID IS NOT NULL
						AND VCA2.DOC_ID IS NOT NULL
						AND TDH2.CREATED_DT > TDH3.CREATED_DT --latest periodical after continuing
						)
					)
				)
			OR (
				ISNULL(@CurrentDate, GETDATE()) > DATEADD(D, 0, CONVERT(VARCHAR(10), CASE 
							WHEN VC.[RISK_CATEGORY] = 3
								THEN DATEADD(YEAR, 1, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 2
								THEN DATEADD(YEAR, 2, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
							WHEN VC.[RISK_CATEGORY] = 1
								THEN DATEADD(YEAR, 3, CONVERT(VARCHAR(10), TDH3.CREATED_DT, 120))
							END, 120))
				AND (
					(
						VCA.DOC_ID IS NULL --continuing without periodical
						AND VCA2.DOC_ID IS NOT NULL
						)
					OR (
						VCA.DOC_ID IS NOT NULL
						AND VCA2.DOC_ID IS NOT NULL
						AND TDH3.CREATED_DT > TDH2.CREATED_DT --latest continuing after periodcal
						)
					)
				)
			)
		AND (
			VC.REV_LATEST_SENT_REMINDER_FWD IS NULL
			OR DATEADD(d, @RecurringDay, CONVERT(VARCHAR(10), VC.REV_LATEST_SENT_REMINDER_FWD, 120)) = @CurrentDate
			)
END
GO
/****** Object:  StoredProcedure [dbo].[UDPS_RISKASSESSMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_RISKASSESSMENT]
@DocID INT
AS
BEGIN
	SELECT * FROM TR_VC_RISK_ASSESSMENT
	WHERE IS_ACTIVE = 1
	AND DOC_ID = @DocID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_RISKMANAGEMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_RISKMANAGEMENT]
@DocID INT
AS
BEGIN
	SELECT UID, COMMENT AS RISK_MANAGEMENT_COMMENT FROM [dbo].[TR_VC_RISK_MANAGEMENT_COMMENT]
	WHERE IS_ACTIVE = 1
	AND DOC_ID = @DocID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_SETTING]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_SETTING]
AS
BEGIN
	SELECT [UID] AS Id,
		[REV_CC] AS RevCC,
		[REV_BWD_ACTIVE] AS RevBwdReminder,
		[REV_BWD_START_FROM] AS RevBwdStartedFrom,
		[REV_BWD_RECUR_DAY] AS RevBwdRecurringDays,
		[REV_FWD_ACTIVE] AS RevFwdReminder,
		[REV_FWD_RECUR_DAY] AS RevFwdRecurringDays,
		[CONT_CC] AS ContCC,
		[CONT_BWD_ACTIVE] AS ContBwdReminder,
		[CONT_BWD_START_FROM] AS ContBwdStartedFrom,
		[CONT_BWD_RECUR_DAY] AS ContBwdRecurringDays,
		[CONT_FWD_ACTIVE] AS ContFwdReminder,
		[CONT_FWD_RECUR_DAY] AS ContFwdRecurringDays,
		[APV_RMD_ACTIVE] AS ApvRmdReminder,
		[APV_RMD_RECUR_DAY] AS ApvRmdRecurringDays,
		[GAD_DEPT_CD] AS GadDeptmentName,
		[GAD_PIC] AS GadPicName,
		[RMD_DEPT_CD] AS RmdDeptmentName,
		[RMD_STAFF] AS RmdStaffname,
		[CPC_DEPT_CD] AS CpcDeptmentName,
		[CPC_STAFF] AS CpcStaffName,
		[FCD_DEPT_CD] AS FcdDeptmentName,
		[FCD_PIC] AS FcdPicName,
		[LGL_DEPT_CD] AS LglDeptmentName,
		[LGL_PIC] AS LglPicName,
		[CREATED_BY] AS CreatedBy,
		[CREATED_DT] AS CreatedDate,
		[CREATED_HOST] AS CreatedHost,
		[MODIFIED_BY] AS ModifiedBy,
		[MODIFIED_DT] AS ModifiedDate,
		[MODIFIED_HOST] AS ModifiedHost
	FROM MS_SETTING
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_SETTINGDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_SETTINGDETAIL]
@ID INT
AS
BEGIN
SELECT 
	   [UID] AS Id
      ,[REV_CC] AS RevCC
      ,[REV_BWD_ACTIVE] AS RevBwdReminder
      ,[REV_BWD_START_FROM] AS RevBwdStartedFrom
      ,[REV_BWD_RECUR_DAY] AS RevBwdRecurringDays
      ,[REV_FWD_ACTIVE] AS RevFwdReminder
      ,[REV_FWD_RECUR_DAY] AS RevFwdRecurringDays

      ,[CONT_CC] AS ContCC
      ,[CONT_BWD_ACTIVE] AS ContBwdReminder
      ,[CONT_BWD_START_FROM] AS ContBwdStartedFrom
      ,[CONT_BWD_RECUR_DAY] AS ContBwdRecurringDays
      ,[CONT_FWD_ACTIVE] AS ContFwdReminder
      ,[CONT_FWD_RECUR_DAY] AS ContFwdRecurringDays

      ,[APV_RMD_ACTIVE] AS ApvRmdReminder
      ,[APV_RMD_RECUR_DAY] AS ApvRmdRecurringDays

      ,[GAD_DEPT_CD] AS GadDeptmentName
      ,[GAD_PIC] AS GadPicName
      
	  ,[RMD_DEPT_CD] AS RmdDeptmentName
      ,[RMD_STAFF] AS RmdStaffname
      
	  ,[CPC_DEPT_CD] AS CpcDeptmentName
      ,[CPC_STAFF] AS CpcStaffName
      
	  ,[FCD_DEPT_CD] AS FcdDeptmentName
      ,[FCD_PIC] AS FcdPicName
      
	  ,[LGL_DEPT_CD] AS LglDeptmentName
      ,[LGL_PIC] AS LglPicName
      
	  ,[CREATED_BY] AS CreatedBy
      ,[CREATED_DT] AS CreatedDate
      ,[CREATED_HOST] AS CreatedHost
      ,[MODIFIED_BY] AS ModifiedBy
      ,[MODIFIED_DT] AS ModifiedDate
      ,[MODIFIED_HOST] AS ModifiedHost

	  FROM MS_SETTING WHERE UID = @ID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_SUBMITTEDTASK]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_SUBMITTEDTASK] @SubmittedBy VARCHAR(50)
	-- add more stored procedure parameters here
AS
BEGIN
	-- body of the stored procedure
	SELECT tw.[UID] AS Id,
		CASE tw.WFTASK
			WHEN 'PERIODICALREVIEW'
				THEN 'Periodical Review'
			WHEN 'REGISTRATION'
				THEN 'Vendor Registration'
			WHEN 'LEGALREQUEST'
				THEN 'Legal Request'
			WHEN 'CONTVENDOR'
				THEN 'Continuing Vendor'
			WHEN 'EDITREGISTRATION'
				THEN 'Edit Registration'
			ELSE WFTASK
			END AS TaskCategory,
		tw.WFTASKNAME AS TaskName,
		tw.VendorId AS VendorId,
		tw.WFACTION AS [Action],
		tw.CREATED_BY AS RequestedBy,
		tw.CREATED_DT AS RequestedDate,
		tw.PROCID AS ProcessId,
		tw.[SN] AS SerialNumber,
		CASE 
			WHEN tw.WFTASK = 'EDITREGISTRATION'
				THEN vct.[VENDOR_NAME]
			ELSE vc.[VENDOR_NAME]
			END AS VendorName,
		CASE 
			WHEN tw.WFTASK = 'PERIODICALREVIEW'
				THEN mp.[VALUE] + 'PeriodicalReview/Detail/' + CAST(tw.DOCID AS VARCHAR(10))
			WHEN tw.WFTASK = 'REGISTRATION'
				AND tw.WFACTION = 'NEW'
				THEN mp.[VALUE] + 'VendorList/Detail/' + CAST(tw.DOCID AS VARCHAR(10))
			WHEN tw.WFTASK = 'EDITREGISTRATION'
				AND tw.WFACTION = 'EDIT'
				THEN mp.[VALUE] + 'VendorList/DetailEdit/' + CAST(tw.DOCID AS VARCHAR(10))
			WHEN tw.WFTASK = 'LEGALREQUEST'
				THEN mp.[VALUE] + 'LegalRequest/Detail/' + CAST(tw.DOCID AS VARCHAR(10))
			WHEN tw.WFTASK = 'CONTVENDOR'
				THEN mp.[VALUE] + 'ContinuingVendor/Detail/' + CAST(tw.DOCID AS VARCHAR(10))
			ELSE mp.[VALUE] + 'Registration/Detail/' + CAST(tw.DOCID AS VARCHAR(10))
			END AS [URL],
		mp2.[VALUE] + CAST(isnull(tw.PROCID, 0) AS VARCHAR(10)) AS [UrlFlow]
	FROM TR_WORKFLOWPROCESS AS tw
	INNER JOIN MS_PARAMETER AS mp ON mp.[GROUP] = 'APP_PATH'
	INNER JOIN MS_PARAMETER AS mp2 ON mp2.[GROUP] = 'K2FLOW_URL'
	-- INNER JOIN TR_VENDOR_CONTROL_ACTIVITY AS tca ON tca.UID = tw.DOCID
	-- INNER JOIN TR_VENDOR_CONTROL AS vc ON vc.UID = tw.DOCID
	INNER JOIN TR_VENDOR_CONTROL AS vc ON vc.VENDOR_ID = tw.VENDORID
	LEFT JOIN TR_VENDOR_CONTROL_TEMP AS vct ON vct.[UID] = tw.DOCID
	WHERE 1 = 1
		AND tw.CREATED_BY = @SubmittedBy
		AND tw.WFTASKNAME = 'Approve'
		AND tw.ISACTIVE = 1
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERACTIVITYLOG]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_USERACTIVITYLOG]

	-- Add the ReminderLog for the stored procedure here

	@CONDITION VARCHAR(20),

	@VALUE VARCHAR(500)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	SELECT mp.UID AS Id

		,mp.ACT_CD AS [ActivityCode]

		,mp.ACT_DESC AS [ActivityDescription]

		,mp.ACT_VAL AS [ActivityValue]

		,mp.ACT_RESULT AS [ActivityResult]

		,mp.ACT_USER AS [ActivityUserName]

		,mp.ACT_DEPT AS [ActivityDepartment]

		,mp.ACT_DT AS [ActivityDate]

		,MP.ACT_WORKSTATION AS [ActivityWorkstation]

		,mp.CREATED_BY AS CreatedBy

		,mp.CREATED_DT AS CreatedDate

		,mp.CREATED_HOST AS CreatedHost

		,mp.MODIFIED_BY AS ModifedBy

		,mp.MODIFIED_DT AS ModifiedDate

		,mp.MODIFIED_HOST AS ModifiedHost

	FROM dbo.TR_USER_ACTIVITY_LOG_HDR AS mp

END




GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERACTIVITYLOG_PARAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[UDPS_USERACTIVITYLOG_PARAM]
	-- Add the ReminderLog for the stored procedure here
	@SearchByDate DATE,
	@SearchByString NVARCHAR(100) = '',
	@KeywordString NVARCHAR(100) = '',
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SortOrder NVARCHAR(100) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @SearchByString = ISNULL(@SearchByString,'')
	SET @KeywordString = ISNULL(@KeywordString,'')
	SET @SortOrder = ISNULL(@SortOrder,'')	
	-- Insert statements for procedure here
	;WITH CTE_Result
	AS (
		SELECT *
		FROM TR_USER_ACTIVITY_LOG_HDR
		WHERE 1 = 1
			AND CONVERT(VARCHAR(10), ACT_DT, 111) = convert(VARCHAR(10), @SearchByDate, 111)
			AND (CONCAT([ACT_RESULT], [ACT_USER], [ACT_DEPT], [ACT_DT], [ACT_CD], [ACT_DESC], [ACT_VAL]) LIKE '%'+@KeywordString+'%' OR @KeywordString = '')
			AND ( 
				  (CASE @SearchByString
					WHEN 'Result' THEN [ACT_RESULT]
					WHEN 'User Name' THEN [ACT_USER]
					WHEN 'User Departement' THEN [ACT_DEPT]
					WHEN 'Activity Code' THEN [ACT_CD]
					WHEN 'Activity Description' THEN [ACT_DESC]
					WHEN 'Activity Value' THEN [ACT_VAL] END) LIKE '%'+@KeywordString+'%'
					OR @SearchByString = ''
				)
		ORDER BY 
		CASE WHEN @SortOrder = 'Result' THEN [ACT_RESULT] END ASC,
		CASE WHEN @SortOrder = 'Result_desc' THEN [ACT_RESULT] END DESC,
		CASE WHEN @SortOrder = 'User' THEN [ACT_USER] END ASC,
		CASE WHEN @SortOrder = 'User_desc' THEN [ACT_USER] END DESC,
		CASE WHEN @SortOrder = 'Dept' THEN [ACT_DEPT] END ASC,
		CASE WHEN @SortOrder = 'Dept_desc' THEN [ACT_DEPT] END DESC,
		CASE WHEN @SortOrder = 'ActivityCode' THEN [ACT_CD] END ASC,
		CASE WHEN @SortOrder = 'ActivityCode_desc' THEN [ACT_CD] END DESC,
		CASE WHEN @SortOrder = 'ActivityDate' THEN [ACT_DT] END ASC,
		CASE WHEN @SortOrder = 'ActivityDate_desc' THEN [ACT_DT] END DESC,
		CASE WHEN @SortOrder = 'ActivtyDescription' THEN [ACT_DESC] END ASC,
		CASE WHEN @SortOrder = 'ActivityDescription_desc' THEN [ACT_DESC] END DESC,
		CASE WHEN @SortOrder = 'ActivityValue' THEN [ACT_VAL] END ASC,
		CASE WHEN @SortOrder = 'ActivityValue_desc' THEN [ACT_VAL] END DESC,
		CASE WHEN @SortOrder = '' THEN [UID] END DESC 
		OFFSET @PageSize * (@PageNo - 1) rows FETCH NEXT @PageSize rows ONLY
		),
	CTE_TotalRows
	AS (
		SELECT count(UID) AS TotalRows
		FROM TR_USER_ACTIVITY_LOG_HDR
		WHERE 1 = 1
			AND CONVERT(VARCHAR(10), ACT_DT, 111) = convert(VARCHAR(10), @SearchByDate, 111)
			AND (CONCAT([ACT_RESULT], [ACT_USER], [ACT_DEPT], [ACT_DT], [ACT_CD], [ACT_DESC], [ACT_VAL]) LIKE '%'+@KeywordString+'%' OR @KeywordString = '')
			AND (
				  (CASE @SearchByString
					WHEN 'Result' THEN [ACT_RESULT]
					WHEN 'User Name' THEN [ACT_USER]
					WHEN 'User Departement' THEN [ACT_DEPT]
					WHEN 'Activity Code' THEN [ACT_CD]
					WHEN 'Activity Description' THEN [ACT_DESC]
					WHEN 'Activity Value' THEN [ACT_VAL] END) LIKE '%'+@KeywordString+'%'
					OR @SearchByString = ''
				)
		)
	SELECT TotalRows,
		mp.UID AS Id,
		mp.ACT_CD AS [ActivityCode],
		mp.ACT_DESC AS [ActivityDescription],
		mp.ACT_VAL AS [ActivityValue],
		mp.ACT_RESULT AS [ActivityResult],
		mp.ACT_USER AS [ActivityUserName],
		mp.ACT_DEPT AS [ActivityDepartment],
		mp.ACT_DT AS [ActivityDate],
		MP.ACT_WORKSTATION AS [ActivityWorkstation],
		mp.CREATED_BY AS CreatedBy,
		mp.CREATED_DT AS CreatedDate,
		mp.CREATED_HOST AS CreatedHost,
		mp.MODIFIED_BY AS ModifedBy,
		mp.MODIFIED_DT AS ModifiedDate,
		mp.MODIFIED_HOST AS ModifiedHost
	FROM dbo.TR_USER_ACTIVITY_LOG_HDR AS mp,
		CTE_TotalRows
	WHERE EXISTS (
			SELECT 1
			FROM CTE_Result
			WHERE CTE_Result.UID = mp.UID
			)
	OPTION (RECOMPILE)
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERACTIVITYLOGDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_USERACTIVITYLOGDETAIL]

	-- Add the ReminderLog for the stored procedure here

	@Id INT

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	SELECT UID AS Id,

		ACT_CD AS ActivityCode,

		FIELD_NM AS FieldName,

		-- OLD_VALUE AS OldValue,

		OldValue = CASE 

			WHEN OLD_VALUE = '(null)'

				THEN ''

			ELSE CASE 

					WHEN ISNUMERIC(OLD_VALUE) = 1

						THEN OLD_VALUE

					WHEN ISDATE(OLD_VALUE) = 1

						THEN FORMAT(CONVERT(DATE, OLD_VALUE), 'dd/MMM/yyyy')

					WHEN CHARINDEX('1/1/0001', OLD_VALUE) = 1

						THEN ''

					ELSE OLD_VALUE

					END

			END,

		-- NEW_VALUE AS NewValue,

		NewValue = CASE 

			WHEN NEW_VALUE = '(null)'

				THEN ''

			ELSE CASE 

					WHEN ISNUMERIC(NEW_VALUE) = 1

						THEN NEW_VALUE

					WHEN len(new_value) <= 4000

						AND ISDATE(NEW_VALUE) = 1

						THEN FORMAT(CONVERT(DATE, NEW_VALUE), 'dd/MMM/yyyy')

					WHEN CHARINDEX('1/1/0001', OLD_VALUE) = 1

						THEN ''

					ELSE NEW_VALUE

					END

			END,

		CREATED_BY AS CreatedBy,

		CREATED_DT AS CreatedDate,

		CREATED_HOST AS CreatedHost,

		MODIFIED_BY AS ModifiedBy,

		MODIFIED_DT AS MODIFIED_DT,

		MODIFIED_HOST AS MODIFIED_HOST,

		DOC_ID AS DocumentId

	FROM TR_USER_ACTIVITY_LOG_DTL

	WHERE [DOC_ID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERACTIVITYLOGHEADER]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPS_USERACTIVITYLOGHEADER]

	-- Add the ReminderLog for the stored procedure here

	@Id INT

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	SELECT mp.UID AS Id

		,mp.ACT_CD AS [ActivityCode]

		,mp.ACT_DESC AS [ActivityDescription]

		,mp.ACT_VAL AS [ActivityValue]

		,mp.ACT_RESULT AS [ActivityResult]

		,mp.ACT_USER AS [ActivityUserName]

		,mp.ACT_DEPT AS [ActivityDepartment]

		,FORMAT(mp.ACT_DT, 'dd/MMM/yyyy HH:mm:ss') AS [ActivityDate]

		,mp.ACT_WORKSTATION AS [ActivityWorkstation]

		,mp.CREATED_BY AS CreatedBy

		,mp.CREATED_DT AS CreatedDate

		,mp.CREATED_HOST AS CreatedHost

		,mp.MODIFIED_BY AS ModifedBy

		,mp.MODIFIED_DT AS ModifiedDate

		,mp.MODIFIED_HOST AS ModifiedHost

	FROM dbo.TR_USER_ACTIVITY_LOG_HDR AS mp 

	WHERE mp.UID = @Id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERCONE]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211118>
-- Description:	<Use from CONE>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_USERCONE]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Username AS [UserName],
		RealName AS [RealName],
		DepartmentName AS [DepartmentName],
		Email AS [Email]
	FROM SN_CONE_CONE_USER
    WHERE ISNULL(Email, '') <> ''
	ORDER BY username ASC
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_USERCONEBYUSERNAMES]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Isti>
-- Create date: <20211118>
-- Description:	<Use from CONE>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_USERCONEBYUSERNAMES] 
	@List VARCHAR(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT Username AS [UserName],
		RealName AS [RealName],
		DepartmentName AS [DepartmentName],
		Email AS [Email]
	FROM SN_CONE_CONE_USER
	WHERE (
			Username IN (
				SELECT Name
				FROM dbo.splitstring(@List, '|')
				)
			)
		OR (
			RealName IN (
				SELECT Name
				FROM dbo.splitstring(@List, '|')
				)
			)
	ORDER BY username ASC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_USRAPPDGM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_USRAPPDGM]
	-- add more stored procedure parameters here
	@Originator NVARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50)
	DECLARE @Department VARCHAR(10) = NULL

	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	DECLARE @DivID NVARCHAR(5)

	SELECT @DivID = DivisionID,
		@Department = DepartmentCode
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

	SELECT Username,
		RealName,
		Email
	FROM dbo.fnGetTabDGMByDept(@Department)
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_USRAPPUHDH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_USRAPPUHDH]
	-- add more stored procedure parameters here
	@Originator NVARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @Originator

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@Originator, 'jakarta01\', '')

	DECLARE @DivID NVARCHAR(5)
	DECLARE @DeptID NVARCHAR(5)

	SELECT @DivID = DivisionID,
		@DeptID = DepartmentID
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

	SELECT Username,
		RealName,
		Email
	FROM [vw_UserByDepartment]
	WHERE DepartmentID = @DeptID
		AND RoleShortName IN ('UH', 'DH')
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VC_RISK_ASSESSMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VC_RISK_ASSESSMENT]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN
if ISNUMERIC(@Id)=1
BEGIN
SELECT * FROM [TR_VC_RISK_ASSESSMENT] WHERE [DOC_ID]=@Id ORDER BY [SEQ_NO] ASC
END
ELSE
BEGIN
SELECT * FROM [TR_VC_RISK_ASSESSMENT] ORDER BY [UID] DESC
END
	

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORDATA]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_VENDORDATA]
	-- add more stored procedure parameters here
	@RefID BIGINT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID,
		VENDOR_ID,
		VENDOR_NAME,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		DOC_DATE_FORM1,
		CONF_CONTRACTOR_NM,
		CONF_OPERATION_TO_OUTSOURCE,
		CONF_CATEGORY,
		CONF_DEPT_CD,
		CONF_BRIEF_EXPLAIN_CONTRACT,
		CONF_DECISION,
		CONF_GAD_COMMENT,
		DOC_DATE_FORM2,
		EXAM_NAME_OF_GROUP,
		EXAM_CORE_BUSINESS,
		EXAM_REL_VENDOR,
		EXAM_REL_VENDOR_REMARK,
		EXAM_HAS_ENGAGED_BEFORE,
		EXAM_HAS_ENGAGED_SINCE,
		EXAM_BACKGROUND,
		EXAM_VENDOR_A,
		EXAM_VENDOR_B,
		EXAM_OVERALL_CONCLUSION,
		RISK_ANNUAL_BILLING,
		RISK_SPECIAL_RELATED,
		RISK_CPC_ASSESSMENT,
		RISK_PROVIDE_ACCESS,
		RISK_AS_VENDOR_A,
		RISK_AS_VENDOR_B,
		RISK_AML_COMMENT,
		RISK_CATEGORY,
		RISK_RMD_RECOMMENDATION,
		RISK_SCREENING_RESULT,
		REG_PROVIDED_SERVICE,
		REG_CONTRACT_NM,
		REG_LEGAL_STATUS,
		REG_NPWP_NO,
		REG_ADDRESS,
		REG_CITY,
		REG_POSTAL,
		REG_WEBSITE,
		REG_EMAIL,
		REG_PHONE_1,
		REG_PHONE_EXT_1,
		REG_PHONE_2,
		REG_PHONE_EXT_2,
		REG_FAX_1,
		REG_FAX_2,
		REG_CP_NM_1,
		REG_CP_POSITION_1,
		REG_CP_NM_2,
		REG_CP_POSITION_2,
		REG_PAYMENT_TYPE,
		REG_REASON_CASH,
		REG_ACC_HOLDER_NM,
		REG_ACC_NO,
		REG_ACC_BANK,
		DOC_DATE_FORM4A,
		ASC_CONTRACTOR_JAPANESE,
		ASC_SUBCONTRACT_OPERATION,
		AGREE_START_DATE,
		AGREE_END_DATE,
		AGREE_CREATED_BY,
		AGREE_CREATED_DT,
		AGREE_MODIFIED_BY,
		AGREE_MODIFIED_DT,
		TERMINATION,
		TERMINATION_BY,
		TERMINATION_DT,
		REGISTRATION_DT,
		APPROVAL_STS,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST,
		L_DOC_ID,
		STATUS_FLOW,
        OLD_DATA = CASE 
		WHEN REQUEST_DT < '2016-01-01'
			THEN 1
		ELSE 0
		END
	FROM TR_VENDOR_CONTROL
	WHERE 1 = 1
		AND [UID] = @RefID
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_VENDORDETAIL]
	-- add more stored procedure parameters here
	@VendorUID INT
AS
BEGIN
	-- body of the stored procedure
	SELECT tvc.UID,
        tvc.VENDOR_ID AS VENDOR_ID,
		tvc.CREATOR AS CREATOR,
		tvc.CREATOR_DEPT AS CREATOR_DEPT,
		tvc.CREATOR_UNIT AS CREATOR_UNIT,
		tvc.CREATOR_ROLE AS CREATOR_ROLE,
		tvc.REQUEST_DT AS REQUEST_DT,
		tvc.DOC_DATE_FORM1 AS DOC_DATE_FORM1,
		tvc.CONF_CONTRACTOR_NM AS CONF_CONTRACTOR_NM,
		tvc.CONF_OPERATION_TO_OUTSOURCE AS CONF_OPERATION_TO_OUTSOURCE,
		tvc.CONF_CATEGORY AS CONF_CATEGORY,
		tvc.CONF_DEPT_CD AS CONF_DEPT_CD,
		tvc.CONF_BRIEF_EXPLAIN_CONTRACT AS CONF_BRIEF_EXPLAIN_CONTRACT,
		conf1.ITEM AS ITEM_1,
		conf1.SUBMITTED AS SUBMITTED_1,
		conf1.REASON AS REASON_1,
		conf1.CHECKED AS CHECKED_1,
        conf1.REASON_CHECKED AS REASON_CHECKED_1,
		conf2.ITEM AS ITEM_2,
		conf2.SUBMITTED AS SUBMITTED_2,
		conf2.REASON AS REASON_2,
		conf2.CHECKED AS CHECKED_2,
        conf2.REASON_CHECKED AS REASON_CHECKED_2,
		conf3.ITEM AS ITEM_3,
		conf3.SUBMITTED AS SUBMITTED_3,
		conf3.REASON AS REASON_3,
		conf3.CHECKED AS CHECKED_3,
        conf3.REASON_CHECKED AS REASON_CHECKED_3,
		conf4.ITEM AS ITEM_4,
		conf4.SUBMITTED AS SUBMITTED_4,
		conf4.REASON AS REASON_4,
		conf4.CHECKED AS CHECKED_4,
        conf4.REASON_CHECKED AS REASON_CHECKED_4,
		conf5.ITEM AS ITEM_5,
		conf5.SUBMITTED AS SUBMITTED_5,
		conf5.REASON AS REASON_5,
		conf5.CHECKED AS CHECKED_5,
        conf5.REASON_CHECKED AS REASON_CHECKED_5,
		conf6.ITEM AS ITEM_6,
		conf6.SUBMITTED AS SUBMITTED_6,
		conf6.REASON AS REASON_6,
		conf6.CHECKED AS CHECKED_6,
        conf6.REASON_CHECKED AS REASON_CHECKED_6,
		conf7.ITEM AS ITEM_7,
		conf7.SUBMITTED AS SUBMITTED_7,
		conf7.REASON AS REASON_7,
		conf7.CHECKED AS CHECKED_7,
        conf7.REASON_CHECKED AS REASON_CHECKED_7,
		conf8.ITEM AS ITEM_8,
		conf8.SUBMITTED AS SUBMITTED_8,
		conf8.REASON AS REASON_8,
		conf8.CHECKED AS CHECKED_8,
        conf8.REASON_CHECKED AS REASON_CHECKED_8,
		conf9.ITEM AS ITEM_9,
		conf9.SUBMITTED AS SUBMITTED_9,
		conf9.REASON AS REASON_9,
		conf9.CHECKED AS CHECKED_9,
        conf9.REASON_CHECKED AS REASON_CHECKED_9,
		conf10.ITEM AS ITEM_10,
		conf10.SUBMITTED AS SUBMITTED_10,
		conf10.REASON AS REASON_10,
		conf10.CHECKED AS CHECKED_10,
        conf10.REASON_CHECKED AS REASON_CHECKED_10,
		conf11.ITEM AS ITEM_11,
		conf11.SUBMITTED AS SUBMITTED_11,
		conf11.REASON AS REASON_11,
		conf11.CHECKED AS CHECKED_11,
        conf11.REASON_CHECKED AS REASON_CHECKED_11,
		conf12.ITEM AS ITEM_12,
		conf12.SUBMITTED AS SUBMITTED_12,
		conf12.REASON AS REASON_12,
		conf12.CHECKED AS CHECKED_12,
        conf12.REASON_CHECKED AS REASON_CHECKED_12,
		conf13.ITEM AS ITEM_13,
		conf13.SUBMITTED AS SUBMITTED_13,
		conf13.REASON AS REASON_13,
		conf13.CHECKED AS CHECKED_13,
        conf13.REASON_CHECKED AS REASON_CHECKED_13,
		conf14.ITEM AS ITEM_14,
		conf14.SUBMITTED AS SUBMITTED_14,
		conf14.REASON AS REASON_14,
		conf14.CHECKED AS CHECKED_14,
        conf14.REASON_CHECKED AS REASON_CHECKED_14,
		conf15.ITEM AS ITEM_15,
		conf15.SUBMITTED AS SUBMITTED_15,
		conf15.REASON AS REASON_15,
		conf15.CHECKED AS CHECKED_15,
        conf15.REASON_CHECKED AS REASON_CHECKED_15,
		tvc.CONF_DECISION AS CONF_DECISION,
		tvc.CONF_GAD_COMMENT AS CONF_GAD_COMMENT,
		-- tvc.CONF_DEPCONF_DECISIONT_CD AS CONF_DEPCONF_DECISIONT_CD,
		tvc.DOC_DATE_FORM2 AS DOC_DATE_FORM2,
		tvc.EXAM_NAME_OF_GROUP AS EXAM_NAME_OF_GROUP,
		tvc.EXAM_CORE_BUSINESS AS EXAM_CORE_BUSINESS,
		tvc.EXAM_REL_VENDOR AS EXAM_REL_VENDOR,
		tvc.EXAM_REL_VENDOR_REMARK AS EXAM_REL_VENDOR_REMARK,
		tvc.EXAM_HAS_ENGAGED_BEFORE AS EXAM_HAS_ENGAGED_BEFORE,
		tvc.EXAM_HAS_ENGAGED_SINCE AS EXAM_HAS_ENGAGED_SINCE,
		tvc.EXAM_BACKGROUND AS EXAM_BACKGROUND,
		tvc.EXAM_VENDOR_A AS EXAM_VENDOR_A,
		tvc.EXAM_VENDOR_B AS EXAM_VENDOR_B,
		exam1.PARAMETER AS EXAM_ITEM_1,
		exam1.UID AS EXAM_UID_1,
		exam1.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_1,
		exam1.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_1,
		exam1.REMARK AS EXAM_REMARK_1,
		exam2.PARAMETER AS EXAM_ITEM_2,
		exam2.UID AS EXAM_UID_2,
		exam2.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_2,
		exam2.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_2,
		exam2.REMARK AS EXAM_REMARK_2,
		exam3.PARAMETER AS EXAM_ITEM_3,
		exam3.UID AS EXAM_UID_3,
		exam3.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_3,
		exam3.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_3,
		exam3.REMARK AS EXAM_REMARK_3,
		exam4.PARAMETER AS EXAM_ITEM_4,
		exam4.UID AS EXAM_UID_4,
		exam4.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_4,
		exam4.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_4,
		exam4.REMARK AS EXAM_REMARK_4,
		tvc.EXAM_OVERALL_CONCLUSION AS EXAM_OVERALL_CONCLUSION,
		tvc.RISK_ANNUAL_BILLING AS RISK_ANNUAL_BILLING,
		tvc.RISK_SPECIAL_RELATED AS RISK_SPECIAL_RELATED,
		tvc.RISK_CPC_ASSESSMENT AS RISK_CPC_ASSESSMENT,
		tvc.RISK_PROVIDE_ACCESS AS RISK_PROVIDE_ACCESS,
		tvc.RISK_AS_VENDOR_A AS RISK_AS_VENDOR_A,
		tvc.RISK_AS_VENDOR_B AS RISK_AS_VENDOR_B,
		risk1.PARAMETER AS ASSESS_ITEM_1,
		risk1.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_1,
		risk1.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_1,
		risk1.REMARK AS ASSESS_REMARK_1,
		risk2.PARAMETER AS ASSESS_ITEM_2,
		risk2.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_2,
		risk2.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_2,
		risk2.REMARK AS ASSESS_REMARK_2,
		risk3.PARAMETER AS ASSESS_ITEM_3,
		risk3.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_3,
		risk3.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_3,
		risk3.REMARK AS ASSESS_REMARK_3,
		risk4.PARAMETER AS ASSESS_ITEM_4,
		risk4.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_4,
		risk4.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_4,
		risk4.REMARK AS ASSESS_REMARK_4,
		tvc.RISK_AML_COMMENT AS RISK_AML_COMMENT,
		tvc.RISK_CATEGORY AS RISK_CATEGORY,
		tvc.RISK_RMD_RECOMMENDATION AS RISK_RMD_RECOMMENDATION,
		tvc.RISK_SCREENING_RESULT AS RISK_SCREENING_RESULT,
		-- rmc.COMMENT AS RISK_MANAGEMENT_COMMENT,
		tvc.REG_PROVIDED_SERVICE AS REG_PROVIDED_SERVICE,
		tvc.REG_CONTRACT_NM AS REG_CONTRACT_NM,
		tvc.VENDOR_NAME AS VENDOR_NAME,
		tvc.REG_LEGAL_STATUS AS REG_LEGAL_STATUS,
		tvc.REG_NPWP_NO AS REG_NPWP_NO,
		tvc.REG_ADDRESS AS REG_ADDRESS,
		tvc.REG_CITY AS REG_CITY,
		tvc.REG_POSTAL AS REG_POSTAL,
		tvc.REG_WEBSITE AS REG_WEBSITE,
		tvc.REG_EMAIL AS REG_EMAIL,
		tvc.REG_PHONE_1 AS REG_PHONE_1,
		tvc.REG_PHONE_EXT_1 AS REG_PHONE_EXT_1,
		tvc.REG_PHONE_2 AS REG_PHONE_2,
		tvc.REG_PHONE_EXT_2 AS REG_PHONE_EXT_2,
		tvc.REG_FAX_1 AS REG_FAX_1,
		tvc.REG_FAX_2 AS REG_FAX_2,
		tvc.REG_CP_NM_1 AS REG_CP_NM_1,
		tvc.REG_CP_POSITION_1 AS REG_CP_POSITION_1,
		tvc.REG_CP_NM_2 AS REG_CP_NM_2,
		tvc.REG_CP_POSITION_2 AS REG_CP_POSITION_2,
		tvc.REG_PAYMENT_TYPE AS REG_PAYMENT_TYPE,
		tvc.REG_REASON_CASH AS REG_REASON_CASH,
		tvc.REG_ACC_HOLDER_NM AS REG_ACC_HOLDER_NM,
		tvc.REG_ACC_NO AS REG_ACC_NO,
		tvc.REG_ACC_BANK AS REG_ACC_BANK,
		tvc.DOC_DATE_FORM4A AS DOC_DATE_FORM4A,
		sheet1.UID AS SHEET_ID_1,
		sheet1.POSITION AS SHEET_POSITION_1,
		sheet1.NAME AS SHEET_NAME_1,
		sheet1.NATIONALITY AS SHEET_NATIONALITY_1,
		sheet1.SHARE AS SHEET_SHARE_1,
		sheet2.UID AS SHEET_ID_2,
		sheet2.POSITION AS SHEET_POSITION_2,
		sheet2.NAME AS SHEET_NAME_2,
		sheet2.NATIONALITY AS SHEET_NATIONALITY_2,
		sheet2.SHARE AS SHEET_SHARE_2,
		sheet3.UID AS SHEET_ID_3,
		sheet3.POSITION AS SHEET_POSITION_3,
		sheet3.NAME AS SHEET_NAME_3,
		sheet3.NATIONALITY AS SHEET_NATIONALITY_3,
		sheet3.SHARE AS SHEET_SHARE_3,
		sheet4.UID AS SHEET_ID_4,
		sheet4.POSITION AS SHEET_POSITION_4,
		sheet4.NAME AS SHEET_NAME_4,
		sheet4.NATIONALITY AS SHEET_NATIONALITY_4,
		sheet4.SHARE AS SHEET_SHARE_4,
		sheet5.UID AS SHEET_ID_5,
		sheet5.POSITION AS SHEET_POSITION_5,
		sheet5.NAME AS SHEET_NAME_5,
		sheet5.NATIONALITY AS SHEET_NATIONALITY_5,
		sheet5.SHARE AS SHEET_SHARE_5,
		sheet6.UID AS SHEET_ID_6,
		sheet6.POSITION AS SHEET_POSITION_6,
		sheet6.NAME AS SHEET_NAME_6,
		sheet6.NATIONALITY AS SHEET_NATIONALITY_6,
		sheet6.SHARE AS SHEET_SHARE_6,
		sheet7.UID AS SHEET_ID_7,
		sheet7.POSITION AS SHEET_POSITION_7,
		sheet7.NAME AS SHEET_NAME_7,
		sheet7.NATIONALITY AS SHEET_NATIONALITY_7,
		sheet7.SHARE AS SHEET_SHARE_7,
		sheet8.UID AS SHEET_ID_8,
		sheet8.POSITION AS SHEET_POSITION_8,
		sheet8.NAME AS SHEET_NAME_8,
		sheet8.NATIONALITY AS SHEET_NATIONALITY_8,
		sheet8.SHARE AS SHEET_SHARE_8,
		sheet9.UID AS SHEET_ID_9,
		sheet9.POSITION AS SHEET_POSITION_9,
		sheet9.NAME AS SHEET_NAME_9,
		sheet9.NATIONALITY AS SHEET_NATIONALITY_9,
		sheet9.SHARE AS SHEET_SHARE_9,
		sheet10.UID AS SHEET_ID_10,
		sheet10.POSITION AS SHEET_POSITION_10,
		sheet10.NAME AS SHEET_NAME_10,
		sheet10.NATIONALITY AS SHEET_NATIONALITY_10,
		sheet10.SHARE AS SHEET_SHARE_10,
		tvc.ASC_CONTRACTOR_JAPANESE AS ASC_CONTRACTOR_JAPANESE,
		tvc.ASC_SUBCONTRACT_OPERATION AS ASC_SUBCONTRACT_OPERATION,
		tvc.AGREE_START_DATE AS AGREE_START_DATE,
		tvc.AGREE_END_DATE AS AGREE_END_DATE,
		tvc.AGREE_CREATED_BY AS AGREE_CREATED_BY,
		tvc.AGREE_CREATED_DT AS AGREE_CREATED_DT,
		tvc.AGREE_MODIFIED_BY AS AGREE_MODIFIED_BY,
		tvc.AGREE_MODIFIED_DT AS AGREE_MODIFIED_DT,
		tvc.TERMINATION AS TERMINATION,
		tvc.TERMINATION_BY AS TERMINATION_BY,
		tvc.TERMINATION_DT AS TERMINATION_DT,
		tvc.REGISTRATION_DT AS REGISTRATION_DT,
		tvc.APPROVAL_STS AS APPROVAL_STS,
		tvc.IS_ACTIVE AS IS_ACTIVE,
		tvc.STATUS_FLOW AS STATUS_FLOW,
		tvc.CREATED_BY AS CREATED_BY,
		tvc.CREATED_DT AS CREATED_DT,
		tvc.CREATED_HOST AS CREATED_HOST,
		tvc.MODIFIED_BY AS MODIFIED_BY,
		tvc.MODIFIED_DT AS MODIFIED_DT,
		tvc.MODIFIED_HOST AS MODIFIED_HOST
	FROM TR_VENDOR_CONTROL AS tvc
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf1 ON conf1.DOC_ID = tvc.UID
		AND conf1.SEQ = 1
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf2 ON conf2.DOC_ID = tvc.UID
		AND conf2.SEQ = 2
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf3 ON conf3.DOC_ID = tvc.UID
		AND conf3.SEQ = 3
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf4 ON conf4.DOC_ID = tvc.UID
		AND conf4.SEQ = 4
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf5 ON conf5.DOC_ID = tvc.UID
		AND conf5.SEQ = 5
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf6 ON conf6.DOC_ID = tvc.UID
		AND conf6.SEQ = 6
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf7 ON conf7.DOC_ID = tvc.UID
		AND conf7.SEQ = 7
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf8 ON conf8.DOC_ID = tvc.UID
		AND conf8.SEQ = 8
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf9 ON conf9.DOC_ID = tvc.UID
		AND conf9.SEQ = 9
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf10 ON conf10.DOC_ID = tvc.UID
		AND conf10.SEQ = 10
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf11 ON conf11.DOC_ID = tvc.UID
		AND conf11.SEQ = 11
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf12 ON conf12.DOC_ID = tvc.UID
		AND conf12.SEQ = 12
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf13 ON conf13.DOC_ID = tvc.UID
		AND conf13.SEQ = 13
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf14 ON conf14.DOC_ID = tvc.UID
		AND conf14.SEQ = 14
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST AS conf15 ON conf15.DOC_ID = tvc.UID
		AND conf15.SEQ = 15
	LEFT JOIN TR_VC_EXAM_COMPARISON AS exam1 ON exam1.DOC_ID = tvc.UID
		AND exam1.SEQ_NO = 1
	LEFT JOIN TR_VC_EXAM_COMPARISON AS exam2 ON exam2.DOC_ID = tvc.UID
		AND exam2.SEQ_NO = 2
	LEFT JOIN TR_VC_EXAM_COMPARISON AS exam3 ON exam3.DOC_ID = tvc.UID
		AND exam3.SEQ_NO = 3
	LEFT JOIN TR_VC_EXAM_COMPARISON AS exam4 ON exam4.DOC_ID = tvc.UID
		AND exam4.SEQ_NO = 4
	LEFT JOIN TR_VC_RISK_ASSESSMENT AS risk1 ON risk1.DOC_ID = tvc.UID
		AND risk1.SEQ_NO = 1
	LEFT JOIN TR_VC_RISK_ASSESSMENT AS risk2 ON risk2.DOC_ID = tvc.UID
		AND risk2.SEQ_NO = 2
	LEFT JOIN TR_VC_RISK_ASSESSMENT AS risk3 ON risk3.DOC_ID = tvc.UID
		AND risk3.SEQ_NO = 3
	LEFT JOIN TR_VC_RISK_ASSESSMENT AS risk4 ON risk4.DOC_ID = tvc.UID
		AND risk4.SEQ_NO = 4
	-- LEFT JOIN TR_VC_RISK_MANAGEMENT_COMMENT AS rmc ON rmc.DOC_ID = tvc.UID
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet1 ON sheet1.DOC_ID = tvc.UID
		AND sheet1.SEQ_NO = 1
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet2 ON sheet2.DOC_ID = tvc.UID
		AND sheet2.SEQ_NO = 2
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet3 ON sheet3.DOC_ID = tvc.UID
		AND sheet3.SEQ_NO = 3
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet4 ON sheet4.DOC_ID = tvc.UID
		AND sheet4.SEQ_NO = 4
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet5 ON sheet5.DOC_ID = tvc.UID
		AND sheet5.SEQ_NO = 5
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet6 ON sheet6.DOC_ID = tvc.UID
		AND sheet6.SEQ_NO = 6
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet7 ON sheet7.DOC_ID = tvc.UID
		AND sheet7.SEQ_NO = 7
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet8 ON sheet8.DOC_ID = tvc.UID
		AND sheet8.SEQ_NO = 8
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet9 ON sheet9.DOC_ID = tvc.UID
		AND sheet9.SEQ_NO = 9
	LEFT JOIN TR_VC_ASC_CHECKSHEET AS sheet10 ON sheet10.DOC_ID = tvc.UID
		AND sheet10.SEQ_NO = 10
	WHERE tvc.UID = @VendorUID
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORDETAILTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_VENDORDETAILTEMP]
	-- add more stored procedure parameters here
	@VendorUID INT
AS
BEGIN
	-- body of the stored procedure
	SELECT tvc.UID,
		tvc.REFID,
		tvc.VENDOR_ID AS VENDOR_ID,
		tvc.CREATOR AS CREATOR,
		tvc.CREATOR_DEPT AS CREATOR_DEPT,
		tvc.CREATOR_UNIT AS CREATOR_UNIT,
		tvc.CREATOR_ROLE AS CREATOR_ROLE,
		tvc.REQUEST_DT AS REQUEST_DT,
		tvc.DOC_DATE_FORM1 AS DOC_DATE_FORM1,
		tvc.CONF_CONTRACTOR_NM AS CONF_CONTRACTOR_NM,
		tvc.CONF_OPERATION_TO_OUTSOURCE AS CONF_OPERATION_TO_OUTSOURCE,
		tvc.CONF_CATEGORY AS CONF_CATEGORY,
		tvc.CONF_DEPT_CD AS CONF_DEPT_CD,
		tvc.CONF_BRIEF_EXPLAIN_CONTRACT AS CONF_BRIEF_EXPLAIN_CONTRACT,
		conf1.ITEM AS ITEM_1,
		conf1.SUBMITTED AS SUBMITTED_1,
		conf1.REASON AS REASON_1,
		conf1.CHECKED AS CHECKED_1,
		conf1.REASON_CHECKED AS REASON_CHECKED_1,
		conf2.ITEM AS ITEM_2,
		conf2.SUBMITTED AS SUBMITTED_2,
		conf2.REASON AS REASON_2,
		conf2.CHECKED AS CHECKED_2,
		conf2.REASON_CHECKED AS REASON_CHECKED_2,
		conf3.ITEM AS ITEM_3,
		conf3.SUBMITTED AS SUBMITTED_3,
		conf3.REASON AS REASON_3,
		conf3.CHECKED AS CHECKED_3,
		conf3.REASON_CHECKED AS REASON_CHECKED_3,
		conf4.ITEM AS ITEM_4,
		conf4.SUBMITTED AS SUBMITTED_4,
		conf4.REASON AS REASON_4,
		conf4.CHECKED AS CHECKED_4,
		conf4.REASON_CHECKED AS REASON_CHECKED_4,
		conf5.ITEM AS ITEM_5,
		conf5.SUBMITTED AS SUBMITTED_5,
		conf5.REASON AS REASON_5,
		conf5.CHECKED AS CHECKED_5,
		conf5.REASON_CHECKED AS REASON_CHECKED_5,
		conf6.ITEM AS ITEM_6,
		conf6.SUBMITTED AS SUBMITTED_6,
		conf6.REASON AS REASON_6,
		conf6.CHECKED AS CHECKED_6,
		conf6.REASON_CHECKED AS REASON_CHECKED_6,
		conf7.ITEM AS ITEM_7,
		conf7.SUBMITTED AS SUBMITTED_7,
		conf7.REASON AS REASON_7,
		conf7.CHECKED AS CHECKED_7,
		conf7.REASON_CHECKED AS REASON_CHECKED_7,
		conf8.ITEM AS ITEM_8,
		conf8.SUBMITTED AS SUBMITTED_8,
		conf8.REASON AS REASON_8,
		conf8.CHECKED AS CHECKED_8,
		conf8.REASON_CHECKED AS REASON_CHECKED_8,
		conf9.ITEM AS ITEM_9,
		conf9.SUBMITTED AS SUBMITTED_9,
		conf9.REASON AS REASON_9,
		conf9.CHECKED AS CHECKED_9,
		conf9.REASON_CHECKED AS REASON_CHECKED_9,
		conf10.ITEM AS ITEM_10,
		conf10.SUBMITTED AS SUBMITTED_10,
		conf10.REASON AS REASON_10,
		conf10.CHECKED AS CHECKED_10,
		conf10.REASON_CHECKED AS REASON_CHECKED_10,
		conf11.ITEM AS ITEM_11,
		conf11.SUBMITTED AS SUBMITTED_11,
		conf11.REASON AS REASON_11,
		conf11.CHECKED AS CHECKED_11,
		conf11.REASON_CHECKED AS REASON_CHECKED_11,
		conf12.ITEM AS ITEM_12,
		conf12.SUBMITTED AS SUBMITTED_12,
		conf12.REASON AS REASON_12,
		conf12.CHECKED AS CHECKED_12,
		conf12.REASON_CHECKED AS REASON_CHECKED_12,
		conf13.ITEM AS ITEM_13,
		conf13.SUBMITTED AS SUBMITTED_13,
		conf13.REASON AS REASON_13,
		conf13.CHECKED AS CHECKED_13,
		conf13.REASON_CHECKED AS REASON_CHECKED_13,
		conf14.ITEM AS ITEM_14,
		conf14.SUBMITTED AS SUBMITTED_14,
		conf14.REASON AS REASON_14,
		conf14.CHECKED AS CHECKED_14,
		conf14.REASON_CHECKED AS REASON_CHECKED_14,
		conf15.ITEM AS ITEM_15,
		conf15.SUBMITTED AS SUBMITTED_15,
		conf15.REASON AS REASON_15,
		conf15.CHECKED AS CHECKED_15,
		conf15.REASON_CHECKED AS REASON_CHECKED_15,
		tvc.CONF_DECISION AS CONF_DECISION,
		tvc.CONF_GAD_COMMENT AS CONF_GAD_COMMENT,
		-- tvc.CONF_DEPCONF_DECISIONT_CD AS CONF_DEPCONF_DECISIONT_CD,
		tvc.DOC_DATE_FORM2 AS DOC_DATE_FORM2,
		tvc.EXAM_NAME_OF_GROUP AS EXAM_NAME_OF_GROUP,
		tvc.EXAM_CORE_BUSINESS AS EXAM_CORE_BUSINESS,
		tvc.EXAM_REL_VENDOR AS EXAM_REL_VENDOR,
		tvc.EXAM_REL_VENDOR_REMARK AS EXAM_REL_VENDOR_REMARK,
		tvc.EXAM_HAS_ENGAGED_BEFORE AS EXAM_HAS_ENGAGED_BEFORE,
		tvc.EXAM_HAS_ENGAGED_SINCE AS EXAM_HAS_ENGAGED_SINCE,
		tvc.EXAM_BACKGROUND AS EXAM_BACKGROUND,
		tvc.EXAM_VENDOR_A AS EXAM_VENDOR_A,
		tvc.EXAM_VENDOR_B AS EXAM_VENDOR_B,
		exam1.PARAMETER AS EXAM_ITEM_1,
		exam1.UID AS EXAM_UID_1,
		exam1.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_1,
		exam1.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_1,
		exam1.REMARK AS EXAM_REMARK_1,
		exam2.PARAMETER AS EXAM_ITEM_2,
		exam2.UID AS EXAM_UID_2,
		exam2.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_2,
		exam2.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_2,
		exam2.REMARK AS EXAM_REMARK_2,
		exam3.PARAMETER AS EXAM_ITEM_3,
		exam3.UID AS EXAM_UID_3,
		exam3.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_3,
		exam3.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_3,
		exam3.REMARK AS EXAM_REMARK_3,
		exam4.PARAMETER AS EXAM_ITEM_4,
		exam4.UID AS EXAM_UID_4,
		exam4.COMPARE_VENDOR_A AS COMPARE_VENDOR_A_4,
		exam4.COMPARE_VENDOR_B AS COMPARE_VENDOR_B_4,
		exam4.REMARK AS EXAM_REMARK_4,
		tvc.EXAM_OVERALL_CONCLUSION AS EXAM_OVERALL_CONCLUSION,
		tvc.RISK_ANNUAL_BILLING AS RISK_ANNUAL_BILLING,
		tvc.RISK_SPECIAL_RELATED AS RISK_SPECIAL_RELATED,
		tvc.RISK_CPC_ASSESSMENT AS RISK_CPC_ASSESSMENT,
		tvc.RISK_PROVIDE_ACCESS AS RISK_PROVIDE_ACCESS,
		tvc.RISK_AS_VENDOR_A AS RISK_AS_VENDOR_A,
		tvc.RISK_AS_VENDOR_B AS RISK_AS_VENDOR_B,
		risk1.PARAMETER AS ASSESS_ITEM_1,
		risk1.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_1,
		risk1.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_1,
		risk1.REMARK AS ASSESS_REMARK_1,
		risk2.PARAMETER AS ASSESS_ITEM_2,
		risk2.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_2,
		risk2.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_2,
		risk2.REMARK AS ASSESS_REMARK_2,
		risk3.PARAMETER AS ASSESS_ITEM_3,
		risk3.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_3,
		risk3.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_3,
		risk3.REMARK AS ASSESS_REMARK_3,
		risk4.PARAMETER AS ASSESS_ITEM_4,
		risk4.ASSESS_VENDOR_A AS ASSESS_VENDOR_A_4,
		risk4.ASSESS_VENDOR_B AS ASSESS_VENDOR_B_4,
		risk4.REMARK AS ASSESS_REMARK_4,
		tvc.RISK_AML_COMMENT AS RISK_AML_COMMENT,
		tvc.RISK_CATEGORY AS RISK_CATEGORY,
		tvc.RISK_RMD_RECOMMENDATION AS RISK_RMD_RECOMMENDATION,
		tvc.RISK_SCREENING_RESULT AS RISK_SCREENING_RESULT,
		-- rmc.COMMENT AS RISK_MANAGEMENT_COMMENT,
		tvc.REG_PROVIDED_SERVICE AS REG_PROVIDED_SERVICE,
		tvc.REG_CONTRACT_NM AS REG_CONTRACT_NM,
		tvc.VENDOR_NAME AS VENDOR_NAME,
		tvc.REG_LEGAL_STATUS AS REG_LEGAL_STATUS,
		tvc.REG_NPWP_NO AS REG_NPWP_NO,
		tvc.REG_ADDRESS AS REG_ADDRESS,
		tvc.REG_CITY AS REG_CITY,
		tvc.REG_POSTAL AS REG_POSTAL,
		tvc.REG_WEBSITE AS REG_WEBSITE,
		tvc.REG_EMAIL AS REG_EMAIL,
		tvc.REG_PHONE_1 AS REG_PHONE_1,
		tvc.REG_PHONE_EXT_1 AS REG_PHONE_EXT_1,
		tvc.REG_PHONE_2 AS REG_PHONE_2,
		tvc.REG_PHONE_EXT_2 AS REG_PHONE_EXT_2,
		tvc.REG_FAX_1 AS REG_FAX_1,
		tvc.REG_FAX_2 AS REG_FAX_2,
		tvc.REG_CP_NM_1 AS REG_CP_NM_1,
		tvc.REG_CP_POSITION_1 AS REG_CP_POSITION_1,
		tvc.REG_CP_NM_2 AS REG_CP_NM_2,
		tvc.REG_CP_POSITION_2 AS REG_CP_POSITION_2,
		tvc.REG_PAYMENT_TYPE AS REG_PAYMENT_TYPE,
		tvc.REG_REASON_CASH AS REG_REASON_CASH,
		tvc.REG_ACC_HOLDER_NM AS REG_ACC_HOLDER_NM,
		tvc.REG_ACC_NO AS REG_ACC_NO,
		tvc.REG_ACC_BANK AS REG_ACC_BANK,
		tvc.DOC_DATE_FORM4A AS DOC_DATE_FORM4A,
		sheet1.UID AS SHEET_ID_1,
		sheet1.POSITION AS SHEET_POSITION_1,
		sheet1.NAME AS SHEET_NAME_1,
		sheet1.NATIONALITY AS SHEET_NATIONALITY_1,
		sheet1.SHARE AS SHEET_SHARE_1,
		sheet2.UID AS SHEET_ID_2,
		sheet2.POSITION AS SHEET_POSITION_2,
		sheet2.NAME AS SHEET_NAME_2,
		sheet2.NATIONALITY AS SHEET_NATIONALITY_2,
		sheet2.SHARE AS SHEET_SHARE_2,
		sheet3.UID AS SHEET_ID_3,
		sheet3.POSITION AS SHEET_POSITION_3,
		sheet3.NAME AS SHEET_NAME_3,
		sheet3.NATIONALITY AS SHEET_NATIONALITY_3,
		sheet3.SHARE AS SHEET_SHARE_3,
		sheet4.UID AS SHEET_ID_4,
		sheet4.POSITION AS SHEET_POSITION_4,
		sheet4.NAME AS SHEET_NAME_4,
		sheet4.NATIONALITY AS SHEET_NATIONALITY_4,
		sheet4.SHARE AS SHEET_SHARE_4,
		sheet5.UID AS SHEET_ID_5,
		sheet5.POSITION AS SHEET_POSITION_5,
		sheet5.NAME AS SHEET_NAME_5,
		sheet5.NATIONALITY AS SHEET_NATIONALITY_5,
		sheet5.SHARE AS SHEET_SHARE_5,
		sheet6.UID AS SHEET_ID_6,
		sheet6.POSITION AS SHEET_POSITION_6,
		sheet6.NAME AS SHEET_NAME_6,
		sheet6.NATIONALITY AS SHEET_NATIONALITY_6,
		sheet6.SHARE AS SHEET_SHARE_6,
		sheet7.UID AS SHEET_ID_7,
		sheet7.POSITION AS SHEET_POSITION_7,
		sheet7.NAME AS SHEET_NAME_7,
		sheet7.NATIONALITY AS SHEET_NATIONALITY_7,
		sheet7.SHARE AS SHEET_SHARE_7,
		sheet8.UID AS SHEET_ID_8,
		sheet8.POSITION AS SHEET_POSITION_8,
		sheet8.NAME AS SHEET_NAME_8,
		sheet8.NATIONALITY AS SHEET_NATIONALITY_8,
		sheet8.SHARE AS SHEET_SHARE_8,
		sheet9.UID AS SHEET_ID_9,
		sheet9.POSITION AS SHEET_POSITION_9,
		sheet9.NAME AS SHEET_NAME_9,
		sheet9.NATIONALITY AS SHEET_NATIONALITY_9,
		sheet9.SHARE AS SHEET_SHARE_9,
		sheet10.UID AS SHEET_ID_10,
		sheet10.POSITION AS SHEET_POSITION_10,
		sheet10.NAME AS SHEET_NAME_10,
		sheet10.NATIONALITY AS SHEET_NATIONALITY_10,
		sheet10.SHARE AS SHEET_SHARE_10,
		tvc.ASC_CONTRACTOR_JAPANESE AS ASC_CONTRACTOR_JAPANESE,
		tvc.ASC_SUBCONTRACT_OPERATION AS ASC_SUBCONTRACT_OPERATION,
		tvc.AGREE_START_DATE AS AGREE_START_DATE,
		tvc.AGREE_END_DATE AS AGREE_END_DATE,
		tvc.AGREE_CREATED_BY AS AGREE_CREATED_BY,
		tvc.AGREE_CREATED_DT AS AGREE_CREATED_DT,
		tvc.AGREE_MODIFIED_BY AS AGREE_MODIFIED_BY,
		tvc.AGREE_MODIFIED_DT AS AGREE_MODIFIED_DT,
		tvc.TERMINATION AS TERMINATION,
		tvc.TERMINATION_BY AS TERMINATION_BY,
		tvc.TERMINATION_DT AS TERMINATION_DT,
		tvc.REGISTRATION_DT AS REGISTRATION_DT,
		tvc.APPROVAL_STS AS APPROVAL_STS,
		tvc.IS_ACTIVE AS IS_ACTIVE,
		tvc.CREATED_BY AS CREATED_BY,
		tvc.CREATED_DT AS CREATED_DT,
		tvc.CREATED_HOST AS CREATED_HOST,
		tvc.MODIFIED_BY AS MODIFIED_BY,
		tvc.MODIFIED_DT AS MODIFIED_DT,
		tvc.MODIFIED_HOST AS MODIFIED_HOST
	FROM TR_VENDOR_CONTROL_TEMP AS tvc
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf1 ON conf1.DOC_ID = tvc.UID
		AND conf1.SEQ = 1
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf2 ON conf2.DOC_ID = tvc.UID
		AND conf2.SEQ = 2
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf3 ON conf3.DOC_ID = tvc.UID
		AND conf3.SEQ = 3
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf4 ON conf4.DOC_ID = tvc.UID
		AND conf4.SEQ = 4
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf5 ON conf5.DOC_ID = tvc.UID
		AND conf5.SEQ = 5
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf6 ON conf6.DOC_ID = tvc.UID
		AND conf6.SEQ = 6
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf7 ON conf7.DOC_ID = tvc.UID
		AND conf7.SEQ = 7
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf8 ON conf8.DOC_ID = tvc.UID
		AND conf8.SEQ = 8
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf9 ON conf9.DOC_ID = tvc.UID
		AND conf9.SEQ = 9
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf10 ON conf10.DOC_ID = tvc.UID
		AND conf10.SEQ = 10
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf11 ON conf11.DOC_ID = tvc.UID
		AND conf11.SEQ = 11
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf12 ON conf12.DOC_ID = tvc.UID
		AND conf12.SEQ = 12
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf13 ON conf13.DOC_ID = tvc.UID
		AND conf13.SEQ = 13
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf14 ON conf14.DOC_ID = tvc.UID
		AND conf14.SEQ = 14
	LEFT JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS conf15 ON conf15.DOC_ID = tvc.UID
		AND conf15.SEQ = 15
	LEFT JOIN TR_VC_EXAM_COMPARISON_TEMP AS exam1 ON exam1.DOC_ID = tvc.UID
		AND exam1.SEQ_NO = 1
	LEFT JOIN TR_VC_EXAM_COMPARISON_TEMP AS exam2 ON exam2.DOC_ID = tvc.UID
		AND exam2.SEQ_NO = 2
	LEFT JOIN TR_VC_EXAM_COMPARISON_TEMP AS exam3 ON exam3.DOC_ID = tvc.UID
		AND exam3.SEQ_NO = 3
	LEFT JOIN TR_VC_EXAM_COMPARISON_TEMP AS exam4 ON exam4.DOC_ID = tvc.UID
		AND exam4.SEQ_NO = 4
	LEFT JOIN TR_VC_RISK_ASSESSMENT_TEMP AS risk1 ON risk1.DOC_ID = tvc.UID
		AND risk1.SEQ_NO = 1
	LEFT JOIN TR_VC_RISK_ASSESSMENT_TEMP AS risk2 ON risk2.DOC_ID = tvc.UID
		AND risk2.SEQ_NO = 2
	LEFT JOIN TR_VC_RISK_ASSESSMENT_TEMP AS risk3 ON risk3.DOC_ID = tvc.UID
		AND risk3.SEQ_NO = 3
	LEFT JOIN TR_VC_RISK_ASSESSMENT_TEMP AS risk4 ON risk4.DOC_ID = tvc.UID
		AND risk4.SEQ_NO = 4
	-- LEFT JOIN TR_VC_RISK_MANAGEMENT_COMMENT AS rmc ON rmc.DOC_ID = tvc.UID
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet1 ON sheet1.DOC_ID = tvc.UID
		AND sheet1.SEQ_NO = 1
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet2 ON sheet2.DOC_ID = tvc.UID
		AND sheet2.SEQ_NO = 2
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet3 ON sheet3.DOC_ID = tvc.UID
		AND sheet3.SEQ_NO = 3
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet4 ON sheet4.DOC_ID = tvc.UID
		AND sheet4.SEQ_NO = 4
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet5 ON sheet5.DOC_ID = tvc.UID
		AND sheet5.SEQ_NO = 5
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet6 ON sheet6.DOC_ID = tvc.UID
		AND sheet6.SEQ_NO = 6
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet7 ON sheet7.DOC_ID = tvc.UID
		AND sheet7.SEQ_NO = 7
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet8 ON sheet8.DOC_ID = tvc.UID
		AND sheet8.SEQ_NO = 8
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet9 ON sheet9.DOC_ID = tvc.UID
		AND sheet9.SEQ_NO = 9
	LEFT JOIN TR_VC_ASC_CHECKSHEET_TEMP AS sheet10 ON sheet10.DOC_ID = tvc.UID
		AND sheet10.SEQ_NO = 10
	WHERE tvc.UID = @VendorUID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST]
AS
BEGIN

SELECT [UID]
      ,[VENDOR_ID]
      ,[VENDOR_NAME]
      ,[CREATOR]
      ,[CREATOR_DEPT]
	  ,[CREATOR_DEPT] 
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      , FORMAT ([REQUEST_DT], 'dd/MM/yyyy ') as [REQUEST_DT]
      ,[DOC_DATE_FORM1]
      ,[CONF_CONTRACTOR_NM]
      ,[CONF_OPERATION_TO_OUTSOURCE]
      ,[CONF_CATEGORY]
      ,[CONF_DEPT_CD]
      ,[CONF_BRIEF_EXPLAIN_CONTRACT]
      ,[CONF_DECISION]
      ,[CONF_GAD_COMMENT]
	  ,FORMAT ([DOC_DATE_FORM2], 'dd/MM/yyyy ') as [DOC_DATE_FORM2]
      ,[EXAM_NAME_OF_GROUP]
      ,[EXAM_CORE_BUSINESS]
      ,[EXAM_REL_VENDOR]
      ,[EXAM_REL_VENDOR_REMARK]
      ,[EXAM_HAS_ENGAGED_BEFORE]
      ,[EXAM_HAS_ENGAGED_SINCE]
      ,[EXAM_BACKGROUND]
      ,[EXAM_VENDOR_A]
      ,[EXAM_VENDOR_B]
      ,[EXAM_OVERALL_CONCLUSION]
      ,[RISK_ANNUAL_BILLING]
      ,[RISK_SPECIAL_RELATED]
      ,[RISK_CPC_ASSESSMENT]
      ,[RISK_PROVIDE_ACCESS]
      ,[RISK_AS_VENDOR_A]
      ,[RISK_AS_VENDOR_B]
      ,[RISK_AML_COMMENT]
      ,[RISK_CATEGORY]
      ,[RISK_RMD_RECOMMENDATION]
      ,[RISK_SCREENING_RESULT]
      ,[REG_PROVIDED_SERVICE]
      ,[REG_CONTRACT_NM]
      ,[REG_LEGAL_STATUS]
      ,[REG_NPWP_NO]
      ,[REG_ADDRESS]
      ,[REG_CITY]
      ,[REG_POSTAL]
      ,[REG_WEBSITE]
      ,[REG_EMAIL]
      ,[REG_PHONE_1]
      ,[REG_PHONE_EXT_1]
      ,[REG_PHONE_2]
      ,[REG_PHONE_EXT_2]
      ,[REG_FAX_1]
      ,[REG_FAX_2]
      ,[REG_CP_NM_1]
      ,[REG_CP_POSITION_1]
      ,[REG_CP_NM_2]
      ,[REG_CP_POSITION_2]
      ,[REG_PAYMENT_TYPE]
      ,[REG_REASON_CASH]
      ,[REG_ACC_HOLDER_NM]
      ,[REG_ACC_NO]
      ,[REG_ACC_BANK]
      ,[DOC_DATE_FORM4A]
      ,[ASC_CONTRACTOR_JAPANESE]
      ,[ASC_SUBCONTRACT_OPERATION]
      ,[AGREE_START_DATE]
      ,[AGREE_END_DATE]
      ,[AGREE_CREATED_BY]
      ,[AGREE_CREATED_DT]
      ,[AGREE_MODIFIED_BY]
      ,[AGREE_MODIFIED_DT]
      ,[TERMINATION]
      ,[TERMINATION_BY]
	  , FORMAT ([TERMINATION_DT], 'dd/MM/yyyy ') as [TERMINATION_DT]
	  , FORMAT ([REGISTRATION_DT], 'dd/MM/yyyy ') as [REGISTRATION_DT]
      ,[APPROVAL_STS]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]
      ,[MODIFIED_BY]
      ,[MODIFIED_DT]
      ,[MODIFIED_HOST]
      ,[L_DOC_ID]
	  ,[STATUS_FLOW] 
	  ,(SELECT ISNULL(NULLIF(LongName, ''),LongName) as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as [DEPARTEMEN_LONG_NAME] 
  FROM [TR_VENDOR_CONTROL]
  ORDER BY [CREATED_DT] DESC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_ACTIVITY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_ACTIVITY]
	-- Add the parameters for the stored procedure here
	@VendorId VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @EDITID BIGINT
	--IF (@HistoryType = 'SETTLEMENTTEMP')
	--BEGIN
	--	SELECT @EDITID = EDITDRAFTID
	--	FROM TR_WORKFLOWPROCESS
	--	WHERE DOCID = @RefId
	--		AND WFACTION = 'EDIT'
	--		AND WFTASK = 'SETTLEMENT'
	--		AND ISACTIVE = 1
	--	SET @RefId = @EDITID
	--	SET @HistoryType = 'SETTLEMENT'
	--END
	--ELSE IF (@HistoryType = 'REGISTRATIONTEMP')
	--BEGIN
	--	SELECT @EDITID = EDITDRAFTID
	--	FROM TR_WORKFLOWPROCESS
	--	WHERE DOCID = @RefId
	--		AND WFACTION = 'EDIT'
	--		AND WFTASK = 'REGISTRATION'
	--		AND ISACTIVE = 1
	--	SET @RefId = @EDITID
	--	SET @HistoryType = 'REGISTRATION'
	--END
	-- Insert statements for procedure here
	SELECT tdh.UID,
		'' AS SequenceNumber,
		tdh.FORM_TYPE,
		tdh.DOC_ID,
		tdh.VENDOR_ID,
		tdh.APPROVAL_STS,
		tdh.CREATED_BY,
		FORMAT(tdh.REGISTRATION_DT, 'dd/MMM/yyyy ') AS REGISTRATION_DT,
		--tdh.REGISTRATION_DT,
		--tdh.CREATED_DT,
		FORMAT(tdh.CREATED_DT, 'dd/MMM/yyyy ') AS [CREATED_DT],
		tdh.CREATED_HOST,
		tdh.MODIFIED_BY,
		FORMAT(tdh.MODIFIED_DT, 'dd/MMM/yyyy ') AS MODIFIED_DT,
		--tdh.MODIFIED_DT,
		tdh.MODIFIED_HOST
	FROM dbo.[TR_VENDOR_CONTROL_ACTIVITY] AS tdh
	WHERE 1 = 1
		AND ISNULL(tdh.IS_DRAFT, 0) <> 1
		AND tdh.DOC_ID = @VendorId
		--AND tdh.APPROVAL_STS='Approval Completed' 
		--AND IS_ACTIVE = 1
		AND ISNULL(tdh.IS_DRAFT, 0) = 0
	ORDER BY tdh.CREATED_DT DESC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_ATTACH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_ATTACH]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN
if ISNUMERIC(@Id)=1
BEGIN
SELECT * FROM [TR_ATTACHMENT] WHERE [DOC_ID]=@Id ORDER BY [UID] ASC
END
ELSE
BEGIN
SELECT * FROM [TR_ATTACHMENT] ORDER BY [UID] DESC 
END
	

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_CHECKDOC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_CHECKDOC]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN
if ISNUMERIC(@Id)=1
BEGIN
SELECT * FROM [TR_VC_CONF_DOC_CHECKLIST] WHERE [DOC_ID]=@Id ORDER BY [SEQ] ASC
END
ELSE
BEGIN
SELECT * FROM [TR_VC_CONF_DOC_CHECKLIST] ORDER BY [UID] DESC 
END
	

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_CHECKSHEET]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_CHECKSHEET]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN
if ISNUMERIC(@Id)=1
BEGIN
SELECT * FROM [TR_VC_ASC_CHECKSHEET] WHERE [DOC_ID]=@Id ORDER BY [SEQ_NO] ASC 
END
ELSE
BEGIN
SELECT * FROM [TR_VC_ASC_CHECKSHEET]
END
	

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_DEPT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_DEPT]
AS
BEGIN

SELECT  
ID as ID
,ShortName as SHORTNAME
,LongName as LONGNAME
,DivisionID as DIVISIONID 
FROM dbo.[SN_BTMUDirectory_Department] 
WHERE ShortName <> 'ALL' 
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_EXAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_EXAM]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
AS
BEGIN
if ISNUMERIC(@Id)=1
BEGIN
SELECT * FROM [TR_VC_EXAM_COMPARISON] WHERE [DOC_ID]=@Id
END
ELSE
BEGIN
SELECT * FROM [TR_VC_EXAM_COMPARISON]
END
	

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_MANAGEMENTCOMMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_MANAGEMENTCOMMENT]
	-- add more stored procedure parameters here
	@VendorUID INT
AS
BEGIN
	-- body of the stored procedure
	SELECT UID,
		DOC_ID,
		USER_APV,
		COMMENT,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST
	FROM TR_VC_RISK_MANAGEMENT_COMMENT
	WHERE DOC_ID = @VendorUID
		AND IS_ACTIVE = 1
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_PARAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_PARAM]
	-- Add the parameters for the stored procedure here
	@Id varchar(50)  
	,@VENDOR_ID VARCHAR(50)
	,@VENDOR_NAME VARCHAR(255)
	,@CREATOR VARCHAR(255)
	,@CREATOR_DEPT VARCHAR(50)
	,@CONF_CONTRACTOR_NM VARCHAR(255)
	,@TERMINATION VARCHAR(50)
	,@REG_ACC_NO VARCHAR(50)
	,@APPROVAL_STS VARCHAR(50)
	,@page INT 
	,@size INT
	,@sort nVARCHAR(50)
	,@ord VARCHAR(10)
AS
BEGIN

	DECLARE @offset INT
	DECLARE @newsize INT
	DECLARE @sql nvarchar(MAX)
	DECLARE @varoffset nvarchar(10)
	DECLARE @varnewsize nvarchar(10)
	
	SET @page=@page-1;
	if (@page<1)
	BEGIN
		SET @offset=0
		SET @newsize=@size
		SET @varoffset='0';
		SET @varnewsize=@size;
	END
	ELSE
	BEGIN
		SET @offset=@page*@size
		SET @newsize=@size-1
		SET @varoffset=@offset;
		SET @varnewsize=@newsize;
	END

	SET NOCOUNT ON;
	SET @sql='SELECT [UID] 
		,[VENDOR_ID]
		,[CONF_CONTRACTOR_NM]
		,[VENDOR_NAME]
		,[CONF_CATEGORY] 
		,[CREATOR]
		,[APPROVAL_STS]
		,(SELECT LongName as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as [DEPARTEMEN_LONG_NAME] 
		,(SELECT COUNT(UID) FROM [TR_VENDOR_CONTROL] ) as TotalRow 
	FROM [TR_VENDOR_CONTROL] 
	WHERE 1=1 ';


	if (ISNUMERIC(@Id)=1 )
	BEGIN
		SET @sql=@sql+ ' AND [UID]='+@Id+' ' ;
	END


	if (@VENDOR_ID IS NOT NULL OR @VENDOR_ID !='')
	BEGIN
		SET @sql=@sql + ' AND [VENDOR_ID] LIKE ''%'+@VENDOR_ID +'%'' ';
	END

	if (@VENDOR_NAME IS NOT NULL OR @VENDOR_NAME !='')
	BEGIN
		SET @sql=@sql + ' AND [VENDOR_NAME] LIKE ''%'+@VENDOR_NAME+'%'' ';
	END

	if (@CREATOR IS NOT NULL OR @CREATOR !='' )
	BEGIN
		SET @sql=@sql + ' AND [CREATOR] LIKE ''%'+@CREATOR+'%'' ';
	END

	if (@APPROVAL_STS IS NOT NULL)
	BEGIN
		SET @sql=@sql + ' AND [APPROVAL_STS] LIKE ''%'+@APPROVAL_STS+'%'' ';
	END

	if (@ord='ASC')
	BEGIN
		SET @ord='ASC';
	END
	ELSE
	BEGIN
		SET @ord='DESC';
	END
	
	SET @sql=@sql+' ORDER BY '+@sort+' '+@ord; 
	/*
	SELECT [UID] 
		,[VENDOR_ID]
		,[CONF_CONTRACTOR_NM]
		,[VENDOR_NAME]
		,[CONF_CATEGORY] 
		,[CREATOR]
		,[APPROVAL_STS]
	FROM [TR_VENDOR_CONTROL] 
	WHERE 1=1  
	ORDER BY [UID] DESC  
	OFFSET @offset ROWS  
	FETCH NEXT @newsize ROWS ONLY;
	*/
	
	EXECUTE( @sql + ' OFFSET '+@varoffset+' ROWS FETCH NEXT '+@varnewsize+' ROWS ONLY');
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_PARAM_FIELD]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_PARAM_FIELD]
	-- Add the parameters for the stored procedure here
	@fi VARCHAR(50),
	@cr VARCHAR(MAX) 
AS
BEGIN
/*
	SELECT UID,
		VENDOR_ID,
		VENDOR_NAME,
		CREATOR,
		CREATOR_DEPT,
		CREATOR_UNIT,
		CREATOR_ROLE,
		REQUEST_DT,
		DOC_DATE_FORM1,
		CONF_CONTRACTOR_NM,
		CONF_OPERATION_TO_OUTSOURCE,
		CONF_CATEGORY,
		CONF_DEPT_CD,
		CONF_BRIEF_EXPLAIN_CONTRACT,
		CONF_DECISION,
		CONF_GAD_COMMENT,
		DOC_DATE_FORM2,
		EXAM_NAME_OF_GROUP,
		EXAM_CORE_BUSINESS,
		EXAM_REL_VENDOR,
		EXAM_REL_VENDOR_REMARK,
		EXAM_HAS_ENGAGED_BEFORE,
		EXAM_HAS_ENGAGED_SINCE,
		EXAM_BACKGROUND,
		EXAM_VENDOR_A,
		EXAM_VENDOR_B,
		EXAM_OVERALL_CONCLUSION,
		RISK_ANNUAL_BILLING,
		RISK_SPECIAL_RELATED,
		RISK_CPC_ASSESSMENT,
		RISK_PROVIDE_ACCESS,
		RISK_AS_VENDOR_A,
		RISK_AS_VENDOR_B,
		RISK_AML_COMMENT,
		RISK_CATEGORY,
		RISK_RMD_RECOMMENDATION,
		RISK_SCREENING_RESULT,
		REG_PROVIDED_SERVICE,
		REG_CONTRACT_NM,
		REG_LEGAL_STATUS,
		REG_NPWP_NO,
		REG_ADDRESS,
		REG_CITY,
		REG_POSTAL,
		REG_WEBSITE,
		REG_EMAIL,
		REG_PHONE_1,
		REG_PHONE_EXT_1,
		REG_PHONE_2,
		REG_PHONE_EXT_2,
		REG_FAX_1,
		REG_FAX_2,
		REG_CP_NM_1,
		REG_CP_POSITION_1,
		REG_CP_NM_2,
		REG_CP_POSITION_2,
		REG_PAYMENT_TYPE,
		REG_REASON_CASH,
		REG_ACC_HOLDER_NM,
		REG_ACC_NO,
		REG_ACC_BANK,
		DOC_DATE_FORM4A,
		ASC_CONTRACTOR_JAPANESE,
		ASC_SUBCONTRACT_OPERATION,
		AGREE_START_DATE,
		AGREE_END_DATE,
		AGREE_CREATED_BY,
		AGREE_CREATED_DT,
		AGREE_MODIFIED_BY,
		AGREE_MODIFIED_DT,
		TERMINATION,
		TERMINATION_BY,
		TERMINATION_DT,
		REGISTRATION_DT,
		APPROVAL_STS,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST,
		L_DOC_ID,
		STATUS_FLOW
	FROM [TR_VENDOR_CONTROL]
	WHERE 1=1 AND UID = @Id
*/
DECLARE @key varchar(10);
DECLARE @sql nvarchar(MAX);
SET @key='-';
SET @sql='SELECT [UID] 
		,[VENDOR_ID]
		,[CONF_CONTRACTOR_NM]
		,[VENDOR_NAME]
		,[CONF_CATEGORY] 
		,[CREATOR]
		,[CREATOR_DEPT]
		,[APPROVAL_STS]
		,(SELECT LongName as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as [DEPARTEMEN_LONG_NAME] 
		,(SELECT COUNT(UID) FROM [TR_VENDOR_CONTROL] ) as TotalRow 
	FROM [TR_VENDOR_CONTROL] 
	WHERE 1=1  ';


	if CHARINDEX(',', @cr)>0
		BEGIN
		
		if @fi='CREATOR_DEPT' 
		
		
			SET @sql=@sql +' AND 
			CASE 
			WHEN (charindex('''+@key+''', [CREATOR_DEPT])-1)>1 THEN 
				LEFT([CREATOR_DEPT],charindex('''+@key+''', [CREATOR_DEPT])-1) 
			ELSE [CREATOR_DEPT] END IN ('+@cr+') ORDER BY UID DESC';
			
			--SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ORDER BY UID DESC';

		
		ELSE
		
			SET @sql=@sql +' AND '+@fi +' IN ('+@cr+') ORDER BY UID DESC';
		END
	ELSE
		BEGIN
		--SET @cr = Replace(@cr, '%[^0-9a-zA-Z]%', '')
		SET @sql=@sql + ' AND ['+@fi+'] LIKE ''%'+@cr +'%''  ORDER BY UID DESC';
	END

	EXECUTE( @sql );
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_PARAM_KEYWORD]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_PARAM_KEYWORD]
	-- Add the parameters for the stored procedure here
	@keyword varchar(50) 
	,@page INT 
	,@size INT
	,@sort nVARCHAR(50)
	,@ord VARCHAR(10)
AS
BEGIN
	DECLARE @offset INT
	DECLARE @newsize INT
	DECLARE @sql nvarchar(MAX)
	SET @page=@page-1;
	if (@page<1)
	BEGIN
		SET @offset=0
		SET @newsize=@size
	END
	ELSE
	BEGIN
		SET @offset=@page*@size
		SET @newsize=@size-1
	END

	SET NOCOUNT ON;

	SELECT [UID] 
		,[VENDOR_ID]
		,[CONF_CONTRACTOR_NM]
		,[VENDOR_NAME]
		,[CONF_CATEGORY] 
		,[CREATOR]
		,[APPROVAL_STS]
		,(SELECT LongName as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as [DEPARTEMEN_LONG_NAME] 
	FROM [TR_VENDOR_CONTROL] 
	WHERE 1=1  
	AND [VENDOR_ID] LIKE '%'+@keyword+'%' 
	OR [CONF_CONTRACTOR_NM] LIKE '%'+@keyword+'%'
	OR [VENDOR_NAME] LIKE '%'+@keyword+'%' 
	OR [CONF_CATEGORY] LIKE '%'+@keyword+'%' 
	OR [CREATOR] LIKE '%'+@keyword+'%' 
	OR [APPROVAL_STS] LIKE '%'+@keyword+'%' 
	ORDER BY ''+@sort+' '+@ord+'' 
	OFFSET @offset ROWS  
	FETCH NEXT @newsize ROWS ONLY;
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_SEARCH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_SEARCH]
	-- Add the parameters for the stored procedure here
	@Id VARCHAR(50)
AS
BEGIN

	SELECT [UID]
      ,[VENDOR_ID]
      ,[VENDOR_NAME]
      ,[CREATOR]
      ,[CREATOR_DEPT]
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      , FORMAT ([REQUEST_DT], 'dd/MMM/yyyy ') as [REQUEST_DT]
	  , FORMAT ([DOC_DATE_FORM1], 'dd/MMM/yyyy ') as [DOC_DATE_FORM1]
      --,[DOC_DATE_FORM1]
      ,[CONF_CONTRACTOR_NM]
      ,[CONF_OPERATION_TO_OUTSOURCE]
      ,[CONF_CATEGORY]
      ,[CONF_DEPT_CD]
      ,[CONF_BRIEF_EXPLAIN_CONTRACT]
      ,[CONF_DECISION]
      ,[CONF_GAD_COMMENT]
	  ,FORMAT ([DOC_DATE_FORM2], 'dd/MMM/yyyy ') as [DOC_DATE_FORM2]
      ,[EXAM_NAME_OF_GROUP]
      ,[EXAM_CORE_BUSINESS]
      ,[EXAM_REL_VENDOR]
      ,[EXAM_REL_VENDOR_REMARK]
      ,[EXAM_HAS_ENGAGED_BEFORE]
	  ,CASE
            WHEN [EXAM_HAS_ENGAGED_SINCE] = '1900-01-01' OR [EXAM_HAS_ENGAGED_SINCE] =NULL
               THEN ''
               ELSE FORMAT ([EXAM_HAS_ENGAGED_SINCE], 'dd/MMM/yyyy ')
       END as [EXAM_HAS_ENGAGED_SINCE] 
      ,[EXAM_BACKGROUND]
      ,[EXAM_VENDOR_A]
      ,[EXAM_VENDOR_B]
      ,[EXAM_OVERALL_CONCLUSION]
      ,[RISK_ANNUAL_BILLING]
      ,[RISK_SPECIAL_RELATED]
      ,[RISK_CPC_ASSESSMENT]
      ,[RISK_PROVIDE_ACCESS]
      ,[RISK_AS_VENDOR_A]
      ,[RISK_AS_VENDOR_B]
      ,[RISK_AML_COMMENT]
      ,[RISK_CATEGORY]
      ,[RISK_RMD_RECOMMENDATION]
      ,[RISK_SCREENING_RESULT]
      ,[REG_PROVIDED_SERVICE]
      ,[REG_CONTRACT_NM]
      ,[REG_LEGAL_STATUS]
      ,[REG_NPWP_NO]
      ,[REG_ADDRESS]
      ,[REG_CITY]
      ,[REG_POSTAL]
      ,[REG_WEBSITE]
      ,[REG_EMAIL]
      ,[REG_PHONE_1]
      ,[REG_PHONE_EXT_1]
      ,[REG_PHONE_2]
      ,[REG_PHONE_EXT_2]
      ,[REG_FAX_1]
      ,[REG_FAX_2]
      ,[REG_CP_NM_1]
      ,[REG_CP_POSITION_1]
      ,[REG_CP_NM_2]
      ,[REG_CP_POSITION_2]
      ,[REG_PAYMENT_TYPE]
      ,[REG_REASON_CASH]
      ,[REG_ACC_HOLDER_NM]
      ,[REG_ACC_NO]
      ,[REG_ACC_BANK]
	  ,FORMAT ([DOC_DATE_FORM4A], 'dd/MMM/yyyy ') as [DOC_DATE_FORM4A]
      ,[ASC_CONTRACTOR_JAPANESE]
      ,[ASC_SUBCONTRACT_OPERATION]
	  ,FORMAT ([AGREE_START_DATE], 'dd/MMM/yyyy ') as [AGREE_START_DATE]
      ,FORMAT ([AGREE_END_DATE], 'dd/MMM/yyyy ') as [AGREE_END_DATE] 
      ,[AGREE_CREATED_BY]
      ,[AGREE_CREATED_DT]
      ,[AGREE_MODIFIED_BY]
      ,[AGREE_MODIFIED_DT]
      ,[TERMINATION]
      ,[TERMINATION_BY]
	  , FORMAT ([TERMINATION_DT], 'dd/MMM/yyyy ') as [TERMINATION_DT]
	  , FORMAT ([REGISTRATION_DT], 'dd/MMM/yyyy ') as [REGISTRATION_DT]
      ,[APPROVAL_STS]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]
      ,[MODIFIED_BY]
      ,[MODIFIED_DT]
      ,[MODIFIED_HOST]
      ,[L_DOC_ID]
	  ,[STATUS_FLOW] 
	  ,(SELECT ISNULL(NULLIF(LongName, ''),LongName) as LongName  FROM dbo.[SN_BTMUDirectory_Department] WHERE ShortName=[CREATOR_DEPT] ) as [DEPARTEMEN_LONG_NAME] 
	FROM [TR_VENDOR_CONTROL]
	WHERE 1=1 AND UID = @Id 
	

END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_TMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_TMP]
AS
BEGIN

SELECT [UID]
      ,[VENDOR_ID]
      ,[VENDOR_NAME]
      ,[CREATOR]
      ,[CREATOR_DEPT]
      ,[CREATOR_UNIT]
      ,[CREATOR_ROLE]
      , FORMAT ([REQUEST_DT], 'dd/MM/yyyy ') as [REQUEST_DT]
      ,[DOC_DATE_FORM1]
      ,[CONF_CONTRACTOR_NM]
      ,[CONF_OPERATION_TO_OUTSOURCE]
      ,[CONF_CATEGORY]
      ,[CONF_DEPT_CD]
      ,[CONF_BRIEF_EXPLAIN_CONTRACT]
      ,[CONF_DECISION]
      ,[CONF_GAD_COMMENT]
	  ,FORMAT ([DOC_DATE_FORM2], 'dd/MM/yyyy ') as [DOC_DATE_FORM2]
      ,[EXAM_NAME_OF_GROUP]
      ,[EXAM_CORE_BUSINESS]
      ,[EXAM_REL_VENDOR]
      ,[EXAM_REL_VENDOR_REMARK]
      ,[EXAM_HAS_ENGAGED_BEFORE]
      ,[EXAM_HAS_ENGAGED_SINCE]
      ,[EXAM_BACKGROUND]
      ,[EXAM_VENDOR_A]
      ,[EXAM_VENDOR_B]
      ,[EXAM_OVERALL_CONCLUSION]
      ,[RISK_ANNUAL_BILLING]
      ,[RISK_SPECIAL_RELATED]
      ,[RISK_CPC_ASSESSMENT]
      ,[RISK_PROVIDE_ACCESS]
      ,[RISK_AS_VENDOR_A]
      ,[RISK_AS_VENDOR_B]
      ,[RISK_AML_COMMENT]
      ,[RISK_CATEGORY]
      ,[RISK_RMD_RECOMMENDATION]
      ,[RISK_SCREENING_RESULT]
      ,[REG_PROVIDED_SERVICE]
      ,[REG_CONTRACT_NM]
      ,[REG_LEGAL_STATUS]
      ,[REG_NPWP_NO]
      ,[REG_ADDRESS]
      ,[REG_CITY]
      ,[REG_POSTAL]
      ,[REG_WEBSITE]
      ,[REG_EMAIL]
      ,[REG_PHONE_1]
      ,[REG_PHONE_EXT_1]
      ,[REG_PHONE_2]
      ,[REG_PHONE_EXT_2]
      ,[REG_FAX_1]
      ,[REG_FAX_2]
      ,[REG_CP_NM_1]
      ,[REG_CP_POSITION_1]
      ,[REG_CP_NM_2]
      ,[REG_CP_POSITION_2]
      ,[REG_PAYMENT_TYPE]
      ,[REG_REASON_CASH]
      ,[REG_ACC_HOLDER_NM]
      ,[REG_ACC_NO]
      ,[REG_ACC_BANK]
      ,[DOC_DATE_FORM4A]
      ,[ASC_CONTRACTOR_JAPANESE]
      ,[ASC_SUBCONTRACT_OPERATION]
      ,[AGREE_START_DATE]
      ,[AGREE_END_DATE]
      ,[AGREE_CREATED_BY]
      ,[AGREE_CREATED_DT]
      ,[AGREE_MODIFIED_BY]
      ,[AGREE_MODIFIED_DT]
      ,[TERMINATION]
      ,[TERMINATION_BY]
	  , FORMAT ([TERMINATION_DT], 'dd/MM/yyyy ') as [TERMINATION_DT]
	  , FORMAT ([REGISTRATION_DT], 'dd/MM/yyyy ') as [REGISTRATION_DT]
      ,[APPROVAL_STS]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]
      ,[MODIFIED_BY]
      ,[MODIFIED_DT]
      ,[MODIFIED_HOST]
      ,[L_DOC_ID]
	  ,[STATUS_FLOW] 
  FROM [TR_VENDOR_CONTROL]
  ORDER BY [CREATED_DT] DESC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLIST_UNIT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLIST_UNIT]
AS
BEGIN

SELECT  
ID as ID
,ShortName as SHORTNAME
,LongName as LONGNAME
,DepartmentID as DEPARTEMENTID 
FROM dbo.[SN_BTMUDirectory_Unit] 
WHERE ShortName <> '*' 
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLISTACTIVITY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLISTACTIVITY]
@DocId INT
AS
BEGIN
	SELECT P.DESCS AS FORM_NAME, VCA.* 
	FROM TR_VENDOR_CONTROL_ACTIVITY VCA
	LEFT JOIN MS_PARAMETER P 
	ON P.[GROUP] = 'FORM_TYPE_LIST'
	AND P.VALUE = VCA.FORM_TYPE 
	WHERE VCA.IS_ACTIVE = 1 AND ISNULL(VCA.IS_DRAFT,0) = 0
	AND VCA.DOC_ID = @DocId
	ORDER BY FORM_NAME
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLISTINDEX]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_VENDORLISTINDEX]
	-- PARAMETER
	@PageNo INT = 1,
	@PageSize INT = 10,
	@SearchBy VARCHAR(255) = '',
	@Value VARCHAR(2000) = '',
	@DeptInChargeValues VARCHAR(2000) = '',
	@ApprovalStatusValues VARCHAR(2000) = '',
	@FullSearch VARCHAR(2000) = '',
	@SortBy VARCHAR(255) = ''
AS
BEGIN
	SELECT VC.UID,
		VC.VENDOR_ID,
		VC.VENDOR_NAME,
		VC.CREATOR,
		VC.CREATOR_DEPT,
		VC.CREATOR_UNIT,
		VC.CREATOR_ROLE,
		VC.REQUEST_DT,
		VC.DOC_DATE_FORM1,
		VC.CONF_CONTRACTOR_NM,
		VC.CONF_OPERATION_TO_OUTSOURCE,
		VC.CONF_CATEGORY,
		VC.CONF_DEPT_CD,
        DEPT.ShortName + ' - ' + DEPT.LongName AS CONF_DEPT_CD_DESC,
		VC.CONF_BRIEF_EXPLAIN_CONTRACT,
		VC.CONF_DECISION,
		VC.CONF_GAD_COMMENT,
		VC.DOC_DATE_FORM2,
		VC.EXAM_NAME_OF_GROUP,
		VC.EXAM_CORE_BUSINESS,
		VC.EXAM_REL_VENDOR,
		VC.EXAM_REL_VENDOR_REMARK,
		VC.EXAM_HAS_ENGAGED_BEFORE,
		VC.EXAM_HAS_ENGAGED_SINCE,
		VC.EXAM_BACKGROUND,
		VC.EXAM_VENDOR_A,
		VC.EXAM_VENDOR_B,
		VC.EXAM_OVERALL_CONCLUSION,
		VC.RISK_ANNUAL_BILLING,
		VC.RISK_SPECIAL_RELATED,
		VC.RISK_CPC_ASSESSMENT,
		VC.RISK_PROVIDE_ACCESS,
		VC.RISK_AS_VENDOR_A,
		VC.RISK_AS_VENDOR_B,
		VC.RISK_AML_COMMENT,
		VC.RISK_CATEGORY,
		VC.RISK_RMD_RECOMMENDATION,
		VC.RISK_SCREENING_RESULT,
		VC.REG_PROVIDED_SERVICE,
		VC.REG_CONTRACT_NM,
		VC.REG_LEGAL_STATUS,
		VC.REG_NPWP_NO,
		VC.REG_ADDRESS,
		VC.REG_CITY,
		VC.REG_POSTAL,
		VC.REG_WEBSITE,
		VC.REG_EMAIL,
		VC.REG_PHONE_1,
		VC.REG_PHONE_EXT_1,
		VC.REG_PHONE_2,
		VC.REG_PHONE_EXT_2,
		VC.REG_FAX_1,
		VC.REG_FAX_2,
		VC.REG_CP_NM_1,
		VC.REG_CP_POSITION_1,
		VC.REG_CP_NM_2,
		VC.REG_CP_POSITION_2,
		VC.REG_PAYMENT_TYPE,
		VC.REG_REASON_CASH,
		VC.REG_ACC_HOLDER_NM,
		VC.REG_ACC_NO,
		VC.REG_ACC_BANK,
		VC.DOC_DATE_FORM4A,
		VC.ASC_CONTRACTOR_JAPANESE,
		VC.ASC_SUBCONTRACT_OPERATION,
		VC.AGREE_START_DATE,
		VC.AGREE_END_DATE,
		VC.AGREE_CREATED_BY,
		VC.AGREE_CREATED_DT,
		VC.AGREE_MODIFIED_BY,
		VC.AGREE_MODIFIED_DT,
		VC.TERMINATION,
		VC.TERMINATION_BY,
		VC.TERMINATION_DT,
		VC.REGISTRATION_DT,
		VC.APPROVAL_STS,
		VC.IS_ACTIVE,
		VC.CREATED_BY,
		VC.CREATED_DT,
		VC.CREATED_HOST,
		VC.MODIFIED_BY,
		VC.MODIFIED_DT,
		VC.MODIFIED_HOST,
		VC.L_DOC_ID,
		VC.STATUS_FLOW
	FROM [TR_VENDOR_CONTROL] AS VC
    LEFT JOIN DBO.SN_BTMUDirectory_Department as DEPT ON VC.CONF_DEPT_CD = DEPT.ShortName
	WHERE VC.IS_ACTIVE = 1
		AND ISNULL(VC.STATUS_FLOW, '') NOT IN ('DRAFT')
		AND (
			CONCAT (
				VC.VENDOR_ID,
				VC.VENDOR_NAME,
				VC.CONF_CONTRACTOR_NM,
				VC.CONF_CATEGORY,
				VC.CONF_DEPT_CD,
				VC.CREATOR,
				VC.APPROVAL_STS
				) LIKE '%' + @FullSearch + '%'
			OR ISNULL(@FullSearch, '') = ''
			)
		AND (
			CASE @SearchBy
				WHEN 'CREATOR'
					THEN VC.CREATOR
				WHEN 'CONF_CONTRACTOR_NM'
					THEN VC.CONF_CONTRACTOR_NM
				WHEN 'VENDOR_ID'
					THEN VC.VENDOR_ID
				WHEN 'VENDOR_NAME'
					THEN VC.VENDOR_NAME
						--WHEN 'TERMINATION' THEN IIF(ISNULL(TERMINATION,0) = 1,'Yes', 'No')
				END LIKE '%' + @Value + '%'
			OR ISNULL(@Value, '') = ''
			OR @SearchBy = 'CONF_DEPT_CD'
			OR @SearchBy = 'APPROVAL_STS'
			OR @SearchBy = 'TERMINATION'
			)
		AND (
			@DeptInChargeValues LIKE '%' + CASE 
				WHEN CHARINDEX('-', VC.CONF_DEPT_CD) > 0
					THEN LEFT(VC.CONF_DEPT_CD, CHARINDEX('-', VC.CONF_DEPT_CD) - 1)
				ELSE VC.CONF_DEPT_CD
				END + '%'
			OR ISNULL(@DeptInChargeValues, '') = ''
			)
		AND (
			@ApprovalStatusValues LIKE '%' + ISNULL(NULLIF(VC.APPROVAL_STS, ''), 'Draft') + '%'
			OR ISNULL(@ApprovalStatusValues, '') = ''
			)
		AND (
			CASE 
				WHEN @SearchBy = 'TERMINATION'
					THEN 1
				ELSE ISNULL(VC.TERMINATION, 0)
				END = ISNULL(VC.TERMINATION, 0)
			)
	ORDER BY VC.CREATED_DT DESC
END


GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLISTINDEX_20220120]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_VENDORLISTINDEX_20220120]
@PageNo INT = 1,
@PageSize INT = 10,
@SearchBy VARCHAR(255) = '',
@Value VARCHAR(2000) = '',
@DeptInChargeValues VARCHAR(2000) = '',
@ApprovalStatusValues VARCHAR(2000) = '',
@FullSearch VARCHAR(2000) = '',
@SortBy VARCHAR(255) = ''
AS
BEGIN

	--DECLARE
	--@PageNo INT = 1,
	--@PageSize INT = 10,
	--@SearchBy VARCHAR(255) = 'CONF_DEPT_CD',
	--@Value VARCHAR(2000) = 'YES',
	--@DeptInChargeValues VARCHAR(2000) = 'GAD,SKAI',
	--@ApprovalStatusValues VARCHAR(2000) = '',
	--@FullSearch VARCHAR(2000) = '',
	--@SortBy VARCHAR(255) = ''

	SELECT * 
	FROM [TR_VENDOR_CONTROL]
	WHERE IS_ACTIVE = 1 AND ISNULL(STATUS_FLOW,'') NOT IN ('DRAFT')
	AND ( CONCAT(VENDOR_ID, VENDOR_NAME, REG_CONTRACT_NM, CONF_CATEGORY, CONF_DEPT_CD, CREATOR, APPROVAL_STS) 
				Like '%'+@FullSearch+'%' OR ISNULL(@FullSearch, '') = '' )
	AND ( CASE @SearchBy
			WHEN 'CREATOR' THEN CREATOR
			WHEN 'REG_CONTRACT_NM' THEN REG_CONTRACT_NM
			WHEN 'VENDOR_ID' THEN VENDOR_ID
			WHEN 'VENDOR_NAME' THEN VENDOR_NAME
			--WHEN 'TERMINATION' THEN IIF(ISNULL(TERMINATION,0) = 1,'Yes', 'No')
			END Like '%'+@Value+'%' 
			OR ISNULL(@Value,'')=''
			OR @SearchBy = 'CONF_DEPT_CD'
			OR @SearchBy = 'APPROVAL_STS'
			OR @SearchBy = 'TERMINATION'
		)
	AND ( @DeptInChargeValues like '%'+
	CASE WHEN CHARINDEX('-', CONF_DEPT_CD) > 0 THEN LEFT(CONF_DEPT_CD, CHARINDEX('-', CONF_DEPT_CD)-1) ELSE CONF_DEPT_CD END
	+'%' OR ISNULL(@DeptInChargeValues, '') = '' )
	AND ( @ApprovalStatusValues like '%'+ISNULL(NULLIF(APPROVAL_STS,''),'Draft')+'%' OR ISNULL(@ApprovalStatusValues, '') = '' )
	AND ( CASE WHEN @SearchBy = 'TERMINATION' THEN 1 ELSE ISNULL(TERMINATION,0) END = ISNULL(TERMINATION,0) )
	
	ORDER BY CREATED_DT DESC
	
END

GO
/****** Object:  StoredProcedure [dbo].[UDPS_VENDORLISTREPORT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPS_VENDORLISTREPORT]
	@STATUS [VARCHAR](2000) = 'ALL',
	@RISK_CATEGORY [VARCHAR](2000) = 'ALL',
	@LAST_REVIEW_START_DATE DATE = '01-01-1900',
	@LAST_REVIEW_END_DATE DATE = '01-01-1900',
	@NEXT_REVIEW_START_DATE DATE = '01-01-1900',
	@NEXT_REVIEW_END_DATE DATE = '01-01-1900'
AS
BEGIN
SELECT

	ROW_NUMBER() OVER (ORDER BY V.VENDOR_ID) AS [No]
	,V.VENDOR_ID AS [VendorID]
	,V.DOC_DATE_FORM1 AS [RegisteredDate]
	,V.CONF_DEPT_CD AS [DepartmentInCharge]
	,V.REG_PROVIDED_SERVICE AS [TypeOfService]
	,V.CONF_CONTRACTOR_NM AS [Vendor]
	,CASE WHEN V.RISK_SPECIAL_RELATED='1' THEN 'Y' ELSE '' END AS [SpecialRelatedCompanyY]
	,CASE WHEN V.RISK_SPECIAL_RELATED='2' THEN 'N' ELSE '' END AS [SpecialRelatedCompanyN]
	,PM.[DESCS] AS [PreviousRiskCategory]
	,NULL AS [RiskCategoryReviewedDecemberYYYY]
	,DT.REGISTRATION_DT AS [LatestConductPeriodicalReviewDate]
	,NULL AS [LatestStatus]
	,V.CREATOR AS [PIC]
	,NULL AS [AgreementStartDate]
	,NULL AS [AgreementEndDate]
	,NULL AS [TotalBillingPaymentDecemberYYYY]
FROM TR_VENDOR_CONTROL V
LEFT JOIN (
	SELECT MAX(UID) AS ID, DOC_ID FROM TR_VENDOR_CONTROL_ACTIVITY
	WHERE APPROVAL_STS = 'APPROVAL COMPLETED'
	AND FORM_TYPE = 'PERIODICAL_REVIEW'
	-- NAMBAHIN FILTER TAMBAHAN DR SCREEN
	GROUP BY DOC_ID 
) AC ON AC.DOC_ID = V.UID
LEFT JOIN TR_VENDOR_CONTROL_ACTIVITY DT
	ON DT.UID = AC.ID
LEFT JOIN MS_PARAMETER PM
	ON PM.[GROUP] = 'MAP_RISK_CATEGORY'
	AND PM.[VALUE] = V.RISK_CATEGORY
WHERE V.IS_ACTIVE = 1 AND ISNULL(STATUS_FLOW, '') <> 'DRAFT'
	AND (V.APPROVAL_STS IN (SELECT ss.Name from splitstring(@STATUS,',') ss) OR @STATUS = 'ALL' )
	AND (ISNULL(PM.[DESCS],'') IN (SELECT src.Name from splitstring(@RISK_CATEGORY,',') src) OR @RISK_CATEGORY = 'ALL')
	AND (DT.REGISTRATION_DT >= @LAST_REVIEW_START_DATE OR @LAST_REVIEW_START_DATE = '01-01-1900')
	AND (DT.REGISTRATION_DT <= @LAST_REVIEW_END_DATE OR @LAST_REVIEW_END_DATE = '01-01-1900')
	AND (DT.REV_REVIEW_DT >= @NEXT_REVIEW_START_DATE OR @NEXT_REVIEW_START_DATE = '01-01-1900')
	AND (DT.REV_REVIEW_DT <= @NEXT_REVIEW_END_DATE OR @NEXT_REVIEW_END_DATE = '01-01-1900')
END






GO
/****** Object:  StoredProcedure [dbo].[UDPS_WAIVERAPPLICATION]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPS_WAIVERAPPLICATION]
	@Id INT
AS
BEGIN

SELECT VCA.[UID] AS Id
      ,VCA.[DOC_ID] AS DocId
      ,VCA.[FORM_TYPE] AS FormType
      ,VCA.[CREATOR] AS Creator
      ,VCA.[CREATOR_DEPT] AS CreatorDepartment
      ,VCA.[CREATOR_SECTION] AS CreatorSection
      ,VCA.[CREATOR_ROLE] AS CreatorRole
      ,VCA.[REQUEST_DT] AS RequestDate
      --,VCA.[VENDOR_ID] AS VendorId
	  ,VC.[VENDOR_NAME] AS VendorId
      ,VCA.[DOC_DATE_FORM6] AS DocDate
      ,VCA.[LGL_REQ_DEPT_CD] AS LegalReqDeptCode
      ,VCA.[LGL_SUBJECT] AS LegalSubject
      ,VCA.[LGL_FACTS] AS LegalFacts
      ,VCA.[LGL_SUPPORTING_DOC] AS LegalSupportingDoc
      ,VCA.[LGL_REQUEST] AS LegalRequest
      ,VCA.[LGL_DEADLINE] AS LegalDeadline
      ,VCA.[REV_REVIEW_DT] AS RevReviewDate
      ,VCA.[REV_GAD_RECOMMENDATION] AS RevGadRecommendation
      ,VCA.[REV_RMD_COMMENT] AS RevRmdComment
      ,VCA.[DOC_DATE_FORM5] 
      ,VCA.[CONT_DESC_PERFORMANCE] AS ContDescPerformance
      ,VCA.[CONT_BIDDING_RESULT] AS ContBiddingResult
      ,VCA.[CONT_REASON_UTILITY] AS ContReasonUtility 
      ,VCA.[CONT_REL_EXPLAIN] AS ContRelExplain
      ,VCA.[CONT_CHANGE_FORM4B] AS ContChangeForm4b
      ,VCA.[CONT_INTV_CREATOR_DGM] AS ContIntvCreatorDgm
      ,VCA.[CONT_INTV_GAD_DGM] AS ContIntvGadDgm
      ,VCA.[CONT_GAD_RECOMMENDATION] AS ContGadRecommendation
      ,VCA.[WAIV_PROVIDED_SERVICE] AS WaivProvidedService
      ,VCA.[WAIV_VENDOR_REFUSAL] AS WaivVendorRefusal
      ,VCA.[WAIV_REASON_UTILIZE] AS WaivReasonUtilize
      ,VCA.[WAIV_GAD_ASSESS] AS WaivGadAssess
      ,VCA.[WAIV_COMMENT] AS WaivComment
      ,VCA.[TERMINATION]
      ,VCA.[TERMINATION_BY]
      ,VCA.[TERMINATION_DT]
      ,VCA.[REGISTRATION_DT]
      ,VCA.[APPROVAL_STS] AS ApprovalStatus
      ,VCA.[IS_ACTIVE] AS IsActive
      ,VCA.[CREATED_BY] AS CreatedBy
      ,VCA.[CREATED_DT] AS CreatedDate
      ,VCA.[CREATED_HOST] AS CreatedHost
      ,VCA.[MODIFIED_BY] AS ModifiedBy
      ,VCA.[MODIFIED_DT] AS ModifiedDate
      ,VCA.[MODIFIED_HOST] AS ModifiedHost
  FROM TR_VENDOR_CONTROL_ACTIVITY AS VCA
  LEFT JOIN dbo.TR_VENDOR_CONTROL AS VC ON VC.UID = VCA.DOC_ID
	WHERE VCA.UID = @Id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_CONTVENDORAPPSTATUS]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_CONTVENDORAPPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_VENDOR_CONTROL_ACTIVITY
	SET APPROVAL_STS = @AppStatus,
		REGISTRATION_DT = GETDATE()
	WHERE [UID] = @DOCID

	IF (@AppStatus = 'Approval Completed')
	BEGIN
		DECLARE @VendorUID BIGINT = 0

		SELECT @VendorUID = DOC_ID
		FROM TR_VENDOR_CONTROL_ACTIVITY
		WHERE [UID] = @DOCID

		IF EXISTS (SELECT TOP 1 1 FROM TR_VENDOR_CONTROL WHERE APPROVAL_STS = 'Terminated' AND UID = @VendorUID)
		BEGIN
			UPDATE TR_VENDOR_CONTROL
			SET APPROVAL_STS = 'Approval Completed', 
				TERMINATION = 0
			WHERE [UID] = @VendorUID

			-- UPDATE NEW REQ USER 20221002 FROM SELPA KEEP ACTIVITY "TERMINATED"
			-- Update All activity form Status Based On Latest Approval status
			--UPDATE VCA 
			--SET VCA.APPROVAL_STS = IIF(DOC_HIST.STATUS = 'Approve', 'Approval Completed', DOC_HIST.STATUS),
			--	VCA.TERMINATION = 0
			--FROM TR_VENDOR_CONTROL_ACTIVITY VCA
			--INNER JOIN (
			--	SELECT A.UID, A.DOC_ID, A.HIST_TYPE, A.STATUS, A.CREATED_DT
			--	FROM TR_DOCUMENT_HISTORY A
			--	INNER JOIN ( 
			--		SELECT DOC_ID, HIST_TYPE, MAX(CREATED_DT) LAST_DATE 
			--		FROM TR_DOCUMENT_HISTORY 
			--		GROUP BY DOC_ID, HIST_TYPE
			--	) B 
			--	ON A.DOC_ID = B.DOC_ID 
			--	AND A.HIST_TYPE = B.HIST_TYPE
			--	AND A.CREATED_DT = B.LAST_DATE
			--	WHERE A.HIST_TYPE IN ('LEGAL_REVIEW', 'PERIODICAL_REVIEW', 'CONTINUING_REVIEW', 'WAIVER')
			--) DOC_HIST
			--ON DOC_HIST.DOC_ID = VCA.UID
			--AND CASE 
			--WHEN DOC_HIST.HIST_TYPE = 'LEGAL_REVIEW' THEN 'LEGAL_REQUEST'
			--WHEN DOC_HIST.HIST_TYPE = 'CONTINUING_REVIEW' THEN 'CONTINUING_VENDOR'
			--ELSE DOC_HIST.HIST_TYPE END  = VCA.FORM_TYPE
			--WHERE VCA.DOC_ID = @VendorUID AND VCA.IS_ACTIVE = 1
			--AND ISNULL(VCA.IS_DRAFT, 0) <> 1

			--UPDATE TR_VENDOR_CONTROL_ACTIVITY
			--SET APPROVAL_STS = 'Approval Completed', TERMINATION = 0
			--WHERE DOC_ID = @VendorUID
			--AND IS_ACTIVE = 1

			----UPDATE TR_VENDOR_CONTROL_ACTIVITY
			----SET REGISTRATION_DT = GETDATE()
			----WHERE [UID] = @DOCID
		END
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_DEACTIVEWF]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_DEACTIVEWF]
	-- add more stored procedure parameters here
	@WFID BIGINT
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_WORKFLOWPROCESS
	SET ISACTIVE = 0,
		MODIFIED_BY = 'SYSTEM',
		MODIFIED_DT = GETDATE()
	WHERE [UID] = @WFID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_DEPARTMENTPIC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPU_DEPARTMENTPIC]

	-- Add the parameters for the stored procedure here

	@Id INT
	,@DepartmentCode VARCHAR(50)
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	UPDATE dbo.MS_DEPT_PIC_CONFIG_HDR

	SET DEPT_CD = @DepartmentCode
		,MODIFIED_BY = @ModifiedBy
		,MODIFIED_DT = @ModifiedDate
		,MODIFIED_HOST = @ModifiedHost

	WHERE [UID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_DEPARTMENTPICDTL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPU_DEPARTMENTPICDTL]

	-- Add the parameters for the stored procedure here

	@Id INT
	,@DeptCode VARCHAR(50)
	,@MemberCode VARCHAR(2000)
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;

	DECLARE @MemberCodeList TABLE (
		id INT IDENTITY(1, 1),
		nama VARCHAR(200)
		)

		
	-- Insert statements for procedure here

		IF (@MemberCode = '')
		BEGIN
			DELETE dbo.MS_DEPT_PIC_CONFIG_DTL
			WHERE DEPT_CD = @DeptCode
		END
		ELSE
		BEGIN
			INSERT INTO @MemberCodeList(nama)
			SELECT *
			FROM dbo.splitstring(@MemberCode, ',')

			DELETE dbo.MS_DEPT_PIC_CONFIG_DTL
			WHERE DEPT_CD = @DeptCode
				AND MEMBER_CD NOT IN (
					SELECT nama
					FROM @MemberCodeList AS rc
					)

			INSERT INTO dbo.MS_DEPT_PIC_CONFIG_DTL (
				DEPT_CD,
				MEMBER_CD,
				CREATED_BY,
				CREATED_DT,
				CREATED_HOST
				)
			SELECT @DeptCode,
				rc.nama,
				@ModifiedBy,
				@ModifiedDate,
				@ModifiedHost
			FROM @MemberCodeList AS rc
			WHERE rc.nama NOT IN (
					SELECT tafrc.MEMBER_CD
					FROM dbo.MS_DEPT_PIC_CONFIG_DTL AS tafrc
					WHERE tafrc.DEPT_CD = @DeptCode
					)
		END
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_DRAFTCONTINUINGVENDOR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DRAFTCONTINUINGVENDOR]
@Id INT,
@DocID INT,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@ContDescPerformance varchar(8000),
@ContBiddingResult varchar(8000),
@ContReasonUtility varchar(8000),
@ContRelExplain varchar(8000),
@ContChangeForm4b varchar(8000),
@ContIntvCreatorDgm varchar(8000),
@ContIntvGadDgm varchar(8000),
@ContGadRecommendation varchar(8000),
@ApprovalStatus varchar(50),
@IsActive bit,
@ModifiedBy varchar(50),
@ModifiedDate datetime,
@ModifiedHost varchar(20),
@IsDraft bit = 1,
@ContComment varchar(8000) = ''
AS
BEGIN
	DECLARE @ContinuingId int = @Id
	
	SELECT 
	 @FormType = 'CONTINUING_VENDOR'
	,@VendorId = COALESCE(VENDOR_ID, '') 
	,@ApprovalStatus = NULL
	,@IsActive = 1 
	FROM TR_VENDOR_CONTROL WHERE UID = @DocID

	IF @IsDraft = 0
	BEGIN
		SET @ApprovalStatus = 'Awaiting Approval'

		--DECLARE @SeqNo int

		--SELECT @SeqNo = COALESCE(COUNT(1),0)
		--FROM TR_DOCUMENT_HISTORY
		--WHERE DOC_ID = @ContinuingId AND HIST_TYPE = 'CONTINUING_REVIEW'

		--INSERT INTO TR_DOCUMENT_HISTORY
		--(DOC_ID, HIST_TYPE, SEQ_NO, VENDOR_ID, 
		--ACTION_BY, ACTION_DT, STATUS, COMMENT, 
		--CREATED_BY, CREATED_DT, CREATED_HOST)
	
		--SELECT @ContinuingId, 'CONTINUING_REVIEW', @SeqNo+1, @VendorId,
		--@Creator, @RequestDate, 'Submit', @ContComment,
		--@ModifiedBy, @ModifiedDate, @ModifiedHost
	END

	UPDATE [TR_VENDOR_CONTROL_ACTIVITY] SET
	
		 [DOC_ID] = @DocID
		,[FORM_TYPE] = @FormType
		,[CREATOR] = @Creator
		,[CREATOR_DEPT] = @CreatorDepartment
		,[CREATOR_SECTION] = @CreatorSection
		,[CREATOR_ROLE] = @CreatorRole
		,[REQUEST_DT] = @RequestDate
		,[VENDOR_ID] = @VendorId
		,[DOC_DATE_FORM5] = @DocDate 
		,[CONT_DESC_PERFORMANCE] = @ContDescPerformance
        ,[CONT_BIDDING_RESULT] = @ContBiddingResult
        ,[CONT_REASON_UTILITY] = @ContReasonUtility
        ,[CONT_REL_EXPLAIN] = @ContRelExplain
        ,[CONT_CHANGE_FORM4B] = @ContChangeForm4b
        ,[CONT_INTV_CREATOR_DGM] = @ContIntvCreatorDgm
        ,[CONT_INTV_GAD_DGM] = @ContIntvGadDgm
        ,[CONT_GAD_RECOMMENDATION] = @ContGadRecommendation
		,[APPROVAL_STS] = @ApprovalStatus
		,[REGISTRATION_DT] = @DocDate
		,[IS_ACTIVE] = @IsActive
		,[MODIFIED_BY] = @ModifiedBy
		,[MODIFIED_DT] = @ModifiedDate
		,[MODIFIED_HOST] = @ModifiedHost
		,[IS_DRAFT] = @IsDraft

		WHERE UID = @Id

	SELECT @Id
END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_DRAFTLEGALREQUEST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DRAFTLEGALREQUEST]
@Id int,
@DocID int,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@LegalReqDeptCode varchar(50),
@LegalSubject varchar(255),
@LegalFacts varchar(8000),
@LegalSupportingDoc varchar(8000),
@LegalRequest varchar(8000),
@LegalDeadline varchar(8000),
@ApprovalStatus varchar(50),
@IsActive bit,
@ModifiedBy varchar(50),
@ModifiedDate datetime,
@ModifiedHost varchar(20),
@IsDraft bit = 1,
@LegalComment varchar(8000) = ''
AS
BEGIN
	DECLARE @LegalId int = @Id
	
	SELECT 
	 @FormType = 'LEGAL_REQUEST'
	,@VendorId = COALESCE(VENDOR_ID, '')
	,@IsActive = 1
	,@ApprovalStatus = IIF(ISNULL(@IsDraft,0)= 0,'Awaiting Approval', null)
	FROM [TR_VENDOR_CONTROL] WHERE UID = @DocID

	--IF @IsDraft = 0
	--BEGIN
	--	SET @ApprovalStatus = 'Awaiting Approval'

	--	DECLARE @SeqNo int

	--	SELECT @SeqNo = COALESCE(COUNT(1),0)
	--	FROM TR_DOCUMENT_HISTORY
	--	WHERE DOC_ID = @LegalId AND HIST_TYPE = 'LEGAL_REVIEW'

	--	INSERT INTO TR_DOCUMENT_HISTORY
	--	(DOC_ID, HIST_TYPE, SEQ_NO, VENDOR_ID, 
	--	ACTION_BY, ACTION_DT, STATUS, COMMENT, 
	--	CREATED_BY, CREATED_DT, CREATED_HOST)
	
	--	SELECT @LegalId, 'LEGAL_REVIEW', @SeqNo+1, @VendorId,
	--	@Creator, @RequestDate, 'Submit', @LegalComment,
	--	@ModifiedBy, @ModifiedDate, @ModifiedHost
	--END

	UPDATE TR_VENDOR_CONTROL_ACTIVITY SET
		-- [DOC_ID] = @DocID
		--,[FORM_TYPE] = @FormType
		--,[CREATOR] = @Creator
		--,[CREATOR_DEPT] = @CreatorDepartment
		--,[CREATOR_SECTION] = @CreatorSection
		--,[CREATOR_ROLE] = @CreatorRole
		[REQUEST_DT] = @RequestDate
		--,[VENDOR_ID] = @VendorId
		,[DOC_DATE_FORM6] = @DocDate
		,[LGL_REQ_DEPT_CD] = @LegalReqDeptCode
		,[LGL_SUBJECT] = @LegalSubject
		,[LGL_FACTS] = @LegalFacts
		,[LGL_SUPPORTING_DOC] = @LegalSupportingDoc
		,[LGL_REQUEST] = @LegalRequest
		,[LGL_DEADLINE] = @LegalDeadline
		,[APPROVAL_STS] = @ApprovalStatus
		,[REGISTRATION_DT] = @DocDate
		,[IS_ACTIVE] = @IsActive
		,[MODIFIED_BY] = @ModifiedBy
		,[MODIFIED_DT] = @ModifiedDate
		,[MODIFIED_HOST] = @ModifiedHost
		,[IS_DRAFT] = @IsDraft

	WHERE UID = @Id

	SELECT @Id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_DRAFTPERIODICALREVIEW]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DRAFTPERIODICALREVIEW]
@Id INT,
@DocID INT,
@SequenceNumber INT,
@FormType varchar(30),
@Creator varchar(255),
@CreatorDepartment varchar(255),
@CreatorSection varchar(255),
@CreatorRole varchar(255),
@RequestDate date,
@VendorId varchar(30),
@DocDate date,
@NextPeriodicalReviewDate varchar(255),
@GADRecommendation varchar(255),
@RMDComment varchar(255),
@CommentBox varchar(255),
@ApprovalStatus varchar(50),
@IsActive bit,
@ModifiedBy varchar(50),
@ModifiedDate datetime,
@ModifiedHost varchar(20),
@IsDraft bit
AS
BEGIN
	DECLARE @PeriodicalId int = @Id
	
	SELECT 
	 @FormType = 'PERIODICAL_REVIEW'
	,@VendorId = COALESCE(VENDOR_ID, '')
	,@IsActive = 1
	FROM [TR_VENDOR_CONTROL] WHERE UID = @DocID

	IF @IsDraft = 0
	BEGIN
		SET @ApprovalStatus = 'Awaiting Approval'
		--DECLARE @SeqNo int

		--SELECT @SeqNo = COALESCE(COUNT(1),0)
		--FROM TR_DOCUMENT_HISTORY
		--WHERE DOC_ID = @PeriodicalId AND HIST_TYPE = 'PERIODICAL_REVIEW'

		--INSERT INTO TR_DOCUMENT_HISTORY
		--(DOC_ID, HIST_TYPE, SEQ_NO, VENDOR_ID, 
		--ACTION_BY, ACTION_DT, STATUS, COMMENT, 
		--CREATED_BY, CREATED_DT, CREATED_HOST)
	
		--SELECT @PeriodicalId, 'PERIODICAL_REVIEW', @SeqNo+1, @VendorId,
		--@Creator, @RequestDate, 'Submit', @CommentBox,
		--@ModifiedBy, @ModifiedDate, @ModifiedHost
	END

	UPDATE TR_VENDOR_CONTROL_ACTIVITY SET
		 [DOC_ID] = @DocID
		,[FORM_TYPE] = @FormType
		,[CREATOR] = @Creator
		,[CREATOR_DEPT] = @CreatorDepartment
		,[CREATOR_SECTION] = @CreatorSection
		,[CREATOR_ROLE] = @CreatorRole
		,[REQUEST_DT] = @RequestDate
		,[VENDOR_ID] = @VendorId
		,[DOC_DATE_FORM4] = @DocDate
		,[REV_REVIEW_DT] = @NextPeriodicalReviewDate
		,[REV_GAD_RECOMMENDATION] = @GADRecommendation
		,[REV_RMD_COMMENT] = @RMDComment
		,[APPROVAL_STS] = @ApprovalStatus
		,[REGISTRATION_DT] = @DocDate
		,[IS_ACTIVE] = @IsActive
		,[MODIFIED_BY] = @ModifiedBy
		,[MODIFIED_DT] = @ModifiedDate
		,[MODIFIED_HOST] = @ModifiedHost
		,[IS_DRAFT] = @IsDraft

	WHERE UID = @Id

	SELECT @Id

END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_DRAFTPERIODICALREVIEW_AML]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DRAFTPERIODICALREVIEW_AML] @DocID INT,
	@SequenceNumber INT,
	@Name VARCHAR(255),
	@Title VARCHAR(255),
	@IsActive BIT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [TR_VC_ACTIVITY_REV_AML]
	SET
		-- [DOC_ID] = @DocID,
		-- [SEQ_NO] = @SequenceNumber,
		[NAME] = @Name,
		[TITLE] = @Title,
		[IS_ACTIVE] = @IsActive,
		[MODIFIED_BY] = @ModifiedBy,
		[MODIFIED_DT] = @ModifiedDate,
		[MODIFIED_HOST] = @ModifiedHost
	WHERE [DOC_ID] = @DocID
		AND SEQ_NO = @SequenceNumber
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_DRAFTPERIODICALREVIEW_EXAMINED]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UDPU_DRAFTPERIODICALREVIEW_EXAMINED]
	----
	@DocID INT,
	@SequenceNumber INT,
	@Point VARCHAR(255),
	@Result BIT,
	@ResultComment VARCHAR(255),
	@IsActive BIT,
	@ModifiedBy VARCHAR(50),
	@ModifiedDate DATETIME,
	@ModifiedHost VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [TR_VC_ACTIVITY_REV_EXAMINED]
	SET
		--  [DOC_ID] = @DocID,
		-- 	[SEQ_NO] = @SequenceNumber,
		[POINT] = @Point,
		[RESULT] = @Result,
		[COMMENT] = @ResultComment,
		[IS_ACTIVE] = @IsActive,
		[MODIFIED_BY] = @ModifiedBy,
		[MODIFIED_DT] = @ModifiedDate,
		[MODIFIED_HOST] = @ModifiedHost
	WHERE [DOC_ID] = @DocID
		AND SEQ_NO = @SequenceNumber
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_LEGALREQUESTAPPSTATUS]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_LEGALREQUESTAPPSTATUS]
-- add more stored procedure parameters here
    @DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
    -- body of the stored procedure
    UPDATE TR_VENDOR_CONTROL_ACTIVITY
	SET APPROVAL_STS = @AppStatus
		,REGISTRATION_DT = GETDATE()
	WHERE [UID] = @DOCID

	--IF (@AppStatus = 'Approval Completed')
	--BEGIN
	--	UPDATE TR_VENDOR_CONTROL_ACTIVITY
	--	SET REGISTRATION_DT = GETDATE()
	--	WHERE [UID] = @DOCID
	--END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_PARAMETER]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================

-- Author:		<Author,,Name>

-- Create date: <Create Date,,>

-- Description:	<Description,,>

-- =============================================

CREATE PROCEDURE [dbo].[UDPU_PARAMETER]

	-- Add the parameters for the stored procedure here

	@Id INT

	,@Group VARCHAR(30)

	,@Value VARCHAR(5000)

	,@Description VARCHAR(8000)

	,@Sequence INT

	,@ModifiedBy VARCHAR(50)

	,@ModifiedDate DATETIME

	,@ModifiedHost VARCHAR(20)

AS

BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

	SET NOCOUNT ON;



	-- Insert statements for procedure here

	UPDATE dbo.MS_PARAMETER

	SET [GROUP] = @Group

		,VALUE = @Value

		,DESCS = @Description

		,SEQ = @Sequence

		,MODIFIED_BY = @ModifiedBy

		,MODIFIED_DT = @ModifiedDate

		,MODIFIED_HOST = @ModifiedHost

	WHERE [UID] = @Id

END




GO
/****** Object:  StoredProcedure [dbo].[UDPU_PERIODICALREVIEWAPPSTATUS]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_PERIODICALREVIEWAPPSTATUS]
-- add more stored procedure parameters here
    @DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
    -- body of the stored procedure
    UPDATE TR_VENDOR_CONTROL_ACTIVITY
	SET APPROVAL_STS = @AppStatus,
		REGISTRATION_DT = GETDATE()
	WHERE [UID] = @DOCID

	--IF (@AppStatus = 'Approval Completed')
	--BEGIN
	--	UPDATE TR_VENDOR_CONTROL_ACTIVITY
	--	SET REGISTRATION_DT = GETDATE()
	--	WHERE [UID] = @DOCID
	--END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_SETTING]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UDPU_SETTING]
	-- Add the parameters for the stored procedure here
	@Id INT
	,@RevCC VARCHAR(8000)
	,@RevBwdReminder BIT
	,@RevBwdStartedFrom INT
	,@RevBwdRecurringDays INT
	,@RevFwdReminder BIT
	,@RevFwdRecurringDays INT
	,@ContCC VARCHAR(8000)
	,@ContBwdReminder BIT
	,@ContBwdStartedFrom INT
	,@ContBwdRecurringDays INT
	,@ContFwdReminder BIT
	,@ContFwdRecurringDays INT
	,@ApvRmdReminder BIT
	,@ApvRmdRecurringDays INT
	,@GadDeptmentName VARCHAR(50)
	,@GadPicName VARCHAR(8000)
	,@RmdDeptmentName VARCHAR(50)
	,@RmdStaffName VARCHAR(8000)
	,@CpcDeptmentName VARCHAR(50)
	,@CpcStaffName VARCHAR(8000)
	,@FcdDeptmentName VARCHAR(50)
	,@FcdPicName VARCHAR(8000)
	,@LglDeptmentName VARCHAR(50)
	,@LglPicName VARCHAR(8000)
	,@ModifiedBy VARCHAR(50)
	,@ModifiedDate DATETIME
	,@ModifiedHost VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	UPDATE dbo.MS_SETTING
	SET REV_CC = @RevCC
		,REV_BWD_ACTIVE = @RevBwdReminder
		,REV_BWD_START_FROM = @RevBwdStartedFrom
		,REV_BWD_RECUR_DAY = @RevBwdRecurringDays
		,REV_FWD_ACTIVE = @RevFwdReminder
		,REV_FWD_RECUR_DAY = @RevFwdRecurringDays
		,CONT_CC = @ContCC
		,CONT_BWD_ACTIVE = @ContBwdReminder
		,CONT_BWD_START_FROM = @ContBwdStartedFrom
		,CONT_BWD_RECUR_DAY = @ContBwdRecurringDays
		,CONT_FWD_ACTIVE = @ContFwdReminder
		,CONT_FWD_RECUR_DAY = @ContFwdRecurringDays
		,APV_RMD_ACTIVE = @ApvRmdReminder
		,APV_RMD_RECUR_DAY = @ApvRmdRecurringDays
		,GAD_DEPT_CD = @GadDeptmentName
		,GAD_PIC = @GadPicName
		,RMD_DEPT_CD = @RmdDeptmentName
		,RMD_STAFF = @RmdStaffName
		,CPC_DEPT_CD = @CpcDeptmentName
		,CPC_STAFF = @CpcStaffName
		,FCD_DEPT_CD = @FcdDeptmentName
		,FCD_PIC = @FcdPicName
		,LGL_DEPT_CD = @LglDeptmentName
		,LGL_PIC = @LglPicName
		,MODIFIED_BY = @ModifiedBy
		,MODIFIED_DT = @ModifiedDate
		,MODIFIED_HOST = @ModifiedHost
	WHERE [UID] = @Id

	SELECT @Id
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_VC_RISK_ASSESSMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_VC_RISK_ASSESSMENT]

	-- Add the parameters for the stored procedure here

	@Id int
      ,@DOC_ID VARCHAR(50) 
      ,@SEQ_NO VARCHAR(50) 
      ,@PARAMETER VARCHAR(50) 
      ,@ASSESS_VENDOR_A VARCHAR(50) 
      ,@ASSESS_VENDOR_B VARCHAR(50) 
      ,@REMARK VARCHAR(50) 
      ,@IS_ACTIVE VARCHAR(50) 
      ,@MODIFIED_BY VARCHAR(50) 
      ,@MODIFIED_HOST VARCHAR(50) 

AS

BEGIN
	SET NOCOUNT ON;
	UPDATE dbo.[TR_VC_RISK_ASSESSMENT]
	SET [DOC_ID]=@DOC_ID
      ,[SEQ_NO]=@SEQ_NO
      ,[PARAMETER]=@PARAMETER
      ,[ASSESS_VENDOR_A]=@ASSESS_VENDOR_A
      ,[ASSESS_VENDOR_B]=@ASSESS_VENDOR_B
      ,[REMARK]=@REMARK
    ,[MODIFIED_BY]=@MODIFIED_BY
    ,[MODIFIED_DT]=GETDATE() 
    ,[MODIFIED_HOST]=@MODIFIED_HOST
	WHERE [UID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORAPPSTATUS]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_VENDORAPPSTATUS]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_VENDOR_CONTROL
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORAPPSTATUS_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_VENDORAPPSTATUS_TEMP]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@AppStatus VARCHAR(20) = ''
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_VENDOR_CONTROL_TEMP
	SET APPROVAL_STS = @AppStatus
	WHERE [UID] = @DOCID

	IF (@AppStatus = 'Reject')
	BEGIN
		DECLARE @REFID BIGINT = 0

		SELECT @REFID = REFID
		FROM TR_VENDOR_CONTROL_TEMP
		WHERE [UID] = @DOCID

		UPDATE TR_VENDOR_CONTROL
		SET APPROVAL_STS = 'Approval Completed'
		WHERE [UID] = @REFID
	END
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_VENDORDETAIL]
	-- add more stored procedure parameters here
    @UID bigint,
	@VENDOR_ID VARCHAR(50) = NULL,
	@CREATOR VARCHAR(255) = NULL,
	@CREATOR_DEPT VARCHAR(255) = NULL,
	@CREATOR_UNIT VARCHAR(255) = NULL,
	@CREATOR_ROLE VARCHAR(255) = NULL,
	@REQUEST_DT DATE = NULL,
	@DOC_DATE_FORM1 DATE = NULL,
	@CONF_CONTRACTOR_NM VARCHAR(255) = NULL,
	@CONF_OPERATION_TO_OUTSOURCE VARCHAR(255) = NULL,
	@CONF_CATEGORY VARCHAR(255) = NULL,
	@CONF_DEPT_CD VARCHAR(50) = NULL,
	@CONF_BRIEF_EXPLAIN_CONTRACT VARCHAR(8000) = NULL,
	@ITEM_1 VARCHAR(255) = NULL,
	@SUBMITTED_1 BIT = NULL,
	@REASON_1 VARCHAR(8000) = NULL,
	@CHECKED_1 BIT = NULL,
    @REASON_CHECKED_1 VARCHAR(8000) = NULL,
	@ITEM_2 VARCHAR(255) = NULL,
	@SUBMITTED_2 BIT = NULL,
	@REASON_2 VARCHAR(8000) = NULL,
	@CHECKED_2 BIT = NULL,
    @REASON_CHECKED_2 VARCHAR(8000) = NULL,
	@ITEM_3 VARCHAR(255) = NULL,
	@SUBMITTED_3 BIT = NULL,
	@REASON_3 VARCHAR(8000) = NULL,
	@CHECKED_3 BIT = NULL,
    @REASON_CHECKED_3 VARCHAR(8000) = NULL,
	@ITEM_4 VARCHAR(255) = NULL,
	@SUBMITTED_4 BIT = NULL,
	@REASON_4 VARCHAR(8000) = NULL,
	@CHECKED_4 BIT = NULL,
    @REASON_CHECKED_4 VARCHAR(8000) = NULL,
	@ITEM_5 VARCHAR(255) = NULL,
	@SUBMITTED_5 BIT = NULL,
	@REASON_5 VARCHAR(8000) = NULL,
	@CHECKED_5 BIT = NULL,
    @REASON_CHECKED_5 VARCHAR(8000) = NULL,
	@ITEM_6 VARCHAR(255) = NULL,
	@SUBMITTED_6 BIT = NULL,
	@REASON_6 VARCHAR(8000) = NULL,
	@CHECKED_6 BIT = NULL,
    @REASON_CHECKED_6 VARCHAR(8000) = NULL,
	@ITEM_7 VARCHAR(255) = NULL,
	@SUBMITTED_7 BIT = NULL,
	@REASON_7 VARCHAR(8000) = NULL,
	@CHECKED_7 BIT = NULL,
    @REASON_CHECKED_7 VARCHAR(8000) = NULL,
	@ITEM_8 VARCHAR(255) = NULL,
	@SUBMITTED_8 BIT = NULL,
	@REASON_8 VARCHAR(8000) = NULL,
	@CHECKED_8 BIT = NULL,
    @REASON_CHECKED_8 VARCHAR(8000) = NULL,
	@ITEM_9 VARCHAR(255) = NULL,
	@SUBMITTED_9 BIT = NULL,
	@REASON_9 VARCHAR(8000) = NULL,
	@CHECKED_9 BIT = NULL,
    @REASON_CHECKED_9 VARCHAR(8000) = NULL,
	@ITEM_10 VARCHAR(255) = NULL,
	@SUBMITTED_10 BIT = NULL,
	@REASON_10 VARCHAR(8000) = NULL,
	@CHECKED_10 BIT = NULL,
    @REASON_CHECKED_10 VARCHAR(8000) = NULL,
	@ITEM_11 VARCHAR(255) = NULL,
	@SUBMITTED_11 BIT = NULL,
	@REASON_11 VARCHAR(8000) = NULL,
	@CHECKED_11 BIT = NULL,
    @REASON_CHECKED_11 VARCHAR(8000) = NULL,
	@ITEM_12 VARCHAR(255) = NULL,
	@SUBMITTED_12 BIT = NULL,
	@REASON_12 VARCHAR(8000) = NULL,
	@CHECKED_12 BIT = NULL,
    @REASON_CHECKED_12 VARCHAR(8000) = NULL,
	@ITEM_13 VARCHAR(255) = NULL,
	@SUBMITTED_13 BIT = NULL,
	@REASON_13 VARCHAR(8000) = NULL,
	@CHECKED_13 BIT = NULL,
    @REASON_CHECKED_13 VARCHAR(8000) = NULL,
	@ITEM_14 VARCHAR(255) = NULL,
	@SUBMITTED_14 BIT = NULL,
	@REASON_14 VARCHAR(8000) = NULL,
	@CHECKED_14 BIT = NULL,
    @REASON_CHECKED_14 VARCHAR(8000) = NULL,
	@ITEM_15 VARCHAR(255) = NULL,
	@SUBMITTED_15 BIT = NULL,
	@REASON_15 VARCHAR(8000) = NULL,
	@CHECKED_15 BIT = NULL,
    @REASON_CHECKED_15 VARCHAR(8000) = NULL,
	@CONF_DECISION BIT = NULL,
	@CONF_GAD_COMMENT VARCHAR(8000) = NULL,
	@DOC_DATE_FORM2 DATE = NULL,
	@EXAM_NAME_OF_GROUP VARCHAR(255) = NULL,
	@EXAM_CORE_BUSINESS VARCHAR(255) = NULL,
	@EXAM_REL_VENDOR BIT = NULL,
	@EXAM_REL_VENDOR_REMARK VARCHAR(8000) = NULL,
	@EXAM_HAS_ENGAGED_BEFORE BIT = NULL,
	@EXAM_HAS_ENGAGED_SINCE DATE = NULL,
	@EXAM_BACKGROUND VARCHAR(8000) = NULL,
	@EXAM_VENDOR_A VARCHAR(255) = NULL,
	@EXAM_VENDOR_B VARCHAR(255) = NULL,
	@EXAM_ITEM_1 VARCHAR(200) = NULL,
	@EXAM_UID_1 INT = NULL,
	@COMPARE_VENDOR_A_1 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_1 VARCHAR(800) = NULL,
	@EXAM_REMARK_1 VARCHAR(8000) = NULL,
	@EXAM_ITEM_2 VARCHAR(200) = NULL,
	@EXAM_UID_2 INT = NULL,
	@COMPARE_VENDOR_A_2 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_2 VARCHAR(800) = NULL,
	@EXAM_REMARK_2 VARCHAR(8000) = NULL,
	@EXAM_ITEM_3 VARCHAR(200) = NULL,
	@EXAM_UID_3 INT = NULL,
	@COMPARE_VENDOR_A_3 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_3 VARCHAR(800) = NULL,
	@EXAM_REMARK_3 VARCHAR(8000) = NULL,
	@EXAM_ITEM_4 VARCHAR(200) = NULL,
	@EXAM_UID_4 INT = NULL,
	@COMPARE_VENDOR_A_4 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_4 VARCHAR(800) = NULL,
	@EXAM_REMARK_4 VARCHAR(8000) = NULL,
	@EXAM_OVERALL_CONCLUSION VARCHAR(8000) = NULL,
	@RISK_ANNUAL_BILLING VARCHAR(80) = NULL,
	@RISK_SPECIAL_RELATED BIT = NULL,
	@RISK_CPC_ASSESSMENT VARCHAR(8000) = NULL,
	@RISK_PROVIDE_ACCESS BIT = NULL,
	@RISK_AS_VENDOR_A VARCHAR(255) = NULL,
	@RISK_AS_VENDOR_B VARCHAR(255) = NULL,
	@ASSESS_ITEM_1 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_1 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_1 VARCHAR(255) = NULL,
	@ASSESS_REMARK_1 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_2 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_2 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_2 VARCHAR(255) = NULL,
	@ASSESS_REMARK_2 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_3 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_3 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_3 VARCHAR(255) = NULL,
	@ASSESS_REMARK_3 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_4 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_4 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_4 VARCHAR(255) = NULL,
	@ASSESS_REMARK_4 VARCHAR(8000) = NULL,
	@RISK_AML_COMMENT VARCHAR(8000) = NULL,
	@RISK_CATEGORY VARCHAR(50) = NULL,
	@RISK_RMD_RECOMMENDATION VARCHAR(8000) = NULL,
	@RISK_SCREENING_RESULT VARCHAR(8000) = NULL,
	@RISK_MANAGEMENT_COMMENT VARCHAR(8000) = NULL,
	@REG_PROVIDED_SERVICE VARCHAR(8000) = NULL,
	@REG_CONTRACT_NM VARCHAR(255) = NULL,
	@VENDOR_NAME VARCHAR(255) = NULL,
	@REG_LEGAL_STATUS VARCHAR(50) = NULL,
	@REG_NPWP_NO VARCHAR(255) = NULL,
	@REG_ADDRESS VARCHAR(255) = NULL,
	@REG_CITY VARCHAR(255) = NULL,
	@REG_POSTAL VARCHAR(255) = NULL,
	@REG_WEBSITE VARCHAR(255) = NULL,
	@REG_EMAIL VARCHAR(255) = NULL,
	@REG_PHONE_1 VARCHAR(255) = NULL,
	@REG_PHONE_EXT_1 VARCHAR(255) = NULL,
	@REG_PHONE_2 VARCHAR(255) = NULL,
	@REG_PHONE_EXT_2 VARCHAR(255) = NULL,
	@REG_FAX_1 VARCHAR(255) = NULL,
	@REG_FAX_2 VARCHAR(255) = NULL,
	@REG_CP_NM_1 VARCHAR(255) = NULL,
	@REG_CP_POSITION_1 VARCHAR(255) = NULL,
	@REG_CP_NM_2 VARCHAR(255) = NULL,
	@REG_CP_POSITION_2 VARCHAR(255) = NULL,
	@REG_PAYMENT_TYPE VARCHAR(50) = NULL,
	@REG_REASON_CASH VARCHAR(8000) = NULL,
	@REG_ACC_HOLDER_NM VARCHAR(255) = NULL,
	@REG_ACC_NO VARCHAR(255) = NULL,
	@REG_ACC_BANK VARCHAR(255) = NULL,
	@DOC_DATE_FORM4A DATE = NULL,
	@SHEET_ID_1 INT = NULL,
	@SHEET_POSITION_1 VARCHAR(50) = NULL,
	@SHEET_NAME_1 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_1 VARCHAR(255) = NULL,
	@SHEET_SHARE_1 DECIMAL(36,8) = NULL,
	@SHEET_ID_2 INT = NULL,
	@SHEET_POSITION_2 VARCHAR(50) = NULL,
	@SHEET_NAME_2 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_2 VARCHAR(255) = NULL,
	@SHEET_SHARE_2 DECIMAL(36,8) = NULL,
	@SHEET_ID_3 INT = NULL,
	@SHEET_POSITION_3 VARCHAR(50) = NULL,
	@SHEET_NAME_3 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_3 VARCHAR(255) = NULL,
	@SHEET_SHARE_3 DECIMAL(36,8) = NULL,
	@SHEET_ID_4 INT = NULL,
	@SHEET_POSITION_4 VARCHAR(50) = NULL,
	@SHEET_NAME_4 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_4 VARCHAR(255) = NULL,
	@SHEET_SHARE_4 DECIMAL(36,8) = NULL,
	@SHEET_ID_5 INT = NULL,
	@SHEET_POSITION_5 VARCHAR(50) = NULL,
	@SHEET_NAME_5 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_5 VARCHAR(255) = NULL,
	@SHEET_SHARE_5 DECIMAL(36,8) = NULL,
	@SHEET_ID_6 INT = NULL,
	@SHEET_POSITION_6 VARCHAR(50) = NULL,
	@SHEET_NAME_6 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_6 VARCHAR(255) = NULL,
	@SHEET_SHARE_6 DECIMAL(36,8) = NULL,
	@SHEET_ID_7 INT = NULL,
	@SHEET_POSITION_7 VARCHAR(50) = NULL,
	@SHEET_NAME_7 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_7 VARCHAR(255) = NULL,
	@SHEET_SHARE_7 DECIMAL(36,8) = NULL,
	@SHEET_ID_8 INT = NULL,
	@SHEET_POSITION_8 VARCHAR(50) = NULL,
	@SHEET_NAME_8 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_8 VARCHAR(255) = NULL,
	@SHEET_SHARE_8 DECIMAL(36,8) = NULL,
	@SHEET_ID_9 INT = NULL,
	@SHEET_POSITION_9 VARCHAR(50) = NULL,
	@SHEET_NAME_9 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_9 VARCHAR(255) = NULL,
	@SHEET_SHARE_9 DECIMAL(36,8) = NULL,
	@SHEET_ID_10 INT = NULL,
	@SHEET_POSITION_10 VARCHAR(50) = NULL,
	@SHEET_NAME_10 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_10 VARCHAR(255) = NULL,
	@SHEET_SHARE_10 DECIMAL(36,8) = NULL,
	@ASC_CONTRACTOR_JAPANESE BIT = NULL,
	@ASC_SUBCONTRACT_OPERATION BIT = NULL,
	@APPROVAL_STS VARCHAR(50) = NULL,
	@IS_ACTIVE BIT = NULL,
	@MODIFIED_BY VARCHAR(50) = NULL,
	@MODIFIED_DT DATETIME = NULL,
	@MODIFIED_HOST VARCHAR(20) = NULL,
	@STATUS_FLOW VARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_VENDOR_CONTROL
    SET VENDOR_ID = @VENDOR_ID,
        VENDOR_NAME = @VENDOR_NAME,
        CREATOR = @CREATOR,
        CREATOR_DEPT = @CREATOR_DEPT,
       CREATOR_UNIT = @CREATOR_UNIT,
        CREATOR_ROLE = @CREATOR_ROLE,
        REQUEST_DT = @REQUEST_DT,
        DOC_DATE_FORM1 = @DOC_DATE_FORM1,
        CONF_CONTRACTOR_NM = @CONF_CONTRACTOR_NM,
        CONF_OPERATION_TO_OUTSOURCE = @CONF_OPERATION_TO_OUTSOURCE,
        CONF_CATEGORY = @CONF_CATEGORY,
        CONF_DEPT_CD = @CONF_DEPT_CD,
        CONF_BRIEF_EXPLAIN_CONTRACT = @CONF_BRIEF_EXPLAIN_CONTRACT,
        CONF_DECISION = @CONF_DECISION,
        CONF_GAD_COMMENT = @CONF_GAD_COMMENT,
        DOC_DATE_FORM2 = @DOC_DATE_FORM2,
        EXAM_NAME_OF_GROUP = @EXAM_NAME_OF_GROUP,
        EXAM_CORE_BUSINESS = @EXAM_CORE_BUSINESS,
        EXAM_REL_VENDOR = @EXAM_REL_VENDOR,
        EXAM_REL_VENDOR_REMARK = @EXAM_REL_VENDOR_REMARK,
        EXAM_HAS_ENGAGED_BEFORE = @EXAM_HAS_ENGAGED_BEFORE,
        EXAM_HAS_ENGAGED_SINCE = @EXAM_HAS_ENGAGED_SINCE,
        EXAM_BACKGROUND = @EXAM_BACKGROUND,
        EXAM_VENDOR_A = @EXAM_VENDOR_A,
        EXAM_VENDOR_B = @EXAM_VENDOR_B,
        EXAM_OVERALL_CONCLUSION = @EXAM_OVERALL_CONCLUSION,
        RISK_ANNUAL_BILLING = @RISK_ANNUAL_BILLING,
        RISK_SPECIAL_RELATED = @RISK_SPECIAL_RELATED,
        RISK_CPC_ASSESSMENT = @RISK_CPC_ASSESSMENT,
        RISK_PROVIDE_ACCESS = @RISK_PROVIDE_ACCESS,
        RISK_AS_VENDOR_A = @RISK_AS_VENDOR_A,
        RISK_AS_VENDOR_B = @RISK_AS_VENDOR_B,
        RISK_AML_COMMENT = @RISK_AML_COMMENT,
        RISK_CATEGORY = @RISK_CATEGORY,
        RISK_RMD_RECOMMENDATION = @RISK_RMD_RECOMMENDATION,
        RISK_SCREENING_RESULT = @RISK_SCREENING_RESULT,
        REG_PROVIDED_SERVICE = @REG_PROVIDED_SERVICE,
        REG_CONTRACT_NM = @REG_CONTRACT_NM,
        REG_LEGAL_STATUS = @REG_LEGAL_STATUS,
        REG_NPWP_NO = @REG_NPWP_NO,
        REG_ADDRESS = @REG_ADDRESS,
        REG_CITY = @REG_CITY,
        REG_POSTAL = @REG_POSTAL,
        REG_WEBSITE = @REG_WEBSITE,
        REG_EMAIL = @REG_EMAIL,
        REG_PHONE_1 = @REG_PHONE_1,
        REG_PHONE_EXT_1 = @REG_PHONE_EXT_1,
        REG_PHONE_2 = @REG_PHONE_2,
        REG_PHONE_EXT_2 = @REG_PHONE_EXT_2,
        REG_FAX_1 = @REG_FAX_1,
        REG_FAX_2 = @REG_FAX_2,
        REG_CP_NM_1 = @REG_CP_NM_1,
        REG_CP_POSITION_1 = @REG_CP_POSITION_1,
        REG_CP_NM_2 = @REG_CP_NM_2,
        REG_CP_POSITION_2 = @REG_CP_POSITION_2,
        REG_PAYMENT_TYPE = @REG_PAYMENT_TYPE,
        REG_REASON_CASH = @REG_REASON_CASH,
        REG_ACC_HOLDER_NM = @REG_ACC_HOLDER_NM,
        REG_ACC_NO = @REG_ACC_NO,
        REG_ACC_BANK = @REG_ACC_BANK,
        DOC_DATE_FORM4A = @DOC_DATE_FORM4A,
        ASC_CONTRACTOR_JAPANESE = @ASC_CONTRACTOR_JAPANESE,
        ASC_SUBCONTRACT_OPERATION = @ASC_SUBCONTRACT_OPERATION,
        APPROVAL_STS = @APPROVAL_STS,
        IS_ACTIVE = @IS_ACTIVE,
        MODIFIED_BY = @MODIFIED_BY,
        MODIFIED_DT = @MODIFIED_DT,
        MODIFIED_HOST = @MODIFIED_HOST,
        STATUS_FLOW = @STATUS_FLOW
    WHERE [UID] = @UID

    -- update conf doc checklist
    UPDATE A 
    SET A.ITEM = B.ITEM
    , A.SUBMITTED = B.SUBMITTED
    , A.REASON = B.REASON
    , A.CHECKED = B.CHECKED
    , A.REASON_CHECKED = B.REASON_CHECKED
    , A.MODIFIED_BY = @MODIFIED_BY
   , A.MODIFIED_DT = @MODIFIED_DT
    , A.MODIFIED_HOST = @MODIFIED_HOST
    FROM TR_VC_CONF_DOC_CHECKLIST AS A 
    JOIN (
    select 1 AS SEQ, @ITEM_1 AS ITEM, @SUBMITTED_1 AS SUBMITTED, @REASON_1 AS REASON, @CHECKED_1 AS CHECKED, @REASON_CHECKED_1 AS REASON_CHECKED UNION
        select 2 AS SEQ, @ITEM_2 AS ITEM, @SUBMITTED_2 AS SUBMITTED, @REASON_2 AS REASON, @CHECKED_2 AS CHECKED, @REASON_CHECKED_2 AS REASON_CHECKED UNION
        select 3 AS SEQ, @ITEM_3 AS ITEM, @SUBMITTED_3 AS SUBMITTED, @REASON_3 AS REASON, @CHECKED_3 AS CHECKED, @REASON_CHECKED_3 AS REASON_CHECKED UNION
        select 4 AS SEQ, @ITEM_4 AS ITEM, @SUBMITTED_4 AS SUBMITTED, @REASON_4 AS REASON, @CHECKED_4 AS CHECKED, @REASON_CHECKED_4 AS REASON_CHECKED UNION
        select 5 AS SEQ, @ITEM_5 AS ITEM, @SUBMITTED_5 AS SUBMITTED, @REASON_5 AS REASON, @CHECKED_5 AS CHECKED, @REASON_CHECKED_5 AS REASON_CHECKED UNION
        select 6 AS SEQ, @ITEM_6 AS ITEM, @SUBMITTED_6 AS SUBMITTED, @REASON_6 AS REASON, @CHECKED_6 AS CHECKED, @REASON_CHECKED_6 AS REASON_CHECKED UNION
        select 7 AS SEQ, @ITEM_7 AS ITEM, @SUBMITTED_7 AS SUBMITTED, @REASON_7 AS REASON, @CHECKED_7 AS CHECKED, @REASON_CHECKED_7 AS REASON_CHECKED UNION
        select 8 AS SEQ, @ITEM_8 AS ITEM, @SUBMITTED_8 AS SUBMITTED, @REASON_8 AS REASON, @CHECKED_8 AS CHECKED, @REASON_CHECKED_8 AS REASON_CHECKED UNION
        select 9 AS SEQ, @ITEM_9 AS ITEM, @SUBMITTED_9 AS SUBMITTED, @REASON_9 AS REASON, @CHECKED_9 AS CHECKED, @REASON_CHECKED_9 AS REASON_CHECKED UNION
        select 10 AS SEQ, @ITEM_10 AS ITEM, @SUBMITTED_10 AS SUBMITTED, @REASON_10 AS REASON, @CHECKED_10 AS CHECKED, @REASON_CHECKED_10 AS REASON_CHECKED UNION
        select 11 AS SEQ, @ITEM_11 AS ITEM, @SUBMITTED_11 AS SUBMITTED, @REASON_11 AS REASON, @CHECKED_11 AS CHECKED, @REASON_CHECKED_11 AS REASON_CHECKED UNION
        select 12 AS SEQ, @ITEM_12 AS ITEM, @SUBMITTED_12 AS SUBMITTED, @REASON_12 AS REASON, @CHECKED_12 AS CHECKED, @REASON_CHECKED_12 AS REASON_CHECKED UNION
        select 13 AS SEQ, @ITEM_13 AS ITEM, @SUBMITTED_13 AS SUBMITTED, @REASON_13 AS REASON, @CHECKED_13 AS CHECKED, @REASON_CHECKED_13 AS REASON_CHECKED UNION
        select 14 AS SEQ, @ITEM_14 AS ITEM, @SUBMITTED_14 AS SUBMITTED, @REASON_14 AS REASON, @CHECKED_14 AS CHECKED, @REASON_CHECKED_14 AS REASON_CHECKED UNION
        select 15 AS SEQ, @ITEM_15 AS ITEM, @SUBMITTED_15 AS SUBMITTED, @REASON_15 AS REASON, @CHECKED_15 AS CHECKED, @REASON_CHECKED_15 AS REASON_CHECKED
    ) AS B ON A.SEQ = B.SEQ
    WHERE DOC_ID = @UID

    -- update exam comparison
    UPDATE A
    SET A.PARAMETER = B.PARAMETER
    , A.COMPARE_VENDOR_A = B.COMPARE_VENDOR_A
    , A.COMPARE_VENDOR_B = B.COMPARE_VENDOR_B
    , A.REMARK = B.REMARK
    FROM TR_VC_EXAM_COMPARISON A 
    JOIN (
        select 1 AS SEQ_NO, @EXAM_ITEM_1 AS PARAMETER, @COMPARE_VENDOR_A_1 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_1 AS COMPARE_VENDOR_B, @EXAM_REMARK_1 AS REMARK UNION
        select 2 AS SEQ_NO, @EXAM_ITEM_2 AS PARAMETER, @COMPARE_VENDOR_A_2 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_2 AS COMPARE_VENDOR_B, @EXAM_REMARK_2 AS REMARK UNION
        select 3 AS SEQ_NO, @EXAM_ITEM_3 AS PARAMETER, @COMPARE_VENDOR_A_3 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_3 AS COMPARE_VENDOR_B, @EXAM_REMARK_3 AS REMARK UNION
        select 4 AS SEQ_NO, @EXAM_ITEM_4 AS PARAMETER, @COMPARE_VENDOR_A_4 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_4 AS COMPARE_VENDOR_B, @EXAM_REMARK_4 AS REMARK
    ) AS B ON A.SEQ_NO = B.SEQ_NO
    WHERE A.DOC_ID = @UID

    -- update risk assessment
    UPDATE A
    SET A.PARAMETER = B.PARAMETER
    , A.ASSESS_VENDOR_A = B.ASSESS_VENDOR_A
    , A.ASSESS_VENDOR_B = B.ASSESS_VENDOR_B
    , A.REMARK = B.REMARK
    FROM TR_VC_RISK_ASSESSMENT A 
    JOIN (
        select 1 AS SEQ_NO, @ASSESS_ITEM_1 AS PARAMETER, @ASSESS_VENDOR_A_1 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_1 AS ASSESS_VENDOR_B, @ASSESS_REMARK_1 AS REMARK UNION
        select 2 AS SEQ_NO, @ASSESS_ITEM_2 AS PARAMETER, @ASSESS_VENDOR_A_2 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_2 AS ASSESS_VENDOR_B, @ASSESS_REMARK_2 AS REMARK UNION
        select 3 AS SEQ_NO, @ASSESS_ITEM_3 AS PARAMETER, @ASSESS_VENDOR_A_3 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_3 AS ASSESS_VENDOR_B, @ASSESS_REMARK_3 AS REMARK UNION
        select 4 AS SEQ_NO, @ASSESS_ITEM_4 AS PARAMETER, @ASSESS_VENDOR_A_4 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_4 AS ASSESS_VENDOR_B, @ASSESS_REMARK_4 AS REMARK
    ) AS B ON A.SEQ_NO = B.SEQ_NO
    WHERE A.DOC_ID = @UID

    -- insert risk management comment

    -- update snti social checksheet
    UPDATE A
    SET A.[POSITION] = B.[POSITION]
    , A.[NAME] = B.[NAME]
    , A.NATIONALITY = B.NATIONALITY
    , A.SHARE = B.SHARE
    FROM TR_VC_ASC_CHECKSHEET A 
    JOIN (
        select 1 AS SEQ_NO, @SHEET_POSITION_1 AS [POSITION], @SHEET_NAME_1 AS [NAME], @SHEET_NATIONALITY_1 AS NATIONALITY, @SHEET_SHARE_1 AS SHARE UNION
        select 2 AS SEQ_NO, @SHEET_POSITION_2 AS [POSITION], @SHEET_NAME_2 AS [NAME], @SHEET_NATIONALITY_2 AS NATIONALITY, @SHEET_SHARE_2 AS SHARE UNION
        select 3 AS SEQ_NO, @SHEET_POSITION_3 AS [POSITION], @SHEET_NAME_3 AS [NAME], @SHEET_NATIONALITY_3 AS NATIONALITY, @SHEET_SHARE_3 AS SHARE UNION
        select 4 AS SEQ_NO, @SHEET_POSITION_4 AS [POSITION], @SHEET_NAME_4 AS [NAME], @SHEET_NATIONALITY_4 AS NATIONALITY, @SHEET_SHARE_4 AS SHARE UNION
        select 5 AS SEQ_NO, @SHEET_POSITION_5 AS [POSITION], @SHEET_NAME_5 AS [NAME], @SHEET_NATIONALITY_5 AS NATIONALITY, @SHEET_SHARE_5 AS SHARE UNION
        select 6 AS SEQ_NO, @SHEET_POSITION_6 AS [POSITION], @SHEET_NAME_6 AS [NAME], @SHEET_NATIONALITY_6 AS NATIONALITY, @SHEET_SHARE_6 AS SHARE UNION
        select 7 AS SEQ_NO, @SHEET_POSITION_7 AS [POSITION], @SHEET_NAME_7 AS [NAME], @SHEET_NATIONALITY_7 AS NATIONALITY, @SHEET_SHARE_7 AS SHARE UNION
        select 8 AS SEQ_NO, @SHEET_POSITION_8 AS [POSITION], @SHEET_NAME_8 AS [NAME], @SHEET_NATIONALITY_8 AS NATIONALITY, @SHEET_SHARE_8 AS SHARE UNION
        select 9 AS SEQ_NO, @SHEET_POSITION_9 AS [POSITION], @SHEET_NAME_9 AS [NAME], @SHEET_NATIONALITY_9 AS NATIONALITY, @SHEET_SHARE_9 AS SHARE UNION
        select 10 AS SEQ_NO, @SHEET_POSITION_10 AS [POSITION], @SHEET_NAME_10 AS [NAME], @SHEET_NATIONALITY_10 AS NATIONALITY, @SHEET_SHARE_10 AS SHARE
    ) AS B ON A.SEQ_NO = B.SEQ_NO
    WHERE A.DOC_ID = @UID
END
GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORDETAILTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_VENDORDETAILTEMP]
	-- add more stored procedure parameters here
    @UID bigint,
	@VENDOR_ID VARCHAR(50) = NULL,
	@CREATOR VARCHAR(255) = NULL,
	@CREATOR_DEPT VARCHAR(255) = NULL,
	@CREATOR_UNIT VARCHAR(255) = NULL,
	@CREATOR_ROLE VARCHAR(255) = NULL,
	@REQUEST_DT DATE = NULL,
	@DOC_DATE_FORM1 DATE = NULL,
	@CONF_CONTRACTOR_NM VARCHAR(255) = NULL,
	@CONF_OPERATION_TO_OUTSOURCE VARCHAR(255) = NULL,
	@CONF_CATEGORY VARCHAR(255) = NULL,
	@CONF_DEPT_CD VARCHAR(50) = NULL,
	@CONF_BRIEF_EXPLAIN_CONTRACT VARCHAR(8000) = NULL,
	@ITEM_1 VARCHAR(255) = NULL,
	@SUBMITTED_1 BIT = NULL,
	@REASON_1 VARCHAR(8000) = NULL,
	@CHECKED_1 BIT = NULL,
    @REASON_CHECKED_1 VARCHAR(8000) = NULL,
	@ITEM_2 VARCHAR(255) = NULL,
	@SUBMITTED_2 BIT = NULL,
	@REASON_2 VARCHAR(8000) = NULL,
	@CHECKED_2 BIT = NULL,
    @REASON_CHECKED_2 VARCHAR(8000) = NULL,
	@ITEM_3 VARCHAR(255) = NULL,
	@SUBMITTED_3 BIT = NULL,
	@REASON_3 VARCHAR(8000) = NULL,
	@CHECKED_3 BIT = NULL,
    @REASON_CHECKED_3 VARCHAR(8000) = NULL,
	@ITEM_4 VARCHAR(255) = NULL,
	@SUBMITTED_4 BIT = NULL,
	@REASON_4 VARCHAR(8000) = NULL,
	@CHECKED_4 BIT = NULL,
    @REASON_CHECKED_4 VARCHAR(8000) = NULL,
	@ITEM_5 VARCHAR(255) = NULL,
	@SUBMITTED_5 BIT = NULL,
	@REASON_5 VARCHAR(8000) = NULL,
	@CHECKED_5 BIT = NULL,
    @REASON_CHECKED_5 VARCHAR(8000) = NULL,
	@ITEM_6 VARCHAR(255) = NULL,
	@SUBMITTED_6 BIT = NULL,
	@REASON_6 VARCHAR(8000) = NULL,
	@CHECKED_6 BIT = NULL,
    @REASON_CHECKED_6 VARCHAR(8000) = NULL,
	@ITEM_7 VARCHAR(255) = NULL,
	@SUBMITTED_7 BIT = NULL,
	@REASON_7 VARCHAR(8000) = NULL,
	@CHECKED_7 BIT = NULL,
    @REASON_CHECKED_7 VARCHAR(8000) = NULL,
	@ITEM_8 VARCHAR(255) = NULL,
	@SUBMITTED_8 BIT = NULL,
	@REASON_8 VARCHAR(8000) = NULL,
	@CHECKED_8 BIT = NULL,
    @REASON_CHECKED_8 VARCHAR(8000) = NULL,
	@ITEM_9 VARCHAR(255) = NULL,
	@SUBMITTED_9 BIT = NULL,
	@REASON_9 VARCHAR(8000) = NULL,
	@CHECKED_9 BIT = NULL,
    @REASON_CHECKED_9 VARCHAR(8000) = NULL,
	@ITEM_10 VARCHAR(255) = NULL,
	@SUBMITTED_10 BIT = NULL,
	@REASON_10 VARCHAR(8000) = NULL,
	@CHECKED_10 BIT = NULL,
    @REASON_CHECKED_10 VARCHAR(8000) = NULL,
	@ITEM_11 VARCHAR(255) = NULL,
	@SUBMITTED_11 BIT = NULL,
	@REASON_11 VARCHAR(8000) = NULL,
	@CHECKED_11 BIT = NULL,
    @REASON_CHECKED_11 VARCHAR(8000) = NULL,
	@ITEM_12 VARCHAR(255) = NULL,
	@SUBMITTED_12 BIT = NULL,
	@REASON_12 VARCHAR(8000) = NULL,
	@CHECKED_12 BIT = NULL,
    @REASON_CHECKED_12 VARCHAR(8000) = NULL,
	@ITEM_13 VARCHAR(255) = NULL,
	@SUBMITTED_13 BIT = NULL,
	@REASON_13 VARCHAR(8000) = NULL,
	@CHECKED_13 BIT = NULL,
    @REASON_CHECKED_13 VARCHAR(8000) = NULL,
	@ITEM_14 VARCHAR(255) = NULL,
	@SUBMITTED_14 BIT = NULL,
	@REASON_14 VARCHAR(8000) = NULL,
	@CHECKED_14 BIT = NULL,
    @REASON_CHECKED_14 VARCHAR(8000) = NULL,
	@ITEM_15 VARCHAR(255) = NULL,
	@SUBMITTED_15 BIT = NULL,
	@REASON_15 VARCHAR(8000) = NULL,
	@CHECKED_15 BIT = NULL,
    @REASON_CHECKED_15 VARCHAR(8000) = NULL,
	@CONF_DECISION BIT = NULL,
	@CONF_GAD_COMMENT VARCHAR(8000) = NULL,
	@DOC_DATE_FORM2 DATE = NULL,
	@EXAM_NAME_OF_GROUP VARCHAR(255) = NULL,
	@EXAM_CORE_BUSINESS VARCHAR(255) = NULL,
	@EXAM_REL_VENDOR BIT = NULL,
	@EXAM_REL_VENDOR_REMARK VARCHAR(8000) = NULL,
	@EXAM_HAS_ENGAGED_BEFORE BIT = NULL,
	@EXAM_HAS_ENGAGED_SINCE DATE = NULL,
	@EXAM_BACKGROUND VARCHAR(8000) = NULL,
	@EXAM_VENDOR_A VARCHAR(255) = NULL,
	@EXAM_VENDOR_B VARCHAR(255) = NULL,
	@EXAM_ITEM_1 VARCHAR(200) = NULL,
	@EXAM_UID_1 INT = NULL,
	@COMPARE_VENDOR_A_1 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_1 VARCHAR(800) = NULL,
	@EXAM_REMARK_1 VARCHAR(8000) = NULL,
	@EXAM_ITEM_2 VARCHAR(200) = NULL,
	@EXAM_UID_2 INT = NULL,
	@COMPARE_VENDOR_A_2 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_2 VARCHAR(800) = NULL,
	@EXAM_REMARK_2 VARCHAR(8000) = NULL,
	@EXAM_ITEM_3 VARCHAR(200) = NULL,
	@EXAM_UID_3 INT = NULL,
	@COMPARE_VENDOR_A_3 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_3 VARCHAR(800) = NULL,
	@EXAM_REMARK_3 VARCHAR(8000) = NULL,
	@EXAM_ITEM_4 VARCHAR(200) = NULL,
	@EXAM_UID_4 INT = NULL,
	@COMPARE_VENDOR_A_4 VARCHAR(800) = NULL,
	@COMPARE_VENDOR_B_4 VARCHAR(800) = NULL,
	@EXAM_REMARK_4 VARCHAR(8000) = NULL,
	@EXAM_OVERALL_CONCLUSION VARCHAR(8000) = NULL,
	@RISK_ANNUAL_BILLING VARCHAR(80) = NULL,
	@RISK_SPECIAL_RELATED BIT = NULL,
	@RISK_CPC_ASSESSMENT VARCHAR(8000) = NULL,
	@RISK_PROVIDE_ACCESS BIT = NULL,
	@RISK_AS_VENDOR_A VARCHAR(255) = NULL,
	@RISK_AS_VENDOR_B VARCHAR(255) = NULL,
	@ASSESS_ITEM_1 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_1 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_1 VARCHAR(255) = NULL,
	@ASSESS_REMARK_1 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_2 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_2 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_2 VARCHAR(255) = NULL,
	@ASSESS_REMARK_2 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_3 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_3 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_3 VARCHAR(255) = NULL,
	@ASSESS_REMARK_3 VARCHAR(8000) = NULL,
	@ASSESS_ITEM_4 VARCHAR(80) = NULL,
	@ASSESS_VENDOR_A_4 VARCHAR(255) = NULL,
	@ASSESS_VENDOR_B_4 VARCHAR(255) = NULL,
	@ASSESS_REMARK_4 VARCHAR(8000) = NULL,
	@RISK_AML_COMMENT VARCHAR(8000) = NULL,
	@RISK_CATEGORY VARCHAR(50) = NULL,
	@RISK_RMD_RECOMMENDATION VARCHAR(8000) = NULL,
	@RISK_SCREENING_RESULT VARCHAR(8000) = NULL,
	@RISK_MANAGEMENT_COMMENT VARCHAR(8000) = NULL,
	@REG_PROVIDED_SERVICE VARCHAR(8000) = NULL,
	@REG_CONTRACT_NM VARCHAR(255) = NULL,
	@VENDOR_NAME VARCHAR(255) = NULL,
	@REG_LEGAL_STATUS VARCHAR(50) = NULL,
	@REG_NPWP_NO VARCHAR(255) = NULL,
	@REG_ADDRESS VARCHAR(255) = NULL,
	@REG_CITY VARCHAR(255) = NULL,
	@REG_POSTAL VARCHAR(255) = NULL,
	@REG_WEBSITE VARCHAR(255) = NULL,
	@REG_EMAIL VARCHAR(255) = NULL,
	@REG_PHONE_1 VARCHAR(255) = NULL,
	@REG_PHONE_EXT_1 VARCHAR(255) = NULL,
	@REG_PHONE_2 VARCHAR(255) = NULL,
	@REG_PHONE_EXT_2 VARCHAR(255) = NULL,
	@REG_FAX_1 VARCHAR(255) = NULL,
	@REG_FAX_2 VARCHAR(255) = NULL,
	@REG_CP_NM_1 VARCHAR(255) = NULL,
	@REG_CP_POSITION_1 VARCHAR(255) = NULL,
	@REG_CP_NM_2 VARCHAR(255) = NULL,
	@REG_CP_POSITION_2 VARCHAR(255) = NULL,
	@REG_PAYMENT_TYPE VARCHAR(50) = NULL,
	@REG_REASON_CASH VARCHAR(8000) = NULL,
	@REG_ACC_HOLDER_NM VARCHAR(255) = NULL,
	@REG_ACC_NO VARCHAR(255) = NULL,
	@REG_ACC_BANK VARCHAR(255) = NULL,
	@DOC_DATE_FORM4A DATE = NULL,
	@SHEET_ID_1 INT = NULL,
	@SHEET_POSITION_1 VARCHAR(50) = NULL,
	@SHEET_NAME_1 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_1 VARCHAR(255) = NULL,
	@SHEET_SHARE_1 DECIMAL(36,8) = NULL,
	@SHEET_ID_2 INT = NULL,
	@SHEET_POSITION_2 VARCHAR(50) = NULL,
	@SHEET_NAME_2 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_2 VARCHAR(255) = NULL,
	@SHEET_SHARE_2 DECIMAL(36,8) = NULL,
	@SHEET_ID_3 INT = NULL,
	@SHEET_POSITION_3 VARCHAR(50) = NULL,
	@SHEET_NAME_3 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_3 VARCHAR(255) = NULL,
	@SHEET_SHARE_3 DECIMAL(36,8) = NULL,
	@SHEET_ID_4 INT = NULL,
	@SHEET_POSITION_4 VARCHAR(50) = NULL,
	@SHEET_NAME_4 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_4 VARCHAR(255) = NULL,
	@SHEET_SHARE_4 DECIMAL(36,8) = NULL,
	@SHEET_ID_5 INT = NULL,
	@SHEET_POSITION_5 VARCHAR(50) = NULL,
	@SHEET_NAME_5 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_5 VARCHAR(255) = NULL,
	@SHEET_SHARE_5 DECIMAL(36,8) = NULL,
	@SHEET_ID_6 INT = NULL,
	@SHEET_POSITION_6 VARCHAR(50) = NULL,
	@SHEET_NAME_6 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_6 VARCHAR(255) = NULL,
	@SHEET_SHARE_6 DECIMAL(36,8) = NULL,
	@SHEET_ID_7 INT = NULL,
	@SHEET_POSITION_7 VARCHAR(50) = NULL,
	@SHEET_NAME_7 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_7 VARCHAR(255) = NULL,
	@SHEET_SHARE_7 DECIMAL(36,8) = NULL,
	@SHEET_ID_8 INT = NULL,
	@SHEET_POSITION_8 VARCHAR(50) = NULL,
	@SHEET_NAME_8 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_8 VARCHAR(255) = NULL,
	@SHEET_SHARE_8 DECIMAL(36,8) = NULL,
	@SHEET_ID_9 INT = NULL,
	@SHEET_POSITION_9 VARCHAR(50) = NULL,
	@SHEET_NAME_9 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_9 VARCHAR(255) = NULL,
	@SHEET_SHARE_9 DECIMAL(36,8) = NULL,
	@SHEET_ID_10 INT = NULL,
	@SHEET_POSITION_10 VARCHAR(50) = NULL,
	@SHEET_NAME_10 VARCHAR(255) = NULL,
	@SHEET_NATIONALITY_10 VARCHAR(255) = NULL,
	@SHEET_SHARE_10 DECIMAL(36,8) = NULL,
	@ASC_CONTRACTOR_JAPANESE BIT = NULL,
	@ASC_SUBCONTRACT_OPERATION BIT = NULL,
	@APPROVAL_STS VARCHAR(50) = NULL,
	@IS_ACTIVE BIT = NULL,
	@MODIFIED_BY VARCHAR(50) = NULL,
	@MODIFIED_DT DATETIME = NULL,
	@MODIFIED_HOST VARCHAR(20) = NULL,
	@STATUS_FLOW VARCHAR(50) = NULL
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_VENDOR_CONTROL_TEMP
    SET VENDOR_ID = @VENDOR_ID,
        VENDOR_NAME = @VENDOR_NAME,
        CREATOR = @CREATOR,
        CREATOR_DEPT = @CREATOR_DEPT,
       CREATOR_UNIT = @CREATOR_UNIT,
        CREATOR_ROLE = @CREATOR_ROLE,
        REQUEST_DT = @REQUEST_DT,
        DOC_DATE_FORM1 = @DOC_DATE_FORM1,
        CONF_CONTRACTOR_NM = @CONF_CONTRACTOR_NM,
        CONF_OPERATION_TO_OUTSOURCE = @CONF_OPERATION_TO_OUTSOURCE,
        CONF_CATEGORY = @CONF_CATEGORY,
        CONF_DEPT_CD = @CONF_DEPT_CD,
        CONF_BRIEF_EXPLAIN_CONTRACT = @CONF_BRIEF_EXPLAIN_CONTRACT,
        CONF_DECISION = @CONF_DECISION,
        CONF_GAD_COMMENT = @CONF_GAD_COMMENT,
        DOC_DATE_FORM2 = @DOC_DATE_FORM2,
        EXAM_NAME_OF_GROUP = @EXAM_NAME_OF_GROUP,
        EXAM_CORE_BUSINESS = @EXAM_CORE_BUSINESS,
        EXAM_REL_VENDOR = @EXAM_REL_VENDOR,
        EXAM_REL_VENDOR_REMARK = @EXAM_REL_VENDOR_REMARK,
        EXAM_HAS_ENGAGED_BEFORE = @EXAM_HAS_ENGAGED_BEFORE,
        EXAM_HAS_ENGAGED_SINCE = @EXAM_HAS_ENGAGED_SINCE,
        EXAM_BACKGROUND = @EXAM_BACKGROUND,
        EXAM_VENDOR_A = @EXAM_VENDOR_A,
        EXAM_VENDOR_B = @EXAM_VENDOR_B,
        EXAM_OVERALL_CONCLUSION = @EXAM_OVERALL_CONCLUSION,
        RISK_ANNUAL_BILLING = @RISK_ANNUAL_BILLING,
        RISK_SPECIAL_RELATED = @RISK_SPECIAL_RELATED,
        RISK_CPC_ASSESSMENT = @RISK_CPC_ASSESSMENT,
        RISK_PROVIDE_ACCESS = @RISK_PROVIDE_ACCESS,
        RISK_AS_VENDOR_A = @RISK_AS_VENDOR_A,
        RISK_AS_VENDOR_B = @RISK_AS_VENDOR_B,
        RISK_AML_COMMENT = @RISK_AML_COMMENT,
        RISK_CATEGORY = @RISK_CATEGORY,
        RISK_RMD_RECOMMENDATION = @RISK_RMD_RECOMMENDATION,
        RISK_SCREENING_RESULT = @RISK_SCREENING_RESULT,
        REG_PROVIDED_SERVICE = @REG_PROVIDED_SERVICE,
        REG_CONTRACT_NM = @REG_CONTRACT_NM,
        REG_LEGAL_STATUS = @REG_LEGAL_STATUS,
        REG_NPWP_NO = @REG_NPWP_NO,
        REG_ADDRESS = @REG_ADDRESS,
        REG_CITY = @REG_CITY,
        REG_POSTAL = @REG_POSTAL,
        REG_WEBSITE = @REG_WEBSITE,
        REG_EMAIL = @REG_EMAIL,
        REG_PHONE_1 = @REG_PHONE_1,
        REG_PHONE_EXT_1 = @REG_PHONE_EXT_1,
        REG_PHONE_2 = @REG_PHONE_2,
        REG_PHONE_EXT_2 = @REG_PHONE_EXT_2,
        REG_FAX_1 = @REG_FAX_1,
        REG_FAX_2 = @REG_FAX_2,
        REG_CP_NM_1 = @REG_CP_NM_1,
        REG_CP_POSITION_1 = @REG_CP_POSITION_1,
        REG_CP_NM_2 = @REG_CP_NM_2,
        REG_CP_POSITION_2 = @REG_CP_POSITION_2,
        REG_PAYMENT_TYPE = @REG_PAYMENT_TYPE,
        REG_REASON_CASH = @REG_REASON_CASH,
        REG_ACC_HOLDER_NM = @REG_ACC_HOLDER_NM,
        REG_ACC_NO = @REG_ACC_NO,
        REG_ACC_BANK = @REG_ACC_BANK,
        DOC_DATE_FORM4A = @DOC_DATE_FORM4A,
        ASC_CONTRACTOR_JAPANESE = @ASC_CONTRACTOR_JAPANESE,
        ASC_SUBCONTRACT_OPERATION = @ASC_SUBCONTRACT_OPERATION,
        APPROVAL_STS = @APPROVAL_STS,
        IS_ACTIVE = @IS_ACTIVE,
        MODIFIED_BY = @MODIFIED_BY,
        MODIFIED_DT = @MODIFIED_DT,
        MODIFIED_HOST = @MODIFIED_HOST,
        STATUS_FLOW = @STATUS_FLOW
    WHERE [UID] = @UID

    -- update conf doc checklist
    UPDATE A 
    SET A.ITEM = B.ITEM
    , A.SUBMITTED = B.SUBMITTED
    , A.REASON = B.REASON
    , A.CHECKED = B.CHECKED
    , A.REASON_CHECKED = B.REASON_CHECKED
    , A.MODIFIED_BY = @MODIFIED_BY
    , A.MODIFIED_DT = @MODIFIED_DT
    , A.MODIFIED_HOST = @MODIFIED_HOST
    FROM TR_VC_CONF_DOC_CHECKLIST_TEMP AS A 
    JOIN (
    select 1 AS SEQ, @ITEM_1 AS ITEM, @SUBMITTED_1 AS SUBMITTED, @REASON_1 AS REASON, @CHECKED_1 AS CHECKED, @REASON_CHECKED_1 AS REASON_CHECKED UNION
        select 2 AS SEQ, @ITEM_2 AS ITEM, @SUBMITTED_2 AS SUBMITTED, @REASON_2 AS REASON, @CHECKED_2 AS CHECKED, @REASON_CHECKED_2 AS REASON_CHECKED UNION
        select 3 AS SEQ, @ITEM_3 AS ITEM, @SUBMITTED_3 AS SUBMITTED, @REASON_3 AS REASON, @CHECKED_3 AS CHECKED, @REASON_CHECKED_3 AS REASON_CHECKED UNION
        select 4 AS SEQ, @ITEM_4 AS ITEM, @SUBMITTED_4 AS SUBMITTED, @REASON_4 AS REASON, @CHECKED_4 AS CHECKED, @REASON_CHECKED_4 AS REASON_CHECKED UNION
        select 5 AS SEQ, @ITEM_5 AS ITEM, @SUBMITTED_5 AS SUBMITTED, @REASON_5 AS REASON, @CHECKED_5 AS CHECKED, @REASON_CHECKED_5 AS REASON_CHECKED UNION
        select 6 AS SEQ, @ITEM_6 AS ITEM, @SUBMITTED_6 AS SUBMITTED, @REASON_6 AS REASON, @CHECKED_6 AS CHECKED, @REASON_CHECKED_6 AS REASON_CHECKED UNION
        select 7 AS SEQ, @ITEM_7 AS ITEM, @SUBMITTED_7 AS SUBMITTED, @REASON_7 AS REASON, @CHECKED_7 AS CHECKED, @REASON_CHECKED_7 AS REASON_CHECKED UNION
        select 8 AS SEQ, @ITEM_8 AS ITEM, @SUBMITTED_8 AS SUBMITTED, @REASON_8 AS REASON, @CHECKED_8 AS CHECKED, @REASON_CHECKED_8 AS REASON_CHECKED UNION
        select 9 AS SEQ, @ITEM_9 AS ITEM, @SUBMITTED_9 AS SUBMITTED, @REASON_9 AS REASON, @CHECKED_9 AS CHECKED, @REASON_CHECKED_9 AS REASON_CHECKED UNION
        select 10 AS SEQ, @ITEM_10 AS ITEM, @SUBMITTED_10 AS SUBMITTED, @REASON_10 AS REASON, @CHECKED_10 AS CHECKED, @REASON_CHECKED_10 AS REASON_CHECKED UNION
        select 11 AS SEQ, @ITEM_11 AS ITEM, @SUBMITTED_11 AS SUBMITTED, @REASON_11 AS REASON, @CHECKED_11 AS CHECKED, @REASON_CHECKED_11 AS REASON_CHECKED UNION
        select 12 AS SEQ, @ITEM_12 AS ITEM, @SUBMITTED_12 AS SUBMITTED, @REASON_12 AS REASON, @CHECKED_12 AS CHECKED, @REASON_CHECKED_12 AS REASON_CHECKED UNION
        select 13 AS SEQ, @ITEM_13 AS ITEM, @SUBMITTED_13 AS SUBMITTED, @REASON_13 AS REASON, @CHECKED_13 AS CHECKED, @REASON_CHECKED_13 AS REASON_CHECKED UNION
        select 14 AS SEQ, @ITEM_14 AS ITEM, @SUBMITTED_14 AS SUBMITTED, @REASON_14 AS REASON, @CHECKED_14 AS CHECKED, @REASON_CHECKED_14 AS REASON_CHECKED UNION
        select 15 AS SEQ, @ITEM_15 AS ITEM, @SUBMITTED_15 AS SUBMITTED, @REASON_15 AS REASON, @CHECKED_15 AS CHECKED, @REASON_CHECKED_15 AS REASON_CHECKED
    ) AS B ON A.SEQ = B.SEQ
    WHERE DOC_ID = @UID

    -- update exam comparison
    UPDATE A
    SET A.PARAMETER = B.PARAMETER
    , A.COMPARE_VENDOR_A = B.COMPARE_VENDOR_A
    , A.COMPARE_VENDOR_B = B.COMPARE_VENDOR_B
    , A.REMARK = B.REMARK
    FROM TR_VC_EXAM_COMPARISON_TEMP A 
    JOIN (
        select 1 AS SEQ_NO, @EXAM_ITEM_1 AS PARAMETER, @COMPARE_VENDOR_A_1 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_1 AS COMPARE_VENDOR_B, @EXAM_REMARK_1 AS REMARK UNION
        select 2 AS SEQ_NO, @EXAM_ITEM_2 AS PARAMETER, @COMPARE_VENDOR_A_2 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_2 AS COMPARE_VENDOR_B, @EXAM_REMARK_2 AS REMARK UNION
        select 3 AS SEQ_NO, @EXAM_ITEM_3 AS PARAMETER, @COMPARE_VENDOR_A_3 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_3 AS COMPARE_VENDOR_B, @EXAM_REMARK_3 AS REMARK UNION
        select 4 AS SEQ_NO, @EXAM_ITEM_4 AS PARAMETER, @COMPARE_VENDOR_A_4 AS COMPARE_VENDOR_A, @COMPARE_VENDOR_B_4 AS COMPARE_VENDOR_B, @EXAM_REMARK_4 AS REMARK
    ) AS B ON A.SEQ_NO = B.SEQ_NO
    WHERE A.DOC_ID = @UID

    -- update risk assessment
    UPDATE A
    SET A.PARAMETER = B.PARAMETER
    , A.ASSESS_VENDOR_A = B.ASSESS_VENDOR_A
    , A.ASSESS_VENDOR_B = B.ASSESS_VENDOR_B
    , A.REMARK = B.REMARK
    FROM TR_VC_RISK_ASSESSMENT_TEMP A 
    JOIN (
        select 1 AS SEQ_NO, @ASSESS_ITEM_1 AS PARAMETER, @ASSESS_VENDOR_A_1 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_1 AS ASSESS_VENDOR_B, @ASSESS_REMARK_1 AS REMARK UNION
        select 2 AS SEQ_NO, @ASSESS_ITEM_2 AS PARAMETER, @ASSESS_VENDOR_A_2 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_2 AS ASSESS_VENDOR_B, @ASSESS_REMARK_2 AS REMARK UNION
        select 3 AS SEQ_NO, @ASSESS_ITEM_3 AS PARAMETER, @ASSESS_VENDOR_A_3 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_3 AS ASSESS_VENDOR_B, @ASSESS_REMARK_3 AS REMARK UNION
        select 4 AS SEQ_NO, @ASSESS_ITEM_4 AS PARAMETER, @ASSESS_VENDOR_A_4 AS ASSESS_VENDOR_A, @ASSESS_VENDOR_B_4 AS ASSESS_VENDOR_B, @ASSESS_REMARK_4 AS REMARK
    ) AS B ON A.SEQ_NO = B.SEQ_NO
    WHERE A.DOC_ID = @UID

    -- insert risk management comment

    -- update snti social checksheet
    UPDATE A
    SET A.[POSITION] = B.[POSITION]
    , A.[NAME] = B.[NAME]
    , A.NATIONALITY = B.NATIONALITY
    , A.SHARE = B.SHARE
    FROM TR_VC_ASC_CHECKSHEET_TEMP A 
    JOIN (
        select 1 AS SEQ_NO, @SHEET_POSITION_1 AS [POSITION], @SHEET_NAME_1 AS [NAME], @SHEET_NATIONALITY_1 AS NATIONALITY, @SHEET_SHARE_1 AS SHARE UNION
        select 2 AS SEQ_NO, @SHEET_POSITION_2 AS [POSITION], @SHEET_NAME_2 AS [NAME], @SHEET_NATIONALITY_2 AS NATIONALITY, @SHEET_SHARE_2 AS SHARE UNION
        select 3 AS SEQ_NO, @SHEET_POSITION_3 AS [POSITION], @SHEET_NAME_3 AS [NAME], @SHEET_NATIONALITY_3 AS NATIONALITY, @SHEET_SHARE_3 AS SHARE UNION
        select 4 AS SEQ_NO, @SHEET_POSITION_4 AS [POSITION], @SHEET_NAME_4 AS [NAME], @SHEET_NATIONALITY_4 AS NATIONALITY, @SHEET_SHARE_4 AS SHARE UNION
        select 5 AS SEQ_NO, @SHEET_POSITION_5 AS [POSITION], @SHEET_NAME_5 AS [NAME], @SHEET_NATIONALITY_5 AS NATIONALITY, @SHEET_SHARE_5 AS SHARE UNION
        select 6 AS SEQ_NO, @SHEET_POSITION_6 AS [POSITION], @SHEET_NAME_6 AS [NAME], @SHEET_NATIONALITY_6 AS NATIONALITY, @SHEET_SHARE_6 AS SHARE UNION
        select 7 AS SEQ_NO, @SHEET_POSITION_7 AS [POSITION], @SHEET_NAME_7 AS [NAME], @SHEET_NATIONALITY_7 AS NATIONALITY, @SHEET_SHARE_7 AS SHARE UNION
        select 8 AS SEQ_NO, @SHEET_POSITION_8 AS [POSITION], @SHEET_NAME_8 AS [NAME], @SHEET_NATIONALITY_8 AS NATIONALITY, @SHEET_SHARE_8 AS SHARE UNION
        select 9 AS SEQ_NO, @SHEET_POSITION_9 AS [POSITION], @SHEET_NAME_9 AS [NAME], @SHEET_NATIONALITY_9 AS NATIONALITY, @SHEET_SHARE_9 AS SHARE UNION
        select 10 AS SEQ_NO, @SHEET_POSITION_10 AS [POSITION], @SHEET_NAME_10 AS [NAME], @SHEET_NATIONALITY_10 AS NATIONALITY, @SHEET_SHARE_10 AS SHARE
    ) AS B ON A.SEQ_NO = B.SEQ_NO
    WHERE A.DOC_ID = @UID
END
GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORFROMTEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_VENDORFROMTEMP]
	-- add more stored procedure parameters here
	@DOCID BIGINT,
	@REFID BIGINT
AS
BEGIN
	-- body of the stored procedure
	UPDATE vc1
	SET vc1.VENDOR_ID = vc2.VENDOR_ID,
		vc1.VENDOR_NAME = vc2.VENDOR_NAME,
		vc1.CREATOR = vc2.CREATOR,
		vc1.CREATOR_DEPT = vc2.CREATOR_DEPT,
		vc1.CREATOR_UNIT = vc2.CREATOR_UNIT,
		vc1.CREATOR_ROLE = vc2.CREATOR_ROLE,
		vc1.REQUEST_DT = vc2.REQUEST_DT,
		vc1.DOC_DATE_FORM1 = vc2.DOC_DATE_FORM1,
		vc1.CONF_CONTRACTOR_NM = vc2.CONF_CONTRACTOR_NM,
		vc1.CONF_OPERATION_TO_OUTSOURCE = vc2.CONF_OPERATION_TO_OUTSOURCE,
		vc1.CONF_CATEGORY = vc2.CONF_CATEGORY,
		vc1.CONF_DEPT_CD = vc2.CONF_DEPT_CD,
		vc1.CONF_BRIEF_EXPLAIN_CONTRACT = vc2.CONF_BRIEF_EXPLAIN_CONTRACT,
		vc1.CONF_DECISION = vc2.CONF_DECISION,
		vc1.CONF_GAD_COMMENT = vc2.CONF_GAD_COMMENT,
		vc1.DOC_DATE_FORM2 = vc2.DOC_DATE_FORM2,
		vc1.EXAM_NAME_OF_GROUP = vc2.EXAM_NAME_OF_GROUP,
		vc1.EXAM_CORE_BUSINESS = vc2.EXAM_CORE_BUSINESS,
		vc1.EXAM_REL_VENDOR = vc2.EXAM_REL_VENDOR,
		vc1.EXAM_REL_VENDOR_REMARK = vc2.EXAM_REL_VENDOR_REMARK,
		vc1.EXAM_HAS_ENGAGED_BEFORE = vc2.EXAM_HAS_ENGAGED_BEFORE,
		vc1.EXAM_HAS_ENGAGED_SINCE = vc2.EXAM_HAS_ENGAGED_SINCE,
		vc1.EXAM_BACKGROUND = vc2.EXAM_BACKGROUND,
		vc1.EXAM_VENDOR_A = vc2.EXAM_VENDOR_A,
		vc1.EXAM_VENDOR_B = vc2.EXAM_VENDOR_B,
		vc1.EXAM_OVERALL_CONCLUSION = vc2.EXAM_OVERALL_CONCLUSION,
		vc1.RISK_ANNUAL_BILLING = vc2.RISK_ANNUAL_BILLING,
		vc1.RISK_SPECIAL_RELATED = vc2.RISK_SPECIAL_RELATED,
		vc1.RISK_CPC_ASSESSMENT = vc2.RISK_CPC_ASSESSMENT,
		vc1.RISK_PROVIDE_ACCESS = vc2.RISK_PROVIDE_ACCESS,
		vc1.RISK_AS_VENDOR_A = vc2.RISK_AS_VENDOR_A,
		vc1.RISK_AS_VENDOR_B = vc2.RISK_AS_VENDOR_B,
		vc1.RISK_AML_COMMENT = vc2.RISK_AML_COMMENT,
		vc1.RISK_CATEGORY = vc2.RISK_CATEGORY,
		vc1.RISK_RMD_RECOMMENDATION = vc2.RISK_RMD_RECOMMENDATION,
		vc1.RISK_SCREENING_RESULT = vc2.RISK_SCREENING_RESULT,
		vc1.REG_PROVIDED_SERVICE = vc2.REG_PROVIDED_SERVICE,
		vc1.REG_CONTRACT_NM = vc2.REG_CONTRACT_NM,
		vc1.REG_LEGAL_STATUS = vc2.REG_LEGAL_STATUS,
		vc1.REG_NPWP_NO = vc2.REG_NPWP_NO,
		vc1.REG_ADDRESS = vc2.REG_ADDRESS,
		vc1.REG_CITY = vc2.REG_CITY,
		vc1.REG_POSTAL = vc2.REG_POSTAL,
		vc1.REG_WEBSITE = vc2.REG_WEBSITE,
		vc1.REG_EMAIL = vc2.REG_EMAIL,
		vc1.REG_PHONE_1 = vc2.REG_PHONE_1,
		vc1.REG_PHONE_EXT_1 = vc2.REG_PHONE_EXT_1,
		vc1.REG_PHONE_2 = vc2.REG_PHONE_2,
		vc1.REG_PHONE_EXT_2 = vc2.REG_PHONE_EXT_2,
		vc1.REG_FAX_1 = vc2.REG_FAX_1,
		vc1.REG_FAX_2 = vc2.REG_FAX_2,
		vc1.REG_CP_NM_1 = vc2.REG_CP_NM_1,
		vc1.REG_CP_POSITION_1 = vc2.REG_CP_POSITION_1,
		vc1.REG_CP_NM_2 = vc2.REG_CP_NM_2,
		vc1.REG_CP_POSITION_2 = vc2.REG_CP_POSITION_2,
		vc1.REG_PAYMENT_TYPE = vc2.REG_PAYMENT_TYPE,
		vc1.REG_REASON_CASH = vc2.REG_REASON_CASH,
		vc1.REG_ACC_HOLDER_NM = vc2.REG_ACC_HOLDER_NM,
		vc1.REG_ACC_NO = vc2.REG_ACC_NO,
		vc1.REG_ACC_BANK = vc2.REG_ACC_BANK,
		vc1.DOC_DATE_FORM4A = vc2.DOC_DATE_FORM4A,
		vc1.ASC_CONTRACTOR_JAPANESE = vc2.ASC_CONTRACTOR_JAPANESE,
		vc1.ASC_SUBCONTRACT_OPERATION = vc2.ASC_SUBCONTRACT_OPERATION,
		vc1.APPROVAL_STS = 'Approval Completed'
	FROM TR_VENDOR_CONTROL AS vc1
	JOIN TR_VENDOR_CONTROL_TEMP AS vc2 ON vc2.VENDOR_ID = vc1.VENDOR_ID
	WHERE vc1.UID = @REFID
		AND vc2.UID = @DOCID

	UPDATE cdc1
	SET cdc1.ITEM = cdc2.ITEM,
		cdc1.SUBMITTED = cdc2.SUBMITTED,
		cdc1.REASON = cdc2.REASON,
		cdc1.CHECKED = cdc2.CHECKED,
		cdc1.REASON_CHECKED = cdc2.REASON_CHECKED
	FROM TR_VC_CONF_DOC_CHECKLIST AS cdc1
	JOIN TR_VC_CONF_DOC_CHECKLIST_TEMP AS cdc2 ON cdc1.SEQ = cdc2.SEQ
	WHERE cdc1.DOC_ID = @REFID
		AND cdc2.DOC_ID = @DOCID

	UPDATE ec1
	SET ec1.PARAMETER = ec2.PARAMETER,
		ec1.COMPARE_VENDOR_A = ec2.COMPARE_VENDOR_A,
		ec1.COMPARE_VENDOR_B = ec2.COMPARE_VENDOR_B,
		ec1.REMARK = ec2.REMARK
	FROM TR_VC_EXAM_COMPARISON AS ec1
	JOIN TR_VC_EXAM_COMPARISON_TEMP AS ec2 ON ec1.SEQ_NO = ec2.SEQ_NO
	WHERE ec1.DOC_ID = @REFID
		AND ec2.DOC_ID = @DOCID

	UPDATE ra1
	SET ra1.PARAMETER = ra2.PARAMETER,
		ra1.ASSESS_VENDOR_A = ra2.ASSESS_VENDOR_A,
		ra1.ASSESS_VENDOR_B = ra2.ASSESS_VENDOR_B,
		ra1.REMARK = ra2.REMARK
	FROM TR_VC_RISK_ASSESSMENT AS ra1
	JOIN TR_VC_RISK_ASSESSMENT_TEMP AS ra2 ON ra1.SEQ_NO = ra2.SEQ_NO
	WHERE ra1.DOC_ID = @REFID
		AND ra2.DOC_ID = @DOCID

	UPDATE ac1
	SET ac1.[POSITION] = ac2.[POSITION],
		ac1.[NAME] = ac2.NAME,
		ac1.NATIONALITY = ac2.NATIONALITY,
		ac1.SHARE = ac2.SHARE
	FROM TR_VC_ASC_CHECKSHEET AS ac1
	JOIN TR_VC_ASC_CHECKSHEET_TEMP AS ac2 ON ac1.SEQ_NO = ac2.SEQ_NO
	WHERE ac1.DOC_ID = @REFID
		AND ac2.DOC_ID = @DOCID

	DELETE TR_ATTACHMENT
	WHERE DOC_ID = @REFID
		AND FILE_TYPE = 'REGISTRATION'
    
    INSERT INTO TR_ATTACHMENT (
		DOC_ID,
		FILE_TYPE,
		FILE_NM,
		SAVE_FILE_NM,
		FILE_URL,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST
		)
	SELECT @REFID,
		FILE_TYPE,
		FILE_NM,
		SAVE_FILE_NM,
		FILE_URL,
		IS_ACTIVE,
		CREATED_BY,
		CREATED_DT,
		CREATED_HOST,
		MODIFIED_BY,
		MODIFIED_DT,
		MODIFIED_HOST
	FROM TR_ATTACHMENT_TEMP
	WHERE 1 = 1
		AND FILE_TYPE = 'REGISTRATION'
		AND DOC_ID = @DOCID

END

GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_VENDORLIST]

	-- Add the parameters for the stored procedure here

	@Id INT
	,@VENDOR_ID VARCHAR(50)
	,@VENDOR_NAME VARCHAR(255)
	,@CREATOR VARCHAR(50)
	,@CREATOR_DEPT VARCHAR(50)
	,@CREATOR_UNIT VARCHAR(50)
	,@CREATOR_ROLE VARCHAR(50)
	,@REQUEST_DT VARCHAR(50)
	,@DOC_DATE_FORM1 VARCHAR(50)
	,@CONF_CONTRACTOR_NM VARCHAR(50)
	,@CONF_OPERATION_TO_OUTSOURCE VARCHAR(50)
	,@CONF_CATEGORY VARCHAR(50)
	,@CONF_DEPT_CD VARCHAR(50)
	,@CONF_BRIEF_EXPLAIN_CONTRACT VARCHAR(50)
	,@CONF_DECISION VARCHAR (50)
	,@CONF_GAD_COMMENT VARCHAR(50)
	,@DOC_DATE_FORM2 VARCHAR(50)
	,@EXAM_NAME_OF_GROUP VARCHAR(50)
	,@EXAM_CORE_BUSINESS VARCHAR(50)
	,@EXAM_REL_VENDOR VARCHAR(50)
	,@EXAM_REL_VENDOR_REMARK VARCHAR(50)
	,@EXAM_HAS_ENGAGED_BEFORE VARCHAR(50)
	,@EXAM_HAS_ENGAGED_SINCE VARCHAR(50)
	,@EXAM_BACKGROUND VARCHAR(50)
	,@EXAM_VENDOR_A VARCHAR(50)
	,@EXAM_VENDOR_B VARCHAR(50)
	,@EXAM_OVERALL_CONCLUSION VARCHAR(50)
	,@RISK_ANNUAL_BILLING VARCHAR(50)
	,@RISK_SPECIAL_RELATED VARCHAR(50)
	,@RISK_CPC_ASSESSMENT VARCHAR(50)
	,@RISK_PROVIDE_ACCESS VARCHAR(50)
	,@RISK_AS_VENDOR_A VARCHAR(50)
	,@RISK_AS_VENDOR_B VARCHAR(50)

	,@RISK_AML_COMMENT VARCHAR(50)
	,@RISK_CATEGORY VARCHAR(50)
	,@RISK_RMD_RECOMMENDATION VARCHAR(50)
	,@RISK_SCREENING_RESULT VARCHAR(50)
	,@REG_PROVIDED_SERVICE VARCHAR (50)
	,@REG_CONTRACT_NM VARCHAR (50)
	,@REG_LEGAL_STATUS VARCHAR(50)
	,@REG_NPWP_NO VARCHAR(50)
	,@REG_ADDRESS VARCHAR(255)
	,@REG_CITY VARCHAR(50)
	,@REG_POSTAL VARCHAR(50)
	,@REG_WEBSITE VARCHAR(100)
	,@REG_EMAIL VARCHAR(100)
	,@REG_PHONE_1 VARCHAR (50)
	,@REG_PHONE_EXT_1 VARCHAR(10)
	,@REG_PHONE_2 VARCHAR(50)
	,@REG_PHONE_EXT_2 VARCHAR(10)
	,@REG_FAX_1 VARCHAR(50)
	,@REG_FAX_2 VARCHAR(50)
	,@REG_CP_NM_1 VARCHAR(150)
	,@REG_CP_POSITION_1 VARCHAR(50)
	,@REG_CP_NM_2 VARCHAR (150)
	,@REG_CP_POSITION_2 VARCHAR(50)
	,@REG_PAYMENT_TYPE VARCHAR(50)
	,@REG_REASON_CASH VARCHAR(255)
	,@REG_ACC_HOLDER_NM VARCHAR(150)
	,@REG_ACC_NO VARCHAR (100)
	,@REG_ACC_BANK VARCHAR(100)
	,@DOC_DATE_FORM4A VARCHAR(50)
	,@ASC_CONTRACTOR_JAPANESE VARCHAR(50)
	,@ASC_SUBCONTRACT_OPERATION VARCHAR(50)
	,@AGREE_START_DATE VARCHAR(50)
	,@AGREE_END_DATE VARCHAR(50)
	,@AGREE_CREATED_BY VARCHAR(50)
	,@AGREE_CREATED_DT VARCHAR(50)
	,@AGREE_MODIFIED_BY VARCHAR(50)
	,@AGREE_MODIFIED_DT VARCHAR(50)
	,@TERMINATION VARCHAR(50)
	,@TERMINATION_BY VARCHAR(50)
	,@TERMINATION_DT VARCHAR(50)
	,@REGISTRATION_DT VARCHAR(50)
	,@APPROVAL_STS VARCHAR(50)
	,@IS_ACTIVE VARCHAR(50)
	
	,@MODIFIED_BY VARCHAR(100)
	,@MODIFIED_HOST VARCHAR(50)

	,@STATUS_FLOW VARCHAR(50)
AS

BEGIN
	SET NOCOUNT ON;
	UPDATE dbo.[TR_VENDOR_CONTROL]
	SET [VENDOR_NAME]=@VENDOR_NAME
    /*
	,[CREATOR]=@CREATOR
    ,[CREATOR_DEPT]=@CREATOR_DEPT
    ,[CREATOR_UNIT]=@CREATOR_UNIT
    ,[CREATOR_ROLE]=@CREATOR_ROLE
	*/
    ,[REQUEST_DT]=@REQUEST_DT
    ,[DOC_DATE_FORM1]=@DOC_DATE_FORM1
    ,[CONF_CONTRACTOR_NM]=@CONF_CONTRACTOR_NM
	,[CONF_OPERATION_TO_OUTSOURCE]=@CONF_OPERATION_TO_OUTSOURCE
    ,[CONF_CATEGORY]=@CONF_CATEGORY
    ,[CONF_DEPT_CD]=@CONF_DEPT_CD
    ,[CONF_BRIEF_EXPLAIN_CONTRACT]=@CONF_BRIEF_EXPLAIN_CONTRACT
    ,[CONF_DECISION]=@CONF_DECISION
    ,[CONF_GAD_COMMENT]=@CONF_GAD_COMMENT
	,[DOC_DATE_FORM2]=@DOC_DATE_FORM2 
    ,[EXAM_NAME_OF_GROUP]=@EXAM_NAME_OF_GROUP
    ,[EXAM_CORE_BUSINESS]=@EXAM_CORE_BUSINESS
    ,[EXAM_REL_VENDOR]=@EXAM_REL_VENDOR
    ,[EXAM_REL_VENDOR_REMARK]=@EXAM_REL_VENDOR_REMARK
    ,[EXAM_HAS_ENGAGED_BEFORE]=@EXAM_HAS_ENGAGED_BEFORE
    ,[EXAM_HAS_ENGAGED_SINCE]=@EXAM_HAS_ENGAGED_SINCE
    ,[EXAM_BACKGROUND]=@EXAM_BACKGROUND
    ,[EXAM_VENDOR_A]=@EXAM_VENDOR_A
    ,[EXAM_VENDOR_B]=@EXAM_VENDOR_B
    ,[EXAM_OVERALL_CONCLUSION]=@EXAM_OVERALL_CONCLUSION
	,[RISK_ANNUAL_BILLING]=@RISK_ANNUAL_BILLING
    ,[RISK_SPECIAL_RELATED]=@RISK_SPECIAL_RELATED
    ,[RISK_CPC_ASSESSMENT]=@RISK_CPC_ASSESSMENT
    ,[RISK_PROVIDE_ACCESS]=@RISK_PROVIDE_ACCESS
    ,[RISK_AS_VENDOR_A]=@RISK_AS_VENDOR_A
    ,[RISK_AS_VENDOR_B]=@RISK_AS_VENDOR_B
    ,[RISK_AML_COMMENT]=@RISK_AML_COMMENT
    ,[RISK_CATEGORY]=@RISK_CATEGORY
    ,[RISK_RMD_RECOMMENDATION]=@RISK_RMD_RECOMMENDATION
    ,[RISK_SCREENING_RESULT]=@RISK_SCREENING_RESULT
	,[REG_PROVIDED_SERVICE]=@REG_PROVIDED_SERVICE
    ,[REG_CONTRACT_NM]=@REG_CONTRACT_NM
    ,[REG_LEGAL_STATUS]=@REG_LEGAL_STATUS
    ,[REG_NPWP_NO]=@REG_NPWP_NO
    ,[REG_ADDRESS]=@REG_ADDRESS
    ,[REG_CITY]=@REG_CITY
    ,[REG_POSTAL]=@REG_POSTAL
    ,[REG_WEBSITE]=@REG_WEBSITE
    ,[REG_EMAIL]=@REG_EMAIL
    ,[REG_PHONE_1]=@REG_PHONE_1
    ,[REG_PHONE_EXT_1]=@REG_PHONE_EXT_1
    ,[REG_PHONE_2]=@REG_PHONE_2
    ,[REG_PHONE_EXT_2]=@REG_PHONE_EXT_2
    ,[REG_FAX_1]=@REG_FAX_1
    ,[REG_FAX_2]=@REG_FAX_2
    ,[REG_CP_NM_1]=@REG_CP_NM_1
    ,[REG_CP_POSITION_1]=@REG_CP_POSITION_1
    ,[REG_CP_NM_2]=@REG_CP_NM_2
    ,[REG_CP_POSITION_2]=@REG_CP_POSITION_2
    ,[REG_PAYMENT_TYPE]=@REG_PAYMENT_TYPE
    ,[REG_REASON_CASH]=@REG_REASON_CASH
    ,[REG_ACC_HOLDER_NM]=@REG_ACC_HOLDER_NM
    ,[REG_ACC_NO]=@REG_ACC_NO
    ,[REG_ACC_BANK]=@REG_ACC_BANK

	,[DOC_DATE_FORM4A]=@DOC_DATE_FORM4A
    ,[ASC_CONTRACTOR_JAPANESE]=@ASC_CONTRACTOR_JAPANESE
    ,[ASC_SUBCONTRACT_OPERATION]=@ASC_SUBCONTRACT_OPERATION
    ,[AGREE_START_DATE]=@AGREE_START_DATE
    ,[AGREE_END_DATE]=@AGREE_END_DATE
    ,[AGREE_CREATED_BY]=@AGREE_CREATED_BY
    ,[AGREE_CREATED_DT]=@AGREE_CREATED_DT
    ,[AGREE_MODIFIED_BY]=@AGREE_MODIFIED_BY
    ,[AGREE_MODIFIED_DT]=@AGREE_MODIFIED_DT
    ,[TERMINATION]=@TERMINATION
    ,[TERMINATION_BY]=@TERMINATION_BY
    ,[TERMINATION_DT]=@TERMINATION_DT

	,[REGISTRATION_DT]=@REGISTRATION_DT
    ,[APPROVAL_STS]=@APPROVAL_STS
    ,[IS_ACTIVE]=@IS_ACTIVE
    ,[MODIFIED_BY]=@MODIFIED_BY
    ,[MODIFIED_DT]=GETDATE() 
    ,[MODIFIED_HOST]=@MODIFIED_HOST
	,[STATUS_FLOW]=@STATUS_FLOW 
	WHERE [UID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORLIST_ATTACH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_VENDORLIST_ATTACH]

	-- Add the parameters for the stored procedure here
	@Id int
	,@DOC_ID VARCHAR(50)
      ,@FILE_TYPE VARCHAR(50)
      ,@FILE_NM VARCHAR(50)
      ,@SAVE_FILE_NM VARCHAR(80)
      ,@FILE_URL VARCHAR(200)
      ,@IS_ACTIVE VARCHAR(50)
      ,@MODIFIED_BY VARCHAR(50)
      ,@MODIFIED_DT VARCHAR(50)
      ,@MODIFIED_HOST VARCHAR(50)

AS

BEGIN
SET NOCOUNT ON;
IF (NOT EXISTS(SELECT [UID] FROM [TR_ATTACHMENT] WHERE [UID] = @Id) ) 
BEGIN 
     INSERT INTO dbo.[TR_ATTACHMENT] 
		(
		  [DOC_ID]
		  ,[FILE_TYPE]
		  ,[FILE_NM]
		  ,[SAVE_FILE_NM]
		  ,[FILE_URL]
		  ,[IS_ACTIVE]
		  ,[CREATED_BY]
		  ,[CREATED_DT]
		  ,[CREATED_HOST]
		 
		)
		VALUES
		(	
		   @DOC_ID 
		  ,@FILE_TYPE 
		  ,@FILE_NM 
		  ,@SAVE_FILE_NM 
		  ,@FILE_URL 
		  ,@IS_ACTIVE 
		  ,@MODIFIED_BY 
		  ,@MODIFIED_DT 
		  ,@MODIFIED_HOST 
		 
		)
		SELECT @@IDENTITY
END 
ELSE 
BEGIN 
    UPDATE [TR_ATTACHMENT] 
	SET [DOC_ID]=@DOC_ID
      ,[FILE_TYPE]=@FILE_TYPE
      ,[FILE_NM]=@FILE_NM
	  ,[SAVE_FILE_NM]=@SAVE_FILE_NM
      ,[FILE_URL]=@FILE_URL
      ,[IS_ACTIVE]=@IS_ACTIVE
      ,[MODIFIED_BY]=@MODIFIED_BY
	  ,[MODIFIED_DT]=GETDATE()
	  ,[MODIFIED_HOST]=@MODIFIED_HOST
	WHERE [UID] = @Id
END 

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORLIST_CHECKDOC]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_VENDORLIST_CHECKDOC]

	-- Add the parameters for the stored procedure here
	@Id int
	,@DOC_ID VARCHAR(50)
    ,@SEQ VARCHAR(50)
    ,@ITEM VARCHAR(255)
    ,@SUBMITTED VARCHAR(50)
    ,@REASON VARCHAR(8000)
    ,@CHECKED VARCHAR(50)
    ,@REASON_CHECKED VARCHAR(8000)
    ,@IS_ACTIVE VARCHAR(50)
    ,@MODIFIED_BY VARCHAR(50)
    ,@MODIFIED_HOST VARCHAR(50)

AS

BEGIN
SET NOCOUNT ON;
IF (NOT EXISTS(SELECT [UID] FROM [TR_VC_CONF_DOC_CHECKLIST] WHERE [UID] = @Id) AND (@ITEM IS NOT NULL) ) 
BEGIN 
    INSERT INTO dbo.[TR_VC_CONF_DOC_CHECKLIST] 
	(
	   [DOC_ID]
      ,[SEQ]
      ,[ITEM]
      ,[SUBMITTED]
      ,[REASON]
      ,[CHECKED]
      ,[REASON_CHECKED]
      ,[IS_ACTIVE]
      ,[CREATED_BY]
      ,[CREATED_DT]
      ,[CREATED_HOST]

	)
	VALUES
	(	
	   @DOC_ID 
      ,@SEQ 
      ,@ITEM 
      ,@SUBMITTED 
      ,@REASON 
      ,@CHECKED 
      ,@REASON_CHECKED 
      ,@IS_ACTIVE 
      ,@MODIFIED_BY 
	  ,GETDATE() 
      ,@MODIFIED_HOST 
	)
END 
ELSE 
BEGIN 
    UPDATE [TR_VC_CONF_DOC_CHECKLIST] 
	SET [DOC_ID]=@DOC_ID
      ,[SUBMITTED]=@SUBMITTED
      ,[REASON]=@REASON
	  ,[REASON_CHECKED]=@REASON_CHECKED
      ,[CHECKED]=@CHECKED
      ,[IS_ACTIVE]=@IS_ACTIVE
      ,[MODIFIED_BY]=@MODIFIED_BY
	  ,[MODIFIED_DT]=GETDATE()
	  ,[MODIFIED_HOST]=@MODIFIED_HOST
	WHERE [UID] = @Id
END 

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORLIST_CHECKSHEET]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_VENDORLIST_CHECKSHEET]

	-- Add the parameters for the stored procedure here
	@Id int
	 ,@DOC_ID VARCHAR(50)
      ,@SEQ_NO VARCHAR(50)
      ,@POSITION VARCHAR(50)
      ,@NAME VARCHAR(255)
      ,@NATIONALITY VARCHAR(255)
      ,@SHARE VARCHAR(50)
      ,@IS_ACTIVE VARCHAR(50)
      ,@MODIFIED_BY VARCHAR(50)
      ,@MODIFIED_HOST VARCHAR(50)
AS

BEGIN
	SET NOCOUNT ON;
	UPDATE [TR_VC_ASC_CHECKSHEET] 
	SET [DOC_ID]=@DOC_ID
      ,[SEQ_NO]=@SEQ_NO
      ,[POSITION]=@POSITION
      ,[NAME]=@NAME
      ,[NATIONALITY]=@NATIONALITY
      ,[SHARE]=@SHARE
      ,[IS_ACTIVE]=@IS_ACTIVE
      ,[MODIFIED_BY]=@MODIFIED_BY
	  ,[MODIFIED_DT]=GETDATE()
	  ,[MODIFIED_HOST]=@MODIFIED_HOST
	WHERE [UID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORLIST_EXAM]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_VENDORLIST_EXAM]

	-- Add the parameters for the stored procedure here
	@Id int
	,@DOC_ID VARCHAR(50)
    ,@SEQ_NO VARCHAR(50)
    ,@PARAMETER VARCHAR(200)
    ,@COMPARE_VENDOR_A VARCHAR(255)
    ,@COMPARE_VENDOR_B VARCHAR(255)
    ,@REMARK nvarchar(max)
    ,@IS_ACTIVE VARCHAR(50)
    ,@MODIFIED_BY VARCHAR(50)
    ,@MODIFIED_HOST VARCHAR(50)

AS

BEGIN
	SET NOCOUNT ON;
	UPDATE [TR_VC_EXAM_COMPARISON] 
	SET [DOC_ID]=@DOC_ID
      ,[SEQ_NO]=@SEQ_NO
      ,[PARAMETER]=@PARAMETER
      ,[COMPARE_VENDOR_A]=@COMPARE_VENDOR_A
      ,[COMPARE_VENDOR_B]=@COMPARE_VENDOR_B
      ,[REMARK]=@REMARK
      ,[IS_ACTIVE]=@IS_ACTIVE
      ,[MODIFIED_BY]=@MODIFIED_BY
	  ,[MODIFIED_DT]=GETDATE()
	  ,[MODIFIED_HOST]=@MODIFIED_HOST
	WHERE [UID] = @Id

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_VENDORLIST_TERMIN]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UDPU_VENDORLIST_TERMIN]

	-- Add the parameters for the stored procedure here

	@Id VARCHAR(50) 
	,@TERMINATION VARCHAR(50)
	,@TERMINATION_BY VARCHAR(50)
	,@TERMINATION_DT VARCHAR(50)

AS

BEGIN
	SET NOCOUNT ON;
	UPDATE dbo.[TR_VENDOR_CONTROL]
	SET [TERMINATION]=@TERMINATION
    ,[TERMINATION_BY]=@TERMINATION_BY
    ,[TERMINATION_DT]=@TERMINATION_DT
	,[APPROVAL_STS]='Terminated' 
	WHERE [UID] = @Id 

	UPDATE dbo.[TR_VENDOR_CONTROL_ACTIVITY]
	SET [TERMINATION]=@TERMINATION
    ,[TERMINATION_BY]=@TERMINATION_BY
    ,[TERMINATION_DT]=@TERMINATION_DT
	,[APPROVAL_STS]='Terminated' 
	WHERE [DOC_ID] = @Id 

END



GO
/****** Object:  StoredProcedure [dbo].[UDPU_WFPROCDETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_WFPROCDETAIL]
	-- add more stored procedure parameters here
	@UserList VARCHAR(8000),
	@WFStep INT,
	@WFID INT,
	@SN VARCHAR(20)
AS
BEGIN
	-- body of the stored procedure
	DECLARE @TabUserList TABLE (
		ID INT IDENTITY(1, 1) NOT NULL,
		UserName VARCHAR(50)
		)
	DECLARE @CountUser INT

    IF charindex('JAKARTA01\', @UserList) > 0
		SET @UserList = replace(@UserList, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @UserList) > 0
		SET @UserList = replace(@UserList, 'jakarta01\', '')

    UPDATE TR_WORKFLOWPROCESS
	SET WFSTEP = @WFStep
	WHERE [UID] = @WFID

	INSERT INTO @TabUserList (UserName)
	SELECT Name
	FROM dbo.splitstring(@UserList, ';')

	-- delete wf detail jika pindah step
	IF NOT EXISTS (
			SELECT 1
			FROM TR_WORKFLOWPROCESS_DETAIL
			WHERE WFID = @WFID
				AND WFSTEP = @WFStep
			)
	BEGIN
		DELETE TR_WORKFLOWPROCESS_DETAIL
		WHERE WFID = @WFID

		INSERT INTO TR_WORKFLOWPROCESS_DETAIL (
			WFID,
			WFSTEP,
			SN,
			ACTION_BY
			)
		SELECT @WFID,
			@WFStep,
			'0',
			UserName
		FROM @TabUserList
	END

	SELECT @CountUser = count(uid) + 1
	FROM TR_WORKFLOWPROCESS_DETAIL
	WHERE WFID = @WFID
		AND WFSTEP = @WFStep
		AND SN <> '0'

	UPDATE twd
	SET twd.SN = @SN
	FROM TR_WORKFLOWPROCESS_DETAIL AS twd
	JOIN @TabUserList AS tul ON twd.ACTION_BY = tul.UserName
	WHERE tul.ID = @CountUser
        AND twd.WFID = @WFID
END


GO
/****** Object:  StoredProcedure [dbo].[UDPU_WFPROCIDTASK]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Create the stored procedure in the specified schema
CREATE PROCEDURE [dbo].[UDPU_WFPROCIDTASK]
	-- add more stored procedure parameters here
	@WFID BIGINT,
	@ProcID VARCHAR(59),
	@WFTASK VARCHAR(50)
AS
BEGIN
	-- body of the stored procedure
	UPDATE TR_WORKFLOWPROCESS
	SET PROCID = @ProcID,
		WFTASKNAME = @WFTASK,
		[MODIFIED_BY] = 'SYSTEM',
		MODIFIED_DT = GETDATE()
	WHERE [UID] = @WFID
END


GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Department]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Department] FOR [vw_BTMUDirectory_Department]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Department_All]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Department_All] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Department]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Division]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Division] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Division]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Group]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Group] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Group]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Role]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Role] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Role]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_RoleParent]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_RoleParent] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Role_Parent]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_Unit]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_Unit] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[Unit]
GO
/****** Object:  Synonym [dbo].[SN_BTMUDirectory_User]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_BTMUDirectory_User] FOR [JKTSVRDEV03].[BTMUDirectory].[dbo].[User]
GO
/****** Object:  Synonym [dbo].[SN_CONE_Cone_User]    Script Date: 16/03/2022 21:02:58 ******/
CREATE SYNONYM [dbo].[SN_CONE_Cone_User] FOR [JKTSVRDEV03].[Cone].[dbo].[Cone_User]
GO
/****** Object:  UserDefinedFunction [dbo].[fnEMAILREQUESTAPPROVALUSRDH]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnEMAILREQUESTAPPROVALUSRDH] (
	@DOCID BIGINT,
	@HISTTYPE VARCHAR(100),
	@ORIGINATOR VARCHAR(100)
	)
RETURNS @tab TABLE (
	id INT identity(1, 1) NOT NULL,
	Column1 VARCHAR(5000) NULL
	)
AS
BEGIN
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @ORIGINATOR

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'jakarta01\', '')

	DECLARE @PreviousApprover VARCHAR(100) = ''
	DECLARE @Comment VARCHAR(8000) = ''
	DECLARE @AssignmentDate DATE
	DECLARE @CharAssignmentDate VARCHAR(50) = ''
	DECLARE @CurrentApprover VARCHAR(2000) = ''

	--{{ PreviousApprover }}
	--{{ Comment }}
	--{{ AssignmentDate }}
	--{{ CurrentApprover }}
	SELECT @CurrentApprover = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE UnitID = (
							SELECT UnitID
							FROM vw_UserByDepartment
							WHERE Username = @StrUsername
							)
						AND RoleShortName = 'UH'
					FOR XML PATH('')
					), 1, 1, ''), '')

	SET @AssignmentDate = GETDATE()
	SET @CharAssignmentDate = convert(VARCHAR, @AssignmentDate, 107) + ' at ' + convert(VARCHAR, @AssignmentDate, 24)

	SELECT TOP 1 @PreviousApprover = ACTION_BY,
		@Comment = COMMENT
	FROM TR_DOCUMENT_HISTORY
	WHERE HIST_TYPE = @HISTTYPE
		AND DOC_ID = @DOCID
	ORDER BY SEQ_NO DESC

	INSERT INTO @tab (Column1)
	SELECT REPLACE(REPLACE(REPLACE(REPLACE(DESCS, '{{ PreviousApprover }}', @PreviousApprover), '{{ Comment }}', @Comment), '{{ AssignmentDate }}', @CharAssignmentDate), '{{ CurrentApprover }}', @CurrentApprover)
	FROM MS_PARAMETER
	WHERE [GROUP] = 'EMAIL_CREATOR_APP'
		AND [VALUE] = 'CONTENT'

	RETURN
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetCOMPDIRByDept]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetCOMPDIRByDept] (@Dept VARCHAR(10))
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @ListCOMPDIR VARCHAR(2000)

	SELECT @ListCOMPDIR = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE 1 = 1
						-- AND DepartmentCode = @Dept
						AND RoleShortName = 'COMPLIANCE DIRECTOR'
					FOR XML PATH('')
					), 1, 1, ''), '')

	RETURN @ListCOMPDIR
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDGMByDept]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnGetDGMByDept] (@Dept VARCHAR(10))
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @ListDGM VARCHAR(2000)
	DECLARE @DivID NVARCHAR(5)

	SELECT @DivID = DivisionID
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @Dept

	-- SELECT @ListDGM = ISNULL(STUFF((
	-- 				SELECT DISTINCT ',' + RealName
	-- 				FROM [vw_UserByDepartment]
	-- 				WHERE 1 = 1
	-- 					AND RoleParentID IN (
	-- 						SELECT RoleParentID
	-- 						FROM vw_UserByDepartment
	-- 						WHERE DivisionID = @DivID
	-- 							AND RoleID >= 53
	-- 							AND RoleID < 70
	-- 						) --modification in 2 july 2018 AGM have a same role as DGM, Mr. Marchelius case
	-- 					--AND IsDeleted = 0
	-- 					-- AND RoleID IN (60, 53) /*RoleID = 60*/
	-- 				FOR XML PATH('')
	-- 				), 1, 1, ''), '')
	SELECT @ListDGM = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM dbo.fnGetTabDGMByDept(@Dept)
					FOR XML PATH('')
					), 1, 1, ''), '')

	RETURN @listDGM
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDGMByOriginator]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnGetDGMByOriginator] (@Originator VARCHAR(100))
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @ListDGM VARCHAR(2000)
	DECLARE @StrUsername NVARCHAR(50)
	DECLARE @Dept VARCHAR(10)

	SET @StrUsername = @ORIGINATOR

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'jakarta01\', '')

	DECLARE @DivID NVARCHAR(5)

	SELECT @DivID = DivisionID,
		@Dept = DepartmentCode
	FROM vw_UserByDepartment
	WHERE Username = @StrUsername

	-- SELECT @ListDGM = ISNULL(STUFF((
	-- 				SELECT DISTINCT ',' + RealName
	-- 				FROM [vw_UserByDepartment]
	-- 				WHERE 1 = 1
	-- 					AND RoleParentID IN (
	-- 						SELECT RoleParentID
	-- 						FROM vw_UserByDepartment
	-- 						WHERE DivisionID = @DivID
	-- 							AND RoleID >= 53
	-- 							AND RoleID < 70
	-- 						) --modification in 2 july 2018 AGM have a same role as DGM, Mr. Marchelius case
	-- 					--AND IsDeleted = 0
	-- 					-- AND RoleID IN (60, 53) /*RoleID = 60*/
	-- 				FOR XML PATH('')
	-- 				), 1, 1, ''), '')
	SELECT @ListDGM = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM dbo.fnGetTabDGMByDept(@Dept)
					FOR XML PATH('')
					), 1, 1, ''), '')

	RETURN @listDGM
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDHByDept]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnGetDHByDept] (@Dept VARCHAR(10))
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @ListDH VARCHAR(2000)

	SELECT @ListDH = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = @Dept
						AND RoleShortName = 'DH'
					FOR XML PATH('')
					), 1, 1, ''), '')

	RETURN @ListDH
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDHByOriginator]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnGetDHByOriginator] (@Originator VARCHAR(100))
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @ListDH VARCHAR(2000)
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @ORIGINATOR

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'jakarta01\', '')

	SELECT @ListDH = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = (
							SELECT DepartmentCode
							FROM vw_UserByDepartment
							WHERE Username = @StrUsername
							)
						AND RoleShortName = 'DH'
					FOR XML PATH('')
					), 1, 1, ''), '')

	RETURN @ListDH
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetTabDGMByDept]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnGetTabDGMByDept] (@DeptCode VARCHAR(10))
RETURNS @tab TABLE (
	id INT identity(1, 1) NOT NULL,
	Username VARCHAR(100) NULL,
	RealName VARCHAR(100) NULL,
	Email VARCHAR(100) NULL
	)
AS
BEGIN
	DECLARE @DivID VARCHAR(5),
		@DeptID VARCHAR(5)

	SELECT @DivID = DivisionID,
		@DeptID = DepartmentID
	FROM vw_UserByDepartment
	WHERE DepartmentCode = @DeptCode

	-- INSERT INTO @tab (
	-- 	Username,
	-- 	RealName,
	-- 	Email
	-- 	)
	-- SELECT Username,
	-- 	RealName,
	-- 	Email
	-- FROM vw_UserByDepartment
	-- WHERE DepartmentCode = @DeptCode
	-- AND RoleShortName = 'DGM'
	INSERT INTO @tab (
		Username,
		RealName,
		Email
		)
	SELECT Username,
		RealName,
		Email
	FROM (
		SELECT ROW_NUMBER() OVER (
				ORDER BY Username
				) AS No,
			Username,
			RealName,
			Email
		FROM [vw_UserByDepartment]
		WHERE 1 = 1
			AND (
				1 = 1
				AND (
					CASE 
						WHEN RTRIM(@DeptID) = '27'
							THEN Username
						END
					) LIKE 'Dadi'
				OR RTRIM(@DeptID) != '27'
				)
			AND (
				1 = 1
				AND (
					CASE 
						WHEN RTRIM(@DivID) = '9'
							AND RTRIM(@DeptID) != '27'
							THEN RoleID
						END
					) >= 53
				AND (
					CASE 
						WHEN RTRIM(@DivID) = '9'
							AND RTRIM(@DeptID) != '27'
							THEN RoleID
						END
					) < 70
				AND (
					CASE 
						WHEN RTRIM(@DivID) = '9'
							AND RTRIM(@DeptID) != '27'
							THEN DivisionID
						END
					) = @DivID
				OR RTRIM(@DivID) != '9'
				OR RTRIM(@DeptID) = '27'
				)
			AND (
				1 = 1
				AND (
					(
						CASE 
							WHEN RTRIM(@DivID) = '7'
								AND RTRIM(@DeptID) != '27'
								THEN RoleID
							END
						) = 55
					AND (
						CASE 
							WHEN RTRIM(@DivID) = '7'
								AND RTRIM(@DeptID) != '27'
								THEN DivisionID
							END
						) = @DivID
					OR RTRIM(@DivID) != '7'
					OR RTRIM(@DeptID) = '27'
					)
				)
			AND (
				1 = 1
				AND (
					(
						CASE 
							WHEN RTRIM(@DivID) = '5'
								AND RTRIM(@DeptID) != '27'
								THEN RoleID
							END
						) = 61
					AND (
						CASE 
							WHEN RTRIM(@DivID) = '5'
								AND RTRIM(@DeptID) != '27'
								THEN DivisionID
							END
						) = @DivID
					OR RTRIM(@DivID) != '5'
					OR RTRIM(@DeptID) = '27'
					)
				)
			AND (
				1 = 1
				AND (
					(
						CASE 
							WHEN RTRIM(@DivID) != '5'
								AND RTRIM(@DivID) != '7'
								AND RTRIM(@DivID) != '9'
								AND RTRIM(@DeptID) != '27'
								THEN RoleID
							END
						) >= 53
					AND (
						CASE 
							WHEN RTRIM(@DivID) != '5'
								AND RTRIM(@DivID) != '7'
								AND RTRIM(@DivID) != '9'
								AND RTRIM(@DeptID) != '27'
								THEN RoleID
							END
						) < 70
					AND (
						CASE 
							WHEN RTRIM(@DivID) != '5'
								AND RTRIM(@DivID) != '7'
								AND RTRIM(@DivID) != '9'
								AND RTRIM(@DeptID) != '27'
								THEN DivisionID
							END
						) = @DivID
					OR RTRIM(@DivID) = '5'
					OR RTRIM(@DivID) = '7'
					OR RTRIM(@DivID) = '9'
					OR RTRIM(@DeptID) = '27'
					)
				)
		) x
	WHERE 1 = 1
		AND (
			1 = 1
			AND (
				CASE 
					WHEN RTRIM(@DivID) = '7'
						AND RTRIM(@DeptID) != '27'
						THEN [No]
					END
				) = 1
			OR RTRIM(@DivID) != '7'
			OR RTRIM(@DeptID) = '27'
			)
		AND (
			1 = 1
			AND (
				CASE 
					WHEN RTRIM(@DivID) = '9'
						AND RTRIM(@DeptID) != '27'
						THEN [No]
					END
				) = 1
			OR RTRIM(@DivID) != '9'
			OR RTRIM(@DeptID) = '27'
			)

	RETURN
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetUHByDept]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fnGetUHByDept] (@Dept VARCHAR(10))
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @ListUH VARCHAR(2000)

	SELECT @ListUH = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE DepartmentCode = @Dept
						AND RoleShortName = 'UH'
					FOR XML PATH('')
					), 1, 1, ''), '')

	RETURN @ListUH
END


GO
/****** Object:  UserDefinedFunction [dbo].[fnGetUHByOriginator]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnGetUHByOriginator] (@Originator VARCHAR(100))
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @ListUH VARCHAR(2000)
	DECLARE @StrUsername NVARCHAR(50)

	SET @StrUsername = @ORIGINATOR

	IF charindex('JAKARTA01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'JAKARTA01\', '')

	IF charindex('jakarta01\', @StrUsername) > 0
		SET @StrUsername = replace(@ORIGINATOR, 'jakarta01\', '')

	SELECT @ListUH = ISNULL(STUFF((
					SELECT DISTINCT ',' + RealName
					FROM vw_UserByDepartment
					WHERE UnitID = (
							SELECT UnitID
							FROM vw_UserByDepartment
							WHERE Username = @StrUsername
							)
						AND RoleShortName = 'UH'
					FOR XML PATH('')
					), 1, 1, ''), '')

	RETURN @ListUH
END


GO
/****** Object:  UserDefinedFunction [dbo].[splitstring]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[splitstring] (@stringToSplit VARCHAR(MAX), @stringDelimiter NVARCHAR(100))
RETURNS @returnList TABLE ([Name] [nvarchar](500))
AS
BEGIN
	DECLARE @name NVARCHAR(255)
	DECLARE @pos INT

	WHILE CHARINDEX(@stringDelimiter, @stringToSplit) > 0
	BEGIN
		SELECT @pos = CHARINDEX(@stringDelimiter, @stringToSplit)

		SELECT @name = SUBSTRING(@stringToSplit, 1, @pos - 1)

		INSERT INTO @returnList
		SELECT @name

		SELECT @stringToSplit = SUBSTRING(@stringToSplit, @pos + 1, LEN(@stringToSplit) - @pos)
	END

	INSERT INTO @returnList
	SELECT @stringToSplit

	RETURN
END



GO
/****** Object:  Table [dbo].[MS_DEPT_PIC_CONFIG_DTL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_DEPT_PIC_CONFIG_DTL](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DEPT_CD] [varchar](50) NULL,
	[MEMBER_CD] [varchar](50) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_DEPT_PIC_CONFIG_HDR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_DEPT_PIC_CONFIG_HDR](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DEPT_CD] [varchar](50) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_PARAMETER]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_PARAMETER](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[GROUP] [varchar](30) NULL,
	[VALUE] [varchar](5000) NULL,
	[DESCS] [varchar](8000) NULL,
	[SEQ] [int] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_PARAMETER_20220114]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_PARAMETER_20220114](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[GROUP] [varchar](30) NULL,
	[VALUE] [varchar](5000) NULL,
	[DESCS] [varchar](8000) NULL,
	[SEQ] [int] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_PARAMETER_20220208]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_PARAMETER_20220208](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[GROUP] [varchar](30) NULL,
	[VALUE] [varchar](5000) NULL,
	[DESCS] [varchar](8000) NULL,
	[SEQ] [int] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MS_SETTING]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MS_SETTING](
	[UID] [smallint] IDENTITY(1,1) NOT NULL,
	[REV_CC] [varchar](8000) NULL,
	[REV_BWD_ACTIVE] [bit] NULL,
	[REV_BWD_START_FROM] [int] NULL,
	[REV_BWD_RECUR_DAY] [int] NULL,
	[REV_FWD_ACTIVE] [bit] NULL,
	[REV_FWD_RECUR_DAY] [int] NULL,
	[CONT_CC] [varchar](8000) NULL,
	[CONT_BWD_ACTIVE] [bit] NULL,
	[CONT_BWD_START_FROM] [int] NULL,
	[CONT_BWD_RECUR_DAY] [int] NULL,
	[CONT_FWD_ACTIVE] [bit] NULL,
	[CONT_FWD_RECUR_DAY] [int] NULL,
	[APV_RMD_ACTIVE] [bit] NULL,
	[APV_RMD_RECUR_DAY] [int] NULL,
	[GAD_DEPT_CD] [varchar](50) NULL,
	[GAD_PIC] [varchar](8000) NULL,
	[RMD_DEPT_CD] [varchar](50) NULL,
	[RMD_STAFF] [varchar](8000) NULL,
	[CPC_DEPT_CD] [varchar](50) NULL,
	[CPC_STAFF] [varchar](8000) NULL,
	[FCD_DEPT_CD] [varchar](50) NULL,
	[FCD_PIC] [varchar](8000) NULL,
	[LGL_DEPT_CD] [varchar](50) NULL,
	[LGL_PIC] [varchar](8000) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_ATTACHMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_ATTACHMENT](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[FILE_TYPE] [varchar](20) NULL,
	[FILE_NM] [varchar](80) NULL,
	[SAVE_FILE_NM] [varchar](80) NULL,
	[FILE_URL] [varchar](200) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_ATTACHMENT_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_ATTACHMENT_TEMP](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[FILE_TYPE] [varchar](20) NULL,
	[FILE_NM] [varchar](80) NULL,
	[SAVE_FILE_NM] [varchar](80) NULL,
	[FILE_URL] [varchar](200) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_DOCUMENT_HISTORY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_DOCUMENT_HISTORY](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[HIST_TYPE] [varchar](20) NULL,
	[SEQ_NO] [int] NULL,
	[VENDOR_ID] [varchar](50) NULL,
	[ACTION_BY] [varchar](50) NULL,
	[ACTION_DT] [datetime] NULL,
	[STATUS] [varchar](20) NULL,
	[COMMENT] [varchar](2000) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[STEP] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_LATEST_REMINDER]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_LATEST_REMINDER](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DEPT_CD] [varchar](200) NULL,
	[REV_LATEST_SENT_REMINDER_FWD] [datetime] NULL,
	[REV_LATEST_SENT_REMINDER_BWD] [datetime] NULL,
	[CONT_LATEST_SENT_REMINDER_FWD] [datetime] NULL,
	[CONT_LATEST_SENT_REMINDER_BWD] [datetime] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_TR_LATEST_REMINDER] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_LATEST_REMINDER_VENDOR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_LATEST_REMINDER_VENDOR](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[FORM_TYPE] [varchar](30) NULL,
	[LATEST_SENT_REMINDER_APP] [datetime] NULL,
	[LATEST_SENT_REMINDER_REV] [datetime] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_TR_LATEST_REMINDER_VENDOR] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_REMINDER_LOG]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_REMINDER_LOG](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[RMD_SOURCE] [varchar](100) NULL,
	[DEPT_CD] [varchar](200) NULL,
	[VENDOR_ID] [varchar](255) NULL,
	[SENT_TO] [varchar](500) NULL,
	[SENT_CC] [varchar](2000) NULL,
	[SENT_DT] [datetime] NULL,
	[MAIL_SUBJECT] [varchar](255) NULL,
	[MAIL_BODY] [varchar](max) NULL,
	[MAIL_TO_EMAIL] [varchar](255) NULL,
	[MAIL_CC_EMAIL] [varchar](2000) NULL,
	[LAST_STATUS] [varchar](30) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_TR_REMINDER_LOG] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_USER_ACTIVITY_LOG_DTL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_USER_ACTIVITY_LOG_DTL](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[FIELD_NM] [varchar](300) NULL,
	[OLD_VALUE] [varchar](8000) NULL,
	[NEW_VALUE] [varchar](8000) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[ACT_CD] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_USER_ACTIVITY_LOG_HDR]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_USER_ACTIVITY_LOG_HDR](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[ACT_CD] [varchar](50) NULL,
	[ACT_DESC] [varchar](100) NULL,
	[ACT_VAL] [varchar](500) NULL,
	[ACT_RESULT] [varchar](50) NULL,
	[ACT_USER] [varchar](50) NULL,
	[ACT_DEPT] [varchar](100) NULL,
	[ACT_DT] [datetime] NULL,
	[ACT_WORKSTATION] [varchar](30) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[WFGUID] [uniqueidentifier] NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_ACTIVITY_REV_AML]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_ACTIVITY_REV_AML](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[NAME] [varchar](255) NULL,
	[TITLE] [varchar](255) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_ACTIVITY_REV_EXAMINED]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_ACTIVITY_REV_EXAMINED](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[POINT] [varchar](300) NULL,
	[RESULT] [bit] NULL,
	[COMMENT] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_ASC_CHECKSHEET]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_ASC_CHECKSHEET](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[POSITION] [varchar](50) NULL,
	[NAME] [varchar](255) NULL,
	[NATIONALITY] [varchar](255) NULL,
	[SHARE] [decimal](36, 8) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_ASC_CHECKSHEET_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_ASC_CHECKSHEET_TEMP](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[POSITION] [varchar](50) NULL,
	[NAME] [varchar](255) NULL,
	[NATIONALITY] [varchar](255) NULL,
	[SHARE] [decimal](36, 8) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_CONF_DOC_CHECKLIST]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_CONF_DOC_CHECKLIST](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ] [int] NULL,
	[ITEM] [varchar](255) NULL,
	[SUBMITTED] [bit] NULL,
	[REASON] [varchar](8000) NULL,
	[CHECKED] [bit] NULL,
	[REASON_CHECKED] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_CONF_DOC_CHECKLIST_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_CONF_DOC_CHECKLIST_TEMP](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ] [int] NULL,
	[ITEM] [varchar](255) NULL,
	[SUBMITTED] [bit] NULL,
	[REASON] [varchar](8000) NULL,
	[CHECKED] [bit] NULL,
	[REASON_CHECKED] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_EXAM_COMPARISON]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_EXAM_COMPARISON](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[PARAMETER] [varchar](200) NULL,
	[COMPARE_VENDOR_A] [varchar](800) NULL,
	[COMPARE_VENDOR_B] [varchar](800) NULL,
	[REMARK] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_EXAM_COMPARISON_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_EXAM_COMPARISON_TEMP](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[PARAMETER] [varchar](200) NULL,
	[COMPARE_VENDOR_A] [varchar](800) NULL,
	[COMPARE_VENDOR_B] [varchar](800) NULL,
	[REMARK] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_RISK_ASSESSMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_RISK_ASSESSMENT](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[PARAMETER] [varchar](80) NULL,
	[ASSESS_VENDOR_A] [varchar](255) NULL,
	[ASSESS_VENDOR_B] [varchar](255) NULL,
	[REMARK] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_RISK_ASSESSMENT_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_RISK_ASSESSMENT_TEMP](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[SEQ_NO] [smallint] NULL,
	[PARAMETER] [varchar](80) NULL,
	[ASSESS_VENDOR_A] [varchar](255) NULL,
	[ASSESS_VENDOR_B] [varchar](255) NULL,
	[REMARK] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_RISK_MANAGEMENT_COMMENT]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_RISK_MANAGEMENT_COMMENT](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[USER_APV] [varchar](50) NULL,
	[COMMENT] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VC_RISK_MANAGEMENT_COMMENT_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VC_RISK_MANAGEMENT_COMMENT_TEMP](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[USER_APV] [varchar](50) NULL,
	[COMMENT] [varchar](8000) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VENDOR_CONTROL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VENDOR_CONTROL](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[VENDOR_ID] [varchar](50) NULL,
	[VENDOR_NAME] [varchar](255) NULL,
	[CREATOR] [varchar](255) NULL,
	[CREATOR_DEPT] [varchar](255) NULL,
	[CREATOR_UNIT] [varchar](255) NULL,
	[CREATOR_ROLE] [varchar](255) NULL,
	[REQUEST_DT] [date] NULL,
	[DOC_DATE_FORM1] [date] NULL,
	[CONF_CONTRACTOR_NM] [varchar](255) NULL,
	[CONF_OPERATION_TO_OUTSOURCE] [varchar](255) NULL,
	[CONF_CATEGORY] [varchar](255) NULL,
	[CONF_DEPT_CD] [varchar](50) NULL,
	[CONF_BRIEF_EXPLAIN_CONTRACT] [varchar](8000) NULL,
	[CONF_DECISION] [bit] NULL,
	[CONF_GAD_COMMENT] [varchar](8000) NULL,
	[DOC_DATE_FORM2] [date] NULL,
	[EXAM_NAME_OF_GROUP] [varchar](255) NULL,
	[EXAM_CORE_BUSINESS] [varchar](255) NULL,
	[EXAM_REL_VENDOR] [bit] NULL,
	[EXAM_REL_VENDOR_REMARK] [varchar](8000) NULL,
	[EXAM_HAS_ENGAGED_BEFORE] [bit] NULL,
	[EXAM_HAS_ENGAGED_SINCE] [date] NULL,
	[EXAM_BACKGROUND] [varchar](8000) NULL,
	[EXAM_VENDOR_A] [varchar](255) NULL,
	[EXAM_VENDOR_B] [varchar](255) NULL,
	[EXAM_OVERALL_CONCLUSION] [varchar](8000) NULL,
	[RISK_ANNUAL_BILLING] [varchar](80) NULL,
	[RISK_SPECIAL_RELATED] [bit] NULL,
	[RISK_CPC_ASSESSMENT] [varchar](8000) NULL,
	[RISK_PROVIDE_ACCESS] [bit] NULL,
	[RISK_AS_VENDOR_A] [varchar](255) NULL,
	[RISK_AS_VENDOR_B] [varchar](255) NULL,
	[RISK_AML_COMMENT] [varchar](8000) NULL,
	[RISK_CATEGORY] [varchar](50) NULL,
	[RISK_RMD_RECOMMENDATION] [varchar](8000) NULL,
	[RISK_SCREENING_RESULT] [varchar](8000) NULL,
	[REG_PROVIDED_SERVICE] [varchar](8000) NULL,
	[REG_CONTRACT_NM] [varchar](255) NULL,
	[REG_LEGAL_STATUS] [varchar](50) NULL,
	[REG_NPWP_NO] [varchar](255) NULL,
	[REG_ADDRESS] [varchar](255) NULL,
	[REG_CITY] [varchar](255) NULL,
	[REG_POSTAL] [varchar](255) NULL,
	[REG_WEBSITE] [varchar](255) NULL,
	[REG_EMAIL] [varchar](255) NULL,
	[REG_PHONE_1] [varchar](255) NULL,
	[REG_PHONE_EXT_1] [varchar](255) NULL,
	[REG_PHONE_2] [varchar](255) NULL,
	[REG_PHONE_EXT_2] [varchar](255) NULL,
	[REG_FAX_1] [varchar](255) NULL,
	[REG_FAX_2] [varchar](255) NULL,
	[REG_CP_NM_1] [varchar](255) NULL,
	[REG_CP_POSITION_1] [varchar](255) NULL,
	[REG_CP_NM_2] [varchar](255) NULL,
	[REG_CP_POSITION_2] [varchar](255) NULL,
	[REG_PAYMENT_TYPE] [varchar](50) NULL,
	[REG_REASON_CASH] [varchar](8000) NULL,
	[REG_ACC_HOLDER_NM] [varchar](255) NULL,
	[REG_ACC_NO] [varchar](255) NULL,
	[REG_ACC_BANK] [varchar](255) NULL,
	[DOC_DATE_FORM4A] [date] NULL,
	[ASC_CONTRACTOR_JAPANESE] [bit] NULL,
	[ASC_SUBCONTRACT_OPERATION] [bit] NULL,
	[AGREE_START_DATE] [date] NULL,
	[AGREE_END_DATE] [date] NULL,
	[AGREE_CREATED_BY] [varchar](50) NULL,
	[AGREE_CREATED_DT] [datetime] NULL,
	[AGREE_MODIFIED_BY] [varchar](50) NULL,
	[AGREE_MODIFIED_DT] [datetime] NULL,
	[TERMINATION] [bit] NULL,
	[TERMINATION_BY] [varchar](50) NULL,
	[TERMINATION_DT] [date] NULL,
	[REGISTRATION_DT] [date] NULL,
	[APPROVAL_STS] [varchar](50) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[STATUS_FLOW] [varchar](50) NULL,
	[REV_LATEST_SENT_REMINDER_FWD] [datetime] NULL,
	[REV_LATEST_SENT_REMINDER_BWD] [datetime] NULL,
	[CONT_LATEST_SENT_REMINDER_FWD] [datetime] NULL,
	[CONT_LATEST_SENT_REMINDER_BWD] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VENDOR_CONTROL_ACTIVITY]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VENDOR_CONTROL_ACTIVITY](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[DOC_ID] [bigint] NULL,
	[FORM_TYPE] [varchar](30) NULL,
	[CREATOR] [varchar](255) NULL,
	[CREATOR_DEPT] [varchar](255) NULL,
	[CREATOR_SECTION] [varchar](255) NULL,
	[CREATOR_ROLE] [varchar](255) NULL,
	[REQUEST_DT] [date] NULL,
	[VENDOR_ID] [varchar](50) NULL,
	[DOC_DATE_FORM6] [date] NULL,
	[LGL_REQ_DEPT_CD] [varchar](50) NULL,
	[LGL_SUBJECT] [varchar](255) NULL,
	[LGL_FACTS] [varchar](8000) NULL,
	[LGL_SUPPORTING_DOC] [varchar](8000) NULL,
	[LGL_REQUEST] [varchar](8000) NULL,
	[LGL_DEADLINE] [varchar](8000) NULL,
	[REV_REVIEW_DT] [date] NULL,
	[REV_GAD_RECOMMENDATION] [varchar](8000) NULL,
	[REV_RMD_COMMENT] [varchar](8000) NULL,
	[DOC_DATE_FORM5] [date] NULL,
	[CONT_DESC_PERFORMANCE] [varchar](8000) NULL,
	[CONT_BIDDING_RESULT] [varchar](8000) NULL,
	[CONT_REASON_UTILITY] [varchar](8000) NULL,
	[CONT_REL_EXPLAIN] [varchar](8000) NULL,
	[CONT_CHANGE_FORM4B] [varchar](8000) NULL,
	[CONT_INTV_CREATOR_DGM] [varchar](8000) NULL,
	[CONT_INTV_GAD_DGM] [varchar](8000) NULL,
	[CONT_GAD_RECOMMENDATION] [varchar](8000) NULL,
	[WAIV_PROVIDED_SERVICE] [varchar](8000) NULL,
	[WAIV_VENDOR_REFUSAL] [varchar](8000) NULL,
	[WAIV_REASON_UTILIZE] [varchar](8000) NULL,
	[WAIV_GAD_ASSESS] [varchar](50) NULL,
	[WAIV_COMMENT] [varchar](8000) NULL,
	[TERMINATION] [bit] NULL,
	[TERMINATION_BY] [varchar](50) NULL,
	[TERMINATION_DT] [date] NULL,
	[REGISTRATION_DT] [date] NULL,
	[APPROVAL_STS] [varchar](50) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[IS_DRAFT] [bit] NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[DOC_DATE_FORM4] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VENDOR_CONTROL_TEMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VENDOR_CONTROL_TEMP](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[REFID] [bigint] NULL,
	[VENDOR_ID] [varchar](50) NULL,
	[VENDOR_NAME] [varchar](255) NULL,
	[CREATOR] [varchar](255) NULL,
	[CREATOR_DEPT] [varchar](255) NULL,
	[CREATOR_UNIT] [varchar](255) NULL,
	[CREATOR_ROLE] [varchar](255) NULL,
	[REQUEST_DT] [date] NULL,
	[DOC_DATE_FORM1] [date] NULL,
	[CONF_CONTRACTOR_NM] [varchar](255) NULL,
	[CONF_OPERATION_TO_OUTSOURCE] [varchar](255) NULL,
	[CONF_CATEGORY] [varchar](255) NULL,
	[CONF_DEPT_CD] [varchar](50) NULL,
	[CONF_BRIEF_EXPLAIN_CONTRACT] [varchar](8000) NULL,
	[CONF_DECISION] [bit] NULL,
	[CONF_GAD_COMMENT] [varchar](8000) NULL,
	[DOC_DATE_FORM2] [date] NULL,
	[EXAM_NAME_OF_GROUP] [varchar](255) NULL,
	[EXAM_CORE_BUSINESS] [varchar](255) NULL,
	[EXAM_REL_VENDOR] [bit] NULL,
	[EXAM_REL_VENDOR_REMARK] [varchar](8000) NULL,
	[EXAM_HAS_ENGAGED_BEFORE] [bit] NULL,
	[EXAM_HAS_ENGAGED_SINCE] [date] NULL,
	[EXAM_BACKGROUND] [varchar](8000) NULL,
	[EXAM_VENDOR_A] [varchar](255) NULL,
	[EXAM_VENDOR_B] [varchar](255) NULL,
	[EXAM_OVERALL_CONCLUSION] [varchar](8000) NULL,
	[RISK_ANNUAL_BILLING] [varchar](80) NULL,
	[RISK_SPECIAL_RELATED] [bit] NULL,
	[RISK_CPC_ASSESSMENT] [varchar](8000) NULL,
	[RISK_PROVIDE_ACCESS] [bit] NULL,
	[RISK_AS_VENDOR_A] [varchar](255) NULL,
	[RISK_AS_VENDOR_B] [varchar](255) NULL,
	[RISK_AML_COMMENT] [varchar](8000) NULL,
	[RISK_CATEGORY] [varchar](50) NULL,
	[RISK_RMD_RECOMMENDATION] [varchar](8000) NULL,
	[RISK_SCREENING_RESULT] [varchar](8000) NULL,
	[REG_PROVIDED_SERVICE] [varchar](8000) NULL,
	[REG_CONTRACT_NM] [varchar](255) NULL,
	[REG_LEGAL_STATUS] [varchar](50) NULL,
	[REG_NPWP_NO] [varchar](255) NULL,
	[REG_ADDRESS] [varchar](255) NULL,
	[REG_CITY] [varchar](255) NULL,
	[REG_POSTAL] [varchar](255) NULL,
	[REG_WEBSITE] [varchar](255) NULL,
	[REG_EMAIL] [varchar](255) NULL,
	[REG_PHONE_1] [varchar](255) NULL,
	[REG_PHONE_EXT_1] [varchar](255) NULL,
	[REG_PHONE_2] [varchar](255) NULL,
	[REG_PHONE_EXT_2] [varchar](255) NULL,
	[REG_FAX_1] [varchar](255) NULL,
	[REG_FAX_2] [varchar](255) NULL,
	[REG_CP_NM_1] [varchar](255) NULL,
	[REG_CP_POSITION_1] [varchar](255) NULL,
	[REG_CP_NM_2] [varchar](255) NULL,
	[REG_CP_POSITION_2] [varchar](255) NULL,
	[REG_PAYMENT_TYPE] [varchar](50) NULL,
	[REG_REASON_CASH] [varchar](8000) NULL,
	[REG_ACC_HOLDER_NM] [varchar](255) NULL,
	[REG_ACC_NO] [varchar](255) NULL,
	[REG_ACC_BANK] [varchar](255) NULL,
	[DOC_DATE_FORM4A] [date] NULL,
	[ASC_CONTRACTOR_JAPANESE] [bit] NULL,
	[ASC_SUBCONTRACT_OPERATION] [bit] NULL,
	[AGREE_START_DATE] [date] NULL,
	[AGREE_END_DATE] [date] NULL,
	[AGREE_CREATED_BY] [varchar](50) NULL,
	[AGREE_CREATED_DT] [datetime] NULL,
	[AGREE_MODIFIED_BY] [varchar](50) NULL,
	[AGREE_MODIFIED_DT] [datetime] NULL,
	[TERMINATION] [bit] NULL,
	[TERMINATION_BY] [varchar](50) NULL,
	[TERMINATION_DT] [date] NULL,
	[REGISTRATION_DT] [date] NULL,
	[APPROVAL_STS] [varchar](50) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[STATUS_FLOW] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_VENDOR_CONTROL_TMP]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_VENDOR_CONTROL_TMP](
	[UID] [bigint] NULL,
	[VENDOR_ID] [varchar](50) NULL,
	[VENDOR_NAME] [varchar](255) NULL,
	[CREATOR] [varchar](255) NULL,
	[CREATOR_DEPT] [varchar](255) NULL,
	[CREATOR_UNIT] [varchar](255) NULL,
	[CREATOR_ROLE] [varchar](255) NULL,
	[REQUEST_DT] [date] NULL,
	[DOC_DATE_FORM1] [date] NULL,
	[CONF_CONTRACTOR_NM] [varchar](255) NULL,
	[CONF_OPERATION_TO_OUTSOURCE] [varchar](255) NULL,
	[CONF_CATEGORY] [varchar](255) NULL,
	[CONF_DEPT_CD] [varchar](50) NULL,
	[CONF_BRIEF_EXPLAIN_CONTRACT] [varchar](8000) NULL,
	[CONF_DECISION] [bit] NULL,
	[CONF_GAD_COMMENT] [varchar](8000) NULL,
	[DOC_DATE_FORM2] [date] NULL,
	[EXAM_NAME_OF_GROUP] [varchar](255) NULL,
	[EXAM_CORE_BUSINESS] [varchar](255) NULL,
	[EXAM_REL_VENDOR] [bit] NULL,
	[EXAM_REL_VENDOR_REMARK] [varchar](8000) NULL,
	[EXAM_HAS_ENGAGED_BEFORE] [bit] NULL,
	[EXAM_HAS_ENGAGED_SINCE] [date] NULL,
	[EXAM_BACKGROUND] [varchar](8000) NULL,
	[EXAM_VENDOR_A] [varchar](255) NULL,
	[EXAM_VENDOR_B] [varchar](255) NULL,
	[EXAM_OVERALL_CONCLUSION] [varchar](8000) NULL,
	[RISK_ANNUAL_BILLING] [varchar](80) NULL,
	[RISK_SPECIAL_RELATED] [bit] NULL,
	[RISK_CPC_ASSESSMENT] [varchar](8000) NULL,
	[RISK_PROVIDE_ACCESS] [bit] NULL,
	[RISK_AS_VENDOR_A] [varchar](255) NULL,
	[RISK_AS_VENDOR_B] [varchar](255) NULL,
	[RISK_AML_COMMENT] [varchar](8000) NULL,
	[RISK_CATEGORY] [varchar](50) NULL,
	[RISK_RMD_RECOMMENDATION] [varchar](8000) NULL,
	[RISK_SCREENING_RESULT] [varchar](8000) NULL,
	[REG_PROVIDED_SERVICE] [varchar](8000) NULL,
	[REG_CONTRACT_NM] [varchar](255) NULL,
	[REG_LEGAL_STATUS] [varchar](50) NULL,
	[REG_NPWP_NO] [varchar](255) NULL,
	[REG_ADDRESS] [varchar](255) NULL,
	[REG_CITY] [varchar](255) NULL,
	[REG_POSTAL] [varchar](255) NULL,
	[REG_WEBSITE] [varchar](255) NULL,
	[REG_EMAIL] [varchar](255) NULL,
	[REG_PHONE_1] [varchar](255) NULL,
	[REG_PHONE_EXT_1] [varchar](255) NULL,
	[REG_PHONE_2] [varchar](255) NULL,
	[REG_PHONE_EXT_2] [varchar](255) NULL,
	[REG_FAX_1] [varchar](255) NULL,
	[REG_FAX_2] [varchar](255) NULL,
	[REG_CP_NM_1] [varchar](255) NULL,
	[REG_CP_POSITION_1] [varchar](255) NULL,
	[REG_CP_NM_2] [varchar](255) NULL,
	[REG_CP_POSITION_2] [varchar](255) NULL,
	[REG_PAYMENT_TYPE] [varchar](50) NULL,
	[REG_REASON_CASH] [varchar](8000) NULL,
	[REG_ACC_HOLDER_NM] [varchar](255) NULL,
	[REG_ACC_NO] [varchar](255) NULL,
	[REG_ACC_BANK] [varchar](255) NULL,
	[DOC_DATE_FORM4A] [date] NULL,
	[ASC_CONTRACTOR_JAPANESE] [bit] NULL,
	[ASC_SUBCONTRACT_OPERATION] [bit] NULL,
	[AGREE_START_DATE] [date] NULL,
	[AGREE_END_DATE] [date] NULL,
	[AGREE_CREATED_BY] [varchar](50) NULL,
	[AGREE_CREATED_DT] [datetime] NULL,
	[AGREE_MODIFIED_BY] [varchar](50) NULL,
	[AGREE_MODIFIED_DT] [datetime] NULL,
	[TERMINATION] [bit] NULL,
	[TERMINATION_BY] [varchar](50) NULL,
	[TERMINATION_DT] [date] NULL,
	[REGISTRATION_DT] [date] NULL,
	[APPROVAL_STS] [varchar](50) NULL,
	[IS_ACTIVE] [bit] NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
	[L_DOC_ID] [varchar](50) NULL,
	[STATUS_FLOW] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TR_WORKFLOWPROCESS]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TR_WORKFLOWPROCESS](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[DOCID] [bigint] NULL,
	[REFID] [bigint] NULL,
	[PROCID] [nvarchar](50) NULL,
	[SN] [nvarchar](50) NULL,
	[WFTASK] [nvarchar](50) NULL,
	[WFACTION] [nvarchar](50) NULL,
	[WFSTEP] [nvarchar](100) NULL,
	[VENDORID] [nvarchar](50) NULL,
	[CREATED_BY] [nvarchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [nvarchar](50) NULL,
	[MODIFIED_BY] [nvarchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [nvarchar](50) NULL,
	[ISACTIVE] [bit] NULL,
	[WFTASKNAME] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TR_WORKFLOWPROCESS_DETAIL]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TR_WORKFLOWPROCESS_DETAIL](
	[UID] [bigint] IDENTITY(1,1) NOT NULL,
	[WFID] [bigint] NOT NULL,
	[WFSTEP] [int] NULL,
	[SN] [varchar](100) NULL,
	[ACTION_BY] [varchar](255) NULL,
	[CREATED_BY] [varchar](50) NULL,
	[CREATED_DT] [datetime] NULL,
	[CREATED_HOST] [varchar](20) NULL,
	[MODIFIED_BY] [varchar](50) NULL,
	[MODIFIED_DT] [datetime] NULL,
	[MODIFIED_HOST] [varchar](20) NULL,
 CONSTRAINT [PK_TR_WORKFLOWPROCESS_DETAIL] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vw_BTMUDirectory_Department]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vw_BTMUDirectory_Department]
AS
SELECT ID,
	ShortName,
	LongName,
	DivisionID,
	LastUpdated,
	UpdatedBy,
	CreatedAt,
	CreatedBy
FROM [JKTSVRDEV03].[BTMUDirectory].[dbo].[Department]
WHERE 1 = 1
	AND (
		1 = 1
		AND (
			CASE 
				WHEN ShortName = 'LCBD'
					THEN LongName
				END
			) LIKE 'Local Corporate Banking Department'
		OR ShortName != 'LCBD'
		)


GO
/****** Object:  View [dbo].[vw_DraftList]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_DraftList]
AS
SELECT [UID] = tvc.UID,
	Form = 'Vendor Registration',
	VendorID = tvc.VENDOR_ID,
	VendorName = tvc.VENDOR_NAME,
	[Action] = 'NEW',
	DraftBy = tvc.CREATED_BY,
	DraftDate = tvc.CREATED_DT
FROM TR_VENDOR_CONTROL AS tvc
WHERE STATUS_FLOW = 'DRAFT'
	AND IS_ACTIVE = 1

UNION

--legal_request
SELECT [UID] = tvca.UID,
	Form = 'Legal Request',
	VendorID = tvc.VENDOR_ID,
	VendorName = tvc.VENDOR_NAME,
	[Action] = 'NEW',
	DraftBy = tvca.CREATED_BY,
	DraftDate = tvca.CREATED_DT
FROM TR_VENDOR_CONTROL_ACTIVITY AS tvca
JOIN TR_VENDOR_CONTROL AS tvc ON tvca.DOC_ID = tvc.UID
WHERE tvca.FORM_TYPE = 'legal_request'
	AND tvca.IS_DRAFT = 1
	AND tvca.IS_ACTIVE = 1

UNION

--PERIODICAL_REVIEW
SELECT [UID] = tvca.UID,
	Form = 'Periodical Review',
	VendorID = tvc.VENDOR_ID,
	VendorName = tvc.VENDOR_NAME,
	[Action] = 'NEW',
	DraftBy = tvca.CREATED_BY,
	DraftDate = tvca.CREATED_DT
FROM TR_VENDOR_CONTROL_ACTIVITY AS tvca
JOIN TR_VENDOR_CONTROL AS tvc ON tvca.DOC_ID = tvc.UID
WHERE tvca.FORM_TYPE = 'PERIODICAL_REVIEW'
	AND tvca.IS_DRAFT = 1
	AND tvca.IS_ACTIVE = 1

UNION

--CONTINUING_VENDOR
SELECT [UID] = tvca.UID,
	Form = 'Continuing Vendor',
	VendorID = tvc.VENDOR_ID,
	VendorName = tvc.VENDOR_NAME,
	[Action] = 'NEW',
	DraftBy = tvca.CREATED_BY,
	DraftDate = tvca.CREATED_DT
FROM TR_VENDOR_CONTROL_ACTIVITY AS tvca
JOIN TR_VENDOR_CONTROL AS tvc ON tvca.DOC_ID = tvc.UID
WHERE tvca.FORM_TYPE = 'CONTINUING_VENDOR'
	AND tvca.IS_DRAFT = 1
	AND tvca.IS_ACTIVE = 1

UNION

--edit registration
SELECT [UID] = tvc.UID,
	Form = 'Edit Registration',
	VendorID = tvc.VENDOR_ID,
	VendorName = tvc.VENDOR_NAME,
	[Action] = 'EDIT',
	DraftBy = tvc.CREATED_BY,
	DraftDate = tvc.CREATED_DT
FROM TR_VENDOR_CONTROL_TEMP AS tvc
WHERE STATUS_FLOW = 'DRAFT'
	AND IS_ACTIVE = 1


GO
/****** Object:  View [dbo].[vw_PICMsSetting]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_PICMsSetting]
AS
SELECT 'GAD' AS DepartmentCode,
			vw.Username,
			vw.RealName,
			vw.Email
		FROM MS_SETTING AS MSGAD
		CROSS APPLY (
			SELECT name
			FROM dbo.splitstring(MSGAD.GAD_PIC, ',')
			) AS A
		LEFT JOIN dbo.SN_CONE_Cone_User AS vw ON a.Name = vw.RealName
		
		UNION
		
		SELECT 'FCD' AS DepartmentCode,
			vw.Username,
			vw.RealName,
			vw.Email
		FROM MS_SETTING AS MSGAD
		CROSS APPLY (
			SELECT name
			FROM dbo.splitstring(MSGAD.FCD_PIC, ',')
			) AS A
		LEFT JOIN dbo.SN_CONE_Cone_User AS vw ON a.Name = vw.RealName
		
		UNION
		
		SELECT 'CPC' AS DepartmentCode,
			vw.Username,
			vw.RealName,
			vw.Email
		FROM MS_SETTING AS MSGAD
		CROSS APPLY (
			SELECT name
			FROM dbo.splitstring(MSGAD.CPC_STAFF, ',')
			) AS A
		LEFT JOIN dbo.SN_CONE_Cone_User AS vw ON a.Name = vw.RealName
		
		UNION
		
		SELECT 'RMA' AS DepartmentCode,
			vw.Username,
			vw.RealName,
			vw.Email
		FROM MS_SETTING AS MSGAD
		CROSS APPLY (
			SELECT name
			FROM dbo.splitstring(MSGAD.RMD_STAFF, ',')
			) AS A
		LEFT JOIN dbo.SN_CONE_Cone_User AS vw ON a.Name = vw.RealName

        UNION
		
		SELECT 'LGL' AS DepartmentCode,
			vw.Username,
			vw.RealName,
			vw.Email
		FROM MS_SETTING AS MSGAD
		CROSS APPLY (
			SELECT name
			FROM dbo.splitstring(MSGAD.LGL_PIC, ',')
			) AS A
		LEFT JOIN dbo.SN_CONE_Cone_User AS vw ON a.Name = vw.RealName




GO
/****** Object:  View [dbo].[vw_UserByDepartment]    Script Date: 16/03/2022 21:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_UserByDepartment]
AS
SELECT a.Username,
	RealName = CASE 
		WHEN a.Realname = ''
			THEN a.Username
		ELSE a.Realname
		END,
	c.Email,
	c.DepartmentName,
	RoleParentID,
	OfficeCode,
	CustomerCode,
	b.UnitID,
	b.GroupID,
	D.ShortName AS DepartmentCode,
	b.DepartmentID,
	b.DivisionID,
	f.ShortName AS RoleShortName,
	b.RoleID
FROM dbo.SN_BTMUDirectory_User AS a
INNER JOIN dbo.SN_BTMUDirectory_RoleParent AS b ON a.Roleparentid = b.ID
INNER JOIN dbo.SN_CONE_Cone_User AS c ON a.Username = c.Username
INNER JOIN dbo.SN_BTMUDirectory_Department_All AS D ON D.ID = b.DepartmentID
INNER JOIN dbo.SN_BTMUDirectory_Role AS f ON f.ID = b.RoleID
WHERE (isdeleted = 0) -- AND (c.Email <> '')

GO
USE [master]
GO
ALTER DATABASE [VENDORCONTROL_ITA] SET  READ_WRITE 
GO
